
$(document).ready(function(){
  
 function readmoreparagraph(paragraph, showtext){
   
   
                       var words = paragraph.split(' ');
   
                       var newText = '';
   
                                       for(i=0; i<words.length; i++){
    
    
                                                                 if(i<= showtext){
     
                                                                               newText += words[i] + ' ';
     
                                                                                                }else {
     
                                                                        if (i == (showtext + 1)) newText += '... <span class="fulltext hide" style="text-decoration:none;">'; 
 
                                                                        newText += words[i] + ' ';
    
                                                              if (words[i+1] == null) newText += '</span><a href="#" class="links" style="text-decoration:none;"> &raquo; read more</a>';
     
                                                                }
    
                                                              }
   
                                               return newText;
   
                                                  }
  
  
                                         $('.text').each(function () {
   
   $(this).html(readmoreparagraph($(this).html(), 50));
   
   $(this).children('span').hide();
   
 
  }).click(function () {
 
 
   var fulltext = $(this).children('span.fulltext');
   var links = $(this).children('a.links');
     
 
   if (fulltext.hasClass('hide')) {
     
    fulltext.show(200);
    links.html(' &raquo; hide'); 
    fulltext.removeClass('hide');
     
   } else {
     
    fulltext.fadeOut(100);
    links.html(' &laquo; read more');  
    fulltext.addClass('hide');
     
   }
 
   return false;
   
  });
  
});
 

