var ExtCommon = function() {
    return {
        baseUrl: null,

        getCmp: function(selector) {
            var dom = Ext.ComponentQuery.query(selector);

            if (dom && dom.length > 0) {
                return dom[0];
            }

            // this.errorMsg("Unable to get results for the following selector: " + selector);
        },

        successMsg: function(msg) {
            Ext.Msg.show({
                title: 'Status',
                msg: msg,
                icon: Ext.Msg.INFO,
                buttons: Ext.Msg.OK
            });
        },

        errorMsg: function(msg) {
            Ext.Msg.show({
                title: 'Status',
                msg: msg,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
        },

        findField: function(form, name) {
            return form.getForm().findField(name);
        },

        redirect: function(url) {
            window.location.href = url;
        },

        ajaxDefaults: function(index, data) {
            if (index == null || !data)
                return;

            switch (index) {
                case 0:
                    Ext.Ajax.extraParams = Ext.apply(Ext.Ajax.extraParams || {}, data);
                    break;
                default:
                    break;
            }
        },

        jsonData: function(store) {
            if (store) {
                var data = [];
                store.getRange().forEach(function(row) {
                    data.push(row.data);
                });
                return data;
            }
            return [];
        },

        windowSize: function(d) {
            var winW = 0,
                winH = 0;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' && document.documentElement && document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }
            var dim = {
                width: winW,
                height: winH
            };
            return dim[d];
        },

        number_format: function(number, decimals, dec_point, thousands_sep) {
            //  discuss at: http://phpjs.org/functions/number_format/
            // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
            // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // improved by: davook
            // improved by: Brett Zamir (http://brett-zamir.me)
            // improved by: Brett Zamir (http://brett-zamir.me)
            // improved by: Theriault
            // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // bugfixed by: Michael White (http://getsprink.com)
            // bugfixed by: Benjamin Lupton
            // bugfixed by: Allan Jensen (http://www.winternet.no)
            // bugfixed by: Howard Yeend
            // bugfixed by: Diogo Resende
            // bugfixed by: Rival
            // bugfixed by: Brett Zamir (http://brett-zamir.me)
            //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
            //  revised by: Luke Smith (http://lucassmith.name)
            //    input by: Kheang Hok Chin (http://www.distantia.ca/)
            //    input by: Jay Klehr
            //    input by: Amir Habibi (http://www.residence-mixte.com/)
            //    input by: Amirouche
            //   example 1: number_format(1234.56);
            //   returns 1: '1,235'
            //   example 2: number_format(1234.56, 2, ',', ' ');
            //   returns 2: '1 234,56'
            //   example 3: number_format(1234.5678, 2, '.', '');
            //   returns 3: '1234.57'
            //   example 4: number_format(67, 2, ',', '.');
            //   returns 4: '67,00'
            //   example 5: number_format(1000);
            //   returns 5: '1,000'
            //   example 6: number_format(67.311, 2);
            //   returns 6: '67.31'
            //   example 7: number_format(1000.55, 1);
            //   returns 7: '1,000.6'
            //   example 8: number_format(67000, 5, ',', '.');
            //   returns 8: '67.000,00000'
            //   example 9: number_format(0.9, 0);
            //   returns 9: '1'
            //  example 10: number_format('1.20', 2);
            //  returns 10: '1.20'
            //  example 11: number_format('1.20', 4);
            //  returns 11: '1.2000'
            //  example 12: number_format('1.2000', 3);
            //  returns 12: '1.200'
            //  example 13: number_format('1 000,50', 2, '.', ' ');
            //  returns 13: '100 050.00'
            //  example 14: number_format(1e-8, 8, '.', '');
            //  returns 14: '0.00000001'

            number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + (Math.round(n * k) / k).toFixed(prec);
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        },

        clone: function(obj) {
            if (null == obj || "object" != typeof obj) return obj;
            var copy = new obj.__proto__;

            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
            }
            return copy;
        },

        showAt: function(menu, e, grid) {
            var el = Ext.fly(e.target).up('td'),
                tr = el.up('tr'),
                gridPos = grid.getXY(),
                trPos = tr.getXY();

            menu.showAt(gridPos[0], trPos[1] + tr.getHeight());
        }
    };
}();
