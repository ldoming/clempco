<?php
/**
 * A base controller for CodeIgniter with view autoloading, layout support,
 * model loading, helper loading, asides/partials and per-controller 404
 *
 * @link http://github.com/jamierumbelow/codeigniter-base-controller
 * @copyright Copyright (c) 2012, Jamie Rumbelow <http://jamierumbelow.net>
 */

class Base_Controller extends CI_Controller
{

    /* --------------------------------------------------------------
     * VARIABLES
     * ------------------------------------------------------------ */

    /**
     * An array of variables to be passed through to the
     * view, layout and any asides
     */
    protected $data = array();

    /**
     * A list of models to be autoloaded
     */
    protected $models = array();

    /**
     * A formatting string for the model autoloading feature.
     * The percent symbol (%) will be replaced with the model name.
     */
    protected $model_string = '%_model';

    /**
     * A list of helpers to be autoloaded
     */
    protected $helpers = array();

    /* --------------------------------------------------------------
     * GENERIC METHODS
     * ------------------------------------------------------------ */

    /**
     * Initialise the controller, tie into the CodeIgniter superobject
     * and try to autoload the models and helpers
     */
    public function __construct()
    {
        parent::__construct();

        $this->_set_timezone();
		$this->_has_module_access();
        $this->_load_models();
        $this->_load_helpers();
    }

    /* --------------------------------------------------------------
     * MODEL LOADING
     * ------------------------------------------------------------ */

    /**
     * Load models based on the $this->models array
     */
    public function _load_models()
    {
        //autoload models here
        $_models = array(
            // 'common/item_quantity'
        );

        $this->models = array_merge($this->models, $_models);

        foreach ($this->models as $model)
        {
            $this->load->model($this->_model_name($model), $this->_model_alias($model));
        }
    }

    /**
     * Returns the loadable model alias based on
     * the model formatting string
     */
    protected function _model_alias($model)
    {
        $alias = explode("/", $model);

        return (count($alias) > 1) ? $alias[(count($alias) - 1)] : $alias[0];
    }

    /**
     * Returns the loadable model name based on
     * the model formatting string
     */
    protected function _model_name($model)
    {
        return str_replace('%', $model, $this->model_string);
    }

    /* --------------------------------------------------------------
     * HELPER LOADING
     * ------------------------------------------------------------ */

    /**
     * Load helpers based on the $this->helpers array
     */
    public function _load_helpers()
    {
        foreach ($this->helpers as $helper)
        {
            $this->load->helper($helper);
        }
    }

    /* --------------------------------------------------------------
     * VALIDATIONS
     * ------------------------------------------------------------ */

    /**
     * Validates the session based on params
     */
    public function is_valid_session($user_type = NULL)
    {
        $valid = TRUE;

        // By username
        if (!isset($this->session->userdata['user_info']))
        {
            $valid = FALSE;
        }

        // By user type
        if ($user_type && $this->session->userdata['user_info']['type'] != $user_type)
        {
            $valid = FALSE;
        }

		if ($valid === FALSE)
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
	}

    private function _set_timezone()
    {
        $CI =& get_instance();
        $CI->load->model('common/loan_vars_model','loan_vars');
        $tz = $CI->loan_vars->get_by('type', 'Timezone');
        if(isset($tz['id']))
            $tz_set = $tz['value'];
        else
            $tz_set = 'Asia/Manila';

        date_default_timezone_set($tz_set);
    }

    private function _has_module_access()
    {
        $CI =& get_instance();
        $session = get_session();
        $msg = 'Access has been denied to this module! Please contact the system administrator regarding this issue.';
        
        //allow admin access
        if(!$session)
            return;
        
        $exclude = array(
            'user_login',
            'common',
            'dashboard',
            'reports',
            'user_request'
        );
        
        $uri = explode('/', $this->uri->uri_string());
        if(in_array($uri[0], $exclude))
            return;

        if($this->uri->uri_string() == 'user_info/end_session')
            return;
        
        $input = $CI->input->{strtolower($_SERVER['REQUEST_METHOD'])}();
        
        if(!isset($input['_m']) OR empty($input['_m']))
        {
            $this->_show_error($msg  . "[" . implode('/', $uri) . "]<br />_m not set!");
        }
        
        $CI->load->model('common/loan_user_access_model', 'user_access');
        $CI->load->model('common/loan_submodules_model', 'user_modules');

        $module = $CI->user_modules->get_by("encrypt(id, 'loan')='{$input['_m']}'");
        if(!isset($module['id']) AND $session['access_type'] != 'Administrator')
        {
            $this->_show_error("Request is not allowed!");
        }
       
        if($CI->user_access->count_by("user_typedef = '{$session['access_type']}' AND encrypt(module_id, 'loan') = '{$input['_m']}'") == 0 AND $session['access_type'] != 'Administrator')
        {
            $this->_show_error($msg  . "[" . implode('/', $uri) . "]<br />no user access!");
        }
    }

    private function _show_error($msg = '')
    {
        if(empty($msg))
            return;

        set_flash('error_msg', $msg);
        if(!$this->input->is_ajax_request())
        {
            redirect('dashboard/index');
        }
    }

}
