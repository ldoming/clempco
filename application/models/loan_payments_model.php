<?php

class Loan_payments_model extends Base_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_table = 'loan_payments';
    }

    public function read()
    {
    	$get = $this->input->get();
		
		if(!isset($get['loan_id']) OR empty($get['loan_id']))
			return;

    	$this->db->limit($get['limit'], $get['start']);
		
		$where = array(
			'loan_id' => $get['loan_id'],
			'archive' => 0
		);
    	$all = $this->order_by('created_at', 'asc')->get_many_by($where);

		$loan_info = $this->loan_application->get($get['loan_id']);
		$running_balance = $loan_info['principal'];
        foreach ($all as $key => $value)
        {
        	$apr = $loan_info['interest'] / 100;
			$apr2 = $apr / 12;
        	$payable = get_pmt($apr, $loan_info['period'], $loan_info['principal']);
			$interest = $running_balance * $apr2;
			$principal = $payable - $interest;
			$running_balance = $running_balance - $principal;
			
        	$value['payable'] = $payable;
			$value['principal'] = $principal;
			$value['interest'] = $interest;
			
			$value['running_balance']= $running_balance;
			$value['ou'] = $value['payment'] - $payable;
			$value['pay_date'] = date('M-d', strtotime($value['pay_date']));
            $all[$key] = $value;
        }


        success_msg($all);
    }

    private function _get_payable($loan_info)
    {
        $apr = $loan_info['interest'] / 100;
        $apr2 = $apr / 12;
        return get_pmt($apr, $loan_info['period'], $loan_info['principal']);
    }

    public function create()
    {
        try
        {
            $post = $this->input->post();

            $data = array(
                'pay_date' => date('Y-m-d', strtotime($post['ded_date'])),
                'member_id' => $post['member_id'],
                'loan_id' => $post['loan_id'],
                'payment' => $post['total'],
                'principal' => $post['principal'],
                'interest' => $post['interest'],
                'remarks' => $post['remarks']
            );

            if($post['action'] == 'create')
            {
                $data['created_at'] = date('Y-m-d H:i:s');
                $this->insert($data);
            }
            else
            {
                $data['updated_at'] = date('Y-m-d H:i:s');
                $this->update($post['id'], $data);
            }

            $this->_check_notify($post['loan_id']);
            success_msg("Payment has been successfully " . ($post['action'] == 'create' ? 'created' : 'updated') . '!');
        }
        catch(Exception $e)
        {
            error_msg($e->getMessage());
        }
    }
	
	public function destroy()
	{
		try
		{
			$post = $this->input->post();
			
			$update = array(
				'archive' => 1
			);
			
			$this->update($post['payment_id'], $update, FALSE);
			
			success_msg("Record has been deleted!");
		}
		catch(Exception $e)
		{
			error_msg($e->getMessage());
		}
	}

    public function get_total($loan_id = NULL)
    {
        if(is_null($loan_id))
            return;


        $query = $this->db->query("
                SELECT sum(payment) AS total FROM {$this->_table}
                    WHERE loan_id = $loan_id
                    AND archive = 0
            ")->row_array();

        return $query['total'];
    }

    public function _check_notify($loan_id)
    {
        $payment = $this->get_total($loan_id);
        $loan_info = $this->loan_application->get($loan_id);
        $payable = $this->_get_payable($loan_info);

        if($payment >= $payable / 2)
        {
            // $member = $this->loan_member_info->get($loan_info['member_id']);
            $update = array(
                'last_notify' => date('Y-m-d')
            );
            $this->loan_application->update($loan_id, $update);

            $this->email('balance', $loan_id);
        }
    }

    public function _last_paydate($loan_id = NULL)
    {
        $query = $this->db->query("
                SELECT MAX(pay_date) as pay_date FROM {$this->_table}
                    WHERE loan_id = $loan_id
                    AND archive = 0
            ")->row_array();

        return date('F j, Y', strtotime($query['pay_date']));
    }

    public function email($tpl = '', $id = NULL)
    {
        if(is_null($id) OR empty($tpl))
            return;

        $record = $this->loan_application->get($id);
        $record['time'] = date('M j, Y h:i:s a', strtotime($record['created_at']));
        $record['name'] = $this->loan_member_info->get_name($record['member_id']);
        $record['last_payment'] = $this->_last_paydate($id);
        $record['payment'] = $this->get_total($id);
        $email_address = $this->loan_member_info->get_email($record['member_id']);

        $body = $this->load->view('common/email/loan/' . $tpl, $record, TRUE);

        // $this->loan_email->add_bcc($this->get_emails());
        return $this->loan_email->send($email_address, 'Loan Application', $body);
    }

}