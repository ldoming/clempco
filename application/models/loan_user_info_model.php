<?php

class Loan_user_info_model extends Base_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_table = 'loan_user_info';
    }

    public function read()
    {
        $get = $this->input->get();
        $my_id = sess_user_id();

        if(isset($get['query']) AND !empty($get['query']))
        {
            $this->db->like("concat_ws(' ',firstname,lastname)", trim($get['query']));
        }

        $this->db->limit($get['limit'], $get['start']);
        
        $all = $this->get_many_by("`archive` = 0 AND id NOT IN ($my_id)");

        foreach ($all as $key => $value) 
        {
            $value['name'] = ucwords(strtolower("{$value['firstname']} {$value['lastname']}"));
            $all[$key] = $value;
        }

        success_msg($all);
    }

    public function destroy()
    {
        try
        {
            $post = $this->input->post();

            $this->update($post['user_id'], array('archive' => 1));

            success_msg("User has been deleted!");
        }
        catch(Exception $e)
        {
            error_msg($e->getMessage());
        }
    }

    public function create()
    {
        try
        {
            $post = $this->input->post();

            if($this->email_exists() AND $post['action'] == 'create')
                error_msg("Email adress is already registered!");

            if(!$this->loan_user_login->valid_login() AND $post['action'] == 'create')
                error_msg("Username already exists!");

            if(isset($post['password']) AND !empty($post['password']))
            {
                if(!valid_pass($post['password']))
                    error_msg("
                            Your password is too weak! We recommend a password that matches the following:<br />
                            - At least 8 characters in length<br />
                            - containing at least one lowercase letter<br />
                            - containing at least one uppercase letter<br />
                            - containing at least one number<br />
                            - and at least a special character (non-word characters)
                        ");
            }

            $insert = array(
                'firstname' => ucfirst(strtolower($post['firstname'])),
                'lastname' => ucfirst(strtolower($post['lastname'])),
                'user_typedef' => $post['user_typedef'],
                'address' => $post['address'],
                'tel_no' => $post['tel_no'],
                'email_address' => $post['email_address'],
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => sess_user_id()
            );
            
            if(isset($post['file_id']) AND !empty($post['file_id']))
            {
                $insert['file_id'] = $post['file_id'];
            }

            if($post['action'] == 'create')
            {
                $id = $this->insert($insert, FALSE);
                $pre = 'registered';
            }
            else
            {
                $this->update($post['id'], $insert);
                $id = $post['id'];
                $pre = 'updated';
            }
            
            //user_login
            $this->loan_user_login->create($id, $post['user_typedef'], $post['action']);

            success_msg("User info has been successfully {$pre}.");
        }
        catch(Exception $e)
        {
            error_msg($e->getMessage());
        }
    }

    public function email_exists()
    {
        $post = $this->input->post();

        if(!isset($post['email_address']) OR empty($post['email_address']))
            return FALSE;

        return $this->count_by("email_address", $post['email_address']) > 0;
    }

    public function end_session()
    {
        $this->session->unset_userdata('User Info', FALSE);
        unset($this->session->userdata['User Info']);
        redirect('user_login/index');
    }

    public function edit()
    {
        $session = get_session();

        $user_info = $this->get($session['id']);
        $login_info = $this->loan_user_login->get_by('user_id', $session['id']);

        unset($login_info['id'], $login_info['password'], $login_info['access_type']);

        $user_info['picture'] = $this->loan_user_files->get_src($user_info['file_id']);

        success_msg($user_info + $login_info);
    }

}