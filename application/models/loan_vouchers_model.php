<?php

class Loan_vouchers_model extends Base_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_table = 'loan_vouchers';
    }

    public function read()
    {
    	$get = $this->input->get();

        if(isset($get['query']) AND !empty($get['query']))
        {
            $this->db->like("remarks", trim($get['query']));
        }

        if(isset($get['vtype']) AND !empty($get['vtype']))
        {
            $this->db->where("voucher_type", trim($get['vtype']));
        }

    	$this->db->limit($get['limit'], $get['start']);
		
		$where = array(
			'archive' => 0
		);
    	$all = $this->order_by('created_at', 'desc')->get_many_by($where);

    	foreach ($all as $key => $value)
    	{
    		$value['total_debit'] = $this->loan_voucher_particulars->get_total('debit', $value['id']);
    		$value['total_credit'] = $this->loan_voucher_particulars->get_total('credit', $value['id']);
    		$value['voucher_date'] = date('M j, Y', strtotime($value['voucher_date']));

    		$all[$key] = $value;
    	}

    	success_msg($all);
    }
	
	public function create()
	{
		try
		{
			$post = $this->input->post();
			$particulars = json_decode($post['particulars']);

			if($post['voucher_type'] == 'Cash Voucher')
			{
				$insert = array(
					'check_no' => isset($post['check_no']) ? $post['check_no'] : NULL,
					'voucher_no' => $post['voucher_no'],
					'tin_no' => isset($post['tin_no']) ? $post['tin_no'] : NULL,
					'remarks' => $post['remarks'],
					'created_at' => date('Y-m-d H:i:s'),
					'created_by' => sess_user_id(),
					'voucher_type' => $post['voucher_type'],
					'voucher_date' => date('Y-m-d H:i:s', strtotime($post['voucher_date'])),
					'acct_id' => $post['acct_id']
				);
			}
			elseif($post['voucher_type'] == 'Journal Voucher')
			{
				$insert = array(
					'voucher_date' => date('Y-m-d H:i:s', strtotime($post['voucher_date'])),
					'remarks' => $post['remarks'],
					'created_at' => date('Y-m-d H:i:s'),
					'created_by' => sess_user_id(),
					'voucher_type' => $post['voucher_type']
				);
			}
			else
			{
				$insert = array(
					'voucher_no' => $post['voucher_no'],
					'acct_name' => $post['voucher_name'],
					'amount' => $post['v_amount'],
					'voucher_date' => date('Y-m-d H:i:s', strtotime($post['voucher_date'])),
					'remarks' => $post['remarks'],
					'created_at' => date('Y-m-d H:i:s'),
					'created_by' => sess_user_id(),
					'voucher_type' => $post['voucher_type'],
					'acct_id' => $post['acct_id']
				);
			}
			
			$id = $this->insert($insert, FALSE);

			//insert particulars
			foreach($particulars as $key => $value)
			{
				$value = (array)$value;
				$value['voucher_id'] = $id;

				$this->loan_voucher_particulars->insert($value);
			}
			
			success_msg("A new voucher has been successfully entered!");
		}
		catch(Exception $e)
		{
			error_msg($e->getMessage());
		}
	}
	
	public function destroy()
	{
		try
		{
			$post = $this->input->post();
			
			$update = array(
				'archive' => 1
			);
			
			$this->update($post['voucher_id'], $update, FALSE);
			
			success_msg("Record has been deleted!");
		}
		catch(Exception $e)
		{
			error_msg($e->getMessage());
		}
	}

	public function get_totals($id = NULL, $vtype = '')
	{
		if(is_null($id) OR empty($vtype))
			return;

		$debit = 0;
		$credit = 0;

		$particulars = $this->db->query("
			SELECT * FROM loan_voucher_particulars a
				JOIN loan_vouchers b ON b.id = a.voucher_id
			WHERE a.acct_id = '$id'
			AND b.voucher_type = '$vtype'
			AND b.archive = 0
		")->result_array();

		foreach ($particulars as $key => $value)
		{
			$debit += $value['debit'];
			$credit += $value['credit'];
		}

		return array($debit, $credit);
	}

	public function export()
	{
		try
		{
			$post = $this->input->post();

			if(!isset($post['voucher_id']) OR empty($post['voucher_id']))
				error_msg('This action is not allowed!');

			$voucher = $this->get($post['voucher_id']);

			switch ($voucher['voucher_type']) 
			{
				case 'Cash Voucher':
					$this->_export_cv($voucher);
					break;
				
				case 'Journal Voucher':
					$this->_export_jv($voucher);
					break;

				case 'Petty Cash Voucher':
					$this->_export_pcv($voucher);
					break;

				default:
					# code...
					break;
			}

			$pre = pad_left($voucher['id'], 5);
			$fname = strtolower(str_replace(' ', '', $voucher['voucher_type'])) . "{$pre}.xls";
			$filename = str_replace(' ', '', "documents/exports/{$fname}");

			if(file_exists($filename))
			{
				$filename = str_replace(' ', '', "documents/exports/" . time() . "_{$fname}");
			}

			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			$objWriter->save($filename);

			out_json(array(
				'success' => TRUE,
				'msg' => 'Your download is ready!',
				'download' => base_url() . $filename
			));
		}
		catch(Exception $e)
		{
			error_msg($e->getMessage());
		}
	}

	public function get_vno()
	{
		$code = $this->input->post('code');
		$uid = sess_user_id();
		$max = $this->count_all();
		success_msg($code . pad_left($uid, 3) . pad_left($max + 1, 4));
	}

	private function _export_cv($voucher = NULL)
	{
		if(is_null($voucher))
			return;

			$payee = $this->loan_accounts->get($voucher['acct_id']);

			$this->load->library('excel');

			$this->excel->setActiveSheetIndex(0);
			$this->excel->getDefaultStyle()->getFont()->setName('Segoe UI');

			//Report Header
			$this->excel->getActiveSheet()->setCellValue('A1', 'COTABATO LIGHT EMPLOYEES MULTI-PURPOSE COOPERATIVE');
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A1:I1');
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			//Report Sub-header
			$this->excel->getActiveSheet()->setCellValue('A2', 'CASH VOUCHER');
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
			$this->excel->getActiveSheet()->mergeCells('A2:I2');
			$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$row = 4;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'PAYEE CODE');
			$this->excel->getActiveSheet()->setCellValue('B' . $row, 'TRANS CODE');
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 'CHECK #');	
			$this->excel->getActiveSheet()->setCellValue('D' . $row, 'REMARKS');
			$this->excel->getActiveSheet()->setCellValue('E' . $row, 'CV #');

			$row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, pad_left($voucher['acct_id'], 6));
			$this->excel->getActiveSheet()->setCellValue('B' . $row, pad_left($voucher['id'], 6));
			$this->excel->getActiveSheet()->setCellValue('C' . $row, $voucher['check_no']);	
			$this->excel->getActiveSheet()->setCellValue('D' . $row, $voucher['remarks']);
			$this->excel->getActiveSheet()->setCellValue('E' . $row, $voucher['voucher_no']);

			$row+=3;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'PARTICULARS');

			$row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'ACCT CODE');
			$this->excel->getActiveSheet()->setCellValue('B' . $row, 'DESCRIPTION');
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 'DEBIT');	
			$this->excel->getActiveSheet()->setCellValue('D' . $row, 'CREDIT');

			$row++;
			$particulars = $this->loan_voucher_particulars->get_many_by('voucher_id', $voucher['id']);

			$tot_debit = 0;
			$tot_credit = 0;

			foreach ($particulars as $key => $value) 
			{
				// $account = $this->loan_accounts->get($voucher['acct_id']);

				$this->excel->getActiveSheet()->setCellValue('A' . $row, $value['acct_code']);
				$this->excel->getActiveSheet()->setCellValue('B' . $row, $value['description']);
				$this->excel->getActiveSheet()->setCellValue('C' . $row, $value['debit']);	
				$this->excel->getActiveSheet()->setCellValue('D' . $row, $value['credit']);

				$tot_debit += $value['debit'];
				$tot_credit += $value['credit'];
				$row++; 
			}

			//add extra space for 5 rows
			$row += 5;

			$this->excel->getActiveSheet()->setCellValue('C' . $row, $tot_debit);
			$this->excel->getActiveSheet()->setCellValue('D' . $row, $tot_credit);

			$row  += 2;
			// $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(200);

			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Prepared By');
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 'Audited By');

			$row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Checked By');
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 'Approved By');

			$row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Received the sum of ' . str_repeat('_', 30));
			$this->excel->getActiveSheet()->mergeCells("A{$row}:C{$row}");
			$row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Pesos (P          ) in full settlement of the above account.');
			$this->excel->getActiveSheet()->mergeCells("A{$row}:C{$row}");
	}
	
	private function _export_jv($voucher = NULL)
	{
		if(is_null($voucher))
			return;

			$payee = $this->loan_accounts->get($voucher['acct_id']);

			$this->load->library('excel');

			$this->excel->setActiveSheetIndex(0);
			$this->excel->getDefaultStyle()->getFont()->setName('Segoe UI');

			//Report Header
			$this->excel->getActiveSheet()->setCellValue('A1', 'COTABATO LIGHT EMPLOYEES MULTI-PURPOSE COOPERATIVE');
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A1:I1');
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			//Report Sub-header
			$this->excel->getActiveSheet()->setCellValue('A2', 'JOURNAL VOUCHER');
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
			$this->excel->getActiveSheet()->mergeCells('A2:I2');
			$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$row = 4;
			// $this->excel->getActiveSheet()->setCellValue('A' . $row, 'PAYEE CODE');
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'TRANS CODE');
			// $this->excel->getActiveSheet()->setCellValue('B' . $row, 'CHECK #');	
			$this->excel->getActiveSheet()->setCellValue('B' . $row, 'REMARKS');
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 'CV #');

			$row++;
			// $this->excel->getActiveSheet()->setCellValue('A' . $row, pad_left($voucher['acct_id'], 6));
			$this->excel->getActiveSheet()->setCellValue('A' . $row, pad_left($voucher['id'], 6));
			// $this->excel->getActiveSheet()->setCellValue('B' . $row, $voucher['check_no']);	
			$this->excel->getActiveSheet()->setCellValue('B' . $row, $voucher['remarks']);
			$this->excel->getActiveSheet()->setCellValue('C' . $row, $voucher['voucher_no']);

			$row+=3;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'PARTICULARS');

			$row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'ACCT CODE');
			$this->excel->getActiveSheet()->setCellValue('B' . $row, 'DESCRIPTION');
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 'DEBIT');	
			$this->excel->getActiveSheet()->setCellValue('D' . $row, 'CREDIT');

			$row++;
			$particulars = $this->loan_voucher_particulars->get_many_by('voucher_id', $voucher['id']);

			$tot_debit = 0;
			$tot_credit = 0;

			foreach ($particulars as $key => $value) 
			{
				// $account = $this->loan_accounts->get($voucher['acct_id']);

				$this->excel->getActiveSheet()->setCellValue('A' . $row, $value['acct_code']);
				$this->excel->getActiveSheet()->setCellValue('B' . $row, $value['description']);
				$this->excel->getActiveSheet()->setCellValue('C' . $row, $value['debit']);	
				$this->excel->getActiveSheet()->setCellValue('D' . $row, $value['credit']);

				$tot_debit += $value['debit'];
				$tot_credit += $value['credit'];
				$row++; 
			}

			//add extra space for 5 rows
			$row += 5;

			$this->excel->getActiveSheet()->setCellValue('C' . $row, $tot_debit);
			$this->excel->getActiveSheet()->setCellValue('D' . $row, $tot_credit);

			$row  += 2;
			// $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(200);

			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Prepared By');
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 'Audited By');

			$row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Checked By');
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 'Approved By');

			$row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Received the sum of ' . str_repeat('_', 30));
			$this->excel->getActiveSheet()->mergeCells("A{$row}:C{$row}");
			$row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Pesos (P          ) in full settlement of the above account.');
			$this->excel->getActiveSheet()->mergeCells("A{$row}:C{$row}");
	}
	
	private function _export_pcv($voucher = NULL)
	{
		if(is_null($voucher))
			return;

			$this->load->library('excel');

			$this->excel->setActiveSheetIndex(0);
			$this->excel->getDefaultStyle()->getFont()->setName('Segoe UI');

			//Report Header
			$this->excel->getActiveSheet()->setCellValue('A1', 'COTABATO LIGHT EMPLOYEES MULTI-PURPOSE COOPERATIVE');
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A1:I1');
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			//Report Sub-header
			$this->excel->getActiveSheet()->setCellValue('A2', 'PETTY CASH VOUCHER');
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
			$this->excel->getActiveSheet()->mergeCells('A2:I2');
			$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$row = 4;
			// $this->excel->getActiveSheet()->setCellValue('A' . $row, 'PAYEE CODE');
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'NAME: ' . $voucher['acct_name']);
			$this->excel->getActiveSheet()->mergeCells("A{$row}:B{$row}");
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 'DATE: ' . date('M j, Y', strtotime($voucher['created_at'])));

			$row+=3;
			// $this->excel->getActiveSheet()->setCellValue('A' . $row, 'PARTICULARS');

			// $row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'ITEM #');
			$this->excel->getActiveSheet()->setCellValue('B' . $row, 'PARTICULARS');
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 'AMOUNT');

			$row++;
			$particulars = $this->loan_voucher_particulars->get_many_by('voucher_id', $voucher['id']);

			$t_amount = 0;
			$item_no = 1;
			foreach ($particulars as $key => $value) 
			{
				// $account = $this->loan_accounts->get($voucher['acct_id']);

				$this->excel->getActiveSheet()->setCellValue('A' . $row, pad_left($item_no, 4));
				$this->excel->getActiveSheet()->setCellValue('B' . $row, $value['description']);
				$this->excel->getActiveSheet()->setCellValue('C' . $row, $value['amount']);

				$t_amount += $value['amount'];
				$item_no++;
				$row++; 
			}

			//add extra space for 5 rows
			$row += 5;

			$this->excel->getActiveSheet()->setCellValue('C' . $row, $t_amount);

			$row  += 2;
			// $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(200);

			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Prepared By');
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 'Audited By');

			$row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Checked By');
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 'Approved By');

			$row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Received the sum of ' . str_repeat('_', 30));
			$this->excel->getActiveSheet()->mergeCells("A{$row}:C{$row}");
			$row++;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, 'Pesos (P          ) in full settlement of the above account.');
			$this->excel->getActiveSheet()->mergeCells("A{$row}:C{$row}");
	}
	
}