<?php

class Loan_status_history_model extends Base_Model
{

	function __construct()
	{
		parent::__construct();

		$this->_table = 'loan_status_history';
	}

	public function read()
	{
		$get = $this->input->get();

		$statuses = $this->order_by('id', 'desc')->get_many_by('loan_id', $get['loan_id']);

		success_msg($statuses);
	}

	public function create()
	{
		try
		{
			$post = $this->input->post();
			$session = get_session();
			$loan_info = $this->loan_application->get($post['loan_id']);

			if(in_array($loan_info['status'], array('Approved', 'Declined')) AND $session['access_type'] != 'Chairperson')
			{
				error_msg("This loan has already been {$loan_info['status']} by the Chairperson!");
			}

			if($session['access_type'] == 'Chairperson')
			{
				$update = array(
					'status' => $post['status']
				);

				$this->loan_application->update($post['loan_id'], $update);
			}


			$insert = array(
				'loan_id' => $post['loan_id'],
				'comment' => $post['comment'],
				'user_id' => sess_user_id(),
				'personnel' => $session['access_type'],
				'status' => $post['status']
			);

			$this->insert($insert, TRUE);

			success_msg("Loan history has been updated!");
		}
		catch(Exception $e)
		{
			error_msg($e->getMessage());
		}
	}

}