<?php

class Reports_model extends Base_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function generate()
    {
    	$get = $this->input->get();
    	$method = str_replace(' ', '_', strtolower($get['rtype']));

    	if(!isset($get['rtype']) OR empty($get['rtype']) OR !isset($get['period-month']) OR empty($get['period-month']) OR !isset($get['period-year']) OR empty($get['period-year']) OR !method_exists($this, $method))
    	{
    		show_error("This transaction is not allowed!", 400);
    	}

    	$this->{$method}();
    }

    public function trial_balance()
    {
    	$get = $this->input->get();

        if($get['rt-type'] == 1)
        {
            $date_year = date("Y", strtotime("{$get['period-year']}-{$get['period-month']}-01"));
            $day = last_day_of_month("{$get['period-year']}-{$get['period-month']}-01");
            $date_from = date("Y-m-d", strtotime("{$date_year}-{$get['period-month']}-01"));
            $date_to = "{$date_year}-{$get['period-month']}-$day";

            $date_where = " AND acct_period between '$date_from' AND '$date_to'";
        }
        else
        {
            $date_where = " AND date_format(acct_period, '%Y-%m') = '{$get['period-year']}-{$get['period-month']}'";
        }
		$accounts = $this->db->query("
            SELECT * FROM loan_accounts
                WHERE acct_level = 1
                $date_where
        ")->result_array();

		$data = array(
			'title' => "Trial Balance as of " . 
                    date('M Y', strtotime("{$get['period-year']}-" . pad_left($get['period-month'], 2) ."-01")),
			'grp_accounts' => $accounts,
			'app' => 'reports',
			'page' => 'trial_balance',
			'content' => 'content_full',
			'period' => "{$get['period-year']}-{$get['period-month']}"
		);

		load_template($data);
    }

    public function monthly_deduction()
    {
        $this->loan_deductions->generate();
    }

    public function capital_savings_withdrawal()
    {
        $this->loan_deductions->generate_csw();
    }
        
    public function get_years()
    {
    	$query = $this->db->query("
    		SELECT distinct date_format(created_at, '%Y') as year
    			FROM loan_application
    		UNION
    		SELECT distinct date_format(ded_date, '%Y') as year
    			FROM loan_deductions
    		UNION
    		SELECT distinct date_format(pay_date, '%Y') as year
    			FROM loan_payments
    		UNION
    		SELECT distinct date_format(acct_period, '%Y') as year
    			FROM loan_accounts
    		UNION
    		SELECT distinct date_format(voucher_date, '%Y') as year
    			FROM loan_vouchers
    	")->result_array();

    	return $query;
    }

    public function income_statement()
    {
        
    }

    public function balance_sheet()
    {
        $get = $this->input->get();
        $period = "{$get['period-year']}-{$get['period-month']}-01";
        
        $period2 = date("M_y", strtotime($period));
        $lday = last_day_of_month($period);
        
        $this->load->library('excel');
        
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getDefaultStyle()->getFont()->setName('Segoe UI');
        
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(2);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

        // $this->excel->getActiveSheet()->getRowDimension('1')->setHeight(20);
        
        $this->excel->getActiveSheet()->setTitle('BalanceSheet_' . $period2);
        
        //Report Header
        $this->excel->getActiveSheet()->setCellValue('A1', 'COTABATO LIGHT EMPLOYEES MULTI-PURPOSE COOPERATIVE');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:I1');
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        //Report Sub-header
        $this->excel->getActiveSheet()->setCellValue('A2', 'Balance Sheet');
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
        $this->excel->getActiveSheet()->mergeCells('A2:I2');
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $this->excel->getActiveSheet()->setCellValue('A3', date("F {$lday}, Y", strtotime($period)));
        $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(13);
        $this->excel->getActiveSheet()->mergeCells('A3:I3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $row = 4;
        //Assets
        $this->excel->getActiveSheet()->setCellValue('A' . $row, 'Assets');
        $this->excel->getActiveSheet()->setCellValue('F' . $row, 'Liabilities');

        $row++;
        $this->excel->getActiveSheet()->setCellValue('A' . $row, 'Cash');
        $this->excel->getActiveSheet()->setCellValue('F' . $row, 'Accounts payable');

        $row++;
        $this->excel->getActiveSheet()->setCellValue('A' . $row, 'Accounts receivable');
        $this->excel->getActiveSheet()->setCellValue('F' . $row, 'Salaries payable');

        $row++;
        $this->excel->getActiveSheet()->setCellValue('F' . $row, 'Interest Payable');

        $row++;
        $this->excel->getActiveSheet()->setCellValue('F' . $row, 'Taxes Payable');

        $row+= 4;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "Prepared By");
        
        $row+= 3;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "WHILHELMINA W. ALOMBRO");
        
        $row++;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "CREDIT COMM");
        
        $fname = "BalanceSheet_{$period2}";
        $filename = "documents/exports/{$fname}";
        
        if (file_exists($filename . '.xls')) {
            $filename = "documents/exports/{$fname}_" . time();
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save($filename . ".xls");
        
        set_flash('success_msg', '<a href="' . base_url() . $filename . '.xls' . '">Your download is ready! Click here to download your file.</a>');
        redirect('reports/index');
    }
}