<?php

class Loan_member_info_model extends Base_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_table = 'loan_member_info';
    }

    public function read($return = FALSE)
    {
    	$get = $this->input->get();

        if(isset($get['query']) AND !empty($get['query']))
        {
            $this->db->like("concat_ws(' ',firstname,lastname)", trim($get['query']));
        }

    	$this->db->limit($get['limit'], $get['start']);
		
		$where = array(
			'archive' => 0
		);
    	$all = $this->order_by('lastname')->get_many_by($where);

    	foreach ($all as $key => $value) 
    	{
    		$login = $this->loan_user_login->get_by('user_id', $value['id']);
    		$value['username'] = $login['username'];
    		$value['name'] = ucwords(strtolower("{$value['firstname']} {$value['lastname']}"));
    		$value['picture'] = $this->loan_user_files->get_src($value['file_id']);

    		$all[$key] = $value;
    	}

    	if($return)
    		return $all;
    	else
    		success_msg($all);
    }
	
	public function read1()
	{
		$get = $this->input->get();
		
		if(isset($get['query']) AND !empty($get['query']))
        {
            $this->db->like("position", trim($get['query']));
        }
		
		$all = $this->order_by('position')->get_all();
		
		success_msg($all);
	}
	
    public function read2()
    {

		$where = array(
			'archive' => 0
		);
    	$all = $this->order_by('lastname')->get_many_by($where);

    	foreach ($all as $key => $value) 
    	{
    		$value['name'] = ucwords(strtolower("{$value['firstname']} {$value['lastname']}"));

    		$all[$key] = $value;
    	}

    	return $all;
    }
	
	public function create()
	{
		try
		{
			$post = $this->input->post();
			
			$insert = array(
				'firstname' => ucfirst(strtolower($post['firstname'])),
                'lastname' => ucfirst(strtolower($post['lastname'])),
				'position' => isset($post['position']) ? $post['position'] : NULL,
				'application_id' => isset($post['application_id']) ? $post['application_id'] : NULL,
				'birth_date' => isset($post['birth_date']) ? date('Y-m-d', strtotime($post['birth_date'])) : NULL,
				'civil_status' => isset($post['civil_status']) ? $post['civil_status'] : NULL,
				'address' => $post['address'],
				'id_no' => isset($post['id_no']) ? $post['id_no'] : NULL,
				'passport_no' => isset($post['passport_no']) ? $post['passport_no'] : NULL,
				'ctc_no' => isset($post['ctc_no']) ? $post['ctc_no'] : NULL,
				'monthly_income' => isset($post['monthly_income']) ? $post['monthly_income'] : NULL,
				'cell_no' => isset($post['cell_no']) ? $post['cell_no'] : NULL,
				'home_phone' => isset($post['home_phone']) ? $post['home_phone'] : NULL,
				'email_address' => $post['email_address'],
				'email_address1' => isset($post['email_address1']) ? $post['email_address1'] : NULL,
				'company_name' => isset($post['company_name']) ? $post['company_name'] : NULL,
				'company_address' => isset($post['company_address']) ? $post['company_address'] : NULL,
				'company_sss' => isset($post['company_sss']) ? $post['company_sss'] : NULL,
				'philhealth_no' => isset($post['philhealth_no']) ? $post['philhealth_no'] : NULL,
				'office_no' => isset($post['office_no']) ? $post['office_no'] : NULL,
				'office_fax' => isset($post['office_fax']) ? $post['office_fax'] : NULL,
				'employer_id' => isset($post['employer_id']) ? $post['employer_id'] : NULL
			);

			if(isset($post['file_id']) AND !empty($post['file_id']))
			{
				$insert['file_id'] = $post['file_id'];
			}

            if(isset($post['password']) AND !empty($post['password']))
            {
                if(!valid_pass($post['password']))
                    error_msg("
                            Your password is too weak! We recommend a password that matches the following:<br />
                            - At least 8 characters in length<br />
                            - containing at least one lowercase letter<br />
                            - containing at least one uppercase letter<br />
                            - containing at least one number<br />
                            - and at least a special character (non-word characters)
                        ");
            }

            if(!$this->valid_email2())
					error_msg("Your primarily and secondary email address should not be the same!");

			if($post['action'] == 'create')
			{
				if(!$this->valid_email())
					error_msg("Email address is already registered!");
				
	            if(!$this->loan_user_login->valid_login())
	                error_msg("Username already exists!");

				$insert['created_at'] = date('Y-m-d H:i:s');
				$insert['created_by'] = sess_user_id();

				$id = $this->insert($insert, FALSE);
			}
			elseif($post['action'] == 'update')
			{
				$insert['updated_at'] = date('Y-m-d H:i:s');
				$this->update($post['id'], $insert, FALSE);
				$id = $post['id'];
			}
			
            //user_login
            $this->loan_user_login->create($id, 'Member', $post['action']);

			success_msg("Member '{$insert['firstname']} {$insert['lastname']}' has been successfully " . ucfirst($post['action']) . 'd!');
		}
		catch(Exception $e)
		{
			error_msg($e->getMessage());
		}
	}
	
    public function valid_email()
    {
        $post = $this->input->post();

        if(!isset($post['email_address']) OR empty($post['email_address']))
            return FALSE;

        return $this->is_unique_by("email_address = '{$post['email_address']}'");
    }

    public function valid_email2()
    {
        $post = $this->input->post();

        return $post['email_address'] != $post['email_address1'];
    }

    public function get_name($id = NULL)
    {
    	if(is_null($id))
    		return;
    	$record = $this->get($id);

    	if(isset($record['id']))
    		return ucwords(strtolower("{$record['firstname']} {$record['lastname']}"));
    	else
    		return NULL;
    }

    public function get_email($id = NULL)
    {
    	if(is_null($id))
    		return;

    	$record = $this->get($id);

    	if(isset($record['id']))
    		return $record['email_address'];
    	else
    		return NULL;
    }

	public function destroy()
	{
		try
		{
			$post = $this->input->post();
			
			$update = array(
				'archive' => 1
			);
			
			if($this->has_active_accounts($post['employee_id']))
				error_msg('Unable to delete member! Member has active accounts.');

			$this->update($post['employee_id'], $update, FALSE);
			
			success_msg("Record has been deleted!");
		}
		catch(Exception $e)
		{
			error_msg($e->getMessage());
		}
	}

	public function has_active_accounts($member_id = NULL)
	{
		if(is_null($member_id))
			return FALSE;

		return $this->loan_application->count_by("member_id = $member_id AND status IN ('New', 'Approved')") > 0;
	}

	public function balance_check()
	{
		$post = $this->input->post();
		$valid = TRUE;

		list($legible, $details) = $this->loan_application->get_active_total($post['member_id'], $post['type']);

		$data = array(
			'success' => TRUE,
			'legible' => $legible,
			'data' => 'This member is ' . ($legible ? '' : ' not ') . ' legible to apply for ' . $post['type'] . ' at this moment!',
			'details' => $details
		);

		out_json($data);
	}

    public function edit()
    {
        $session = get_session();

        $user_info = $this->get($session['id']);
        $login_info = $this->loan_user_login->get_by('user_id', $session['id']);

        unset($login_info['id'], $login_info['password'], $login_info['access_type']);

        $user_info['picture'] = $this->loan_user_files->get_src($user_info['file_id']);

        success_msg($user_info + $login_info);
    }

}