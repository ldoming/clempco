<?php
class Loan_deductions_model extends Base_Model
{
    
    function __construct() {
        parent::__construct();
        
        $this->_table = 'loan_deductions';
    }
    
    public function read() {
        $get = $this->input->get();
        $dsql = '';
        $sql = '';
        
        $session = get_session();
        if ($session['access_type'] == 'Member') {
            $sql.= " AND a.member_id = {$session['id']}";
        } else {
            if (isset($get['member_id']) AND !empty($get['member_id'])) {
                $sql.= " AND a.member_id = {$get['member_id']}";
            }
        }
        
        if (isset($get['period']) AND !empty($get['period'])) {
            $ded_date = date('Y-m', strtotime(str_replace('T', ' ', $get['period'])));
        }
        
        if (isset($get['ded_type']) AND !empty($get['ded_type'])) {
            if ($get['ded_type'] == 'deduction') {
                if (isset($get['loan_type']) AND !empty($get['loan_type'])) $dsql.= " AND a.type = '{$get['loan_type']}'";
                
                if(isset($ded_date))
                    $dsql = " AND date_format(b.pay_date, '%Y-%m') = '{$ded_date}'";

                $all = $this->db->query("
                    SELECT b.*, 'payment' as record_type, b.payment as total, b.pay_date as ded_date, a.member_id, a.id as loan_id  FROM loan_payments b
                        LEFT JOIN loan_application a ON a.id = b.loan_id
                    WHERE b.archive = 0
                        AND a.archive = 0
                    {$dsql}
                    {$sql}
                ")->result_array();
            } else {
                if(isset($ded_date))
                    $dsql = " AND date_format(a.ded_date, '%Y-%m') = '{$ded_date}'";
                
                $all = $this->db->query("
                    SELECT a.*, a.ded_type as record_type FROM {$this->_table} a
                        LEFT JOIN loan_member_info c ON c.id = a.member_id
                    WHERE a.archive = 0
                        AND c.archive = 0
                        AND a.ded_type = '{$get['ded_type']}'
                    {$dsql}
                    {$sql}
                ")->result_array();
            }
        } else {
            if (isset($get['loan_type']) AND !empty($get['loan_type'])) {
                $sql.= " AND b.type = '{$get['loan_type']}'";
            }
            $all = $this->db->query("
                    SELECT * FROM (
                        SELECT a.ded_type as record_type, a.total, a.ded_date, a.member_id, NULL as type, NULL as loan_id, NULL as principal, NULL as interest, a.created_at FROM {$this->_table} a
                            LEFT JOIN loan_member_info c ON c.id = a.member_id
                        WHERE a.archive = 0
                            AND c.archive = 0
                            {$sql}

                        UNION

                        SELECT 'payment' as record_type, b.payment as total, b.pay_date as ded_date, a.member_id, a.type, b.loan_id, a.principal, a.interest, a.created_at FROM loan_payments b
                            LEFT JOIN loan_application a ON a.id = b.loan_id
                        WHERE b.archive = 0
                            AND a.archive = 0
                            {$sql}
                    ) z
                    order by z.created_at desc
                ")->result_array();
        }
        
        foreach ($all as $key => $value) {
            if ($value['record_type'] == 'payment') {
                $loan_info = $this->loan_application->get($value['loan_id']);
                $value['name'] = $this->loan_member_info->get_name($loan_info['member_id']);
            } else {
                $value['name'] = $this->loan_member_info->get_name($value['member_id']);
            }

            $value['ded_date'] = date('F Y', strtotime($value['ded_date']));
            $value['record_type'] = ucfirst($value['record_type']);
            
            if (isset($value['loan_id'])) {
                $loan_info = $this->loan_application->get($value['loan_id']);
                $value['loan_info'] = array(
                    'id' => $loan_info['id'],
                    'type' => $loan_info['type'],
                    'status' => $loan_info['status'],
                    'principal' => number_format($value['principal'], 2)
                );
            }
            
            $value['total'] = number_format($value['total'], 2);
            
            if (!is_null($value['interest'])) $value['interest'] = number_format($value['interest'], 2);
            
            if (!is_null($value['principal'])) $value['principal'] = number_format($value['principal'], 2);
            
            $all[$key] = $value;
        }
        
        success_msg($all);
    }
    
    public function read2($return = FALSE, $data = NULL, $format = TRUE) {
        $get = is_null($data) ? $this->input->get() : $data;
        $data = array(
            'success' => TRUE,
            'data' => array()
        );
        
        $ded_date = date('Y-m', strtotime(str_replace('Y', ' ', $get['period'])));
        
        $sql = '';
        $session = get_session();
        
        if ($session['access_type'] == 'Member') {
            $sql.= " AND a.id = {$session['id']}";
        } else {
            if (isset($get['member_id']) AND !empty($get['member_id'])) {
                $sql.= " AND a.id = {$get['member_id']}";
            }
        }
        
        $members = $this->db->query("
                SELECT a.id, a.firstname, a.lastname FROM loan_member_info a
                WHERE a.archive = 0 " . $sql . " ORDER BY a.lastname ASC")->result_array();
        
        $date_headers = array();
        
        $fields = array(
            'name',
            'insurance',
            'total_deduction',
            'id',
            'cb_amount',
            'sa_amount'
        );
        foreach ($members as $key => $value) {
            $value['name'] = ucwords(strtolower("{$value['firstname']} {$value['lastname']}"));
            $ded_dates = $this->db->query("
                    SELECT a.*, b.type FROM loan_payments a 
                        JOIN loan_application b ON b.id = a.loan_id 
                    WHERE b.member_id = {$value['id']}
                        AND a.archive = 0 
                        AND b.archive = 0 
                        AND date_format(pay_date, '%Y-%m') = '{$ded_date}'" . ((isset($get['loan_type']) AND !empty($get['loan_type'])) ? " AND b.type = '{$get['loan_type']}' " : '') . " ORDER BY a.pay_date ASC")->result_array();
            
            $tot_ded = 0;
            if (count($ded_dates) > 0) {
                foreach ($ded_dates as $dkey => $dvalue) {
                    $_header = array(
                        'ded_date' => $dvalue['pay_date'],
                        'type' => $dvalue['type']
                    );
                    $date_headers[] = $_header;
                    
                    $dvalue['pay_date'] = date('Ymd', strtotime($dvalue['pay_date']));
                    
                    $_data = array();
                    $_key = $dvalue['pay_date'] . '_' . str_replace(' ', '', $dvalue['type']);
                    
                    $fields[] = 'principal_' . $_key;
                    $fields[] = 'interest_' . $_key;
                    $fields[] = 'total_' . $_key;
                    
                    $_data['principal_' . $_key] = $format ? float_to_money($dvalue['principal']) : $dvalue['principal'];
                    $_data['interest_' . $_key] = $format ? float_to_money($dvalue['interest']) : $dvalue['interest'];
                    $_data['total_' . $_key] = $format ? float_to_money($dvalue['payment']) : $dvalue['payment'];
                    
                    $tot_ded+= $dvalue['payment'];
                    $value = array_merge($value, $_data);
                }
            }
            
            //read insurance
            $insurance = $this->db->query("
                    SELECT id, total FROM loan_deductions
                        WHERE member_id = {$value['id']}
                        AND ded_type = 'insurance'
                        AND date_format(ded_date, '%Y-%m') = '{$ded_date}'
                        AND archive = 0
                ")->row_array();
            
            if (isset($insurance['id'])) {
                $tot_ded+= $insurance['total'];
                $value['insurance'] = $format ? float_to_money($insurance['total']) : $insurance['total'];
            }
            
            //read savings
            $savings = $this->db->query("
                    SELECT id, total FROM loan_deductions
                        WHERE member_id = {$value['id']}
                        AND ded_type = 'savings'
                        AND date_format(ded_date, '%Y-%m') = '{$ded_date}'
                        AND archive = 0
                ")->row_array();
            
            if (isset($savings['id'])) {
                $tot_ded+= $savings['total'];
                $value['sa_amount'] = $format ? float_to_money($savings['total']) : $savings['total'];
            }
            
            //read capital build-up
            $capital = $this->db->query("
                    SELECT id, total FROM loan_deductions
                        WHERE member_id = {$value['id']}
                        AND ded_type = 'capital'
                        AND date_format(ded_date, '%Y-%m') = '{$ded_date}'
                        AND archive = 0
                ")->row_array();
            
            if (isset($capital['id'])) {
                $tot_ded+= $capital['total'];
                $value['cb_amount'] = $format ? float_to_money($capital['total']) : $capital['total'];
            }
            
            $value['id'] = pad_left($value['id'], 4);
            $value['total_deduction'] = $format ? float_to_money($tot_ded) : $tot_ded;
            $data['data'][] = $value;
        }
        
        $data['date_headers'] = arr_unique($date_headers);
        $data['fields'] = $fields;
        
        if ($return) return $data;
        else out_json($data);
    }
    
    public function get_total_deductions($loan_id = NULL) {
        if (is_null($loan_id)) return;
        
        $where = array(
            'archive' => 0,
            'loan_id' => $loan_id
        );
        $ptotal = 0;
        
        $deductions = $this->get_many_by($where);
        foreach ($deductions as $key => $value) {
            $ptotal+= $value['total'];
        }
        
        return $ptotal;
    }
    
    public function get_total_deductions2($loan_id = NULL, $date_to) {
        if (is_null($loan_id)) return;
        
        $pay_date = date('Y-m', strtotime($date_to));
        $deductions = $this->db->query("
            SELECT sum(total) as total FROM {$this->_table}
                WHERE archive = 0 
                AND loan_id = $loan_id
                AND date_format(ded_date, '%Y-%m') = '$pay_date'
        ")->row_array();
        
        return $deductions['total'];
    }
    
    public function create() {
        try {
            $post = $this->input->post();
            $ded_date = date('Y-m-d', strtotime($post['ded_date']));
            
            if ($post['type'] == 'deduction') {
                $this->loan_payments->create();
                
            } else {
                $data = array(
                    'ded_date' => $ded_date,
                    'member_id' => $post['member_id'],
                    'total' => $post['total']
                );
            }
            
            $data['ded_type'] = $post['type'];
            
            if ($post['action'] == 'create') {
                $data['created_at'] = date('Y-m-d H:i:s');
                $this->insert($data, FALSE);
            } else {
                $data['updated_at'] = date('Y-m-d H:i:s');
                $this->update($post['id'], $data);
            }
            
            success_msg("Deduction has been successfully " . ($post['action'] == 'create' ? 'added!' : 'updated!'));
        }
        catch(Exception $e) {
            error_msg($e->getMessage());
        }
    }
    
    public function destroy() {
        try {
            $post = $this->input->post();
            
            $update = array(
                'archive' => 1,
                'updated_at' => date('Y-m-d H:i:s')
            );
            
            if ($post['type'] == 'payment') $this->loan_payments->update($post['ded_id'], $update, FALSE);
            else $this->update($post['ded_id'], $update, FALSE);
            
            success_msg("Deduction has been successfully added!");
        }
        catch(Exception $e) {
            error_msg($e->getMessage());
        }
    }
    
    public function generate() {
        $get = $this->input->get();
        $period = "{$get['period-year']}-{$get['period-month']}-01";
        $data = $this->read2(TRUE, array(
            'period' => $period
        ) , FALSE);
        
        $period2 = date("M_y", strtotime($period));
        
        $this->load->library('excel');
        
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getDefaultStyle()->getFont()->setName('Segoe UI');
        
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        
        $this->excel->getActiveSheet()->setTitle('MonthlyDeductionReport_' . $period2);
        
        //Report Header
        $this->excel->getActiveSheet()->setCellValue('A1', 'COTABATO LIGHT EMPLOYEES MULTI-PURPOSE COOPERATIVE');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:I1');
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        //Report Sub-header
        $this->excel->getActiveSheet()->setCellValue('A2', 'SCHEDULE FOR DEDUCTION FOR ' . $period2);
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
        $this->excel->getActiveSheet()->mergeCells('A2:I2');
        
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        //setup report headers
        $head_map = array();
        
        $row = 4;
        $this->excel->getActiveSheet()->mergeCells('B' . $row . ':' . 'B' . ($row + 2));
        $this->excel->getActiveSheet()->setCellValue('B' . $row, 'NAME OF EMPLOYEE');
        
        $colA = 0;
        foreach ($data['date_headers'] as $key => $value) {
            $colA+= $colA == 0 ? 2 : 3;
            $this->excel->getActiveSheet()->getStyle(num_to_alpha($colA) . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->mergeCells(num_to_alpha($colA) . $row . ':' . num_to_alpha($colA + 2) . $row);
            $this->excel->getActiveSheet()->setCellValue(num_to_alpha($colA) . $row, $value['ded_date']);
            
            $this->excel->getActiveSheet()->getStyle(num_to_alpha($colA) . ($row + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->mergeCells(num_to_alpha($colA) . ($row + 1) . ':' . num_to_alpha($colA + 2) . ($row + 1));
            $this->excel->getActiveSheet()->setCellValue(num_to_alpha($colA) . ($row + 1) , $value['type']);
            
            $ded_date = str_replace('-', '', $value['ded_date']);
            $type = str_replace(' ', '', $value['type']);
            
            $this->excel->getActiveSheet()->setCellValue(num_to_alpha($colA) . ($row + 2) , 'PRINCIPAL');
            $head_map['principal_' . $ded_date . '_' . $type] = num_to_alpha($colA);
            
            $this->excel->getActiveSheet()->setCellValue(num_to_alpha($colA + 1) . ($row + 2) , strtolower($value['type']) == 'regular loan' ? '15% INT' : 'INTEREST');
            $head_map['interest_' . $ded_date . '_' . $type] = num_to_alpha($colA + 1);
            
            $this->excel->getActiveSheet()->setCellValue(num_to_alpha($colA + 2) . ($row + 2) , 'TOTAL');
            $head_map['total_' . $ded_date . '_' . $type] = num_to_alpha($colA + 2);
        }
        
        $colA+= 3;
        $this->excel->getActiveSheet()->setCellValue(num_to_alpha($colA) . ($row + 1) , 'CAPITAL');
        $this->excel->getActiveSheet()->setCellValue(num_to_alpha($colA) . ($row + 2) , 'BUILD-UP');
        $head_map['cb_amount'] = num_to_alpha($colA);
        
        $colA++;
        $this->excel->getActiveSheet()->mergeCells(num_to_alpha($colA) . ($row + 1) . ':' . num_to_alpha($colA) . ($row + 2));
        $this->excel->getActiveSheet()->setCellValue(num_to_alpha($colA) . ($row + 1) , 'SAVINGS');
        $head_map['sa_amount'] = num_to_alpha($colA);
        
        $colA++;
        $this->excel->getActiveSheet()->setCellValue(num_to_alpha($colA) . ($row + 1) , 'PHILAM');
        $this->excel->getActiveSheet()->setCellValue(num_to_alpha($colA) . ($row + 2) , 'INSURANCE');
        $head_map['insurance'] = num_to_alpha($colA);
        
        $colA++;
        $this->excel->getActiveSheet()->setCellValue(num_to_alpha($colA) . ($row + 1) , 'TOTAL');
        $this->excel->getActiveSheet()->setCellValue(num_to_alpha($colA) . ($row + 2) , 'DEDUCTION');
        $head_map['total_deduction'] = num_to_alpha($colA);
        
        $row+= 3;
        foreach ($data['data'] as $key => $value) {
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $value['name']);
            foreach ($value as $vkey => $dvalue) {
                if (property_exists((object)$head_map, $vkey)) {
                    $this->excel->getActiveSheet()->setCellValue($head_map[$vkey] . $row, $dvalue);
                }
            }
            $row++;
        }
        
        $lastCol = $colA;
        for ($col2 = 2; $col2 <= $lastCol; $col2++) {
            $acol = num_to_alpha($col2);
            $this->excel->getActiveSheet()->setCellValue($acol . $row, "=SUM({$acol}7:{$acol}" . ($row - 1) . ")");
        }
        
        $row+= 4;
        $this->excel->getActiveSheet()->setCellValue('C' . $row, "Prepared By");
        
        $row+= 3;
        $this->excel->getActiveSheet()->setCellValue('C' . $row, "WHILHELMINA W. ALOMBRO");
        
        $row++;
        $this->excel->getActiveSheet()->setCellValue('C' . $row, "CREDIT COMM");
        
        $fname = "SCHEDULE_FOR_MOTHLY_DEDUCTION_{$period2}";
        $filename = "documents/exports/{$fname}";
        
        if (file_exists($filename . '.xls')) {
            $filename = "documents/exports/{$fname}_" . time();
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save($filename . ".xls");
        
        set_flash('success_msg', '<a href="' . base_url() . $filename . '.xls' . '">Your download is ready! Click here to download your file.</a>');
        redirect('reports/index');
    }
    
    public function generate_csw() {
        $get = $this->input->get();
        
        if (!isset($get['member']) OR empty($get['member'])) {
            error_msg('Please select a member!', 'reports/index');
        }
        
        $member = $this->loan_member_info->get($get['member']);
        
        $period = date("M y", strtotime("{$get['period-year']}-{$get['period-month']}-01"));
        
        $this->load->library('excel');
        
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getDefaultStyle()->getFont()->setName('Segoe UI');
        
        $this->excel->getActiveSheet()->setTitle($period);
        
        //Report Header
        $this->excel->getActiveSheet()->setCellValue('A1', 'COTABATO LIGHT EMPLOYEES MULTI-PURPOSE COOPERATIVE');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:I1');
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        //Report Sub-header
        $this->excel->getActiveSheet()->setCellValue('A2', 'SUMMARY OF CAPITAL & SAVINGS WITHDRAWAL');
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
        $this->excel->getActiveSheet()->mergeCells('A2:I2');
        
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $row = 5;
        $this->excel->getActiveSheet()->setCellValue('A' . $row, 'Name: ' . strtoupper("{$member['lastname']}, {$member['firstname']}"));
        $this->excel->getActiveSheet()->setCellValue('H' . $row, "Date:");
        $this->excel->getActiveSheet()->setCellValue('J' . $row, date('M j, y'));
        $this->excel->getActiveSheet()->getStyle('A' . $row)->getFont()->setSize(13);
        $this->excel->getActiveSheet()->mergeCells("A{$row}:B{$row}");
        
        //capital/deposit
        $capital = $this->get_amount('capital', $get);
        $row+= 2;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "CAPITAL AS OF: $period");
        $this->excel->getActiveSheet()->setCellValue('I' . $row, "P*");
        $this->excel->getActiveSheet()->setCellValue('J' . $row, $capital);
        $this->excel->getActiveSheet()->getStyle('B' . $row)->getFont()->setSize(12);
        $this->excel->getActiveSheet()->mergeCells("B{$row}:C{$row}");
        
        $row++;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "add: Paid up Capital for {$get['period-year']}");
        $this->excel->getActiveSheet()->getStyle('B' . $row)->getFont()->setSize(12);
        $this->excel->getActiveSheet()->mergeCells("B{$row}:C{$row}");
        
        $row++;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "transfer from savings");
        $this->excel->getActiveSheet()->getStyle('B' . $row)->getFont()->setSize(12);
        $this->excel->getActiveSheet()->mergeCells("B{$row}:C{$row}");
        
        $row+= 4;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "Build up Capital");
        $this->excel->getActiveSheet()->getStyle('B' . $row)->getFont()->setSize(12);
        $this->excel->getActiveSheet()->mergeCells("B{$row}:C{$row}");
        
        $row+= 2;
        $this->excel->getActiveSheet()->setCellValue('A' . $row, "TOTAL CAPITAL");
        $this->excel->getActiveSheet()->setCellValue('k' . $row, "P*");
        $this->excel->getActiveSheet()->setCellValue('L' . $row, $capital);
        $this->excel->getActiveSheet()->getStyle('A' . $row)->getFont()->setSize(13);
        $this->excel->getActiveSheet()->mergeCells("A{$row}:B{$row}");
        
        //savings
        $savings = $this->get_amount('savings', $get);
        $row+= 2;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "SAVINGS AS OF: $period");
        $this->excel->getActiveSheet()->setCellValue('I' . $row, "P*");
        $this->excel->getActiveSheet()->setCellValue('J' . $row, $savings);
        $this->excel->getActiveSheet()->getStyle('B' . $row)->getFont()->setSize(12);
        $this->excel->getActiveSheet()->mergeCells("B{$row}:C{$row}");
        
        $row++;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "add: Paid up Savings for {$get['period-year']}");
        $this->excel->getActiveSheet()->getStyle('B' . $row)->getFont()->setSize(12);
        $this->excel->getActiveSheet()->mergeCells("B{$row}:C{$row}");
        
        $row++;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "Transfer");
        $this->excel->getActiveSheet()->getStyle('B' . $row)->getFont()->setSize(12);
        $this->excel->getActiveSheet()->mergeCells("B{$row}:C{$row}");
        
        $row+= 4;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "Montlhy Deposit");
        $this->excel->getActiveSheet()->getStyle('B' . $row)->getFont()->setSize(12);
        $this->excel->getActiveSheet()->mergeCells("B{$row}:C{$row}");
        
        $row+= 2;
        $this->excel->getActiveSheet()->setCellValue('A' . $row, "TOTAL SAVINGS");
        $this->excel->getActiveSheet()->setCellValue('K' . $row, "P*");
        $this->excel->getActiveSheet()->setCellValue('L' . $row, $savings);
        $this->excel->getActiveSheet()->getStyle('A' . $row)->getFont()->setSize(13);
        $this->excel->getActiveSheet()->mergeCells("A{$row}:B{$row}");
        
        $row+= 2;
        $this->excel->getActiveSheet()->setCellValue('A' . $row, "(A) TOTAL CAPITAL & SAVINGS DEPOSIT as of {$period}");
        $this->excel->getActiveSheet()->setCellValue('K' . $row, 'P*');
        $this->excel->getActiveSheet()->setCellValue('L' . $row, $capital + $savings);
        $this->excel->getActiveSheet()->getStyle('A' . $row)->getFont()->setSize(13);
        $this->excel->getActiveSheet()->mergeCells("A{$row}:B{$row}");
        
        $row+= 2;
        $this->excel->getActiveSheet()->setCellValue('A' . $row, "(B) ADVANCE PAYMENT/INTEREST/DEDUCTION");
        $this->excel->getActiveSheet()->setCellValue('I' . $row, 'P*');
        $this->excel->getActiveSheet()->getStyle('A' . $row)->getFont()->setSize(13);
        $this->excel->getActiveSheet()->mergeCells("A{$row}:B{$row}");
        
        $row+= 2;
        $this->excel->getActiveSheet()->setCellValue('A' . $row, "LESS:");
        $this->excel->getActiveSheet()->getStyle('A' . $row)->getFont()->setSize(13);
        $this->excel->getActiveSheet()->mergeCells("A{$row}:B{$row}");
        
        $row+= 2;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "WITHDRAWAL SAVINGS for the period Jan - {$period}");
        $this->excel->getActiveSheet()->setCellValue('I' . $row, 'P*');
        $this->excel->getActiveSheet()->setCellValue('J' . $row, $this->get_from_start('savings', $get));
        $this->excel->getActiveSheet()->getStyle('B' . $row)->getFont()->setSize(13);
        $this->excel->getActiveSheet()->mergeCells("B{$row}:E{$row}");
        
        $row+= 2;
        $this->excel->getActiveSheet()->setCellValue('C' . $row, "LOAN BALANCE AS OF {$period}");
        $this->excel->getActiveSheet()->setCellValue('E' . $row, 'P*');
        
        // $this->excel->getActiveSheet()->setCellValue('F' . $row, 'Principal');
        // $this->excel->getActiveSheet()->setCellValue('H' . $row, 'Interest');
        $this->excel->getActiveSheet()->setCellValue('J' . $row, 'Current Balance');
        $this->excel->getActiveSheet()->mergeCells("C{$row}:D{$row}");
        
        $row++;
        $loan_type = $this->loan_vars->get_many_by('type', 'Loan Type');
        $balance_total = 0;
        foreach ($loan_type as $key => $value) {
            $balance_info = $this->get_balance_info($value['value'], $get);
            foreach ($balance_info as $bkey => $bvalue) {
                $this->excel->getActiveSheet()->setCellValue('C' . $row, $value['value']);
                
                // $this->excel->getActiveSheet()->setCellValue('F' . $row, $bvalue['principal']);
                // $this->excel->getActiveSheet()->setCellValue('H' . $row, $bvalue['interest']);
                $this->excel->getActiveSheet()->setCellValue('J' . $row, $bvalue['balance']);
                $balance_total+= $bvalue['balance'];
                $row++;
            }
        }
        
        $row+= 2;
        $this->excel->getActiveSheet()->setCellValue('A' . $row, "(B) TOTAL LOAN BALANCES as of $period");
        $this->excel->getActiveSheet()->setCellValue('K' . $row, 'P*');
        $this->excel->getActiveSheet()->setCellValue('L' . $row, $balance_total);
        
        $row+= 2;
        $this->excel->getActiveSheet()->setCellValue('A' . $row, "(C) WITHDRAWABLE");
        $this->excel->getActiveSheet()->mergeCells("A{$row}:J{$row}");
        
        $row++;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "RETIREE [ 100% loan vs CAP+SAV: D = (A + B)-C ]");
        $this->excel->getActiveSheet()->setCellValue('K' . $row, 'P*');
        $this->excel->getActiveSheet()->mergeCells("B{$row}:J{$row}");
        
        $row++;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "A) [ CAPITAL can cover 50% Loan then withdrawable SAVINGS less 5T]");
        $this->excel->getActiveSheet()->setCellValue('K' . $row, 'P*');
        $this->excel->getActiveSheet()->mergeCells("B{$row}:J{$row}");
        
        $row++;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "B) [ CAPITAL+SAVINGS less 50% Loan balance less 5T]");
        $this->excel->getActiveSheet()->setCellValue('K' . $row, 'P*');
        $this->excel->getActiveSheet()->mergeCells("B{$row}:J{$row}");
        
        $row++;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "Note: Only 50% of the excess in capital & savings deposit against loan balance is subject for withdrawal");
        $this->excel->getActiveSheet()->mergeCells("B{$row}:J{$row}");
        
        $row++;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "          but if it will cause a reduction in the minimum paid-up capital of P* 5,000, only an excess to P* 5,000");
        $this->excel->getActiveSheet()->mergeCells("B{$row}:J{$row}");
        
        $row++;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "          is withdrawable.");
        $this->excel->getActiveSheet()->mergeCells("B{$row}:J{$row}");
        
        $row+= 2;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "Prepared By:");
        $this->excel->getActiveSheet()->setCellValue('I' . $row, "Noted By:");
        
        $row+= 3;
        $pby = $this->loan_vars->get_by(array(
            'type' => 'Report Signatories',
            'subtype' => 'Prepared_By'
        ));
        $nby = $this->loan_vars->get_by(array(
            'type' => 'Report Signatories',
            'subtype' => 'Noted_By'
        ));
        
        if (!isset($pby['id']) OR !isset($nby)) {
            error_msg('One of the report signatories is not set!');
            redirect('dashboard/index');
        }
        
        $this->excel->getActiveSheet()->setCellValue('B' . $row, strtoupper($pby['value']));
        $this->excel->getActiveSheet()->setCellValue('I' . $row, strtoupper($nby['value']));
        
        $row+= 1;
        $this->excel->getActiveSheet()->setCellValue('B' . $row, "Audit Committee - Chairman");
        $this->excel->getActiveSheet()->setCellValue('I' . $row, "CLEMPCO - Chairman");
        
        $fname = strtoupper("{$member['lastname']}, {$member['firstname']}_Withdrawal of Savings");
        $filename = "documents/exports/{$fname}";
        
        if (file_exists($filename . '.xls')) {
            $filename = "documents/exports/{$fname}_" . time();
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save($filename . ".xls");
        
        set_flash('success_msg', '<a href="' . base_url() . $filename . '.xls' . '">Your download is ready! Click here to download your file.</a>');
        redirect('reports/index');
    }
    
    public function get_amount($type, $data = NULL) {
        if (is_null($data)) return;
        
        $day = date('t', strtotime("{$data['period-year']}-{$month}-01"));
        $result = $this->db->query("
                SELECT sum(total) as total FROM 
                    {$this->_table}
                WHERE
                    member_id = {$data['member']}
                    AND ded_date <= '{$data['period-year']}-{$data['period-month']}-$day'
                    AND archive = 0
                    AND ded_type IN ('$type')
            ")->row_array();
        
        return $result['total'];
    }
    
    public function get_from_start($type, $data = NULL) {
        if (is_null($data)) return;
        
        $month = pad_left($data['period-month'] > 1 ? $data['period-month'] - 1 : $data['period-month'], 2);
        $day = date('t', strtotime("{$data['period-year']}-{$month}-01"));
        
        $result = $this->db->query("
                SELECT sum(total) as total FROM 
                    {$this->_table}
                WHERE
                    member_id = {$data['member']}
                    AND ded_date between '2014-01-01' AND '{$data['period-year']}-{$month}-{$day}'
                    AND archive = 0
                    AND ded_type IN ('$type')
            ")->row_array();
        
        return $result['total'];
    }
    
    public function get_balance_info($loan_type = '', $data = NULL) {
        if (empty($loan_type) OR is_null($data)) return;
        
        $month = pad_left(($data['period-month'] > 1 ? $data['period-month'] - 1 : $data['period-month']) , 2);
        $day = date('t', strtotime("{$data['period-year']}-{$month}-01"));
        $date_to = date('Y-m-d', strtotime("{$data['period-year']}-$month-$day"));
        
        $where = array(
            'archive' => 0,
            'status' => 'Approved',
            'member_id' => $data['member'],
            'type' => $loan_type
        );
        
        $loan_info = $this->loan_application->get_many_by($where);
        
        if (!is_array($loan_info) OR count($loan_info) == 0) return;
        
        foreach ($loan_info as $key => $value) {
            $value['balance'] = $this->loan_application->running_balance2($value['id'], $date_to);
            $loan_info[$key] = $value;
        }
        return $loan_info;
    }
    
    public function get_totals($type) {
        $session = get_session();
        if ($session['access_type'] == 'Member') {
            $query = $this->db->query("
                    SELECT coalesce(sum(total), 0) as total
                        FROM {$this->_table}
                    WHERE archive = 0
                        AND member_id = {$session['id']}
                        AND ded_type IN('$type')
                ")->row_array();
            
            return $query['total'];
        } else {
            return;
        }
    }
}
