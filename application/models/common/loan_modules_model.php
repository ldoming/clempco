<?php

class Loan_modules_model extends Base_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_table = 'loan_modules';
    }

    public function read()
    {
    	$get = $this->input->get();
    	$modules = $this->get_many_by('active', 1);
    	$session = get_session();

    	$data = array();

    	foreach ($modules as $key => $value)
    	{
			$submodules = $this->loan_submodules->get_many_id($value['id']);

			foreach ($submodules as $skey => $svalue) 
			{
                if(isset($get['user_typedef']))
                {
                    $value['access'] = $this->loan_user_access->has_access($get['user_typedef'], $svalue['id']);
                }
                else
                {
                    $value['access'] = FALSE;
                }
				$value['group'] = $value['name'];
				$value['group_id'] = $value['id'];
				$value['module'] = $svalue['name'];
				$value['id'] = $svalue['id'];
                $data[] = $value;
			}
    	}

    	success_msg($data);
    }
}