<?php

class Loan_user_access_model extends Base_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_table = 'loan_user_access';
    }

    public function get_modules($access_type)
    {
    	$all = $this->get_many_by('user_typedef', $access_type);
    	$ids = array();
    	foreach ($all as $key => $value)
    	{
    		$ids[] = $value['module_id'];
    	}
    	
    	return $ids;
    }

    public function modify()
    {
        try
        {
            $post = $this->input->post();

            $this->delete_by('user_typedef', $post['user_typedef']);

            $module_ids = explode(',', $post['ids']);
            foreach ($module_ids as $key => $value) 
            {
                $insert = array(
                    'module_id' => $value,
                    'user_typedef' => $post['user_typedef']
                );
                $this->insert($insert, FALSE);
            }
			
			success_msg('Successfully saved!');
        }
        catch(Exception $e)
        {
            error_msg($e->getMessage());
        }
    }
	
	public function has_access($user_typedef, $module_id)
	{
		$where = array(
			'user_typedef'  => $user_typedef,
			'module_id' => $module_id
		);
		return ($this->count_by($where) > 0);
	}
}