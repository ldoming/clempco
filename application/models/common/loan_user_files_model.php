<?php

class Loan_user_files_model extends Base_Model
{

    var $basedir = 'documents/uploads/';

    function __construct()
    {
        parent::__construct();

        $this->_table = 'loan_user_files';
    }

    public function upload()
    {
    	try
        {
            $fileName = $_FILES["file"]["name"]; 
            $fileTmpLoc = $_FILES["file"]["tmp_name"];

            $pathAndName = $this->basedir . $fileName;
            
            $moveResult = move_uploaded_file($fileTmpLoc, $pathAndName);
            
            if ($moveResult) 
            {
                $info = new SplFileInfo($pathAndName);
                $extension = $info->getExtension();

                $valid_exts = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif',
                    'bmp'
                );

                if(in_array(strtolower($extension), $valid_exts) === FALSE)
                {
                    $data = array(
                        'success' => FALSE,
                        'data' => "ERROR: Invalid file extension. Accepted file types: " . implode(',', $valid_exts)
                    );
                    out_json($data);
                }

                $insert = array(
                    'path' => $pathAndName,
                    'created_at' => date('Y-m-d H:i:s'),
                    'user_id' => sess_user_id()
                );

                $id = $this->insert($insert, FALSE);

                $data = array(
                    'success' => TRUE,
                    'tmp_path' => base_url() . $pathAndName,
                    'file_id' => $id
                );

                out_json($data);
            } 
            else 
            {
                error_msg("ERROR: File not moved completely!");
            }
        } 
        catch (Exception $e) 
        {
            error_msg($e->getMessage());
        }
    }

    public function get_src($id = NULL)
    {
        if(is_null($id))
            return;

        $row = $this->get($id);
        if(isset($row['id']))
            return base_url() . $row['path'];
    }
}