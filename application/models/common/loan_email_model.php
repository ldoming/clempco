<?php

require 'phpmailer/PHPMailerAutoload.php';

class Loan_email_model extends Base_Model
{
	public $mail;

    function __construct()
    {
        parent::__construct();
		
		$user = 'support@clempco.com';
		$name = 'Clempco Support';
		
		$this->mail = new PHPMailer;
		// $this->mail->SMTPDebug = 3;                               // Enable verbose debug output
		$this->mail->isSMTP();                                      // Set this->mailer to use SMTP
		$this->mail->Host = 'metro704.hostmetro.com';  // Specify main and backup SMTP servers
		$this->mail->SMTPAuth = true;                               // Enable SMTP authentication
		$this->mail->Username = $user;                 // SMTP username
		$this->mail->Password = $user;                           // SMTP password
		$this->mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$this->mail->Port = 465;                                    // TCP port to connect to

		$this->mail->From = $user;
		$this->mail->FromName = $name;
		$this->mail->addReplyTo($user, $name);

		$this->mail->WordWrap = 50;                                 // Set word wrap to 50 characters
		$this->mail->isHTML(true);                                  // Set email format to HTML
    }

	public function send($to, $subject, $body)
	{
		$this->mail->Body = $body;
		$this->mail->addAddress($to);     // Add a recipient
		$this->mail->Subject = $subject;

		return $this->mail->send();
	}

	public function add_bcc($bcc = '')
	{
		if(is_array($bcc))
		{
			foreach ($bcc as $key => $value)
			{
				$this->add_bcc($value);
			}
			return;
		}

		$this->mail->addBCC($bcc);
	}

}