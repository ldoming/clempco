<?php

class Loan_submodules_model extends Base_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_table = 'loan_submodules';
    }

    public function get_many_id($group_id = NULL)
    {
    	if(is_null($group_id))
    		return;

        $where = array(
            'module_id' => $group_id,
            'active' => 1
        );

    	return $this->get_many_by($where);
    }

    public function has_access($user_typedef = '', $module_id = NULL)
    {
        if(is_null($module_id) OR empty($user_typedef))
            return;

        $where = array(
            'user_typedef' => $user_typedef,
            'module_id' => $module_id
        );

        return ($this->count_by($where) > 0);
    }
}