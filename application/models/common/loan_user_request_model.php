<?php

class Loan_user_request_model extends Base_Model
{
    function __construct()
    {
        parent::__construct();
		$this->_table = 'loan_user_request';                             // Set email format to HTML
    }
	
	public function forgot_password()
	{
		try
		{
			$post = $this->input->post();
			if(!isset($post['email_address']) OR empty($post['email_address']))
			{
				error_msg('Please provide a valid email address!');
				redirect('user_login/forgot');	
			}
			
			if(!$this->loan_user_info->email_exists())
			{
				error_msg('There is no account associated with this email address on this website!');
				redirect('user_login/forgot_password');
			}
			
			$data = array(
				'request' => 'Change Password',
				'email_address' => $post['email_address']
			);

			list($token, $check) = $this->security_keys($data);
			
			$data['link'] = base_url() . "user_request/password_help?t={$token}&c={$check}";			
			
			$body = $this->load->view('common/email/forgot_password', $data, TRUE);

			if(!$this->loan_email->send($post['email_address'], 'Forgot Password', $body))
			{
				error_msg($this->email_helper->mail->ErrorInfo);
			}
			else
			{
				success_msg('Email has been sent! Check your inbox for the instructions on how to reset your password.');
			}
			
			redirect('user_login/forgot_password');
		}
		catch(Exception $e)
		{
			error_msg($e->getMessage());
		}
	}

	public function security_keys($data = NULL)
	{
		$data1 = array(
			'created_at' => date('Y-m-d H:i:s')
		);
		
		$id = $this->insert(array_merge($data, $data1), TRUE);
		return array(md5($id), md5($data['email_address']));
	}
}