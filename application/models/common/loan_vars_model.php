<?php

class Loan_vars_model extends Base_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_table = 'loan_vars';
    }

	public function read()
	{
		$get = $this->input->get();
		$data = $this->get_many_by('type', $get['type']);

		$data2 = array();
		switch ($get['type']) {
			case 'Loan Type':
				$session = get_session();
				$access_type = strtolower($session['access_type']);

				if(in_array($access_type, 
					array(
						'administrator',
						'member',
						'chairperson',
						'treasurer'
					)
				))
				{
					$data2 = $data;
				}
				else
				{
					foreach ($data as $key => $value) 
					{
						$access = json_decode($value['subtype']);
						if(in_array($access_type, $access))
						{
							$data2[] = $value;
						}
					}
				}
				break;
			
			default:
				$data2 = $data;
				break;
		}

		success_msg($data2);
	}

	public function read_by_type($type = NULL, $full = FALSE)
	{
		if(!is_null($type))
		{
			$all = $this->get_many_by('type', $type);
			$types = array();

			foreach($all as $key => $value)
			{
				if(!$full)
					$types[] = $value['value'];
				else
					$types[] = $value;
			}

			return $types;
		}
	}

	public function get_interest($loan_type = NULL)
	{
		if(!is_null($loan_type))
		{
			$where = array(
				'type' => 'Interest Rate',
				'subtype' => $loan_type
			);

			$row = $this->get_by($where);

			if(isset($row['id']))
				return $row['value'];
		}
	}

	public function get_period($loan_type = NULL)
	{
		if(!is_null($loan_type))
		{
			$where = array(
				'type' => 'Payment Period',
				'subtype' => $loan_type
			);

			$row = $this->get_by($where);

			if(isset($row['id']))
				return $row['value'];
		}
	}

	public function get_amounts($loan_type = NULL)
	{
		$where = array(
			'type' => 'Loan Amount',
		);

		if(!is_null($loan_type))
		{
			$where['subtype'] = $loan_type;
		}

		return $this->get_many_by($where);
	}

	public function get_acct_groups()
	{
		$where = array(
			'type' => 'Account Groups',
		);

		success_msg($this->get_many_by($where));
	}

	public function get_user_types()
	{
		$where = array(
			'type' => 'User Type',
		);

		$all = $this->get_many_by($where);
		$data = array();
		foreach($all as $key => $value)
		{
			if($value['value'] == 'Administrator')
				continue;

			$data[] = $value;
		}

		$data[] = array('value' => 'Member');

		success_msg($data);
	}

	public function modify_signatories($data = NULL)
	{
		try
		{
			if(is_null($data))
				return;

			$this->delete_by('type', 'Report Signatories');

			foreach($data as $key => $value)
			{
				$insert = array(
					'type' => 'Report Signatories',
					'subtype' => $key,
					'value' => $value
				);

				$this->insert($insert, FALSE);
			}

			$post = $this->input->post();
			success_msg('Settings has been successfully updated!');
			redirect('settings/reports/index?_m=' . $post['_m']);
		}
		catch(Exception $e)
		{
			error_msg($e->getMessage());
		}
	}
}