<?php

class Loan_account_groups_model extends Base_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_table = 'loan_account_groups';
    }

    public function read()
    {
    	$all = $this->get_all();

    	success_msg($all);
    }
}