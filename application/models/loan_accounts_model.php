<?php
class Loan_accounts_model extends Base_Model
{
	
	function __construct() {
		parent::__construct();
		
		$this->_table = 'loan_accounts';
	}
	
	public function read() {
		$get = $this->input->get();
		
		if (isset($get['query']) AND !empty($get['query'])) {
			$this->db->like("name", trim($get['query']));
		}
		
		$this->db->limit($get['limit'], $get['start']);
		
		$where = array(
			'archive' => 0
		);
		$all = $this->order_by('created_at', 'desc')->get_many_by($where);

		foreach ($all as $key => $value) {
			$value['acct_period'] = date('M j, Y', strtotime($value['acct_period']));
			$all[$key] = $value;
		}
		
		success_msg($all);
	}
	
	public function read2() {
		$where = array(
			'acct_level' => 1,
			'archive' => 0
		);
		
		$all = $this->get_many_by($where);
		
		foreach ($all as $key => $value) {
			$value['group'] = $value['acct_name'];
			$value['abbr'] = $this->get_group($value['acct_name']);
			unset($value['acct_name'], $value['acct_group']);
			$all[$key] = $value;
		}
		
		success_msg($all);
	}
	
	public function get_group($acct_name) {
		if (strpos(strtolower($acct_name) , 'regular')) return 'REG. LOAN';
		elseif (strpos(strtolower($acct_name) , 'special')) return 'SPECIAL LOAN';
		elseif (strpos(strtolower($acct_name) , 'emergency')) return 'EMERGENCY LOAN';
		elseif (strpos(strtolower($acct_name) , 'grocery')) return 'GROCERY LOAN';
	}
	
	public function create() {
		try {
			$post = $this->input->post();
			
			if (!$this->_valid_acct_code()) {
				error_msg('Account code already exists!');
			}
			
			//head account
			if ($post['acct_level'] == 1) {
				$insert = array(
					'acct_code' => $post['acct_code'],
					'acct_name' => $this->_get_account_name($post['acct_group']) ,
					'member_id' => isset($post['member_id']) ? $post['member_id'] : NULL,
					'ob_type' => $post['ob_type'],
					'ob_amount' => $post['ob_amount'],
					'details' => $post['details'],
					'created_at' => date('Y-m-d H:i:s') ,
					'created_by' => sess_user_id() ,
					'acct_period' => "{$post['period_year']}-{$post['period_month']}-01",
					'acct_level' => $post['acct_level']
				);
			} else {
				$insert = array(
					'acct_code' => $this->_get_account_code($post['acct_group']) ,
					'acct_name' => $post['acct_name'],
					'acct_group' => $post['acct_group'],
					'member_id' => isset($post['member_id']) ? $post['member_id'] : NULL,
					'ob_type' => $post['ob_type'],
					'ob_amount' => $post['ob_amount'],
					'details' => $post['details'],
					'created_at' => date('Y-m-d H:i:s') ,
					'created_by' => sess_user_id() ,
					'acct_period' => "{$post['period_year']}-{$post['period_month']}-01",
					'acct_level' => $post['acct_level']
				);
			}
			
			$id = $this->insert($insert, FALSE);
			
			success_msg("A new account has been successfully created!");
		}
		catch(Exception $e) {
			error_msg($e->getMessage());
		}
	}
	
	public function _valid_acct_code() {
		$post = $this->input->post();
		
		return $this->count_by('acct_code', $post['acct_code']) == 0;
	}
	
	public function _get_account_name($group = NULL) {
		if (is_null($group)) return;
		
		$group = $this->loan_account_groups->get($group);
		return $group['group'];
	}
	
	public function _get_account_code($group = NULL) {
		if (is_null($group)) return;
		
		$_group = $this->get($group);
		$child_count = $this->count_by('acct_group', $group);
		
		$segments = explode('.', $_group['acct_code']);
		
		for ($i = 0; $i < count($segments); $i++) {
			if($i == count($segments)){
				if((int)$segments[$i] > 0)
					error_msg('Maximum account code reached!<br /><br /> Acct code ' . $_group['acct_code']);
			}

			if($segments[$i] == 0){
				$segments[$i] = pad_left($child_count + 1, 2);
				return implode('.', $segments);
			}
		}

		// $segments[1] = pad_left($child_count + 1, 2);
		// return implode('-', $segments);
	}
	
	public function destroy() {
		try {
			$post = $this->input->post();
			
			$update = array(
				'archive' => 1
			);
			
			$this->update($post['account_id'], $update, FALSE);
			
			success_msg("Record has been deleted!");
		}
		catch(Exception $e) {
			error_msg($e->getMessage());
		}
	}
	
	public function get_by_acct($id) {
		return $this->db->query("
			SELECT * FROM {$this->_table}
				WHERE acct_group = $id
				AND archive = 0
		")->result_array();
	}
	
	public function get_by_group($acct_group, $period) {
		return $this->db->query("
			SELECT * FROM {$this->_table}
				WHERE lcase(acct_group) = '" . strtolower(ucfirst($acct_group)) . "'
					AND date_format(acct_period, '%Y-%m') = '$period'
					AND lcase(acct_name) != '" . strtolower(ucfirst($acct_group)) . "'
					AND archive = 0
		")->result_array();
	}
	
	public function get_code() {
		$post = $this->input->post();
		
		$counted = $this->db->query("
			SELECT count(*) AS counted FROM {$this->_table}
				WHERE acct_group = '{$post['id']}'
				AND archive = 0
		")->row_array();
		
		$data = array(
			'success' => TRUE,
			'code' => pad_left($post['id'], '4') . '.' . pad_left($counted['counted'] + 1, 4) . '.00'
		);
		
		out_json($data);
	}
}
