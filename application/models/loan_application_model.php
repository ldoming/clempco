<?php
class Loan_application_model extends Base_Model
{
	
	function __construct() {
		parent::__construct();
		
		$this->_table = 'loan_application';

		log_message('DEBUG', __CLASS__ . ' has been loaded!');
	}
	
	public function read() {
		$get = $this->input->get();
		$session = get_session();
		
		if ($session['access_type'] == 'Member') $this->db->where('member_id', sess_user_id());
		
		$this->db->limit($get['limit'], $get['start']);
		
		if (isset($get['query']) && !empty($get['query'])) $this->db->like('concat_ws(" ", b.firstname, b.lastname)', $get['query']);
		
		if (isset($get['loan_type']) && !empty($get['loan_type'])) $this->db->where('type', $get['loan_type']);
		
		if (isset($get['member_id']) && !empty($get['member_id'])) $this->db->where('a.member_id', $get['member_id']);
		
		$this->db->select('a.*')->join('loan_member_info b', 'b.id=a.member_id');
		
		$all = $this->db->order_by('a.created_at', 'desc')->where('a.archive', 0)->where_in('a.status', array(
			'New',
			'Approved',
			'Declined'
		))->get("{$this->_table} a")->result_array();
		
		$data = array();
		foreach ($all as $key => $value) {
			$value['editable'] = $this->has_loan_access($value['type']);
			
			$employee = $this->loan_member_info->get($value['member_id']);
			$value['position'] = $employee['position'];
			$value['name'] = ucwords(strtolower("{$employee['firstname']} {$employee['lastname']}"));
			$value['balance'] = $this->running_balance($value['id']);
			$value['payable'] = $value['monthly'] * $value['period'];
			$value['deductions'] = float_to_money($this->loan_payments->get_total($value['id']));
			$value['period'] = $value['period'] . ' Months';
			$value['status'] = $value['status'] == 'New' ? 'Pending Approval' : $value['status'];
			$value['created_at'] = date('M j, Y', strtotime($value['created_at']));
			$data[] = $value;
		}

		$url = base_url() . 'loans/batch_notify';
		async_request($url, array());
		
		success_msg($data);
	}
	
	public function read3() {
		$get = $this->input->get();
		
		if (!isset($get['member_id']) && !empty($get['member_id'])) return;
		
		$this->db->where('a.member_id', $get['member_id']);
		
		$this->db->select('a.*')->join('loan_member_info b', 'b.id=a.member_id');
		
		$where = array(
			'a.archive' => 0,
			'a.status' => 'Approved'
		);
		
		$all = $this->db->order_by('a.created_at', 'desc')->where($where)->get("{$this->_table} a")->result_array();
		
		$data = array();
		foreach ($all as $key => $value) {
			$value['editable'] = $this->has_loan_access($value['type']);
			
			$employee = $this->loan_member_info->get($value['member_id']);
			$value['position'] = $employee['position'];
			$value['name'] = ucwords(strtolower("{$employee['firstname']} {$employee['lastname']}"));
			$value['balance'] = $this->running_balance($value['id']);
			$value['total_payable'] = $value['monthly'] * $value['period'];
			$value['period'] = $value['period'] . ' Months';
			$value['status'] = $value['status'] == 'New' ? 'Pending Approval' : $value['status'];
			$data[] = $value;
		}
		
		success_msg($data);
	}
	
	public function has_loan_access($type) {
		$session = get_session();
		$access_type = strtolower($session['access_type']);
		
		if ($access_type == 'administrator') return TRUE;
		
		$where2 = array(
			'type' => 'Loan Type',
			'value' => $type
		);
		$loan_type = $this->loan_vars->get_by($where2);
		
		$access = (array)json_decode($loan_type['subtype']);
		return in_array($access_type, $access);
	}
	
	public function read1() {
		$get = $this->input->get();
		
		$this->db->limit($get['limit'], $get['start']);
		
		if (isset($get['loan_type']) && !empty($get['loan_type'])) $this->db->where('type', $get['loan_type']);
		
		$this->db->select('a.*')->join('loan_member_info b', 'b.id=a.member_id');
		
		$where = array(
			'a.archive' => 0
		);
		
		$all = $this->db->order_by('a.created_at', 'desc')->where($where)->get("{$this->_table} a")->result_array();
		
		foreach ($all as $key => $value) {
			$member = $this->loan_member_info->get($value['member_id']);
			$value['balance'] = $this->running_balance($value['id']);
			$value['total_payable'] = $value['monthly'] * $value['period'];
			$value['status'] = $value['status'] == 'New' ? 'Pending Approval' : $value['status'];
			$all[$key] = $value;
		}
		
		success_msg($all);
	}
	
	public function create() {
		try {
			$post = $this->input->post();
			
			$apr = $this->loan_vars->get_interest($post['loan_type']);
			$length = $this->loan_vars->get_period($post['loan_type']);
			
			// $loan = $post['actual'];
			
			$insert = array(
				'created_at' => date('Y-m-d H:i:s') ,
				'member_id' => $post['member_id'],
				'principal' => $post['principal'],
				'interest' => $apr,
				'period' => $post['period'],
				'monthly' => get_pmt($apr / 100, $post['period'], $post['principal'], $post['loan_type']) ,
				'status' => 'New',
				'terms' => $post['terms'],
				'type' => $post['loan_type'],
				'member_id' => $post['member_id'],
				'data' => json_encode($post['details']),
				'last_notify' => date('Y-m-d')
			);
			
			$id = $this->insert($insert, FALSE);
			
			//notify others
			if (!$this->email('new', $id)) {
				error_msg($this->loan_email->mail->ErrorInfo);
			} else {
				success_msg("Record has been successfully saved!");
			}
		}
		catch(Exception $e) {
			error_msg($e->getMessage());
		}
	}
	
	public function running_balance($loan_id = NULL) {
		if (is_null($loan_id)) return;
		
		$where = array(
			'member_id' => sess_user_id() ,
			'loan_id' => $loan_id
		);
		
		$loan_info = $this->get($loan_id);
		$payable = $loan_info['monthly'] * $loan_info['period'];
		
		//payments
		$query = $this->db->query("SELECT sum(payment) as total FROM loan_payments WHERE archive = 0 AND loan_id = $loan_id")->row_array();
		$payments = $query['total'];
		
		return float_to_money($payable - $payments);
	}
	
	public function running_balance2($loan_id = NULL, $date_to = NULL) {
		if (is_null($loan_id) OR is_null($date_to)) return;
		
		$where = array(
			'member_id' => sess_user_id() ,
			'loan_id' => $loan_id
		);
		
		$loan_info = $this->get($loan_id);
		$payable = $loan_info['monthly'] * $loan_info['period'];
		
		//payments
		$query = $this->db->query("
			SELECT sum(payment) as total FROM loan_payments 
				WHERE archive = 0 AND loan_id = $loan_id
				AND pay_date <= '$date_to'
		")->row_array();
		$payments = $query['total'];
		
		return $payable - $query['total'];
	}
	
	public function destroy() {
		try {
			$post = $this->input->post();
			$loan_info = (array)json_decode($post['loan_info']);
			
			$update = array(
				'archive' => 1
			);
			
			$record = $this->get($loan_info['id']);
			if ($record['status'] == 'Complete') {
				error_msg('You cannot delete completed loans!');
			}
			
			$this->update($loan_info['id'], $update, FALSE);
			
			success_msg("Record has been deleted!");
		}
		catch(Exception $e) {
			error_msg($e->getMessage());
		}
	}
	
	public function email($tpl = '', $id = NULL) {
		if (is_null($id) OR empty($tpl)) return;
		
		$record = $this->get($id);
		$record['time'] = date('M j, Y h:i:s a', strtotime($record['created_at']));
		$record['name'] = $this->loan_member_info->get_name($record['member_id']);
		$email_address = $this->loan_member_info->get_email($record['member_id']);
		
		$body = $this->load->view('common/email/loan/' . $tpl, $record, TRUE);
		
		$this->loan_email->add_bcc($this->get_emails());
		return $this->loan_email->send($email_address, 'Loan Application', $body);
	}
	
	public function email2($tpl = '', $id = NULL) {
		if (is_null($id) OR empty($tpl)) return;
		
		$record = $this->get($id);
		$record['time'] = date('M j, Y h:i:s a', strtotime($record['created_at']));
		$record['name'] = $this->loan_member_info->get_name($record['member_id']);
		$email_address = $this->loan_member_info->get_email($record['member_id']);
		
		$body = $this->load->view('common/email/loan/' . $tpl, $record, TRUE);
		
		// $this->loan_email->add_bcc($this->get_emails());
		return $this->loan_email->send($email_address, 'Loan Application', $body);
	}
	
	public function get_emails() {
		$records = $this->db->query("
			SELECT email_address FROM loan_user_info
				WHERE user_typedef IN('Treasurer','Chairperson','Credit','Audit')
				AND status = 'Active'
		")->result_array();
		
		$mails = array();
		foreach ($records as $key => $value) {
			$mails[] = $value['email_address'];
		}
		
		return $mails;
	}
	
	public function export() {
		try {
			$post = $this->input->post();
			$loan_info = $this->get($post['loan_id']);
			$applicant = $this->loan_member_info->get($loan_info['member_id']);
			
			if ($loan_info['type'] == 'Regular Loan') {
				$this->export_rl();
			} else {
				$this->export_ml();
			}
			
			$pre = pad_left($post['loan_id'], 5);
			$fname = "{$pre}_{$applicant['firstname']}_{$applicant['lastname']}_{$loan_info['type']}.xls";
			$filename = str_replace(' ', '', "documents/exports/{$fname}");
			
			if (file_exists($filename)) {
				$filename = str_replace(' ', '', "documents/exports/" . time() . "_{$fname}");
			}
			
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			$objWriter->save($filename);
			
			out_json(array(
				'success' => TRUE,
				'msg' => 'Your download is ready!',
				'download' => base_url() . $filename
			));
		}
		catch(Exception $e) {
			error_msg($e->getMessage());
		}
	}
	
	public function get_active_total($member_id = NULL, $type = '') {
		if (is_null($member_id) OR empty($type)) return;
		
		$where = "member_id = $member_id AND `status` IN('Approved', 'New') AND `type` = '$type' AND archive = 0";
		
		$all = $this->order_by('id', 'desc')->limit(1)->get_many_by($where);
		
		$legible = FALSE;
		$data = NULL;
		
		if (count($all) == 0) {
			$legible = TRUE;
		} else {
			foreach ($all as $key => $value) {
				$payable = $value['monthly'] * $value['period'];
				
				$ptotal = 0;
				$pwhere = array(
					'loan_id' => $value['id'],
					'archive' => 0
				);
				
				$payments = $this->loan_payments->get_many_by($pwhere);
				foreach ($payments as $kkey => $vvalue) {
					$ptotal+= $vvalue['payment'];
				}
				
				//if paid amount is above 50% of the payable
				if ($type == 'Regular Loan') {
					if ($ptotal >= ($payable / 2) AND $ptotal <= $payable) {
						$data = array(
							'payments' => $ptotal,
							'payable' => $payable,
							'balance' => $payable - $ptotal
						);
						$legible = TRUE;
						break;
					} elseif ($ptotal < ($payable / 2)) {
						$value['payments'] = $ptotal;
						$value['payable'] = $payable;
						$value['balance'] = $payable - $ptotal;
						$data = $value;
						$legible = FALSE;
						break;
					}
				} else {
					if ($ptotal < $payable) {
						$value['payments'] = $ptotal;
						$value['payable'] = $payable;
						$value['balance'] = $payable - $ptotal;
						$data = $value;
						$legible = FALSE;
						break;
					}
				}
				
				$legible = TRUE;
			}
		}
		
		return array(
			$legible,
			$data
		);
	}
	
	public function export_rl() {
		try {
			$post = $this->input->post();
			$loan_info = $this->get($post['loan_id']);
			$applicant = $this->loan_member_info->get($loan_info['member_id']);
			
			$this->load->library('excel');
			
			$this->excel->setActiveSheetIndex(0);
			$this->excel->getDefaultStyle()->getFont()->setName('Segoe UI');
			
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
			
			$this->excel->getActiveSheet()->setTitle($applicant['firstname'] {
				0
			} . $applicant['lastname']);
			
			//Report Header
			$this->excel->getActiveSheet()->setCellValue('A1', 'COTABATO LIGHT EMPLOYEES MULTI-PURPOSE COOPERATIVE');
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A1:I1');
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			//Report Sub-header
			$this->excel->getActiveSheet()->setCellValue('A2', 'AUDIT REPORT/ FINDINGS');
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
			$this->excel->getActiveSheet()->mergeCells('A2:I2');
			
			$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$row = 4;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, "NAME: {$applicant['firstname']} {$applicant['lastname']}");
			$this->excel->getActiveSheet()->mergeCells("A{$row}:E{$row}");
			
			$this->excel->getActiveSheet()->setCellValue('I' . $row, date('d-M-Y', strtotime($loan_info['created_at'])));
			$this->excel->getActiveSheet()->getStyle('I' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('C' . $row, "-----------------");
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, "LOAN AMOUNT:");
			$this->excel->getActiveSheet()->mergeCells("A{$row}:B{$row}");
			$this->excel->getActiveSheet()->setCellValue('D' . $row, number_format($loan_info['principal'], 2, '.', ','));
			$this->excel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('D' . $row, " ==========");
			$this->excel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "TERMS =>");
			$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('F' . $row, "50% - 50%");
			
			$this->excel->getActiveSheet()->setCellValue('G' . $row, "60% - 40%");
			
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, "LAND BANK LOAN thru CLEMPCO");
			$this->excel->getActiveSheet()->setCellValue('F' . $row, "---------------");
			
			$this->excel->getActiveSheet()->setCellValue('G' . $row, "---------------");
			
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('B' . $row, "Monthly amortization is =====>>>>>>");
			$this->excel->getActiveSheet()->setCellValue('F' . $row, number_format($loan_info['monthly'], 2, '.', ','));
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('F' . $row, " ==========");
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "15TH ->");
			$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$this->excel->getActiveSheet()->setCellValue('F' . $row, number_format($loan_info['monthly'] * 0.5, 2));
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$this->excel->getActiveSheet()->setCellValue('G' . $row, number_format($loan_info['monthly'] * 0.6, 2));
			$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "30TH ->");
			$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('F' . $row, number_format($loan_info['monthly'] * 0.5, 2));
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$this->excel->getActiveSheet()->setCellValue('G' . $row, number_format($loan_info['monthly'] * 0.4, 2));
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('F' . $row, " ==========");
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$this->excel->getActiveSheet()->setCellValue('G' . $row, " ==========");
			$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$formula = '@PMT(p, i, n)';
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('B' . $row, "FORMULA = {$formula}");
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('C' . $row, "where p = principal");
			$this->excel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('F' . $row, number_format($loan_info['principal'], 2, '.', ','));
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('C' . $row, "i = interest rate");
			$this->excel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "{$loan_info['interest']}%");
			$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('F' . $row, "{$loan_info['interest']}% p.a.");
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('C' . $row, "n = time or period");
			$this->excel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "{$loan_info['period']}");
			$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('F' . $row, "{$loan_info['period']} months");
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('C' . $row, "of payment");
			$this->excel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			//Report body
			$row+= 2;
			$headers = array(
				'#',
				'Date',
				'Payments',
				'Payable',
				'Principal payment',
				'Interest payment',
				'Principal Balance',
				'Over/Under',
				'Remarks'
			);
			foreach ($headers as $key => $value) {
				$this->excel->getActiveSheet()->setCellValue(num_to_alpha($key) . $row, $value);
				if ($value == 'Remarks') $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
			}
			
			//payment details
			$row+= 2;
			$running_balance = $loan_info['principal'];
			$period = $loan_info['period'];
			$start = new DateTime($loan_info['created_at'], new DateTimeZone("UTC"));
			
			$payable_tot = 0;
			$interest_tot = 0;
			$principal_tot = 0;
			$ou_tot = 0;
			
			for ($r = 1; $r <= $period; $r++) {
				$month_later = clone $start;
				$month_later->add(new DateInterval("P1M"));
				
				$this->excel->getActiveSheet()->setCellValue('A' . $row, $r);
				$this->excel->getActiveSheet()->setCellValue('B' . $row, $month_later->format('M-y'));
				
				$start = $month_later;
				
				$apr = $loan_info['interest'] / 100;
				$apr2 = $apr / 12;
				
				$payable = get_pmt($apr, $loan_info['period'], $loan_info['principal'], $loan_info['type']);
				$interest = $running_balance * $apr2;
				$principal = $payable - $interest;
				$running_balance = $running_balance - $principal;
				
				$this->excel->getActiveSheet()->setCellValue('D' . $row, number_format($payable, 2, '.', ','));
				$this->excel->getActiveSheet()->setCellValue('E' . $row, number_format($principal, 2, '.', ','));
				$this->excel->getActiveSheet()->setCellValue('F' . $row, number_format($interest, 2, '.', ','));
				$this->excel->getActiveSheet()->setCellValue('G' . $row, number_format($running_balance, 2, '.', ','));
				$this->excel->getActiveSheet()->setCellValue('H' . $row, number_format(-(0 - $payable) , 2, '.', ','));
				
				$this->excel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('H' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$payable_tot+= $payable;
				$interest_tot+= $interest;
				$principal_tot+= $principal;
				$ou_tot+= - (0 - $payable);
				
				$row++;
			}
			
			$this->excel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('H' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 0);
			$this->excel->getActiveSheet()->setCellValue('D' . $row, number_format($payable_tot, 2, '.', ','));
			$this->excel->getActiveSheet()->setCellValue('E' . $row, number_format($principal_tot, 2, '.', ','));
			$this->excel->getActiveSheet()->setCellValue('F' . $row, number_format($interest_tot, 2, '.', ','));
			$this->excel->getActiveSheet()->setCellValue('G' . $row, number_format($running_balance, 2, '.', ','));
			$this->excel->getActiveSheet()->setCellValue('H' . $row, number_format($ou_tot, 2, '.', ','));
			
			$row+= 2;
			$this->excel->getActiveSheet()->setCellValue('G' . $row, number_format($loan_info['principal'], 2, '.', ','));
			$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('H' . $row, "LOAN AMOUNT");
			$this->excel->getActiveSheet()->getStyle('H' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('G' . $row, 0);
			$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('H' . $row, "LOAN BAL.");
			$this->excel->getActiveSheet()->getStyle('H' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('G' . $row, '100');
			$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('H' . $row, "PROCESSING FEE");
			$this->excel->getActiveSheet()->getStyle('H' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('G' . $row, number_format($loan_info['principal'] * 0.5, 2, '.', ','));
			$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('H' . $row, "CAPITAL BUILD-UP");
			$this->excel->getActiveSheet()->getStyle('H' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			
			$row+= 2;
			$this->excel->getActiveSheet()->setCellValue('B' . $row, "Prepared By:");
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "Noted By:");
			
			$row+= 3;
			$pby = $this->loan_vars->get_by(array(
				'type' => 'Report Signatories',
				'subtype' => 'Prepared_By'
			));
			$nby = $this->loan_vars->get_by(array(
				'type' => 'Report Signatories',
				'subtype' => 'Noted_By'
			));
			
			if (!isset($pby['id']) OR !isset($nby)) {
				error_msg('One of the report signatories is not set!');
				redirect('dashboard/index?_m=' . $post['_m']);
			}
			
			$this->excel->getActiveSheet()->setCellValue('B' . $row, strtoupper($pby['value']));
			$this->excel->getActiveSheet()->setCellValue('E' . $row, strtoupper($nby['value']));
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('B' . $row, "Audit Committee - Chairman");
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "CLEMPCO - Chairman");
			
			//set row height to all rows
			for ($rd = 0; $rd <= $row; $rd++) {
				$this->excel->getActiveSheet()->getRowDimension($rd)->setRowHeight(20);
			}
		}
		catch(Exception $e) {
			error_msg($e->getMessage());
		}
	}
	
	public function export_ml() {
		
		try {
			$post = $this->input->post();
			$loan_info = $this->get($post['loan_id']);
			$applicant = $this->loan_member_info->get($loan_info['member_id']);
			
			$this->load->library('excel');
			
			$this->excel->setActiveSheetIndex(0);
			$this->excel->getDefaultStyle()->getFont()->setName('Segoe UI');
			
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
			
			$this->excel->getActiveSheet()->setTitle($applicant['firstname'] {
				0
			} . $applicant['lastname']);
			
			//Report Header
			$this->excel->getActiveSheet()->setCellValue('A1', 'COTABATO LIGHT EMPLOYEES MULTI-PURPOSE COOPERATIVE');
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A1:I1');
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			//Report Sub-header
			$this->excel->getActiveSheet()->setCellValue('A2', 'AUDIT REPORT/ FINDINGS');
			$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(13);
			$this->excel->getActiveSheet()->mergeCells('A2:I2');
			
			$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$row = 4;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, "NAME: {$applicant['firstname']} {$applicant['lastname']}");
			$this->excel->getActiveSheet()->mergeCells("A{$row}:E{$row}");
			
			$this->excel->getActiveSheet()->setCellValue('I' . $row, date('d-M-Y', strtotime($loan_info['created_at'])));
			$this->excel->getActiveSheet()->getStyle('I' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('C' . $row, "-----------------");
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, "LOAN AMOUNT:");
			$this->excel->getActiveSheet()->mergeCells("A{$row}:B{$row}");
			$this->excel->getActiveSheet()->setCellValue('D' . $row, number_format($loan_info['principal'], 2, '.', ','));
			$this->excel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('D' . $row, " ==========");
			$this->excel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "TERMS =>");
			$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('F' . $row, "50% - 50%");
			
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('A' . $row, "LAND BANK LOAN thru CLEMPCO");
			$this->excel->getActiveSheet()->setCellValue('F' . $row, "---------------");
			
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('B' . $row, "Monthly amortization is =====>>>>>>");
			$this->excel->getActiveSheet()->setCellValue('F' . $row, number_format($loan_info['monthly'], 2, '.', ','));
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('F' . $row, " ==========");
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "15TH ->");
			$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$this->excel->getActiveSheet()->setCellValue('F' . $row, number_format($loan_info['monthly'] * 0.5, 2));
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "30TH ->");
			$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('F' . $row, number_format($loan_info['monthly'] * 0.5, 2));
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('F' . $row, " ==========");
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$formula = 'p*(i*n+1)';
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('B' . $row, "FORMULA = {$formula}");
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('C' . $row, "where p = principal");
			$this->excel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('F' . $row, number_format($loan_info['principal'], 2, '.', ','));
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('C' . $row, "i = interest rate");
			$this->excel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "{$loan_info['interest']}%");
			$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('F' . $row, "{$loan_info['interest']}% p.a.");
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('C' . $row, "n = time or period");
			$this->excel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "{$loan_info['period']}");
			$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('F' . $row, "{$loan_info['period']} months");
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('C' . $row, "of payment");
			$this->excel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			//Report body
			$row+= 2;
			$headers = array(
				'#',
				'Date',
				'Payments',
				'Payable',
				'Principal payment',
				'Interest payment',
				'Principal Balance',
				'Over/Under',
				'Remarks'
			);
			foreach ($headers as $key => $value) {
				$this->excel->getActiveSheet()->setCellValue(num_to_alpha($key) . $row, $value);
				if ($value == 'Remarks') $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
			}
			
			//payment details
			$row+= 2;
			$running_balance = $loan_info['principal'];
			$period = $loan_info['period'];
			$start = new DateTime($loan_info['created_at'], new DateTimeZone("UTC"));
			
			$payable_tot = 0;
			$interest_tot = 0;
			$principal_tot = 0;
			$ou_tot = 0;
			
			for ($r = 1; $r <= $period; $r++) {
				$month_later = clone $start;
				$month_later->add(new DateInterval("P1M"));
				
				foreach (array(
					'15',
					't'
				) as $pkey => $pvalue) {
					$this->excel->getActiveSheet()->setCellValue('A' . $row, $r);
					$this->excel->getActiveSheet()->setCellValue('B' . $row, $month_later->format("{$pvalue}-M-y"));
					
					$start = $month_later;
					
					$apr = $loan_info['interest'] / 100;
					$apr2 = $apr / 12;
					
					$payable = $loan_info['monthly'] * 0.5;
					$interest = $loan_info['principal'] * (($loan_info['interest'] / 100) * $loan_info['period']) / ($loan_info['period'] * 2);
					$principal = $payable - $interest;
					$running_balance = $running_balance - $principal;
					
					$this->excel->getActiveSheet()->setCellValue('D' . $row, number_format($payable, 2, '.', ','));
					$this->excel->getActiveSheet()->setCellValue('E' . $row, number_format($principal, 2, '.', ','));
					$this->excel->getActiveSheet()->setCellValue('F' . $row, number_format($interest, 2, '.', ','));
					$this->excel->getActiveSheet()->setCellValue('G' . $row, number_format($running_balance, 2, '.', ','));
					$this->excel->getActiveSheet()->setCellValue('H' . $row, number_format(-(0 - $payable) , 2, '.', ','));
					
					$this->excel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$this->excel->getActiveSheet()->getStyle('H' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					
					$payable_tot+= $payable;
					$interest_tot+= $interest;
					$principal_tot+= $principal;
					$ou_tot+= - (0 - $payable);
					
					$row++;
				}
			}
			
			$this->excel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('H' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			
			$this->excel->getActiveSheet()->setCellValue('C' . $row, 0);
			$this->excel->getActiveSheet()->setCellValue('D' . $row, number_format($payable_tot, 2, '.', ','));
			$this->excel->getActiveSheet()->setCellValue('E' . $row, number_format($principal_tot, 2, '.', ','));
			$this->excel->getActiveSheet()->setCellValue('F' . $row, number_format($interest_tot, 2, '.', ','));
			$this->excel->getActiveSheet()->setCellValue('G' . $row, number_format($running_balance, 2, '.', ','));
			$this->excel->getActiveSheet()->setCellValue('H' . $row, number_format($ou_tot, 2, '.', ','));
			
			$row+= 2;
			$this->excel->getActiveSheet()->setCellValue('G' . $row, number_format($loan_info['principal'], 2, '.', ','));
			$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('H' . $row, "LOAN AMOUNT");
			$this->excel->getActiveSheet()->getStyle('H' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('G' . $row, 0);
			$this->excel->getActiveSheet()->getStyle('G' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->setCellValue('H' . $row, "LOAN BAL.");
			$this->excel->getActiveSheet()->getStyle('H' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			
			$row+= 2;
			$this->excel->getActiveSheet()->setCellValue('B' . $row, "Prepared By:");
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "Noted By:");
			
			$row+= 3;
			$pby = $this->loan_vars->get_by(array(
				'type' => 'Report Signatories',
				'subtype' => 'Prepared_By'
			));
			$nby = $this->loan_vars->get_by(array(
				'type' => 'Report Signatories',
				'subtype' => 'Noted_By'
			));
			
			if (!isset($pby['id']) OR !isset($nby)) {
				error_msg('One of the report signatories is not set!');
				redirect('loans/browse?_m=' . $post['_m']);
			}
			
			$this->excel->getActiveSheet()->setCellValue('B' . $row, strtoupper($pby['value']));
			$this->excel->getActiveSheet()->setCellValue('E' . $row, strtoupper($nby['value']));
			
			$row+= 1;
			$this->excel->getActiveSheet()->setCellValue('B' . $row, "Audit Committee - Chairman");
			$this->excel->getActiveSheet()->setCellValue('E' . $row, "CLEMPCO - Chairman");
			
			//set row height to all rows
			for ($rd = 0; $rd <= $row; $rd++) {
				$this->excel->getActiveSheet()->getRowDimension($rd)->setRowHeight(20);
			}
		}
		catch(Exception $e) {
			error_msg($e->getMessage());
		}
	}
	
	public function batch_notify() {
		log_message('DEBUG', 'Batch Notify');
		$result = $this->db->query("
        		SELECT * FROM {$this->_table}
        			WHERE last_notify < CURRENT_DATE() - INTERVAL 18 MONTH
        ")->result_array();
		
		foreach ($result as $key => $value) {
			$update = array(
				'last_notify' => date('Y-m-d')
			);
			$this->update($value['id'], $update);

			$this->email2('reminder', $value['id']);
		}
	}
}
