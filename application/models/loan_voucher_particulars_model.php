<?php

class Loan_voucher_particulars_model extends Base_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_table = 'loan_voucher_particulars';
    }

    public function read($id = NULL)
    {
    	$get = $this->input->get();

    	$where = array(
			'archive' => 0,
			'voucher_id' => !is_null($id) ? $id : $get['voucher_id']
		);

    	$all = $this->get_many_by($where);

    	success_msg($all);
    }

	public function get_by_acct($id)
	{
		$particulars = $this->db->query("
			SELECT a.* FROM {$this->_table} a
				JOIN loan_vouchers b ON b.id = a.voucher_id
				WHERE acct_id = $id
				AND b.archive = 0
		")->result_array();

		$debit = 0;
		$credit = 0;

		foreach($particulars as $key => $value)
		{
			$debit += $value['debit'];
			$credit += $value['credit'];
		}

		return array($debit, $credit);
	}

	public function get_total($atype, $voucher_id = NULL)
	{
		if(is_null($voucher_id))
			return;

		$query = $this->db->query("
				SELECT sum($atype) as total FROM {$this->_table}
					WHERE voucher_id = $voucher_id
					AND archive = 0
			")->row_array();

		return $query['total'];
	}
	
}
