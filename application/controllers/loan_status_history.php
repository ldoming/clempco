<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loan_status_history extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'loan_status_history',
			'loan_member_info',
			'loan_application'
		);

		parent::__construct();

		is_valid_session();
	}

	public function read()
	{
		$this->loan_status_history->read();
	}

	public function create()
	{
		$this->loan_status_history->create();
	}

}