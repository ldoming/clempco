<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_login extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'loan_user_login',
			'loan_user_info',
			'loan_member_info',
			'common/loan_vars',
			'common/loan_user_request'
		);

		parent::__construct();
	}
	
	public function index()
	{
		return $this->index2();

		if(get_session() !== FALSE)
			redirect('members/index');

		$data = array(
			'title' => 'User Login',
			'app' => 'user_login'
		);

		load_template($data);
	}

	public function index2()
	{
		$this->load->view('user_login/v2/header');
		$this->load->view('user_login/v2/view');
	}

	public function validate()
	{
		$this->loan_user_login->validate_login();
	}

	public function update_password()
	{
		$cp_session = $this->session->userdata('Change Password');
		if(!$cp_session)
		{
			error_msg("You need to login first in order to change your password!");
			redirect('user_login/index');
		}

		$this->loan_user_login->update_password($cp_session);
	}

	public function change_password()
	{
		$cp_session = $this->session->userdata('Change Password');
		if(!$cp_session)
		{
			error_msg("You need to login first in order to change your password!");
			redirect('user_login/index');
		}

		$data = array(
			'title' => 'Change Password',
			'app' => 'change_password',
			'change_info' => $this->session->userdata('Change Password')
		);

		load_template($data);
	}

	public function forgot_password()
	{
		$data = array(
			'title' => 'Forgot Password',
			'app' => 'forgot_password'
		);

		load_template($data);
	}

	public function generate_password()
	{
		$this->loan_user_login->generate_password();
	}

}