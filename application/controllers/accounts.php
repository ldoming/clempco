<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'loan_accounts',
			'loan_user_login',
			'common/loan_vars',
			'common/loan_account_groups'
		);

		parent::__construct();

		is_valid_session();
	}

	//for user access
	public function index()
	{
		$data = array(
			'title' => 'Accounts',
			'app' => 'accounts'
		);

		load_template($data);
	}

	public function browse()
	{
		$this->index();
	}

	public function create()
	{
		$this->loan_accounts->create();
	}

	public function read()
	{
		$this->loan_accounts->read();
	}

	public function read2()
	{
		$this->loan_accounts->read2();
	}

	public function destroy()
	{
		$this->loan_accounts->destroy();
	}

	public function get_code()
	{
		$this->loan_accounts->get_code();
	}
}