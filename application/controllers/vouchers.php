<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vouchers extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'loan_vouchers',
			'loan_voucher_particulars',
			'common/loan_vars',
			'loan_accounts'
		);

		parent::__construct();

		is_valid_session();
	}

	public function journal()
	{
		$data = array(
			'title' => 'Journal Vouchers',
			'app' => 'vouchers',
			'vtype' => 'Journal Voucher'
		);

		load_template($data);
	}

	public function cash()
	{
		$data = array(
			'title' => 'Cash Vouchers',
			'app' => 'vouchers',
			'vtype' => 'Cash Voucher'
		);

		load_template($data);
	}

	public function petty_cash()
	{
		$data = array(
			'title' => 'Petty Cash Vouchers',
			'app' => 'vouchers',
			'vtype' => 'Petty Cash Voucher'
		);

		load_template($data);
	}

	public function create()
	{
		$this->loan_vouchers->create();
	}

	public function read()
	{
		$this->loan_vouchers->read();
	}

	public function destroy()
	{
		$this->loan_vouchers->destroy();
	}

	public function export()
	{
		$this->loan_vouchers->export();
	}

	public function get_vno()
	{
		$this->loan_vouchers->get_vno();
	}
}