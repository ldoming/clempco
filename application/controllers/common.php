<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'common/loan_vars',
			'loan_member_info',
			'common/loan_account_groups'
		);

		parent::__construct();

		is_valid_session();
	}

	public function read()
	{
		$this->loan_vars->read();
	}
	
	public function includes_images_png()
	{
		header("Content-type: text/css");
		$this->load->view('common/css');
	}
	
	public function read1()
	{
		$this->employee_info->read1();
	}

	public function read2()
	{
		$this->loan_account_groups->read();
	}

	public function read3()
	{
		$this->loan_vars->get_user_types();
	}
}