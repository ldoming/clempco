<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_files extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'common/loan_user_files'
		);

		parent::__construct();
	}

	public function upload()
	{
		$this->loan_user_files->upload();
	}
}