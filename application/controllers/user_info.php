<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_info extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'loan_user_info',
			'loan_user_login',
			'common/loan_user_files'
		);

		parent::__construct();
	}
	
	public function end_session()
	{
		$this->loan_user_info->end_session();
	}

	public function edit()
	{
		$this->loan_user_info->edit();
	}
	
	public function profile()
	{
		$data = array(
			'title' => 'User Profile',
			'app' => 'profile'
		);

		load_template($data);
	}
}