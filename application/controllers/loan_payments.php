<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loan_payments extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'loan_application',
			'loan_payments',
			'loan_member_info'
		);

		parent::__construct();

		is_valid_session();
	}

	public function read()
	{
		$this->loan_payments->read();
	}

	public function create()
	{
		$this->loan_payments->create();
	}

	public function destroy()
	{
		$this->loan_payments->destroy();
	}
}