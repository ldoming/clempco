<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'reports',
			'common/loan_vars',
			'loan_vouchers',
			'loan_voucher_particulars',
			'loan_accounts',
			'common/loan_account_groups',
			'loan_deductions',
			'loan_member_info',
			'loan_application'
		);

		parent::__construct();

		is_valid_session();
	}

	public function index()
	{
		$data = array(
			'title' => 'Reports',
			'app' => 'reports',
			'members' => $this->loan_member_info->read2()
		);

		load_template($data);
	}

	public function generate()
	{
		$this->reports->generate();
	}
}