<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'loan_member_info',
			'loan_user_login',
			'common/loan_vars',
			'common/loan_user_files',
			'loan_application',
			'loan_payments'
		);

		parent::__construct();

		is_valid_session();
	}

	public function index()
	{
		$data = array(
			'title' => 'Members',
			'app' => 'members'
		);

		load_template($data);
	}

	//for member access
	public function info($action = '')
	{
		switch ($action) {
			case 'Loans':
				$data = array(
					'title' => 'Loans',
					'app' => 'member_info/loans'
				);
				break;
			case 'Balances':
				$data = array(
					'title' => 'Balance Info',
					'app' => 'member_info/balances'
				);
				break;
			default:
				error_msg("Action not supported! Please login to refresh this page.");
				$this->session->unset_userdata('User Info');
				redirect('user_login/index');
				break;
		}

		$data['loan_amount'] = $this->loan_vars->get_amounts();

		load_template($data);
	}

	public function browse()
	{
		$this->index();
	}

	public function read()
	{
		$this->loan_member_info->read();
	}

	public function create()
	{
		$this->loan_member_info->create();
	}
	
	public function destroy()
	{
		$this->loan_member_info->destroy();
	}

	public function balance_check()
	{
		$this->loan_member_info->balance_check();
	}

	public function edit()
	{
		$this->loan_member_info->edit();
	}

}