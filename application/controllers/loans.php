<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loans extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'loan_application',
			'loan_member_info',
			'loan_payments',
			'common/loan_vars',
			'loan_status_history',
			'common/loan_email',
			'loan_deductions'
		);

		parent::__construct();

		$uri = explode('/', $this->uri->uri_string());
		if($uri[1] != 'batch_notify')
			is_valid_session();
	}

	public function index()
	{
		$session = get_session();

		$data = array(
			'title' => 'Loans',
			'total_savings' => $this->loan_deductions->get_totals('savings'),
			'total_deposit' => $this->loan_deductions->get_totals('deposit'),
			'app' => strtolower("loans/{$session['access_type']}")
		);

		load_template($data);
	}

	public function browse()
	{
		$this->index();
	}

	public function read()
	{
		$this->loan_application->read();
	}

	public function read1()
	{
		$this->loan_application->read1();
	}

	public function read2()
	{
		$this->loan_status_history->read();
	}

	public function read3()
	{
		$this->loan_application->read3();
	}

	public function create()
	{
		$this->loan_application->create();
	}

	public function create1()
	{
		$this->loan_application->create1();
	}
	
	public function destroy()
	{
		$this->loan_application->destroy();
	}

	public function export()
	{
		$this->loan_application->export();
	}
	
	public function batch_notify()
	{
		$this->loan_application->batch_notify();
	}
}