<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'loan_user_info',
			'loan_user_login'
		);

		parent::__construct();

		is_valid_session();
	}

	public function index()
	{
		$data = array(
			'title' => 'Users',
			'app' => 'settings/users'
		);

		load_template($data);
	}

	public function users($action = 'browse')
	{
		if(empty($action))
			error_msg("You have no access to view this page!");

		switch ($action) {
			case 'browse':
				$this->index();
				break;
			case 'read':
				$this->loan_user_info->read();
				break;
			case 'create':
				$this->loan_user_info->create();
				break;
			case 'destroy':
				$this->loan_user_info->destroy();
				break;
			default:
				error_msg("Invalid request!");
				break;
		}
	}

	public function create()
	{
		$this->loan_member_info->create();
	}

	public function user_access()
	{
		$data = array(
			'title' => 'Access Definitions',
			'app' => 'settings/user_access'
		);

		load_template($data);
	}
	
	public function reports($action = 'index')
	{
		if(empty($action))
			error_msg("Invalid request!");

		switch ($action) {
			case 'index':
				$data = array(
					'title' => 'Report Settings',
					'app' => 'settings/reports',
					'data' => $this->loan_vars->read_by_type("Report Signatories", TRUE)
				);

				load_template($data);
				break;
			case 'modify':
				$post = $this->input->post();

				if(!isset($post['Prepared_By']) OR empty($post['Prepared_By']) OR !isset($post['Noted_By']) OR empty($post['Noted_By']))
				{
					error_msg('Please fill-up required fields!');
					redirect('settings/reports/index?_m=' . $post['_m']);
				}

				$data = array(
					'Prepared_By' => $post['Prepared_By'],
					'Noted_By' => $post['Noted_By']
				);

				$this->loan_vars->modify_signatories($data);
				break;
			default:
				error_msg("Invalid request!");
				break;
		}
	}
}