<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deductions extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'loan_member_info',
			'loan_deductions',
			'loan_application',
			'loan_payments',
			'common/loan_email'
		);

		parent::__construct();

		is_valid_session();
	}

	public function index($action = 'manage')
	{
		$data = array(
			'title' => 'Deductions',
			'app' => 'member_info/deductions',
			'total_capital' => $this->loan_deductions->get_totals('capital'),
			'total_savings' => $this->loan_deductions->get_totals('savings'),
			'ded_action' => $action
		);

		load_template($data);
	}

	public function read()
	{
		$this->loan_deductions->read();
	}

	public function read2()
	{
		$this->loan_deductions->read2();
	}

	public function create()
	{
		$this->loan_deductions->create();
	}

	public function destroy()
	{
		$this->loan_deductions->destroy();
	}
}