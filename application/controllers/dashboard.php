<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			
		);

		parent::__construct();

		is_valid_session();
	}

	public function index()
	{
		list($modules, $_submodules) = get_modules();

		$data = array(
			'title' => 'Dashboard',
			'app' => 'dashboard',
			'modules' => $modules,
			'submodules' => $_submodules
		);

		load_template($data);
	}
}