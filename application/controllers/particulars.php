<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Particulars extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'loan_voucher_particulars'
		);
		parent::__construct();
	}

	public function read()
	{
		$this->loan_voucher_particulars->read();
	}
}