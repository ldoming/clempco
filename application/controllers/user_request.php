<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_request extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'common/loan_user_request',
			'common/loan_email',
			'loan_user_info'
		);

		parent::__construct();
	}
	
	public function forgot_password()
	{
		$this->loan_user_request->forgot_password();
	}

	public function get_icons()
	{
		get_icons();
	}
	
}