<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_access extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'common/loan_user_access'
		);

		parent::__construct();

		is_valid_session();
	}

	public function read()
	{
		$this->loan_user_access->read();
	}

	public function modify()
	{
		$this->loan_user_access->modify();
	}
}