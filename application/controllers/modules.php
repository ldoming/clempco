<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modules extends Base_Controller {

	function __construct()
	{
		$this->models = array(
			'common/loan_modules',
			'common/loan_submodules',
			'common/loan_user_access'
		);
		parent::__construct();
	}

	public function read()
	{
		$this->loan_modules->read();
	}
}