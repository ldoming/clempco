<script type="text/javascript">
Ext.define('LOAN.controller.Members', {
    extend: 'Ext.app.Controller',

    init: function() {
        this.control({
            'memberinfo button[text=New Member]': {
                click: this.onNewMemberClick
            },
            'memberinfo': {
                itemclick: this.onMemberItemClick
            }
        });
    },

    views: [
        'Members'
    ],

    models: [
        'Members'
    ],

    stores: [
        'Members'
    ],

    onNewMemberClick: function(ths) {
        var member = new LOAN.view.common.AddMember();
        member.show();
        member._action = 'create';
    },

    onMemberItemClick: function(ths, rec, item, i, e) {
        var menu = new Ext.widget('gridmenu', {
            record: rec,
            items: [{
                text: 'Modify',
                handler: this.onModifyMember
            }, {
                text: 'Delete',
                handler: this.onDeleteMember
            }],
            _e: {
                e: e,
                grid: ths
            }
        });

        ExtCommon.showAt(menu, e, ths);
    },

    onModifyMember: function(ths) {
        var data = ths.up('menu').record.data;

        var member = new LOAN.view.common.AddMember();
        member.show();

        member._action = 'update';

        for (d in data) {
            var obj = ExtCommon.getCmp('[name=' + d + ']');
            if (obj && obj.setValue)
                obj.setValue(data[d]);
        }

        ExtCommon.getCmp('addmember textfield[name=password]').allowBlank = true;
        ExtCommon.getCmp('addmember textfield[name=confirm]').allowBlank = true;

        if (data.picture)
            ExtCommon.getCmp('addmember image[name=picture]').setSrc(data.picture);
    },

    onDeleteMember: function(ths) {
        var data = ths.up('menu').record.data;

        Ext.Msg.show({
            msg: 'Delete this record?',
            title: 'Confirm',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn) {
                if (btn == 'ok') {
                    Ext.widget('form').getForm().submit({
                        url: ExtCommon.baseUrl + 'members/destroy',
                        params: {
                            employee_id: data.id
                        },
                        waitMsg: 'Please wait...',
                        success: function(f, action) {
                            if (action.result.success)
                                ExtCommon.successMsg(action.result.data);
                            else
                                ExtCommon.errorMsg(action.result.data);

                            grid.getStore().loadPage(1);
                        },
                        failure: function(f, action) {
                            ExtCommon.errorMsg(action.result.data);
                        }
                    });
                }
            }
        })
    }

});
</script>
