<style type="text/css">
	#addmember-1035 {
		margin-top: 15px !important;
		margin-bottom: 20px !important;
		z-index: 19000 !important;
		box-shadow: rgb(136, 136, 136) 0px 0px 6px !important;
	}
	#ext-gen1209 {
		display: none !important;
	}
</style>

<div id="ext-view"></div>

<script type="text/javascript">

	Ext.define('LOAN.view.Members', {
		extend: 'Ext.grid.Panel',
		alias: 'widget.memberinfo',
		store: 'Members',
		cls: 'span12',
		renderTo: 'ext-view',
		height: 500,

		initComponent: function(){

			this.tbar = [{
				xtype: 'searchfield',
				store: Ext.getStore('Members'),
				emptyText: 'Search',
				fieldLabel: 'Search',
				labelWidth: 60
			}, {
				xtype: 'button',
				text: 'New Member'
			}];

			this.columns = [{
				text: 'ID',
				dataIndex: 'id',
				width: 100
			}, {
				text: 'name',
				dataIndex: 'name',
				flex: 1
			}, {
				text: 'Email Address',
				dataIndex: 'email_address',
				flex: 1
			}];

			this.dockedItems = [{
				xtype: 'pagingtoolbar',
				dock: 'bottom',
				displayInfo: true,
				store: Ext.getStore('Members'),
				items: [{
					xtype: 'button',
					text: 'Export',
					hidden: true
				}]
			}];

			this.callParent(arguments);
		},

		onEditClick: function(grid, rowIndex){
			var data = grid.getStore().getAt(rowIndex).data;

			var member = new LOAN.view.common.AddMember();
			member.show();

			member._action = 'update';

			for(d in data){
				var obj = ExtCommon.getCmp('[name=' + d + ']');
				if(obj && obj.setValue)
					obj.setValue(data[d]);
			}

			ExtCommon.getCmp('addmember textfield[name=password]').allowBlank = true;
			ExtCommon.getCmp('addmember textfield[name=confirm]').allowBlank = true;

			if(data.picture)
				ExtCommon.getCmp('addmember image[name=picture]').setSrc(data.picture);
		}
	});

</script>