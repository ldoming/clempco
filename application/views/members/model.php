<script type="text/javascript">

	Ext.define('LOAN.model.Members', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'name',
			type : 'string'
		}, {
			name : 'email_address',
			type : 'string'
		}, {
			name : 'firstname',
			type : 'string'
		}, {
			name : 'lastname',
			type : 'string'
		}, {
			name : 'address',
			type : 'string'
		}, {
			name : 'tel_no',
			type : 'string'
		}, {
			name : 'username',
			type : 'string'
		}, {
			name : 'position',
			type : 'string'
		}, {
			name : 'email_address',
			type : 'string'
		}, {
			name : 'application_id',
			type : 'string'
		}, {
			name : 'birth_date',
			type : 'string'
		}, {
			name : 'civil_status',
			type : 'string'
		}, {
			name : 'id_no',
			type : 'string'
		}, {
			name : 'passport_no',
			type : 'string'
		}, {
			name : 'ctc_no',
			type : 'string'
		}, {
			name : 'monthly_income',
			type : 'string'
		}, {
			name : 'cell_no',
			type : 'string'
		}, {
			name : 'home_phone',
			type : 'string'
		}, {
			name : 'company_name',
			type : 'string'
		}, {
			name : 'company_address',
			type : 'string'
		}, {
			name : 'company_sss',
			type : 'string'
		}, {
			name : 'philhealth_no',
			type : 'string'
		}, {
			name : 'office_no',
			type : 'string'
		}, {
			name : 'office_fax',
			type : 'string'
		}, {
			name : 'employer_id',
			type : 'string'
		}, {
			name : 'picture',
			type : 'string'
		}, {
			name : 'file_id',
			type : 'int'
		}]
	});

	Ext.define('LOAN.store.Members', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.Members',
		storeId: 'Members',
		queryMode : 'remote',
		autoLoad: true,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'members/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

</script>