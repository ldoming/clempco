<br />
<?php foreach ($modules as $key => $value) { ?>
	<div class="panel">
	    <div class="panel-header clempco-prevails">
	        <?php echo $value['name'];?>
	    </div>
		<div class="panel-content">
	    	<?php foreach (${'submodule_' . $value['id']} as $skey => $svalue) { ?>
				<button class="command-button" onclick="javascript:$.visitLink('<?php echo site_url($svalue['link']);?>')">
				    <i class="icon-arrow-right-3 on-left"></i>
				    <?php echo $svalue['name'];?>
				    <small>Visit this link</small>
				</button>
			<?php } ?>
		</div>
	</div>
	<br />
<?php } ?>

<script type="text/javascript">
	$.visitLink = function(link){
		if(link){
			$(location).attr('href', link);
		}
	}
</script>