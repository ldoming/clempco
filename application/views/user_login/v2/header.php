<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Official Loan Website of Clempco</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" type="image/png" href="<?php echo base_url();?>images/clempo-logo.png" />
	<link href="<?php echo base_url();?>v2/css/master.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>
	<script src="<?php echo base_url();?>v2/js/vendor/jquery-1.10.2.min.js"></script>
	<script src="<?php echo base_url();?>v2/js/vendor/modernizr-2.6.2.min.js"></script>

	<!--metro css headers -->
	
    <link href="<?=base_url(); ?>includes/metro/css/metro-bootstrap.css" rel="stylesheet">
    <link href="<?=base_url(); ?>includes/metro/css/metro-bootstrap-responsive.css" rel="stylesheet">
    <link href="<?=base_url(); ?>includes/metro/css/iconFont.min.css" rel="stylesheet">
</head>
	<!-- HTML5 shim and respond.js IE8 support of HTML5 elements and medai queries -->
	<!-- [if lt IE 9] >
		<script src="<?php echo base_url();?>js/html5shiv.js"><script>
		<script src="<?php echo base_url();?>js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<!--[if lt IE 7]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="page-site">