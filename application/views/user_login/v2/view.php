<div class="metro" style="text-align: center">
	<?php $this->load->view('common/info');?>
</div>
<div class="login-page">
	<div class="header-bar"></div>
	<div class="col-md-5 col-xs-10 col-md-push-3 col-xs-push-1">
		<div class="logo">
			<img src="<?php echo base_url();?>v2/images/clempo-logo.png" class="img-responsive" alt="" title="">
		</div>
		<div class="panel panel-info" id="loginpage">
			<div class="panel-body">
				 <div class="panel-heading">

				</div>
				<hr/>
				<?php $attr = array('class' => 'form-horizontal');?>
				<?php echo form_open('user_login/validate', $attr);?>
				<div class="form-group">

				<label for="inputusername" class="col-sm-2 control-label"><p>Username</p></label>
				<div class="col-sm-10">
				<input type="text" class="form-control"  id="username" name="username" class="input" placeholder="Username" />
				</div>
				</div>
				<div class="form-group">
				<label for="inputPassword" class="col-sm-2 control-label "><p>password</p></label>
				<div class="col-sm-10">
				<input type="password" name="password" class="form-control" id="password" placeholder="Password">
				</div>
				</div>
				<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
				</div>
				</div>
				<div class="form-group">
				<div class="col-sm-offset-1 col-sm-10">
				<table>
					<tr>
						<td><button type="submit" name="submit" class="cta">Log-in</button></td>
						<td></td>
					</tr>
				</table>


				</div>
				</div>
				<?php echo form_close();?>

				<!--<span><a href="#" id="register"><font color="#ffffff">I want to Register -></font></a></span>-->

			</div>
			<div class="panel-footer"></div>
		</div>
	</div>
	<div class="clearfix"></div>
</div><!-- #login page -->