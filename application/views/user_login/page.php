<div class="span3 no-tablet-portrait">
    <?php $this->load->view("{$app}/menu");?>
</div>

<div class="span9">
    <?php $this->load->view("common/info");?>
    <?php $this->load->view("{$app}/view"); ?>
</div>
