<div class="span4">
    <?php echo form_open('user_login/validate');?>
        <fieldset>
            <legend>Provide login credentials</legend>
            <label>Username | Email</label>
            <div class="input-control text" data-role="input-control">
                <input name="username" type="text" placeholder="" autofocus="" value="admin@test.com">
                <button class="btn-clear" tabindex="-1" type="button"></button>
            </div>
            <label>Password</label>
            <div class="input-control password" data-role="input-control">
                <input name="password" type="password" placeholder="" value="password">
                <button class="btn-reveal" tabindex="-1" type="button"></button>
            </div><br />

            <input type="submit" value="Submit">
        </fieldset>
    <?php echo form_close();?>
</div>