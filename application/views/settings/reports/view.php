<div class="panel">
    <div class="panel-header clempco-prevails">
        <?php echo $title;?>
    </div>
    <div class="panel-content">

        <?php echo form_open('settings/reports/modify');?>
            <input type="hidden" name="_m" value="<?php echo $this->input->get('_m');?>" />
            <fieldset>
                <legend>Signatories</legend>
                <div class="span4">
                    <?php $this->load->view("common/info"); ?>
                    <label>Prepared By (Required)</label>
                    <div class="input-control text" data-role="input-control">
                        <input name="Prepared_By" type="text" placeholder="Enter Name" autofocus="">
                        <button class="btn-clear" tabindex="-1" type="button"></button>
                    </div>
                    <label>Noted By (Required)</label>
                    <div class="input-control text" data-role="input-control">
                        <input name="Noted_By" type="text" placeholder="Enter Name">
                        <button class="btn-clear" tabindex="-1" type="button"></button>
                    </div>
                    <input class="place-right" type="submit" value="Submit">
                </div>
            </fieldset>
        <?php echo form_close();?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var data = <?php echo json_encode($data);?>;

        data.forEach(function(d){
            var el = $('input[name="' + d.subtype + '"]');
            el.val(d.value);
        });
    });
</script>