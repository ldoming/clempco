<div id="ext-view"></div>

<script type="text/javascript">
Ext.define('LOAN.view.Users', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.userinfo',
    store: 'Users',
    cls: 'span12',
    renderTo: 'ext-view',
    height: 500,

    initComponent: function() {

        this.tbar = [{
            xtype: 'searchfield',
            store: Ext.getStore('Users'),
            emptyText: 'Search',
            fieldLabel: 'Search',
            labelWidth: 60
        }, {
            xtype: 'button',
            text: 'New User'
        }];

        this.columns = [{
            text: 'ID',
            dataIndex: 'id',
            width: 100
        }, {
            text: 'name',
            dataIndex: 'name',
            flex: 1
        }, {
            text: 'Email Address',
            dataIndex: 'email_address',
            flex: 1
        }, {
            text: 'Access Type',
            dataIndex: 'user_typedef',
            flex: 1
        }, {
            xtype: 'actioncolumn',
            text: 'Options',
            items: [{
                icon: ExtCommon.baseUrl + 'includes/images/png/Writing.png',
                tooltip: 'Edit',
                handler: this.onEditClick
            }, {
                icon: ExtCommon.baseUrl + 'includes/images/png/Xion.png',
                tooltip: 'Delete',
                handler: this.onDeleteClick
            }]
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true,
            store: Ext.getStore('Users'),
            items: [{
                xtype: 'button',
                text: 'Export'
            }]
        }];

        this.callParent(arguments);
    },

    onDeleteClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        Ext.Msg.show({
            msg: 'Delete this record?',
            title: 'Confirm',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn) {
                if (btn == 'ok') {
                    Ext.widget('form').getForm().submit({
                        url: ExtCommon.baseUrl + 'settings/users/destroy',
                        params: {
                            user_id: data.id
                        },
                        waitMsg: 'Please wait...',
                        success: function(f, action) {
                            if (action.result.success)
                                ExtCommon.successMsg(action.result.data);
                            else
                                ExtCommon.errorMsg(action.result.data);

                            grid.getStore().loadPage(1);
                        },
                        failure: function(f, action) {
                            ExtCommon.errorMsg(action.result.data);
                        }
                    });
                }
            }
        })
    },


    onEditClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        var user = new LOAN.view.common.AddUser();
        user.show();

        user._action = 'update';

        for(d in data){
            var obj = ExtCommon.getCmp('[name=' + d + ']');
            if(obj && obj.setValue)
                obj.setValue(data[d]);
        }

        ExtCommon.getCmp('adduser textfield[name=password]').allowBlank = true;
        ExtCommon.getCmp('adduser textfield[name=confirm]').allowBlank = true;

        if(data.picture)
            ExtCommon.getCmp('adduser image[name=picture]').setSrc(data.picture);
    }
});
</script>

