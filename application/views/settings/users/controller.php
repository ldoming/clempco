<script type="text/javascript">
	Ext.define('LOAN.controller.Users', {
	    extend: 'Ext.app.Controller',

	    init: function() {
	        this.control({
	        	'userinfo button[text=New User]': {
	        		click: this.onNewUserClick
	        	}
	        });
	    },

	    views : [
	    	'Users'
	    ],

	    models : [
	    	'Users'
	    ],

	    stores : [
	    	'Users'
	    ],
	    
	    onNewUserClick: function(ths){
	    	new LOAN.view.common.AddUser().show();
	    }

	});
</script>