<script type="text/javascript">

	Ext.define('LOAN.model.Users', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'name',
			type : 'string'
		}, {
			name : 'email_address',
			type : 'string'
		}, {
			name : 'user_typedef',
			type : 'string'
		}, {
			name : 'firstname',
			type : 'string'
		}, {
			name : 'lastname',
			type : 'string'
		}, {
			name : 'address',
			type : 'string'
		}, {
			name : 'tel_no',
			type : 'string'
		}, {
			name : 'username',
			type : 'string'
		}, {
			name : 'picture',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.Users', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.Users',
		storeId: 'Users',
		queryMode : 'remote',
		autoLoad: true,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'settings/users/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

</script>