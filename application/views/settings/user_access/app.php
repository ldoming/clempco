<script type="text/javascript" src="<?php echo base_url();?>includes/js/SearchField.js"></script>
<script type="text/javascript">
	Ext.application({

		requires : ['Ext.container.Viewport', 'Ext.form.SearchField'],

		name : 'LOAN',

		appFolder : 'employee_info',

		controllers : ['UserTypedef', 'Common'],

		launch : function() {
			new LOAN.view.UserTypedefs();
		}
	}); 
</script>