<script type="text/javascript">
	Ext.define('LOAN.controller.UserTypedef', {
	    extend: 'Ext.app.Controller',

	    init: function() {
	        this.control({
	        	'usermodulesform button[text=Save]': {
	        		click: this.onSaveSettings
	        	},
				'usermodulesform': {
					show: this.onShowSettings
				},
				'usermodulesform button[text=Clear All]': {
	        		click: this.onClearSettings
	        	}
	        });
	    },

	    views : [
	    	'UserTypedefs'
	    ],

	    models : [
	    	'UserTypedef'
	    ],

	    stores : [
	    	'UserTypedefs'
	    ],

	    onSaveSettings: function(ths){
	    	var form = ths.up('form'),
	    		grid = form.down('usermodules'),
				sel = grid.selModel.getSelection();

	    	var data = [];
	    	for(r in sel){
	    		data.push(sel[r].data.id);
	    	}

	    	form.getForm().submit({
	    		url: ExtCommon.baseUrl + 'user_access/modify',
	    		params: {
	    			ids: data.join(','),
	    			user_typedef: form._data
	    		},
    			waitMsg: 'Please wait...',
    			success: function(f, action){
    				if(action.result.success)
    					ExtCommon.successMsg(action.result.data);
    				else
    					ExtCommon.errorMsg(action.result.data);
    			},
    			failure: function(f, action){
    				ExtCommon.errorMsg(action.result.data);
    			}
	    	});
	    },
		
		onClearSettings: function(ths){
			var form = ths.up('form'),
				grid = form.down('grid'),
				sel = grid.selModel;
			
			if((sel.getSelection()).length == 0)
				return;
				
			Ext.Msg.show({
				msg: 'Do you want to clear selection?',
				title: 'Confirm',
				icon:Ext.Msg.QUESTION,
				buttons: Ext.Msg.OKCANCEL,
				fn: function(btn){
					if(btn == 'ok')
						sel.deselectAll();
				}
			});
		},
		
		onShowSettings: function(ths){
			ths.setTitle('User Access - ' + ths._data.value);
		}
	});
</script>