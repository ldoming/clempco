<div id="ext-view"></div>

<script type="text/javascript">
Ext.define('LOAN.view.UserTypedefs', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.usertypedefs',
    store: 'UserTypedefs',
    cls: 'span12',
    renderTo: 'ext-view',
    height: 500,

    initComponent: function() {

        this.columns = [{
            text: '#',
            xtype: 'rownumberer',
            width: 35,
            align: 'center'
        }, {
            text: 'User Type',
            dataIndex: 'value',
            width: 200
        }, {
            xtype: 'actioncolumn',
            text: 'Options',
            flex: 1,
            items: [{
                icon: ExtCommon.baseUrl + 'includes/images/png/Lock.png',
                tooltip: 'View Access',
                handler: this.onAccessClick
            }]
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true,
            store: Ext.getStore('Users')
        }];

        this.callParent(arguments);
    },

    onAccessClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        new LOAN.view.UserModulesForm({
            _data: data
        }).show();
    }
});
</script>
