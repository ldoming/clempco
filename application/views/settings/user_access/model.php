<script type="text/javascript">

	Ext.define('LOAN.model.UserTypedef', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'value',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.UserTypedefs', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.UserTypedef',
		storeId: 'UserTypedefs',
		queryMode : 'remote',
		autoLoad: true,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'common/read3',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

</script>