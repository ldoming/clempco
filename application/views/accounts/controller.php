<script type="text/javascript">
	Ext.define('LOAN.controller.Accounts', {
	    extend: 'Ext.app.Controller',

	    init: function() {
	        this.control({
	        	'accountinfo button[text=New Account]': {
	        		click: this.onNewAccountClick
	        	},
	        	'accountinfo button[text=New Account]': {
	        		click: this.onNewAccountClick
	        	},
	        	'addaccount button[text=Save Account]': {
	        		click: this.onSaveAccountClick
	        	},
	        	'addaccount acctgroups': {
	        		select: this.onAcctSelect
	        	},
	        	'addaccount accountlevel': {
	        		select: this.onAcctLevelSelect
	        	},
	        	'addaccount textfield[name=acct_code]': {
	        		render: this.onPhoneFieldRender
	        	}
	        });
	    },

	    views : [
	    	'Accounts',
	    	'AddAccount',
	    	'ObType',
	    	'AccountLevel'
	    ],

	    models : [
	    	'Accounts'
	    ],

	    stores : [
	    	'Accounts',
	    	'ObType',
	    	'AccountLevel',
	    	'AcctGroups1'
	    ],

	    onNewAccountClick: function(ths){
	    	new LOAN.view.AddAccount().show();
	    },

	    onSaveAccountClick: function(ths){
	    	var form = ths.up('form');

	    	if(!form.getForm().isValid())
	    		return;

	    	form.getForm().submit({
	    		url: ExtCommon.baseUrl + 'accounts/create',
	    		waitMsg: 'Please wait...',
	    		success: function(f, action){
	    			if(action.result.success){
	    				ExtCommon.successMsg(action.result.data);
	    				form.getForm().reset();
	    			}
	    			else{
	    				ExtCommon.errorMsg(action.result.data);
	    			}
	    		},
	    		failure: function(f, action){
	    			ExtCommon.errorMsg(action.result.data);
	    		}
	    	});
	    },

	    onAcctSelect: function(ths, record){
	    	// Ext.Ajax.request({
	    	// 	url: ExtCommon.baseUrl + 'accounts/get_code',
	    	// 	method: 'post',
	    	// 	params: {
	    	// 		id: record[0].get('id')
	    	// 	},
	    	// 	success: function(resp){
	    	// 		var data = Ext.decode(resp.responseText);
	    	// 		if(data.success && data.code){
	    	// 			ExtCommon.getCmp('textfield[name=acct_code]').setValue(data.code);
	    	// 		}
	    	// 	}
	    	// });

			// console.log(record);
			var member = ExtCommon.getCmp('addaccount members').getRawValue();

			ExtCommon.getCmp('[name=acct_name]').setValue(((record[0].data.abbr.length > 0 ? record[0].data.abbr + ' - ' : '') + member).toUpperCase());
	    },

	    onAcctLevelSelect: function(ths, records){
	    	var val = ths.getValue();

	    	ExtCommon.getCmp('[name=acct_group]').reset();
	    	ExtCommon.getCmp('[name=acct_code]').reset();

	    	if(val == 1){
	    		//head account
	    		ExtCommon.getCmp('[name=acct_group]').setFieldLabel('Account Name');
	    		ExtCommon.getCmp('[name=acct_group]').bindStore('AcctGroups');
	    		ExtCommon.getCmp('[name=acct_code]').show();
	    		ExtCommon.getCmp('[name=acct_code]').allowBlank = false;
	    		ExtCommon.getCmp('[name=acct_name]').hide();
	    		ExtCommon.getCmp('[name=acct_name]').allowBlank = true;
	    	}else{
	    		//sub account
	    		ExtCommon.getCmp('[name=acct_group]').setFieldLabel('Account Group');
	    		ExtCommon.getCmp('[name=acct_group]').bindStore('AcctGroups1');
	    		ExtCommon.getCmp('[name=acct_code]').hide();
	    		ExtCommon.getCmp('[name=acct_code]').allowBlank = true;
	    		ExtCommon.getCmp('[name=acct_name]').show();
	    		ExtCommon.getCmp('[name=acct_name]').allowBlank = false;
	    	}

	    	ExtCommon.getCmp('[name=acct_group]').store.load();
	    },

	    onPhoneFieldRender: function (ths) {
	        if($){
	            $(function($){
	            	$('input[name="acct_code"]').mask(ths.numberFormat);
	            });
	        }
	    }

	});
</script>