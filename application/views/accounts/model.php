<script type="text/javascript">

	Ext.define('LOAN.model.Accounts', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'member_id',
			type : 'int'
		}, {
			name : 'acct_code',
			type : 'string'
		}, {
			name : 'acct_name',
			type : 'string'
		}, {
			name : 'acct_group',
			type : 'string'
		}, {
			name : 'ob_type',
			type : 'string'
		}, {
			name : 'ob_amount',
			type : 'string'
		}, {
			name : 'details',
			type : 'string'
		}, {
			name : 'acct_period',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.Accounts', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.Accounts',
		storeId: 'Accounts',
		queryMode : 'remote',
		autoLoad: true,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'accounts/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

	Ext.define('LOAN.store.ObType', {
		extend : 'Ext.data.Store',
		fields: ['type'],
		queryMode : 'local',
		data: [{
			type: 'Debit'
		}, {
			type: 'Credit'
		}]
	});

	Ext.define('LOAN.store.AccountLevel', {
		extend : 'Ext.data.Store',
		fields: ['level', 'id'],
		queryMode : 'local',
		data: [{
			id: 1,
			level: 'Head',
		}, {
			id: 2,
			level: 'Sub-account'
		}]
	});

	Ext.define('LOAN.store.AcctGroups1', {
	    model: 'LOAN.model.AcctGroup',
	    extend: 'Ext.data.Store',
	    storeId: 'AcctGroups1',
	    queryMode: 'remote',
	    autoLoad: true,
	    proxy: {
	        type: 'ajax',
	        url: ExtCommon.baseUrl + 'accounts/read2',
	        reader: {
	            type: 'json',
	            root: 'data'
	        }
	    }
	});

</script>