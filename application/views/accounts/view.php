<div id="ext-view"></div>

<script type="text/javascript">
Ext.define('LOAN.view.Accounts', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.accountinfo',
    store: 'Accounts',
    cls: 'span12',
    renderTo: 'ext-view',
    height: 500,

    initComponent: function() {

        this.tbar = [{
            xtype: 'searchfield',
            store: Ext.getStore('Accounts'),
            emptyText: 'Search',
            fieldLabel: 'Search',
            labelWidth: 60
        }, {
            xtype: 'button',
            text: 'New Account'
        }];

        this.columns = [{
            text: 'Acct Period',
            dataIndex: 'acct_period',
            width: 100
        }, {
            text: 'Acct Code',
            dataIndex: 'acct_code',
            width: 120
        }, {
            text: 'Acct Name',
            dataIndex: 'acct_name',
            width: 250
        }, {
            xtype: 'gridcolumn',
            text: 'Opening Balance',
            columns: [{
                text: 'Type',
                dataIndex: 'ob_type'
            }, {
                text: 'Amount',
                dataIndex: 'ob_amount',
                renderer: this.moneyRenderer
            }]
        }, {
            text: 'Details',
            dataIndex: 'details',
            width: 300
        }, {
            xtype: 'actioncolumn',
            text: 'Options',
            items: [{
                icon: ExtCommon.baseUrl + 'includes/images/png/Xion.png',
                tooltip: 'Delete',
                handler: this.onDeleteClick
            }]
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true,
            store: Ext.getStore('Accounts'),
            items: [{
                xtype: 'button',
                text: 'Export'
            }]
        }];

        this.callParent(arguments);
    },

    onDeleteClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        Ext.Msg.show({
            msg: 'Delete this record?',
            title: 'Confirm',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn) {
                if (btn == 'ok') {
                    Ext.widget('form').getForm().submit({
                        url: ExtCommon.baseUrl + 'accounts/destroy',
                        params: {
                            account_id: data.id
                        },
                        waitMsg: 'Please wait...',
                        success: function(f, action) {
                            if (action.result.success)
                                ExtCommon.successMsg(action.result.data);
                            else
                                ExtCommon.errorMsg(action.result.data);

                            grid.getStore().loadPage(1);
                        },
                        failure: function(f, action) {
                            ExtCommon.errorMsg(action.result.data);
                        }
                    });
                }
            }
        })
    },

    moneyRenderer: function(value){
        return parseFloat(value).toFixed(2);
    }
});

Ext.define('LOAN.view.AddAccount', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addaccount',
    floating: true,
    width: 400,
    height: 450,
    modal: true,
    closable: true,
    title: 'Add Account',
    layout: 'anchor',
    defaults: {
        allowBlank: false,
        anchor: '100%',
        xtype: 'textfield',
        labelWidth: 150
    },
    bodyPadding: 10,
    border: false,

    initComponent: function() {

        this.items = [{
            xtype: 'members',
            fieldLabel: 'Select Member (optional)',
            allowBlank: true,
            editable: false
        }, {
            fieldLabel: 'Account Type',
            name: 'acct_level',
            xtype: 'accountlevel',
            editable: false
        }, {
            fieldLabel: 'Account Group',
            name: 'acct_group',
            xtype: 'acctgroups',
            editable: false
        }, {
            fieldLabel: 'Account Code',
            name: 'acct_code',
            numberFormat: '99.99.99.99.99',
            hidden: true
        }, {
            fieldLabel: 'Account Name',
            name: 'acct_name',
            hidden: true
        }, {
            xtype: 'fieldset',
            title: 'Opening Balance',
            anchor: '100%',
            layout: 'fit',
            defaults: {
                labelWidth: 150
            },
            items: [{
                layout: 'column',
                border: false,
                items: [{
                    fieldLabel: 'Period',
                    columnWidth: .80,
                    xtype: 'common.periodmonths',
                    name: 'period_month',
                    allowBlank: false,
                    labelWidth: 150,
                    editable: false
                }, {
                    xtype: 'common.periodyears',
                    style: {
                        'margin': '4px 0 0 5px !important'
                    },
                    columnWidth: .2,
                    name: 'period_year',
                    allowBlank: false,
                    editable: false
                }]
            }, {
                xtype: 'obtype',
                fieldLabel: 'Type',
                name: 'ob_type',
                editable: false
            }, {
                xtype: 'numberfield',
                fieldLabel: 'Amount',
                name: 'ob_amount'
            }]
        }, {
            fieldLabel: 'Details',
            xtype: 'textareafield',
            name: 'details',
            allowBlank: true,
            style: {
                'margin-top': '5px'
            }
        }];

        this.buttons = [{
            text: 'Save Account'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.ObType', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.obtype',
    displayField: 'type',
    valueField: 'type',
    store: 'ObType'
});

Ext.define('LOAN.view.AccountLevel', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.accountlevel',
    displayField: 'level',
    valueField: 'id',
    store: 'AccountLevel'
});

</script>
