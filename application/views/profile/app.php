<script type="text/javascript" src="<?php
echo base_url(); ?>includes/js/SearchField.js"></script>
<script type="text/javascript">
	Ext.application({

		requires : ['Ext.container.Viewport', 'Ext.form.SearchField'],

		name : 'LOAN',

		controllers : ['Common'],

		launch : function() {
			var session = <?php
echo json_encode(get_session()); ?>;

			if(session['access_type'] == 'Member'){
				var member = new LOAN.view.common.AddMember({
					floating: false,
					renderTo: 'ext-view',
					header: false,
					closable: false,
					height: 800,
					fieldDefaults:{
						height: 35
					},
					autoScroll: false,
					listeners: {
						afterrender: function(){
							var me = this;

							me.getForm().findField('password').allowBlank = true;
							me.getForm().findField('confirm').allowBlank = true;
							me.getForm().findField('change_password').hide();
							me.down('button[text=Save]').setText('Update');
							me.down('button[text=Reset]').hide();

							this.load({
								url: ExtCommon.baseUrl + 'members/edit',
								waitMsg: 'Loading...',
								success: function(f, action){
									ExtCommon.getCmp('photoupload').down('image').setSrc(action.result.data.picture);
									
									for(key in action.result.data){
										var el = ExtCommon.getCmp('[name='+key+']');
										try{
											if(typeof el != 'undefined')
												el.setValue(action.result.data[key]);
										}
										catch(e){
										}
									}

								}
							});
						}
					}
				});
		        member.show();
		        member._action = 'update';
			}else {
				var user = new LOAN.view.common.AddUser({
					floating: false,
					renderTo: 'ext-view',
					header: false,
					closable: false,
					height: 600,
					fieldDefaults:{
						height: 35,
						margin: '5 0 0 0'
					},
					autoScroll: false,
					listeners: {
						afterrender: function(){
							var me = this;

							me.getForm().findField('password').allowBlank = true;
							me.getForm().findField('confirm').allowBlank = true;
							me.getForm().findField('change_password').hide();
							me.getForm().findField('user_typedef').hide();
							me.getForm().findField('user_typedef').allowBlank = true;
							me.down('button[text=Save]').setText('Update');
							me.down('button[text=Reset]').hide();

							this.load({
								url: ExtCommon.baseUrl + 'user_info/edit',
								waitMsg: 'Loading...',
								success: function(f, action){
									ExtCommon.getCmp('photoupload').down('image').setSrc(action.result.data.picture);

									for(key in action.result.data){
										var el = ExtCommon.getCmp('[name='+key+']');
										try{
											if(typeof el != 'undefined')
												el.setValue(action.result.data[key]);
										}
										catch(e){
										}
									}

								}
							});
						}
					}
				});
		        user.show();
		        user._action = 'update';
			}
			
		}
	}); 
</script>