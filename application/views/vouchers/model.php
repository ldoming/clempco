<script type="text/javascript">

	Ext.define('LOAN.model.Vouchers', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'voucher_type',
			type : 'string'
		}, {
			name : 'amount',
			type : 'float'
		}, {
			name : 'remarks',
			type : 'string'
		}, {
			name :  'total_debit',
			type : 'float'
		}, {
			name :  'total_credit',
			type : 'float'
		}, {
			name : 'voucher_date',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.Vouchers', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.Vouchers',
		storeId: 'Vouchers',
		queryMode : 'remote',
		autoLoad: true,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'vouchers/read',
			extraParams: {
				vtype: '<?php echo $vtype;?>'
			},
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

	Ext.define('LOAN.store.ObType', {
		extend : 'Ext.data.Store',
		fields: ['type'],
		queryMode : 'local',
		data: [{
			type: 'Debit'
		}, {
			type: 'Credit'
		}]
	});

	Ext.define('LOAN.model.CVParticular', {
		extend : 'Ext.data.Model',
		fields : [{
			name: 'acct_id',
			type: 'string'
		}, {
			name: 'acct_code',
			type: 'string'
		}, {
			name : 'description',
			type : 'string'
		}, {
			name : 'debit',
			type : 'float'
		}, {
			name : 'credit',
			type : 'float'
		}]
	});

	Ext.define('LOAN.store.CVParticulars', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.CVParticular',
		storeId: 'CVParticulars',
		queryMode : 'local'
	});

	Ext.define('LOAN.model.JVParticular', {
		extend : 'Ext.data.Model',
		fields : [{
			name: 'acct_id',
			type: 'string'
		}, {
			name: 'acct_code',
			type: 'string'
		}, {
			name : 'description',
			type : 'string'
		}, {
			name : 'debit',
			type : 'float'
		}, {
			name : 'credit',
			type : 'float'
		}]
	});

	Ext.define('LOAN.store.JVParticulars', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.JVParticular',
		storeId: 'JVParticulars',
		queryMode : 'local'
	});

	Ext.define('LOAN.model.PCVParticular', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'description',
			type : 'string'
		}, {
			name : 'amount',
			type : 'float'
		}]
	});

	Ext.define('LOAN.store.PCVParticulars', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.PCVParticular',
		storeId: 'PCVParticulars',
		queryMode : 'local'
	});

	Ext.define('LOAN.model.VoucherType', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'value',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.VoucherTypes', {
	    model: 'LOAN.model.VoucherType',
	    extend: 'Ext.data.Store',
	    queryMode: 'remote',
	    proxy: {
	        type: 'ajax',
	        url: ExtCommon.baseUrl + 'common/read',
	        extraParams: {
	            type: 'Vouchers'
	        },
	        reader: {
	            type: 'json',
	            root: 'data'
	        }
	    }
	});

	Ext.define('LOAN.model.Particular', {
		extend : 'Ext.data.Model',
		fields : [{
			name: 'id',
			type: 'int'
		}, {
			name: 'description',
			type: 'string'
		}, {
			name : 'debit',
			type : 'float'
		}, {
			name : 'credit',
			type : 'float'
		}, {
			name : 'amount',
			type : 'float'
		}]
	});

	Ext.define('LOAN.store.Particulars', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.Particular',
		storeId: 'Particulars',
		queryMode: 'remote',
	    proxy: {
	        type: 'ajax',
	        url: ExtCommon.baseUrl + 'particulars/read',
	        reader: {
	            type: 'json',
	            root: 'data'
	        }
	    }
	});

</script>