<script type="text/javascript">
Ext.define('LOAN.controller.Vouchers', {
    extend: 'Ext.app.Controller',

    init: function() {
        this.control({
            'voucherinfo': {
                afterrender: this.onVoucherRender,
                itemclick: this.onVoucherRowClick
            },
            'voucherinfo button[text=New <?php echo $vtype;?>]': {
                click: this.onNewVoucherClick
            },
            'button[text=Save Voucher]': {
                click: this.onSaveVoucherClick
            },
            '[name=cvaccounts]': {
                // keypress: this.onAddParticular,
                select: this.onSelectAcct
            },
            'cvparticulars textfield[name=cvdescription]': {
                // keypress: this.onAddParticular
            },
            'cvparticulars numberfield[name=cvdebit]': {
                // keypress: this.onAddParticular
            },
            'cvparticulars numberfield[name=cvcredit]': {
                // keypress: this.onAddParticular
            },
            'cvparticulars button[action=cvadd]': {
                click: this.onAddParticular
            },
            '[name=jvaccounts]': {
                // keypress: this.onAddParticular1,
                select: this.onSelectAcct1
            },
            'jvparticulars textfield[name=jvdescription]': {
                // keypress: this.onAddParticular1
            },
            'jvparticulars numberfield[name=jvdebit]': {
                // keypress: this.onAddParticular1
            },
            'jvparticulars numberfield[name=jvcredit]': {
                // keypress: this.onAddParticular1
            },
            'jvparticulars button[action=jvadd]': {
                click: this.onAddParticular1
            },
            'pcvparticulars textfield[name=pcvdescription]': {
                // keypress: this.onAddParticular2
            },
            'pcvparticulars numberfield[name=pcvamount]': {
                // keypress: this.onAddParticular2
            },
            'pcvparticulars button[action=pcvadd]': {
                click: this.onAddParticular2
            },
            '[name=acct_id]': {
                select: this.onAcctCodeSelect
            },
            'particulars': {
                afterrender: this.onParticularRender
            },
            'addpettycashvoucher': {
                show: this.onShowAddVoucher
            },
            'addjournalvoucher': {
                show: this.onShowAddVoucher
            },
            'addcashvoucher': {
                show: this.onShowAddVoucher
            }
        });
    },

    views: [
        'Vouchers',
        'AddCashVoucher',
        'AddJournalVoucher',
        'ObType',
        'CVParticulars',
        'JVParticulars',
        'PCVParticulars',
        'VoucherTypes',
        'Particulars',
        'WParticulars',
        'AddPettyCashVoucher'
    ],

    models: [
        'Vouchers',
        'CVParticular',
        'JVParticular',
        'PCVParticular',
        'VoucherType',
        'Particular'
    ],

    stores: [
        'Vouchers',
        'ObType',
        'CVParticulars',
        'JVParticulars',
        'PCVParticulars',
        'VoucherTypes',
        'Particulars'
    ],

    onSelectAcct: function(ths, records){
        var acct_name = records[0].get('acct_name');
        ExtCommon.getCmp('textfield[name=cvid]').setValue(records[0].get('id'));
        ExtCommon.getCmp('textfield[name=cvdescription]').setValue(acct_name);
    },

    onSelectAcct1: function(ths, records){
        var acct_name = records[0].get('acct_name');
        ExtCommon.getCmp('textfield[name=jvid]').setValue(records[0].get('id'));
        ExtCommon.getCmp('textfield[name=jvdescription]').setValue(acct_name);
    },

    onNewVoucherClick: function(ths) {
        var vtype = '<?php echo $vtype;?>';

        switch(vtype){
            case 'Cash Voucher':
                new LOAN.view.AddCashVoucher().show();
                break;
            case 'Journal Voucher':
                new LOAN.view.AddJournalVoucher().show();
                break;
            default:
                new LOAN.view.AddPettyCashVoucher().show();
                break;
        }
    },

    onSaveVoucherClick: function(ths) {
        var form = ths.up('form'),
        	vtype = '<?php echo $vtype;?>',
            xtype = '',
            me = this;

        if (!form.getForm().isValid())
            return;

        switch(vtype){
            case 'Cash Voucher':
                xtype = 'cvparticulars';
                break;
            case 'Journal Voucher':
                xtype = 'jvparticulars';
                break;
            default:
                xtype = 'pcvparticulars';
                break;
        }

        var particulars = ExtCommon.jsonData(ExtCommon.getCmp(xtype).getStore());
        if(particulars.length == 0){
        	ExtCommon.errorMsg('You must add particulars which is required for this type of voucher!');
        	return;
        }

        form.getForm().submit({
            url: ExtCommon.baseUrl + 'vouchers/create',
            waitMsg: 'Please wait...',
            params: {
                voucher_type: vtype,
                particulars: Ext.encode(particulars)
            },
            success: function(f, action) {
                if (action.result.success) {
                    ExtCommon.successMsg(action.result.data);
                    form.getForm().reset();
                    ExtCommon.getCmp(xtype).store.removeAll();
                } else {
                    ExtCommon.errorMsg(action.result.data);
                }

                ExtCommon.getCmp('voucherinfo').store.loadPage(1);
                me.onShowAddVoucher(form);
            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    },

    onAddParticular: function(ths, e) {
        var acct_id = ExtCommon.getCmp('[name=cvid]'),
            acct = ExtCommon.getCmp('[name=cvaccounts]'),
            desc = ExtCommon.getCmp('textfield[name=cvdescription]'),
            debit = ExtCommon.getCmp('numberfield[name=cvdebit]'),
            credit = ExtCommon.getCmp('numberfield[name=cvcredit]');

        if (e.keyCode && e.keyCode != 13)
            return;

        if (acct.getValue()== null || desc.getValue().trim().length == 0 || (debit.getValue() == null && credit.getValue() == null)) {
            ExtCommon.errorMsg('Account, Description, Debit, and Credit are both required fields!');
            return;
        }

        var row = {
            acct_id: acct_id.getValue(),
            acct_code: acct.getValue(),
            description: desc.getValue(),
            debit: debit.getValue() == null ? 0 : debit.getValue(),
            credit: credit.getValue() == null ? 0 : credit.getValue()
        };

        ths.up('grid').getStore().add(row);
        
        acct.reset();
        desc.reset();
        debit.reset();
        credit.reset();

        desc.focus();
    },

    onAddParticular1: function(ths, e) {
        var acct_id = ExtCommon.getCmp('[name=jvid]'),
            acct = ExtCommon.getCmp('[name=jvaccounts]'),
        	desc = ExtCommon.getCmp('textfield[name=jvdescription]'),
            debit = ExtCommon.getCmp('numberfield[name=jvdebit]'),
            credit = ExtCommon.getCmp('numberfield[name=jvcredit]');

        if (e.keyCode && e.keyCode != 13)
            return;

        if (acct.getValue()== null || desc.getValue().trim().length == 0 || (debit.getValue() == null && credit.getValue() == null)) {
            ExtCommon.errorMsg('Account, Description, Debit, and Credit are both required fields!');
            return;
        }

        var row = {
            acct_id: acct_id.getValue(),
        	acct_code: acct.getValue(),
            description: desc.getValue(),
            debit: debit.getValue() == null ? 0 : debit.getValue(),
            credit: credit.getValue() == null ? 0 : credit.getValue()
        };

        ths.up('grid').getStore().add(row);
        
        acct.reset();
        desc.reset();
        debit.reset();
        credit.reset();

        desc.focus();
    },


    onAddParticular2: function(ths, e) {
        var desc = ExtCommon.getCmp('textfield[name=pcvdescription]'),
            amt = ExtCommon.getCmp('numberfield[name=pcvamount]');

        if (e.keyCode && e.keyCode != 13)
            return;

        if (desc.getValue().trim().length == 0 || amt.getValue() == null) {
            ExtCommon.errorMsg('Description and Amount are both required fields!');
            return;
        }

        var row = {
            description: desc.getValue(),
            amount: amt.getValue()
        };

        ths.up('grid').getStore().add(row);
        desc.reset();
        amt.reset();
        desc.focus();
    },

    onAcctCodeSelect: function(ths, rec){
        var store = ths.getStore(),
            record = rec[0];

        ExtCommon.getCmp('textfield[fieldLabel=Account Name]').setValue(record.get('acct_name'));
    },

    onVoucherRender: function (ths) {
        var vtype = '<?php echo $vtype;?>';

        if(vtype == 'Journal Voucher' || vtype == 'Cash Voucher'){
            ths.columns[2].hide();
            ths.columns[3].show();
            ths.columns[4].show();
        }else{
            ths.columns[2].show();
            ths.columns[3].hide();
            ths.columns[4].hide();
        }

    },


    onVoucherRowClick: function(ths, rec, item, i, e) {
        var menu = new Ext.widget('gridmenu', {
            record: rec,
            items: [{
                text: 'Particulars',
                handler: this.onViewParticulars
            }, {
                text: 'Export',
                handler: this.onVoucherExport
            }, {
                text: 'Delete',
                handler: this.onVoucherDelete
            }],
            _e: {
                e: e,
                grid: ths
            }
        });

        ExtCommon.showAt(menu, e, ths);
    },

    onViewParticulars: function(ths){
        var data = ths.up('menu').record,
            particulars = new LOAN.view.WParticulars(),
            grid = particulars.down('grid');

        grid.store.proxy.extraParams['voucher_id'] = data.get('id');

        particulars.show();
        grid.store.loadPage(1);
    },

    onParticularRender: function(ths){
        var vtype = '<?php echo $vtype;?>';

        if(vtype == 'Petty Cash Voucher'){
            ths.columns[2].hide();
            ths.columns[3].hide();
            ths.columns[4].show();
        }else{
            ths.columns[2].show();
            ths.columns[3].show();
            ths.columns[4].hide();
        }
    },

    onVoucherDelete: function(ths){
        data = ths.up('menu').record.data;

        Ext.Msg.show({
            msg: 'Delete this record?',
            title: 'Confirm',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn) {
                if (btn == 'ok') {
                    Ext.widget('form').getForm().submit({
                        url: ExtCommon.baseUrl + 'vouchers/destroy',
                        params: {
                            voucher_id: data.id
                        },
                        waitMsg: 'Please wait...',
                        success: function(f, action) {
                            if (action.result.success)
                                ExtCommon.successMsg(action.result.data);
                            else
                                ExtCommon.errorMsg(action.result.data);

                            ExtCommon.getCmp('voucherinfo').getStore().loadPage(1);
                        },
                        failure: function(f, action) {
                            ExtCommon.errorMsg(action.result.data);
                        }
                    });
                }
            }
        });
    },

    onVoucherExport: function(ths){
        var data = ths.up('menu').record;

        Ext.create('widget.form').getForm().submit({
            url: ExtCommon.baseUrl + 'vouchers/export',
            timeout: 30000,
            waitMsg: 'Preparing your download. Please wait!',
            title: 'Export',
            params: {
                voucher_id: data.get('id')
            },
            success: function(f, action) {
                if (action.result.success) {
                    if (action.result.download) {
                        Ext.Msg.show({
                            title: 'Status',
                            msg: 'Your download is ready!',
                            buttonText: {
                                ok: "Download",
                                cancel: "Cancel"
                            },
                            fn: function(btn) {
                                if (btn == 'ok')
                                    window.location.href = action.result.download;
                            }
                        });
                    }
                }

            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    },

    onShowAddVoucher: function(component){
        Ext.create('Ext.form.Panel').getForm().submit({
            url: ExtCommon.baseUrl + 'vouchers/get_vno',
            params: {
                code: component.code
            },
            success: function(f, action){
                if(action.result.success)
                    ExtCommon.getCmp('textfield[fieldLabel=Voucher #]').setValue(action.result.data);
            }
        });
    }

});
</script>
