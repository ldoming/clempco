<div id="ext-view"></div>

<script type="text/javascript">
Ext.define('LOAN.view.Vouchers', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.voucherinfo',
    store: 'Vouchers',
    cls: 'span12',
    renderTo: 'ext-view',
    height: 500,

    initComponent: function() {

        this.tbar = [{
            xtype: 'searchfield',
            fieldLabel: 'Search',
            labelWidth: 60,
            store: Ext.getStore('Vouchers')
        }, {
            xtype: 'button',
            text: 'New <?php echo $vtype;?>'
        }];

        this.columns = [{
            text: 'Voucher',
            dataIndex: 'voucher_type',
            width: 150
        }, {
            text: 'Period',
            dataIndex: 'voucher_date',
            width: 100
        }, {
            text: 'Amount',
            dataIndex: 'amount',
            width: 150
        }, {
            text: 'Total Debit',
            dataIndex: 'total_debit',
            width: 150
        }, {
            text: 'Total Credit',
            dataIndex: 'total_credit',
            width: 150
        }, {
            text: 'Remarks',
            dataIndex: 'remarks',
            flex: 1
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true,
            store: Ext.getStore('Vouchers'),
            items: [{
                xtype: 'button',
                text: 'Export'
            }]
        }];

        this.callParent(arguments);
    },

    onDeleteClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        
    }
});

Ext.define('LOAN.view.ObType', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.obtype',
    displayField: 'type',
    valueField: 'type',
    store: 'ObType'
});

Ext.define('LOAN.view.AddCashVoucher', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addcashvoucher',
    floating: true,
    width: 600,
    height: 550,
    modal: true,
    closable: true,
    title: 'Add Cash Voucher',
    layout: 'anchor',
    defaults: {
        allowBlank: false,
        anchor: '100%',
        xtype: 'textfield',
        labelWidth: 100
    },
    bodyPadding: 10,
    border: false,
    autoScroll: true,

    initComponent: function() {

        this.code = 'C';

        this.items = [{
            fieldLabel: 'Accounts',
            xtype: 'common.accounts',
            anchor: '60%',
            displayField: 'acct_name',
            valueField: 'id',
            name: 'acct_id'
        }, {
            fieldLabel: 'Voucher #',
            name: 'voucher_no',
            anchor: '60%',
            readOnly: true
        }, {
            fieldLabel: 'Voucher Date',
            name: 'voucher_date',
            anchor: '60%',
            xtype: 'datefield'
        }, {
            fieldLabel: 'Check #',
            name: 'check_no',
            allowBlank: true,
            anchor: '60%'
        }, {
            fieldLabel: 'Tin #',
            name: 'tin_no',
            allowBlank: true,
            anchor: '60%'
        }, {
            fieldLabel: 'Remarks',
            name: 'remarks',
            xtype: 'textareafield',
            allowBlank: true,
            style: {
                'margin-top': '5px'
            }
        }, {
            style: {
                'margin-top': '10px'
            },
            xtype: 'cvparticulars'
        }];

        this.buttons = [{
            text: 'Save Voucher'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.CVParticulars', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.cvparticulars',
    title: 'Particulars',
    store: 'CVParticulars',
    features: [{
        ftype: 'summary'
    }],

    initComponent: function(){
        this.tbar = [{
            xtype: 'textfield',
            name: 'cvid',
            hidden: true
        }, {
            xtype: 'common.accounts',
            name: 'cvaccounts',
            emptyText: 'Acct Code',
            enableKeyEvents: true,
            width: 100
        }, {
            xtype: 'textfield',
            name: 'cvdescription',
            emptyText: 'Description',
            enableKeyEvents: true,
            width: 160
        }, {
            xtype: 'numberfield',
            name: 'cvdebit',
            emptyText: 'Debit',
            enableKeyEvents: true,
            width: 100
        }, {
            xtype: 'numberfield',
            name: 'cvcredit',
            emptyText: 'Credit',
            enableKeyEvents: true,
            width: 100
        }, {
            xtype: 'button',
            text: 'Add',
            action: 'cvadd'
        }];

        this.columns = [{
            dataIndex: 'acct_id',
            hidden: true
        }, {
            text: 'Acct Code',
            dataIndex: 'acct_code',
            width: 115
        }, {
            text: 'Description',
            dataIndex: 'description',
            width: 165
        }, {
            text: 'Debit',
            dataIndex: 'debit',
            summaryType: 'sum',
            summaryRenderer: this.onDebitSum,
            width: 100
        }, {
            text: 'Credit',
            dataIndex: 'credit',
            summaryType: 'sum',
            summaryRenderer: this.onCreditSum,
            width: 100
        }];

        this.bbar = ['->', {
            xtype: 'textfield',
            fieldLabel: 'Total',
            labelWidth: 50, 
            width: 150,
            name: 't_debit'
        }, {
            xtype: 'textfield',
            labelWidth: 120, 
            width: 100,
            name: 't_credit',
            style: {
                'margin-right': '80px !important'
            }
        }];

        this.callParent(arguments);
    },

    onDebitSum: function(value){
        ExtCommon.getCmp('textfield[name=t_debit]').setValue(value);
    },

    onCreditSum: function(value){
        ExtCommon.getCmp('textfield[name=t_credit]').setValue(value);
    }
});

Ext.define('LOAN.view.AddJournalVoucher', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addjournalvoucher',
    floating: true,
    width: 650,
    height: 450,
    modal: true,
    closable: true,
    title: 'Add Journal Voucher',
    layout: 'anchor',
    defaults: {
        allowBlank: false,
        anchor: '100%',
        xtype: 'textfield',
        labelWidth: 100
    },
    bodyPadding: 10,
    border: false,
    autoScroll: true,

    initComponent: function() {

        this.code = 'J';

        this.items = [{
            fieldLabel: 'Voucher #',
            name: 'voucher_no',
            anchor: '60%',
            readOnly: true
        }, {
            fieldLabel: 'Voucher Date',
            name: 'voucher_date',
            anchor: '60%',
            xtype: 'datefield'
        }, {
            fieldLabel: 'Remarks',
            name: 'remarks',
            xtype: 'textareafield',
            allowBlank: true,
            style: {
                'margin-top': '5px'
            }
        }, {
            style: {
                'margin-top': '10px'
            },
            xtype: 'jvparticulars'
        }];

        this.buttons = [{
            text: 'Save Voucher'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.JVParticulars', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.jvparticulars',
    title: 'Particulars',
    store: 'JVParticulars',
    features: [{
        ftype: 'summary'
    }],

    initComponent: function(){
        this.tbar = [{
            xtype: 'textfield',
            name: 'jvid',
            hidden: true
        }, {
            xtype: 'common.accounts',
            name: 'jvaccounts',
            emptyText: 'Acct Code',
            enableKeyEvents: true,
            width: 160
        }, {
            xtype: 'textfield',
            name: 'jvdescription',
            emptyText: 'Description',
            enableKeyEvents: true,
            width: 160
        }, {
            xtype: 'numberfield',
            name: 'jvdebit',
            emptyText: 'Debit',
            enableKeyEvents: true,
            width: 100
        }, {
            xtype: 'numberfield',
            name: 'jvcredit',
            emptyText: 'Credit',
            enableKeyEvents: true,
            width: 100
        }, {
            xtype: 'button',
            text: 'Add',
            action: 'jvadd'
        }];

        this.columns = [{
            dataIndex: 'acct_id',
            hidden: true
        }, {
            text: 'Acct Code',
            dataIndex: 'acct_code',
            flex: 1
        }, {
            text: 'Description',
            dataIndex: 'description',
            flex: 1
        }, {
            text: 'Debit',
            dataIndex: 'debit',
            summaryType: 'sum',
            summaryRenderer: this.onDebitSum,
            width: 100
        }, {
            text: 'Credit',
            dataIndex: 'credit',
            summaryType: 'sum',
            summaryRenderer: this.onCreditSum,
            width: 100
        }];

        this.bbar = ['->', {
            xtype: 'textfield',
            fieldLabel: 'Total',
            labelWidth: 50, 
            width: 150,
            name: 't_debit'
        }, {
            xtype: 'textfield',
            labelWidth: 120, 
            width: 100,
            name: 't_credit'
        }];

        this.callParent(arguments);
    },

    onDebitSum: function(value){
        ExtCommon.getCmp('textfield[name=t_debit]').setValue(value);
    },

    onCreditSum: function(value){
        ExtCommon.getCmp('textfield[name=t_credit]').setValue(value);
    }
});

Ext.define('LOAN.view.AddPettyCashVoucher', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addpettycashvoucher',
    floating: true,
    width: 650,
    height: 450,
    modal: true,
    closable: true,
    title: 'Add Petty Cash Voucher',
    layout: 'anchor',
    defaults: {
        allowBlank: false,
        anchor: '100%',
        xtype: 'textfield',
        labelWidth: 100
    },
    bodyPadding: 10,
    border: false,
    autoScroll: true,

    initComponent: function() {

        this.code = 'P';
        
        this.items = [{
            fieldLabel: 'Voucher #',
            name: 'voucher_no',
            anchor: '60%',
            readOnly: true
        }, {
            xtype: 'common.accounts',
            fieldLabel: 'Account Code',
            name: 'acct_id',
            anchor: '60%',
            valueField: 'id'
        }, {
            fieldLabel: 'Account Name',
            name: 'voucher_name',
            anchor: '60%',
            readOnly: true
        }, {
            fieldLabel: 'Voucher Date',
            name: 'voucher_date',
            anchor: '60%',
            xtype: 'datefield'
        }, {
            fieldLabel: 'Remarks',
            name: 'remarks',
            xtype: 'textareafield',
            allowBlank: true,
            style: {
                'margin-top': '5px'
            }
        }, {
            style: {
                'margin-top': '10px'
            },
            xtype: 'pcvparticulars'
        }];

        this.buttons = [{
            text: 'Save Voucher'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.PCVParticulars', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pcvparticulars',
    title: 'Particulars',
    store: 'PCVParticulars',
    features: [{
        ftype: 'summary'
    }],

    initComponent: function(){
        this.tbar = [{
            xtype: 'textfield',
            name: 'pcvdescription',
            emptyText: 'Description',
            enableKeyEvents: true,
            width: 400
        }, {
            xtype: 'numberfield',
            name: 'pcvamount',
            emptyText: 'Amount',
            enableKeyEvents: true,
            width: 140
        }, {
            xtype: 'button',
            text: 'Add',
            action: 'pcvadd'
        }];

        this.columns = [{
            text: 'Description',
            dataIndex: 'description',
            flex: 1
        }, {
            text: 'Amount',
            dataIndex: 'amount',
            summaryType: 'sum',
            summaryRenderer: this.onAmountSum,
            width: 200
        }];

        this.bbar = ['->', {
            xtype: 'textfield',
            fieldLabel: 'Total',
            labelWidth: 50, 
            width: 250,
            name: 'v_amount'
        }];

        this.callParent(arguments);
    },

    onAmountSum: function(value){
        ExtCommon.getCmp('textfield[name=v_amount]').setValue(value);
    }
});

Ext.define('LOAN.view.WParticulars', {
    extend: 'Ext.form.Panel',
    alias: 'widget.wparticulars',
    floating: true,
    width: 500,
    height: 300,
    modal: true,
    closable: true,
    title: 'Particulars',
    layout: 'fit',
    border: false,
    autoScroll: true,

    initComponent: function() {

        this.items = [{
            xtype: 'particulars'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.Particulars', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.particulars',
    store: 'Particulars',
    features: [{
        ftype: 'summary'
    }],
    border: false,

    initComponent: function(){

        this.columns = [{
            xtype: 'rownumberer',
            text: '#',
            width: 35,
            align: 'center'
        }, {
            text: 'Description',
            dataIndex: 'description',
            flex: 1
        }, {
            text: 'Debit',
            dataIndex: 'debit',
            summaryType: 'sum',
            width: 100
        }, {
            text: 'Credit',
            dataIndex: 'credit',
            summaryType: 'sum',
            width: 100
        }, {
            text: 'Amount',
            dataIndex: 'amount',
            summaryType: 'sum',
            width: 100
        }];

        this.callParent(arguments);
    },

    onDebitSum: function(value){
        ExtCommon.getCmp('textfield[name=t_debit]').setValue(value);
    },

    onCreditSum: function(value){
        ExtCommon.getCmp('textfield[name=t_credit]').setValue(value);
    }
});

Ext.define('LOAN.view.VoucherTypes', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.vouchertypes',
    displayField: 'value',
    valueField: 'value',
    store: 'VoucherTypes'
});

</script>
