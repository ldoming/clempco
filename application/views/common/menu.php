<nav class="vertical-menu">
	<ul>
		<li class="<?=$title == 'About' ? 'title' : ''; ?>">
			<?php if($title != 'About') {
			?>
			<a href="<?=base_url() . 'page/about'; ?>">About</a>
			<?php } else { ?>
			About
			<?php } ?>
		</li>
		<li class="<?=$title == 'Portfolio' ? 'title' : ''; ?>">
			<?php if($title != 'Portfolio') {
			?>
			<a href="<?=base_url() . 'page/portfolio'; ?>">Portfolio</a>
			<?php } else { ?>
			Portfolio
			<?php } ?>
		</li>
		<li class="<?=$title == 'Services' ? 'title' : ''; ?>">
			<?php if($title != 'Services') {
			?>
			<a href="<?=base_url() . 'page/services'; ?>">Services</a>
			<?php } else { ?>
			Services
			<?php } ?>
		</li>
		<li class="<?=$title == 'Contact' ? 'title' : ''; ?>">
			<?php if($title != 'Contact') {
			?>
			<a href="<?=base_url() . 'page/contact'; ?>">Contact</a>
			<?php } else { ?>
			Contact
			<?php } ?>
		</li>
		<li class="<?=$title == 'Blog' ? 'title' : ''; ?>">
			<?php if($title != 'Blog') {
			?>
			<a href="<?=base_url() . 'page/blog'; ?>">Blog</a>
			<?php } else { ?>
			Blog
			<?php } ?>
		</li>
	</ul>
</nav>