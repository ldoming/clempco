<script type="text/javascript">
Ext.define('LOAN.model.LoanVars', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'type',
        type: 'string'
    }, {
        name: 'subtype',
        type: 'string'
    }, {
        name: 'value',
        type: 'string'
    }]
});

Ext.define('LOAN.store.LoanTerms', {
    model: 'LOAN.model.LoanVars',
    extend: 'Ext.data.Store',
    queryMode: 'remote',
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'common/read',
        extraParams: {
            type: 'Loan Terms'
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('LOAN.store.UserType', {
    model: 'LOAN.model.LoanVars',
    extend: 'Ext.data.Store',
    queryMode: 'remote',
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'common/read',
        extraParams: {
            type: 'User Type'
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('LOAN.store.LoanStatus', {
    model: 'LOAN.model.LoanVars',
    extend: 'Ext.data.Store',
    queryMode: 'remote',
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'common/read',
        extraParams: {
            type: 'Loan Status'
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('LOAN.store.LoanType', {
    model: 'LOAN.model.LoanVars',
    extend: 'Ext.data.Store',
    queryMode: 'remote',
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'common/read',
        extraParams: {
            type: 'Loan Type'
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('LOAN.model.Position', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'position',
        type: 'string'
    }]
});

Ext.define('LOAN.store.Positions', {
    model: 'LOAN.model.Position',
    extend: 'Ext.data.Store',
    queryMode: 'remote',
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'common/read1',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('LOAN.model.Member', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'name',
        type: 'string'
    }]
});

Ext.define('LOAN.store.Members', {
    model: 'LOAN.model.Member',
    extend: 'Ext.data.Store',
    queryMode: 'remote',
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'members/read',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('LOAN.model.AcctGroup', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'group',
        type: 'string'
    }, {
        name: 'code',
        type: 'int'
    }, {
        name: 'abbr',
        type: 'string'
    }]
});

Ext.define('LOAN.store.AcctGroups', {
    model: 'LOAN.model.AcctGroup',
    extend: 'Ext.data.Store',
    storeId: 'AcctGroups',
    queryMode: 'remote',
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'common/read2',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('LOAN.model.common.Account', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'acct_code',
        type: 'string'
    }, {
        name: 'acct_name',
        type: 'string'
    }]
});

Ext.define('LOAN.store.common.Accounts', {
    model: 'LOAN.model.common.Account',
    extend: 'Ext.data.Store',
    queryMode: 'remote',
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'accounts/read',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('LOAN.model.common.MemberLoan', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'name',
        type: 'string'
    }, {
        name: 'status',
        type: 'string'
    }, {
        name: 'type',
        type: 'string'
    }, {
        name: 'principal',
        type: 'float'
    }]
});

Ext.define('LOAN.store.common.MemberLoans', {
    model: 'LOAN.model.common.MemberLoan',
    extend: 'Ext.data.Store',
    queryMode: 'remote',
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'loans/read3',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('LOAN.store.common.PeriodMonths', {
    fields: [{
        name: 'val',
        type: 'string'
    }, {
        name: 'month',
        type: 'string'
    }],
    extend: 'Ext.data.Store',
    queryMode: 'local',
    data: [{
        val: '01',
        month: 'January'
    }, {
        val: '02',
        month: 'February'
    }, {
        val: '03',
        month: 'March'
    }, {
        val: '04',
        month: 'April'
    }, {
        val: '05',
        month: 'May'
    }, {
        val: '06',
        month: 'June'
    }, {
        val: '07',
        month: 'July'
    }, {
        val: '08',
        month: 'August'
    }, {
        val: '09',
        month: 'September'
    }, {
        val: '10',
        month: 'October'
    }, {
        val: '11',
        month: 'November'
    }, {
        val: '12',
        month: 'December'
    }]
});

Ext.define('LOAN.store.common.PeriodYears', {
    extend: 'Ext.data.Store',
    queryMode: 'local',
    fields: [{
        name: 'year',
        type: 'string'
    }],
    data: [{
        year: 2014
    }]
});

Ext.define('LOAN.model.Module', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'group',
        type: 'string'
    }, {
        name: 'access',
        type: 'bool'
    }, {
        name: 'module'
    }]
});

Ext.define('LOAN.store.Modules', {
    model: 'LOAN.model.Module',
    extend: 'Ext.data.Store',
    queryMode: 'remote',
    groupField: 'group',
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'modules/read',
        reader: {
            type: 'json',
            root: 'data'
        }
    },
    listeners: {
        load: function() {
            var all = this.getRange(),
                grid = ExtCommon.getCmp('usermodules'),
                sm = grid.selModel;

            sm.deselectAll();
            for (r in all) {
                if (all[r].data.access == true) {
                    sm.select(all[r], true);
                }
            }
        }
    }
});

Ext.define('LOAN.model.LoanApp', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'name',
        type: 'string'
    }, {
        name: 'principal',
        type: 'float'
    }, {
        name: 'type',
        type: 'string'
    }, {
        name: 'interest',
        type: 'float'
    }, {
        name: 'period',
        type: 'int'
    }, {
        name: 'payable',
        type: 'string'
    }, {
        name: 'balance',
        type: 'float'
    }, {
        name: 'position',
        type: 'string'
    }, {
        name: 'status',
        type: 'string'
    }, {
        name: 'monthly',
        type: 'float'
    }, {
        name: 'total_payable',
        type: 'float'
    }, {
        name: 'editable',
        type: 'bool'
    }, {
        name: 'deductions',
        type: 'string'
    }, {
        name: 'created_at',
        type: 'string'
    }]
});

Ext.define('LOAN.store.LoanApp', {
    extend: 'Ext.data.Store',
    model: 'LOAN.model.LoanApp',
    storeId: 'LoanApp',
    queryMode: 'remote',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'loans/read',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('LOAN.store.LoanBalances', {
    extend: 'Ext.data.Store',
    model: 'LOAN.model.LoanApp',
    storeId: 'LoanBalances',
    queryMode: 'remote',
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'loans/read',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('LOAN.model.LoanPayment', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'pay_date',
        type: 'string'
    }, {
        name: 'payment',
        type: 'float'
    }, {
        name: 'payable',
        type: 'float'
    }, {
        name: 'principal',
        type: 'float'
    }, {
        name: 'interest',
        type: 'float'
    }, {
        name: 'running_balance',
        type: 'float'
    }, {
        name: 'ou',
        type: 'float'
    }, {
        name: 'remarks',
        type: 'string'
    }]
});

Ext.define('LOAN.store.LoanPayments', {
    extend: 'Ext.data.Store',
    model: 'LOAN.model.LoanPayment',
    storeId: 'LoanPayments',
    queryMode: 'remote',
    proxy: {
        type: 'ajax',
        url: ExtCommon.baseUrl + 'loan_payments/read',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
</script>
