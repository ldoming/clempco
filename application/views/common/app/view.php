<script type="text/javascript">
Ext.define('LOAN.view.common.LoanTerms', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.loanterms',
    displayField: 'subtype',
    valueField: 'value',
    store: 'LoanTerms'
});

Ext.define('LOAN.view.common.LoanStatus', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.loanstatus',
    displayField: 'value',
    valueField: 'value',
    store: 'LoanStatus'
});

Ext.define('LOAN.view.common.LoanType', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.loantype',
    displayField: 'value',
    valueField: 'value',
    store: 'LoanType'
});

Ext.define('LOAN.view.common.UserType', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.usertype',
    displayField: 'value',
    valueField: 'value',
    store: 'UserType'
});

Ext.define('LOAN.view.common.MemberLoans', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.memberloans',
    displayField: 'type',
    valueField: 'id',
    store: 'common.MemberLoans',
    tpl: '<tpl for="."><div class="x-boundlist-item" >{type}<br><small>{status} | {principal}</small></div></tpl>',
});

Ext.define('LOAN.view.common.AddMember', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addmember',
    floating: true,
    title: 'New Member',
    width: ExtCommon.windowSize('width') - 200,
    height: 750,
    closable: true,
    defaults: {
        bodyPadding: 20,
        border: false
    },
    resizable: true,
    draggable: true,
    border: false,
    modal: true,
    layout: 'border',
    _action: 'create',

    initComponent: function() {
        this.items = [{
            region: 'west',
            width: 250,
            layout: 'anchor',
            items: [{
                xtype: 'photoupload',
                border: false,
                height: 300
            }]
        }, {
            region: 'center',
            xtype: 'form',
            items: [{
                xtype: 'fieldset',
                title: 'User Information',
                defaults: {
                    allowBlank: true,
                    anchor: '100%',
                    xtype: 'textfield'
                },
                items: [{
                    name: 'file_id',
                    hidden: true
                }, {
                    name: 'id',
                    hidden: true
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        columnWidth: 0.25,
                        xtype: 'tbtext',
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        text: 'Firstname<span class=\'tb-required\'>*</span>'
                    }, {
                        text: 'Lastname<span class=\'tb-required\'>*</span>'
                    }, {
                        text: 'Position<span class=\'tb-required\'>*</span>',
                        columnWidth: 0.5
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        allowBlank: false,
                        anchor: '100%',
                        xtype: 'textfield',
                        columnWidth: 0.25,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        name: 'firstname'
                    }, {
                        name: 'lastname'
                    }, {
                        name: 'position',
                        columnWidth: 0.5
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        xtype: 'tbtext',
                        columnWidth: 0.25,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        text: 'Application Id<span class=\'tb-required\'>*</span>'
                    }, {
                        text: 'Date of Birth<span class=\'tb-required\'>*</span>'
                    }, {
                        text: 'Civil Status<span class=\'tb-required\'>*</span>',
                        columnWidth: 0.5
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        allowBlank: false,
                        anchor: '100%',
                        columnWidth: 0.25,
                        xtype: 'textfield',
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        name: 'application_id'
                    }, {
                        name: 'birth_date',
                        xtype: 'datefield'
                    }, {
                        name: 'civil_status',
                        columnWidth: 0.5
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        xtype: 'tbtext',
                        columnWidth: 1,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        text: 'Address'
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        allowBlank: true,
                        anchor: '100%',
                        xtype: 'textfield',
                        columnWidth: 1,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        name: 'address'
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        xtype: 'tbtext',
                        columnWidth: 0.25,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        text: 'ID No.<span class=\'tb-required\'>*</span>'
                    }, {
                        text: 'Passport No.'
                    }, {
                        text: 'CTC No.'
                    }, {
                        text: 'Monthly Income'
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        allowBlank: true,
                        anchor: '100%',
                        xtype: 'textfield',
                        columnWidth: 0.25,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        name: 'id_no',
                        allowBlank: false
                    }, {
                        name: 'passport_no'
                    }, {
                        name: 'ctc_no'
                    }, {
                        name: 'monthly_income',
                        xtype: 'numberfield',
                        minValue: 0
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        xtype: 'tbtext',
                        columnWidth: 0.5,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        text: 'Cell No.'
                    }, {
                        text: 'Home Tel No.'
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        allowBlank: true,
                        anchor: '100%',
                        xtype: 'textfield',
                        columnWidth: 0.5,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        name: 'cell_no'
                    }, {
                        name: 'home_phone'
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        xtype: 'tbtext',
                        columnWidth: 0.5,
                        margin: '5 0 0 5',
                        allowBlank: false
                    },
                    border: false,
                    items: [{
                        text: 'Primary Email Address<span class=\'tb-required\'>*</span>'
                    }, {
                        text: 'Secondary Email Address<span class=\'tb-required\'>*</span>'
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        allowBlank: false,
                        anchor: '100%',
                        xtype: 'textfield',
                        columnWidth: 0.5,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        name: 'email_address',
                        vtype: 'email'
                    }, {
                        name: 'email_address1',
                        vtype: 'email'
                    }]
                }, {
                    xtype: 'checkboxfield',
                    boxLabel: 'Set as Username',
                    name: 'set_username'
                }]
            }, {
                xtype: 'fieldset',
                title: 'Borrower Company',
                style: {
                    'margin-top': '10px !important'
                },
                defaults: {
                    allowBlank: false,
                    anchor: '100%',
                    labelWidth: 100,
                    xtype: 'textfield'
                },
                items: [{
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        xtype: 'tbtext',
                        columnWidth: .5,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        text: 'Company Name'
                    }, {
                        text: 'Company Address'
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        allowBlank: true,
                        anchor: '100%',
                        xtype: 'textfield',
                        columnWidth: .5,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        name: 'company_name'
                    }, {
                        name: 'company_address'
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        xtype: 'tbtext',
                        columnWidth: .2,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        text: 'Company SSS'
                    }, {
                        text: 'Philhealth No'
                    }, {
                        text: 'Office No'
                    }, {
                        text: 'Office Fax'
                    }, {
                        text: 'Employer Id'
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        allowBlank: true,
                        anchor: '100%',
                        xtype: 'textfield',
                        columnWidth: .2,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        name: 'company_sss'
                    }, {
                        name: 'philhealth_no'
                    }, {
                        name: 'office_no'
                    }, {
                        name: 'office_fax'
                    }, {
                        name: 'employer_id'
                    }]
                }]
            }, {
                xtype: 'fieldset',
                title: 'Login Information',
                style: {
                    'margin-top': '10px !important'
                },
                defaults: {
                    allowBlank: false,
                    anchor: '100%',
                    labelWidth: 100,
                    xtype: 'textfield'
                },
                items: [{
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        xtype: 'tbtext',
                        columnWidth: .25,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        text: 'Username<span class=\'tb-required\'>*</span>'
                    }, {
                        text: 'Password<span class=\'tb-required\'>*</span>'
                    }, {
                        text: 'Confirm Password<span class=\'tb-required\'>*</span>',
                        columnWidth: .5
                    }]
                }, {
                    xtype: 'panel',
                    layout: 'column',
                    defaults: {
                        allowBlank: false,
                        anchor: '100%',
                        xtype: 'textfield',
                        columnWidth: .25,
                        margin: '5 0 0 5'
                    },
                    border: false,
                    items: [{
                        name: 'username'
                    }, {
                        name: 'password',
                        inputType: 'password'
                    }, {
                        name: 'confirm',
                        columnWidth: .5,
                        inputType: 'password'
                    }]
                }, {
                    xtype: 'button',
                    text: 'Generate Password',
                    style: {
                        'margin': '10px 0 0 5px'
                    },
                    anchor: '20%'
                }, {
                    xtype: 'checkboxfield',
                    name: 'change_password',
                    allowBlank: true,
                    boxLabel: 'Prompt to change password on login'
                }]
            }]
        }];

        this.buttons = [{
            xtype: 'tbtext',
            text: "NOTE: '<span class=\'tb-required\'>*</span>' Denotes required fields!"
        }, '->', {
            text: 'Save'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.common.AddUser', {
    extend: 'Ext.form.Panel',
    alias: 'widget.adduser',
    floating: true,
    title: 'New User',
    height: 480,
    width: 600,
    closable: true,
    defaults: {
        border: false,
        bodyPadding: 20
    },
    border: false,
    modal: true,
    layout: 'border',
    _action: 'create',

    initComponent: function() {
        this.items = [{
            region: 'west',
            width: 200,
            layout: 'anchor',
            items: [{
                xtype: 'photoupload',
                border: false,
                height: 300
            }]
        }, {
            region: 'center',
            xtype: 'form',
            items: [{
                xtype: 'fieldset',
                title: 'User Info',
                defaults: {
                    allowBlank: false,
                    anchor: '100%',
                    labelWidth: 100,
                    xtype: 'textfield'
                },
                items: [{
                    name: 'file_id',
                    hidden: true,
                    allowBlank: true
                }, {
                    name: 'id',
                    hidden: true,
                    allowBlank: true
                }, {
                    fieldLabel: 'Firstname',
                    name: 'firstname'
                }, {
                    fieldLabel: 'Lastname',
                    name: 'lastname'
                }, {
                    xtype: 'usertype',
                    fieldLabel: 'Access Type',
                    name: 'user_typedef',
                    editable: false
                }, {
                    fieldLabel: 'Address',
                    name: 'address'
                }, {
                    fieldLabel: 'Tel #',
                    name: 'tel_no'
                }, {
                    fieldLabel: 'Email Address',
                    vtype: 'email',
                    name: 'email_address',
                    enableKeyEvents: true
                }, {
                    xtype: 'checkboxfield',
                    boxLabel: 'Set as Username',
                    name: 'set_username'
                }]
            }, {
                xtype: 'fieldset',
                title: 'Login Info',
                style: {
                    'margin-top': '10px !important'
                },
                defaults: {
                    allowBlank: false,
                    anchor: '100%',
                    labelWidth: 100,
                    xtype: 'textfield'
                },
                items: [{
                    name: 'username',
                    fieldLabel: 'Username'
                }, {
                    name: 'password',
                    fieldLabel: 'Password',
                    inputType: 'password'
                }, {
                    xtype: 'button',
                    text: 'Generate Password',
                    style: {
                        'margin-left': '105px'
                    }
                }, {
                    name: 'confirm',
                    fieldLabel: 'Confirm',
                    inputType: 'password'
                }, {
                    xtype: 'checkboxfield',
                    name: 'change_password',
                    boxLabel: 'Change password on login'
                }]
            }]
        }];

        this.buttons = [{
            text: 'Save'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.common.Positions', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.positions',
    displayField: 'position',
    valueField: 'position',
    store: 'Positions'
});

Ext.define('LOAN.view.common.Members', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.members',
    displayField: 'name',
    valueField: 'id',
    store: 'Members'
});

Ext.define('LOAN.view.common.AcctGroups', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.acctgroups',
    displayField: 'group',
    valueField: 'id',
    store: 'AcctGroups'
});

Ext.define('LOAN.view.common.Accounts', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.common.accounts',
    displayField: 'acct_code',
    valueField: 'acct_code',
    store: 'common.Accounts'
});

Ext.define('LOAN.view.common.PeriodMonths', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.common.periodmonths',
    displayField: 'month',
    valueField: 'val',
    store: 'common.PeriodMonths'
});

Ext.define('LOAN.view.common.PeriodYears', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.common.periodyears',
    displayField: 'year',
    valueField: 'year',
    store: 'common.PeriodYears'
});

Ext.define('LOAN.view.common.PhotoUpload', {
    alias: 'widget.photoupload',
    extend: 'Ext.form.Panel',

    initComponent: function() {
       
        this.items = [{
            xtype: 'image',
            name: 'picture',
            id: 'img',
            width: 245,
            src: ExtCommon.baseUrl + 'includes/images/stock/icon-user-default.png'
        }, {
            xtype: 'filefield',
            buttonOnly: true,
            buttonText: 'Upload a new photo...',
            hideLabel: true,
            name: 'file',
            cls: 'x-upload-clear',
            buttonConfig: {
                style: {
                    border: 0,
                    'margin-top': '5px',
                    background: 'transparent !important',
                    border: 'none !important'
                }
            }
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.common.BalanceDetails', {
    extend: 'Ext.form.Panel',
    alias: 'widget.balancedetails',
    floating: true,
    width: 350,
    title: 'Previous Balance',
    defaults: {
        readOnly: true,
        xtype: 'textfield',
        anchor: '100%',
        labelWidth: 100
    },
    bodyPadding: 10,
    closable: true,
    modal: true,
    border: false,

    initComponent: function() {
        this.items = [{
            name: 'id',
            fieldLabel: 'Loan ID'
        }, {
            name: 'type',
            fieldLabel: 'Loan Type'
        }, {
            name: 'principal',
            fieldLabel: 'Principal'
        }, {
            name: 'monthly',
            fieldLabel: 'Monthly'
        }, {
            name: 'interest',
            fieldLabel: 'Interest'
        }, {
            name: 'payments',
            fieldLabel: 'Total Payments'
        }, {
            name: 'payable',
            fieldLabel: 'Total Payable'
        }, {
            name: 'balance',
            fieldLabel: 'Balance'
        }, {
            name: 'status',
            fieldLabel: 'Status',
            hidden: true
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.UserModules', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.usermodules',
    border: false,
    store: 'Modules',
    features: [{
        ftype: 'grouping',
        groupHeaderTpl: '{name}'
    }],

    initComponent: function() {
        this.columns = [{
            dataIndex: 'group',
            text: 'Group',
            width: 150,
            hidden: true
        }, {
            dataIndex: 'module',
            text: 'Accessible Modules',
            flex: 1
        }, {
            dataIndex: 'access',
            width: 0
        }];

        this.selModel = new Ext.selection.CheckboxModel({
            checkOnly: true
        });

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.UserModulesForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.usermodulesform',
    floating: true,
    modal: true,
    closable: true,
    width: 450,
    height: 400,
    layout: 'fit',
    title: 'User Access',
    border: false,

    initComponent: function() {
        this.items = [{
            xtype: 'usermodules'
        }];

        this.buttons = [{
            text: 'Save'
        }, {
            text: 'Clear All'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.common.LoanCalculator', {
    extend: 'Ext.form.Panel',
    alias: 'widget.loancalculator',
    modal: true,
    floating: true,
    title: 'Loan Amount Calculator',
    bodyPadding: 15,
    width: 500,
    layout: 'anchor',
    frame: true,
    autoScroll: true,
    defaults: {
        anchor: '100%',
        xtype: 'numberfield',
        style: {
            'margin-bottom': '10px'
        },
        minValue: 0,
        allowBlank: false
    },
    closable: true,
    draggable: true,

    initComponent: function() {

        this.items = [{
            fieldLabel: 'Desired Amount',
            name: 'desired',
            anchor: '70%'
        }, {
            title: 'Previous Accounts',
            xtype: 'loanbalances', 
            width: 400,
            style: {
                'margin': '10px 0 10px 0'
            }
        }, {
            xtype: 'button',
            text: 'Calculate',
            scale: 'medium'
        }, {
            fieldLabel: 'Loan Principal',
            name: 'principal',
            anchor: '70%',
            readOnly: true
        }, {
            name: 'balance',
            hidden: true
        }];

        this.buttons = [{
            text: 'Fill-in'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.common.LoanBalances', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.loanbalances',
    store: 'LoanBalances',
    features: {
        ftype: 'summary'
    },

    initComponent: function() {

        this.columns = [{
            text: 'Type',
            dataIndex: 'type',
            width: 150,
            summaryType: 'sum',
            summaryRenderer: this.onTypeRender
        }, {
            text: 'Payable',
            dataIndex: 'payable',
            width: 100
        }, {
            text: 'Balance',
            dataIndex: 'payable',
            width: 100
        }, {
            text: 'Balance',
            dataIndex: 'balance',
            width: 100,
            summaryType: 'sum',
            summaryRenderer: this.onSumBalance
        }];

        this.callParent(arguments);
    },

    onTypeRender: function(value){
        return 'Balance Total:';
    },

    onSumBalance: function(value){
        ExtCommon.getCmp('numberfield[name=balance]').setValue(value);
        return value;
    }
});

Ext.define('LOAN.view.common.LoanAppAdd', {
    extend: 'Ext.form.Panel',
    alias: 'widget.loanappadd',
    modal: true,
    floating: true,
    title: 'New Loan Application',
    bodyPadding: 15,
    width: 450,
    layout: 'anchor',
    frame: true,
    autoScroll: true,
    defaults: {
        anchor: '100%',
        style: {
            'margin-bottom': '10px'
        },
        minValue: 0,
        allowBlank: false
    },
    closable: true,
    draggable: true,

    initComponent: function() {

        this.items = [{
            xtype: 'loantype',
            fieldLabel: 'Loan Type',
            name: 'loan_type'
        }, {
            xtype: 'members',
            fieldLabel: 'Select Member',
            name: 'member_id'
        }, {
            xtype: 'datefield',
            name: 'date_applied',
            fieldLabel: 'Date Applied'
        }, {
            layout: 'column',
            border: false,
            items: [{
                xtype: 'numberfield',
                fieldLabel: 'Loan Principal',
                name: 'principal',
                columnWidth: 0.8
            }, {
                xtype: 'button',
                text: 'Calculate',
                columnWidth: 0.2,
                style: {
                    'margin-left': '5px'
                }
            }]
        }, {
            xtype: 'loanterms',
            fieldLabel: 'Loan Term',
            name: 'terms'
        }, {
            xtype: 'container',
            layout: 'column',
            items: [{
                xtype: 'numberfield',
                fieldLabel: 'Interest Rate',
                name: 'interest',
                decimalPrecision: 4,
                allowBlank: false
            }, {
                xtype: 'tbtext',
                text: '&nbsp;%',
                style: {
                    'margin-top': '4px'
                }
            }]
        }, {
            xtype: 'container',
            layout: 'column',
            items: [{
                xtype: 'numberfield',
                fieldLabel: 'Payment Period',
                name: 'period',
                minValue: 1,
                step: 12,
                allowBlank: false
            }, {
                xtype: 'tbtext',
                text: '&nbsp;Months',
                style: {
                    'margin-top': '4px'
                }
            }]
        }];

        this.buttons = [{
            text: 'Save Record'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('Ext.form.field.Month', {
    extend: 'Ext.form.field.Date',
    alias: 'widget.monthfield',
    requires: ['Ext.picker.Month'],
    alternateClassName: ['Ext.form.MonthField', 'Ext.form.Month'],
    selectMonth: null,

    createPicker: function() {
        var me = this,
            format = Ext.String.format;

        return Ext.create('Ext.picker.Month', {
            pickerField: me,
            ownerCt: me.ownerCt,
            renderTo: document.body,
            floating: true,
            hidden: true,
            focusOnShow: true,
            minDate: me.minValue,
            maxDate: me.maxValue,
            disabledDatesRE: me.disabledDatesRE,
            disabledDatesText: me.disabledDatesText,
            disabledDays: me.disabledDays,
            disabledDaysText: me.disabledDaysText,
            format: me.format,
            showToday: me.showToday,
            startDay: me.startDay,
            minText: format(me.minText, me.formatDate(me.minValue)),
            maxText: format(me.maxText, me.formatDate(me.maxValue)),
            listeners: {
                select: {
                    scope: me,
                    fn: me.onSelect
                },
                monthdblclick: {
                    scope: me,
                    fn: me.onOKClick
                },
                yeardblclick: {
                    scope: me,
                    fn: me.onOKClick
                },
                OkClick: {
                    scope: me,
                    fn: me.onOKClick
                },
                CancelClick: {
                    scope: me,
                    fn: me.onCancelClick
                }
            },
            keyNavConfig: {
                esc: function() {
                    me.collapse();
                }
            }
        });
    },
    onCancelClick: function() {
        var me = this;
        me.selectMonth = null;
        me.collapse();
    },
    onOKClick: function() {
        var me = this;
        if (me.selectMonth) {
            me.setValue(me.selectMonth);
            me.fireEvent('select', me, me.selectMonth);
        }
        me.collapse();
    },
    onSelect: function(m, d) {
        var me = this;
        me.selectMonth = new Date((d[0] + 1) + '/1/' + d[1]);
    }
});

Ext.define('LOAN.view.LoanPayments', {
    extend: 'Ext.window.Window',
    alias: 'widget.loanpayments',
    width: 800,
    height: 500,
    title: 'Loan Info',
    layout: 'border',
    draggable: false,
    movable: false,
    resizable: false,
    _record: null,
    border: false,

    initComponent: function() {
        this.items = [{
            region: 'center',
            xtype: 'loanpaymentsgrid'
        }];

        this.buttons = [{
            xtype: 'button',
            text: 'Refresh'
        }, {
            xtype: 'button',
            text: 'Close'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.LoanPaymentsGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.loanpaymentsgrid',
    store: 'LoanPayments',
    features: {
        ftype: 'summary'
    },

    initComponent: function() {

        this.columns = [{
            xtype: 'rownumberer',
            width: 40
        }, {
            text: 'Date',
            dataIndex: 'pay_date',
            width: 150
        }, {
            text: 'Payment',
            dataIndex: 'payment',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Payable',
            dataIndex: 'payable',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Principal',
            dataIndex: 'principal',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Interest',
            dataIndex: 'interest',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Running Balance',
            dataIndex: 'running_balance',
            renderer: this.moneyRenderer,
            align: 'right',
            width: 150,
            hidden: true
        }, {
            text: 'Over/Under',
            dataIndex: 'ou',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Remarks',
            dataIndex: 'remarks',
            width: 250
        }, {
            xtype: 'actioncolumn',
            text: 'Options',
            hidden: true,
            items: [{
                icon: ExtCommon.baseUrl + 'includes/images/png/Xion.png',
                tooltip: 'Delte',
                handler: this.onDeleteClick
            }]
        }];

        this.callParent(arguments);
    },

    moneyRenderer: function(value) {
        return ExtCommon.number_format(value, 2);
    },

    summaryRenderer: function(value) {
        return ExtCommon.number_format(Math.round(value), 2);
    },

    onDeleteClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        Ext.Msg.show({
            msg: 'Delete this record?',
            title: 'Confirm',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn) {
                if (btn == 'ok') {
                    Ext.widget('form').getForm().submit({
                        url: ExtCommon.baseUrl + 'loan_payments/destroy',
                        params: {
                            payment_id: data.id
                        },
                        waitMsg: 'Please wait...',
                        success: function(f, action) {
                            if (action.result.success)
                                ExtCommon.successMsg(action.result.data);
                            else
                                ExtCommon.errorMsg(action.result.data);

                            grid.getStore().loadPage(1);
                        },
                        failure: function(f, action) {
                            ExtCommon.errorMsg(action.result.data);
                        }
                    });
                }
            }
        })
    }
});

Ext.define('LOAN.view.common.GridMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.gridmenu',
    action: 'item_click',
    border: false,
    margin: '0 0 10 0',
    initComponent: function() {
        if (ExtCommon.getCmp('menu[action=item_click]'))
            ExtCommon.getCmp('menu[action=item_click]').close();

        this.callParent(arguments);
    }
});
</script>
