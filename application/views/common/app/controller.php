<script type="text/javascript">
Ext.define('LOAN.controller.Common', {
    extend: 'Ext.app.Controller',

    init: function() {
        this.control({
            'addmember button[text=Save]': {
                click: this.onSaveMember
            },
            'addmember button[text=Update]': {
                click: this.onSaveMember
            },
            'adduser button[text=Save]': {
                click: this.onSaveUser
            },
            'adduser button[text=Update]': {
                click: this.onSaveUser
            },
            'addmember button[text=Reset]': {
                click: this.onResetEmployee
            },
            'adduser button[text=Reset]': {
                click: this.onResetUser
            },
            'addmember checkboxfield[boxLabel=Set as Username]': {
                change: this.onSetUsername
            },
            'addmember textfield[name=email_address]': {
                change: this.onEmailChange
            },
            'adduser checkboxfield[boxLabel=Set as Username]': {
                change: this.onSetUsername
            },
            'adduser textfield[name=email_address]': {
                change: this.onEmailChange
            },
            'photoupload filefield': {
                change: this.onSelectFile
            },
            'usermodulesform': {
                show: this.onUserModulesFormShow
            },
            'adduser button[text=Generate Password]': {
                click: this.onGenerateClick
            },
            'addmember button[text=Generate Password]': {
                click: this.onGenerateClick
            },
            'loanappadd button[text=Save Record]': {
                click: this.onSaveLoan
            },
            'loanappadd members': {
                select: this.onSelectLoanMember
            },
            'loanappadd loantype': {
                select: this.onSelectLoanType
            },
            'loanpayments': {
                show: this.onLoanPaymentsShow
            },
            'loanpayments button[text=Close]': {
                click: this.onPaymentsCloseClick
            },
            'loanpayments button[text=Refresh]': {
                click: this.onPaymentsRefreshClick
            },
            'loanappadd button[text=Calculate]': {
                click: this.onCalculateClick
            },
            'loancalculator': {
                show: this.onLoanCalatorShow
            },
            'loancalculator button[text=Calculate]': {
                click: this.onCalculateClick1
            },
            'loancalculator button[text=Fill-in]': {
                click: this.onFillInClick
            }
        });
    },

    views: [
        'common.LoanTerms',
        'common.LoanStatus',
        'common.LoanType',
        'common.UserType',
        'common.AddMember',
        'common.AddUser',
        'common.Members',
        'common.AcctGroups',
        'common.Accounts',
        'common.PeriodMonths',
        'common.PeriodYears',
        'common.PhotoUpload',
        'common.BalanceDetails',
        'UserModules',
        'common.MemberLoans',
        'LoanPayments',
        'LoanPaymentsGrid',
        'common.GridMenu',
        'common.LoanCalculator',
        'common.LoanBalances'
    ],

    stores: [
        'LoanTerms',
        'LoanStatus',
        'LoanType',
        'UserType',
        'Positions',
        'Members',
        'AcctGroups',
        'common.Accounts',
        'common.PeriodMonths',
        'common.PeriodYears',
        'Modules',
        'common.MemberLoans',
        'LoanApp',
        'LoanPayments',
        'LoanBalances'
    ],

    models: [
        'LoanVars',
        'Position',
        'Member',
        'AcctGroup',
        'common.Account',
        'Module',
        'common.MemberLoan',
        'LoanApp',
        'LoanPayment'
    ],

    onSaveMember: function(ths) {
        var form = ExtCommon.getCmp('form[region=center]'),
            list = ExtCommon.getCmp('memberinfo');

        if (!form.getForm().isValid())
            return;

        var p = ExtCommon.getCmp('textfield[name=password]').getValue(),
            c = ExtCommon.getCmp('textfield[name=confirm]').getValue();

        if (p != c) {
            ExtCommon.errorMsg('Password confirmation did not match!');
            return;
        }

        form.getForm().submit({
            url: ExtCommon.baseUrl + 'members/create',
            waitMsg: 'Please wait...',
            params: {
                action: ths.up('form')._action
            },
            success: function(f, action) {
                if (action.result.success)
                    ExtCommon.successMsg(action.result.data);
                else
                    ExtCommon.errorMsg(action.result.data);

                if (ths.up('form')._action == 'create') {
                    ths.up('form').getForm().reset();
                } else {
                    if(ths.up('form').floating == false)
                        return;
                    
                    ths.up('form').close();
                }

                if (list)
                    list.store.loadPage(1);
            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    },

    onResetEmployee: function(ths) {
        ths.up('form').getForm().reset();
    },

    onSaveUser: function(ths) {
        var form = ExtCommon.getCmp('form[region=center]'),
            list = ExtCommon.getCmp('userinfo');

        if (!form.getForm().isValid())
            return;

        var p = form.getForm().findField('password').getValue(),
            c = form.getForm().findField('confirm').getValue();

        if (p.trim() != c.trim()) {
            ExtCommon.errorMsg("Password confirmation did not match!");
            return;
        }

        form.getForm().submit({
            url: ExtCommon.baseUrl + 'settings/users/create',
            waitMsg: 'Please wait...',
            params: {
                action: ths.up('form')._action
            },
            success: function(f, action) {
                if (action.result.success)
                    ExtCommon.successMsg(action.result.data);
                else
                    ExtCommon.errorMsg(action.result.data);

                if(ths.up('form').floating == true)
                    form.getForm().reset();

                if (list)
                    list.store.loadPage(1);
            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    },

    onResetUser: function(ths) {
        ths.up('form').getForm().reset();
    },

    onSetUsername: function(ths) {
        var form = ths.up('form'),
            email = form.getForm().findField('email_address'),
            uname = form.getForm().findField('username');

        uname.setValue(email.getValue());
        uname.setReadOnly(ths.getValue());
    },

    onEmailChange: function(ths) {
        var form = ths.up('form'),
            uname = form.getForm().findField('username'),
            set = form.getForm().findField('set_username');

        if (set.getValue())
            uname.setValue(ths.getValue());
    },

    onSelectFile: function(ths) {
        var form = ths.up('form');

        if (!form.getForm().isValid())
            return;

        form.setLoading('Uploading...');

        form.getForm().submit({
            url: ExtCommon.baseUrl + 'user_files/upload',
            success: function(f, action) {
                if (action.result.success) {
                    ExtCommon.getCmp('textfield[name=file_id]').setValue(action.result.file_id);
                    ExtCommon.getCmp('photoupload image').setSrc(action.result.tmp_path);
                } else {
                    ExtCommon.errorMsg(action.result.data);
                }

                form.setLoading(false);
            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
                form.setLoading(false);
            }
        });

    },

    onUserModulesFormShow: function(ths) {
        var grid = ths.down('usermodules');
        grid.store.proxy.extraParams['user_typedef'] = ths._data.value;
        grid.store.loadPage(1);
    },

    onGenerateClick: function(ths) {
        ths.up('form').setLoading('Generate password...');
        Ext.widget('form').getForm().submit({
            url: ExtCommon.baseUrl + 'user_login/generate_password',
            success: function(f, action) {
                if (action.result.success) {
                    ExtCommon.getCmp('textfield[name=password]').setValue(action.result.data);
                    ExtCommon.getCmp('textfield[name=confirm]').setValue(action.result.data);
                    ExtCommon.successMsg("You have chosen a random password. Please be sure to save the following text on a secure location and hit 'OK'.<br /><br />" + action.result.data);
                } else {
                    ExtCommon.errorMsg(action.result.data);
                }

                ths.up('form').setLoading(false);
            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
                ths.up('form').setLoading(false);
            }
        });
    },

    onSaveLoan: function(ths) {
        var form = ths.up('form');

        if (!form.getForm().isValid())
            return;

        form.getForm().submit({
            url: ExtCommon.baseUrl + 'loans/create',
            params: {
                details: form._details
            },
            waitMsg: 'Please wait...',
            success: function(f, action) {
                if (action.result.success)
                    ExtCommon.successMsg(action.result.data);
                else
                    ExtCommon.errorMsg(action.result.data);

                form.getForm().reset();
                ExtCommon.getCmp('loanapp').store.loadPage(1);
            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        })
    },

    onSelectLoanType: function(ths) {
        var form = ths.up('form');

        form.getForm().findField('member_id').reset();
    },

    onSelectLoanMember: function(ths) {
        var form = ths.up('form'),
            loantype = form.down('loantype').getValue(),
            loanamt = ExtCommon.getCmp('numberfield[fieldLabel=Loan Principal]').getValue();

        if (loantype === null) {
            ExtCommon.errorMsg('You must select Loan Type!');
            // ths.reset();
            // return;
        }

        Ext.widget('form').getForm().submit({
            waitMsg: "Checking member's balance...",
            url: ExtCommon.baseUrl + 'members/balance_check',
            params: {
                member_id: ths.getValue(),
                type: loantype
            },
            success: function(f, action) {
                var legible = false;
                if (action.result.success) {
                    if (!action.result.legible) {
                        legible = false;
                        ths.reset();
                    } else {
                        legible = true;
                    }
                } else {
                    legible = false;
                    ths.reset();
                }

                var msg = '',
                    icon = Ext.Msg.ERROR,
                    buttons = Ext.Msg.OK;

                if (!legible) {
                    msg = '<br />Do you want to view the details now?';
                    buttons = Ext.Msg.YESNO;
                } else {
                    // if (action.result.details != null && parseFloat(action.result.details.balance) > 0) {
                    //     form._details = action.result.details;
                    //     console.log(form._details);
                    //     ExtCommon.successMsg('The member is legible to apply for a new loan, however, the loan principal amount will be deducted with the remaining balance of ' + action.result.details.balance + ' from previous accounts.');

                    //     var prinicipal = ExtCommon.getCmp('numberfield[fieldLabel=Loan Principal]'),
                    //         actual = parseFloat(prinicipal.getValue()) - parseFloat(action.result.details.balance),
                    //         actualel = ExtCommon.getCmp('numberfield[fieldLabel=Actual Principal]');

                    //     actualel.setValue(parseFloat(actual).toFixed(2));
                    // }
                    return;
                }

                Ext.Msg.show({
                    title: 'Status',
                    msg: action.result.data + msg,
                    icon: icon,
                    buttons: buttons,
                    fn: function(btn) {
                        if (btn == 'yes') {
                            var details = new LOAN.view.common.BalanceDetails();
                            details.show();
                            for (d in action.result.details) {
                                var obj = ExtCommon.getCmp('balancedetails').getForm().findField(d);
                                if (obj && obj.setValue)
                                    obj.setValue(action.result.details[d]);
                            }
                        }
                    }
                });
            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
                ths.reset();
            }
        });
    },

    onPaymentsCloseClick: function(ths) {
        ExtCommon.getCmp('loanpayments').close();
    },

    onPaymentsRefreshClick: function(ths) {
        ExtCommon.getCmp('loanpayments').down('loanpaymentsgrid').getStore().load();
    },

    onLoanPaymentsShow: function(ths) {
        ExtCommon.getCmp('loanpaymentsgrid').store.proxy.extraParams['loan_id'] = ths._record.id;
        ExtCommon.getCmp('loanpaymentsgrid').store.load();
    },

    onCalculateClick: function(ths) {
        var form = ths.up('form'),
            member = form.down('members'),
            loan = form.down('loantype');

        if (!member || member.getValue() == null) {
            ExtCommon.errorMsg('You must select a member!');
            return;
        }

        if (!loan || loan.getValue() == null) {
            ExtCommon.errorMsg('You must select a loan type!');
            return;
        }

        new LOAN.view.common.LoanCalculator({
            _data: {
                member: member.getValue(),
                loan: loan.getValue()
            }
        }).show();
    },

    onCalculateClick1: function(ths) {
        var form = ths.up('form'),
            desired = ExtCommon.getCmp('loancalculator numberfield[name=desired]'),
            balance = ExtCommon.getCmp('loancalculator numberfield[name=balance]'),
            principal = ExtCommon.getCmp('loancalculator numberfield[name=principal]');

        if(desired.getValue() == null || parseFloat(desired.getValue()) <= 0){
            ExtCommon.errorMsg('Please input a valid desired amount!');
            return;
        }

        principal.setValue(desired.getValue() - balance.getValue());
    },

    onLoanCalatorShow: function(ths) {
        ths.down('button[text=Calculate]').setDisabled(true);
        ths.down('button[text=Fill-in]').setDisabled(true);

        var store = ths.down('loanbalances').store,
            data = ths._data;

        store.proxy.extraParams['loan_type'] = data.loan;
        store.proxy.extraParams['member_id'] = data.member;

        store.load({
            callback: function() {
                ths.down('button[text=Calculate]').setDisabled(false);
                ths.down('button[text=Fill-in]').setDisabled(false);
            }
        });
    },

    onFillInClick: function(ths){
        var form = ths.up('form'),
            prinicipal = form.down('numberfield[name=principal]');

        if(prinicipal.getValue() == null || prinicipal.getValue() <= 0){
            ExtCommon.errorMsg('Invalid loan principal amount!');
            return;
        }

        ExtCommon.getCmp('loanappadd numberfield[fieldLabel=Loan Principal]').setValue(prinicipal.getValue());
        form.close();
    }
});
</script>
