<?php

foreach(scandir('includes/images/png') as $key => $value)
{
	if($value == '.' OR $value == '..')
		continue;
	
	$value = explode(".", $value);
	echo ".icon-{$value[0]}" . 
		'{'. 
			'background: url(' . 
			base_url() . 
			'includes/images/png/' . 
			implode('.', $value) . ') !important;' .
			'background-size: 25px 23px !important;' . 
			'background-repeat: no-repeat !important;' . 
			'background-position: -7px -4px !important;' . 
		'}';
}

?>