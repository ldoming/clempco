<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="product" content="Loan Application System">
    <meta name="description" content="Loan Application">
    <meta name="author" content="John Rey Lopena">
    <meta name="keywords" content="js, css, metro, framework, windows 8, metro ui">

    <link href="<?=base_url(); ?>includes/metro/css/metro-bootstrap.css" rel="stylesheet">
    <link href="<?=base_url(); ?>includes/metro/css/metro-bootstrap-responsive.css" rel="stylesheet">
    <link href="<?=base_url(); ?>includes/metro/css/iconFont.min.css" rel="stylesheet">
    <link href="<?=base_url(); ?>includes/css/docs.css" rel="stylesheet">
    <link href="<?=base_url(); ?>includes/css/prettify.css" rel="stylesheet">
    <!-- <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'> -->
    <!-- <link href="<?=base_url(); ?>includes/css/system.css" rel='stylesheet' type='text/css'> -->

    <!-- Load JavaScript Libraries -->
    <script src="<?=base_url(); ?>includes/jquery/jquery.min.js"></script>
    <script src="<?=base_url(); ?>includes/jquery/jquery.widget.min.js"></script>
    <script src="<?=base_url(); ?>includes/jquery/jquery.mousewheel.js"></script>
    <script src="<?=base_url(); ?>includes/jquery/jquery.maskedinput.js"></script>
    <script src="<?=base_url(); ?>includes/js/prettify.js"></script>
    <script src="<?=base_url(); ?>includes/metro/metro.min.js"></script>

    <!-- Load ExtJS Libraries -->
    <?php if(ENVIRONMENT == 'production') { ?>
        <script src="http://cdn.sencha.com/ext/gpl/4.2.0/ext-all.js"></script>
        <link href="http://cdn.sencha.io/ext/gpl/4.2.0/resources/css/ext-all-neptune.css" rel="stylesheet" />
    <?php } else { ?>
        <script src="<?php echo base_url();?>includes/js/ext/ext-all.js"></script>
        <link href="<?php echo base_url();?>includes/js/ext/resources/css/ext-all-neptune.css" rel="stylesheet" />
    <?php } ?>
    <link href="<?=base_url(); ?>includes/css/ext.css" rel="stylesheet">
    <link href="<?=base_url(); ?>user_request/get_icons" rel="stylesheet">

    <!-- Load JS Common Library -->
    <script src="<?php echo base_url();?>includes/js/common.js"></script>
    <script type="text/javascript">
    ExtCommon.baseUrl = '<?php echo base_url();?>';
    ExtCommon.ajaxDefaults(0, {
        '<?php echo $this->security->get_csrf_token_name();?>': '<?php echo $this->security->get_csrf_hash();?>',
		'_m': "<?php echo (isset($_GET['_m']) ? $_GET['_m'] : '');?>"
    });
    </script>

    <title>CLEMPCO Loan Application |
        <?=$title; ?>
    </title>
</head>

<body class="metro">
