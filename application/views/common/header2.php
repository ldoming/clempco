<header class="bg-dark">
    <div class="navigation-bar dark fixed-top shadow">
        <div class="navigation-bar-content">
            <ul class="element-menu">
                <li>
                    <a href="<?php echo base_url();?>dashboard/index" class="element">
                        <span class="icon-home"></span>&nbsp;&nbsp;CLEMPCO Loan Application</a>
                </li>

                <?php if(get_session()) { ?>
                    <?php foreach ($modules as $key => $value) {
                        $drop = '';
                        $submodules = ${'submodule_' . $value['id']};
                        if(isset($submodules) && count($submodules) > 0){
                            $drop = 'dropdown-toggle';
                        }
                    ?>
                        <li>
                            <a href="<?php echo site_url($value['link']);?>" class="element <?=$drop;?>"><?php echo $value['name'];?></a>
                            <?php if(count($submodules) > 0){ ?>
                                <ul class="dropdown-menu dark" data-role="dropdown">
                                    <?php foreach ($submodules as $kkey => $vvalue) { ?>
                                        <li>
                                            <a href="<?php echo site_url($vvalue['link']);?>"><?php echo $vvalue['name'];?></a>
                                        </li>
                                    <?php } ?>
                                    <?php if($vvalue['end'] == 1) { ?> 
                                        <li class="divider"></li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>

            <?php if(get_session()) { ?>
                <ul class="element-menu place-right">
                    <li style="min-width: 150px;width: auto">
                        <a href="#" class="element dropdown-toggle">
                            <span class="icon-user"></span>&nbsp;
                            <?php $session = get_session(); echo ucwords(strtolower("{$session[ 'firstname']} {$session['lastname']}"));?>
                        </a>
                        <ul class="dropdown-menu dark" data-role="dropdown">
                            <li><a href="<?php echo base_url();?>user_info/profile">Profile</a>
                            </li>
                            <li><a href="<?php echo base_url();?>user_info/end_session">Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            <?php } ?>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function (argument) {
            $('body').click(function(){
                $('.always-prevail').removeClass('always-prevail');
            }).mouseleave(function(){
                $('.always-prevail').removeClass('always-prevail');
            })
            $('.dropdown-toggle').mouseenter(function(){
                $('.always-prevail').removeClass('always-prevail');
                $(this).addClass('always-prevail');
            });
            $('.icon-home').parent('a').mouseover(function(){
                $('.always-prevail').removeClass('always-prevail');
            });
        })
    </script>
</header>
