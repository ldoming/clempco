<div class="row">
	<?php
		$error = $this->session->flashdata('error_msg');
		// exit($error);
		if(!empty($error))
		{
	?>
		<div class="notice marker-on-bottom bg-darkRed fg-white">
		    <div class=""><?=$error;?></div>
		</div>
	<?php } ;?>

	<?php
		$info = $this->session->flashdata('info_msg');
		// exit($info);
		if(!empty($info))
		{
	?>
		<div class="notice marker-on-bottom bg-amber fg-white">
		    <div class=""><?=$info;?></div>
		</div>
	<?php } ;?>

	<?php
		$success = $this->session->flashdata('success_msg');
		// exit($success);
		if(!empty($success))
		{
	?>
		<div class="notice marker-on-bottom fg-white">
		    <div class=""><?=$success;?></div>
		</div>
	<?php } ;?>
</div>
<script type="text/javascript">
	$(window).load(function(){
		setTimeout(function(){
			$('.notice').fadeOut('slow', function(){
				$(this).remove();
			});
		}, 10000);
	});
</script>