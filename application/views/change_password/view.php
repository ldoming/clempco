<div class="span4">
    <?php echo form_open('user_login/update_password');?>
    <fieldset>
        <legend>Your new password</legend>
        <label>Password</label>
        <div class="input-control text" data-role="input-control">
            <input name="password" type="password" placeholder="" autofocus="" value="newpassword">
            <button class="btn-clear" tabindex="-1" type="button"></button>
        </div>
        <label>Confirm Password</label>
        <div class="input-control password" data-role="input-control">
            <input name="confirm" type="password" placeholder="" value="newpassword">
            <button class="btn-reveal" tabindex="-1" type="button"></button>
        </div>
        <br />

        <input type="button" value="Generate Password">
        <input type="submit" value="Submit">
    </fieldset>
    <?php echo form_close();?>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('form').submit(function(e) {
        window.allowLeave = true;
    });

    $('input[type="button"]').click(function(){
        Ext.widget('form').getForm().submit({
            url: ExtCommon.baseUrl + 'user_login/generate_password',
            waitMsg: 'Generate password...',
            success: function(f, action) {
                if (action.result.success) {
                    // ExtCommon.getCmp('textfield[name=password]').setValue(action.result.data);
                    // ExtCommon.getCmp('textfield[name=confirm]').setValue(action.result.data);
                    $('input[type="password"]').val(action.result.data);
                    ExtCommon.successMsg("You have chosen a random password. Please be sure to save the following text on a secure location and hit 'OK'.<br /><br />" + action.result.data);
                } else {
                    ExtCommon.errorMsg(action.result.data);
                }

                // ths.up('form').setLoading(false);
            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    });
});
$(window).bind('beforeunload', function() {
    if (!window.allowLeave)
        return 'You have not yet updated your password!';
});
</script>
