<div id="ext-view"></div>

<script type="text/javascript">
Ext.define('LOAN.view.LoanApp', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.loanapp',
    store: 'LoanApp',
    cls: 'span12',
    renderTo: 'ext-view',
    height: 500,

    initComponent: function() {

        this.tbar = [{
            xtype: 'searchfield',
            store: Ext.getStore('LoanApp'),
            emptyText: 'Search',
            fieldLabel: 'Search',
            labelWidth: 60
        }, {
            xtype: 'loantype',
            fieldLabel: 'Loan Type',
            editable: false,
            labelWidth: 80
        }, {
            xtype: 'button',
            text: 'New Record'
        }];

        this.columns = [{
            text: 'Name',
            dataIndex: 'name',
            width: 250
        }, {
            text: 'Date',
            dataIndex: 'created_at',
            width: 100
        }, {
            text: 'Type',
            dataIndex: 'type',
            width: 150
        }, {
            text: 'Status',
            dataIndex: 'status',
            width: 150
        }, {
            text: 'Amount',
            dataIndex: 'principal',
            renderer: this.money_renderer,
            width: 150
        }, {
            text: 'Payable',
            dataIndex: 'payable',
            renderer: this.money_renderer,
            width: 150
        }, {
            text: 'Deductions',
            dataIndex: 'deductions',
            width: 100
        }, {
            text: 'Balance',
            dataIndex: 'balance',
            width: 100
        }, {
            text: 'Total Payable',
            dataIndex: 'total_payable',
            width: 100,
            renderer: this.money_renderer,
            hidden: true
        }, {
            text: 'Loan Balance',
            dataIndex: 'balance',
            width: 150,
            renderer: this.money_renderer,
            hidden: true
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true,
            store: Ext.getStore('LoanApp'),
            items: [{
                xtype: 'button',
                text: 'Export'
            }]
        }];

        this.callParent(arguments);
    },

    accessAllowed: function(v, r, c, item, record){
        return !record.get('editable');
    },

    money_renderer: function(value) {
        return ExtCommon.number_format(value, 2);
    },

    onOptionsClick: function(grid, rowIndex, colIndex, re, event) {
        
    },

    statusRenderer: function (value) {
        return '<a href="javascript:">' + value + '</a>';
    }
});

Ext.define('LOAN.view.LoanStatus', {
    extend: 'Ext.window.Window',
    alias: 'widget.loanstatus',
    title: 'Loan Info',
    width: 550,
    height: 350,
    layout: 'fit',
    title: 'Loan Approval Status',
    modal: true,

    initComponent: function () {
        this.items = [{
            xtype: 'loanstatusgrid'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.LoanStatusGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.loanstatusgrid',
    draggable: false,
    movable: false,
    resizable: false,
    store: 'LoanStatus',

    initComponent: function () {

        this.tbar = [{
            xtype: 'button',
            text: 'Add Status'
        }];

        this.columns = [{
            text: 'Personnel',
            dataIndex: 'personnel',
            width: 120
        }, {
            text: 'Comment',
            dataIndex: 'comment',
            flex: 1
        }, {
            text: 'Status',
            dataIndex: 'status',
            width: 120
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            store: Ext.getStore('LoanStatus'),
            displayInfo: true,
            dock: 'bottom'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.AddLoanHistory', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addloanhistory',
    floating: true,
    modal: true,
    title: 'Add Record',
    width: 350,
    height: 250,
    layout: 'anchor',
    defaults: {
        anchor: '100%',
        allowBlank: false
    },
    bodyPadding: 10,
    border: false,
    closable: true,

    initComponent: function () {
        this.items = [{
            fieldLabel: 'Status',
            xtype: 'loanstatuscmb',
            editable: false,
            name: 'status'
        }, {
            fieldLabel: 'Comment',
            xtype: 'textareafield',
            name: 'comment'
        }];

        this.buttons = [{
            text: 'Save',
            formBind: true,
            disabled: true
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.LoanStatusCmb', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.loanstatuscmb',
    store: 'LoanStatusCmb',
    displayField: 'action',
    valueField: 'action'
});

Ext.define('LOAN.view.Applicants', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.applicants',
    store: 'Applicants',

    initComponent: function() {

        this.tbar = [{
            xtype: 'applicantsdd',
            emptyText: 'Search',
            fieldLabel: 'Select Applicant',
            labelWidth: 100,
            flex: 1
        }, {
            xtype: 'button',
            text: 'New Member'
        }];

        this.columns = [{
            text: 'name',
            dataIndex: 'name',
            flex: 1
        }, {
            xtype: 'actioncolumn',
            width: 40,
            items: [{
                icon: ExtCommon.baseUrl + 'includes/images/png/Xion.png',
                tooltip: 'Delete Record',
                handler: this.onDeleteClick
            }]
        }];

        this.callParent(arguments);
    },

    onDeleteClick: function(grid, rowIndex) {
        grid.store.removeAt(rowIndex);
    }
});

</script>
