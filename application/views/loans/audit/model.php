<script type="text/javascript">

	Ext.define('LOAN.store.LoanApp', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.LoanApp',
		storeId: 'LoanApp',
		queryMode : 'remote',
		autoLoad: true,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'loans/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

</script>