<script type="text/javascript">
Ext.define('LOAN.controller.LoanApp', {
    extend: 'Ext.app.Controller',

    init: function() {
        this.control({
            'loanapp': {
                itemclick: this.onLoanRowClick
            },
            'loanapp button[text=New Record]': {
                click: this.onNewRecord,
            },
            'applicantsdd': {
                select: this.onSelectApplicant
            },
            'applicants button[text=New Member]': {
                click: this.onNewMemberClick
            },
            'loandetails': {
                show: this.onLoanDetailsShow
            },
            'loandetails button[text=Refresh]': {
                click: this.onRefreshClick
            },
            'loandetails button[text=Close]': {
                click: this.onCloseClick
            },
            'paymenthistory button[text=Add Payment]': {
                click: this.onAddPaymentClick
            },
            'loanapp loantype': {
                select: this.onLoanTypeSelect
            },
            'loanstatus': {
                render: this.onLoanStatusRender
            },
            'loanstatusgrid button[text=Add Status]': {
                click: this.onAddStatus
            },
            'addloanhistory button[text=Save]': {
                click: this.onAddHistory
            },
            'addloanhistory button[text=Reset]': {
                click: this.onAddHistoryReset
            }
        })
    },

    views: ['LoanApp'],

    models: ['LoanApp'],

    stores: ['LoanApp'],

    onNewRecord: function() {
        new LOAN.view.common.LoanAppAdd().show();
    },

    onSelectApplicant: function(ths, rec) {
        var grid = ths.up('grid'),
            store = grid.store,
            data = rec[0].data;

        var found = store.findBy(function(row) {
            if (row.data.id == data.id) {
                return true;
            } else {
                return false;
            }
        });

        if (found == -1) {
            store.add(data);
        }

        ths.reset();

    },

    onLoanDetailsShow: function(ths) {
        var grid = ths.down('grid'),
            store = grid.getStore();

        store.proxy.extraParams['loan_id'] = ths._record.get('id');
        store.loadPage(1);
    },

    onRefreshClick: function(ths) {
        ths.up('window').down('grid').store.loadPage(1);
    },

    onCloseClick: function(ths) {
        ths.up('window').close();
    },

    onNewMemberClick: function() {
        new LOAN.view.common.AddMember().show();
    },

    onLoanTypeSelect: function(ths) {
        var grid = ths.up('grid');

        grid.store.proxy.extraParams['loan_type'] = ths.getValue();
        grid.store.load();
    },

    onLoanStatusRender: function(ths) {
        var grid = ExtCommon.getCmp('loanstatusgrid');

        grid.store.proxy.extraParams['loan_id'] = ths._record.id;
        grid.store.loadPage(1);
    },

    onAddStatus: function(ths) {
        new LOAN.view.AddLoanHistory({
            _record: ths.up('window')._record
        }).show();
    },

    onAddHistory: function(ths) {
        var form = ths.up('form');

        if (!form.getForm().isValid())
            return;

        form.getForm().submit({
            url: ExtCommon.baseUrl + 'loan_status_history/create',
            waitMsg: 'Please wait...',
            params: {
                loan_id: form._record.id
            },
            success: function(f, action) {
                if (action.result.success) {
                    ExtCommon.successMsg(action.result.data);
                    form.getForm().reset();
                    ExtCommon.getCmp('loanstatusgrid').store.loadPage(1);
                } else {
                    ExtCommon.errorMsg(action.result.data);
                }
            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    },

    onAddHistoryReset: function(ths) {
        ths.up('form').reset();
    },

    onLoanRowClick: function(ths, rec, item, i, e) {
        var menu = new Ext.widget('gridmenu', {
            record: rec,
            items: [{
                text: 'View Payments',
                handler: this.onLoanViewPayments,
                hidden: !rec.get('editable')
            }, {
                text: 'Export',
                handler: this.onLoanExport
            }, {
                text: 'Delete',
                handler: this.onLoanDelete,
                hidden: !rec.get('editable')
            }],
            _e: {
                e: e,
                grid: ths
            }
        });

        ExtCommon.showAt(menu, e, ths);
    },

    onLoanExport: function(ths) {
        var data = ths.up('menu').record;

        Ext.create('widget.form').getForm().submit({
            url: ExtCommon.baseUrl + 'loans/export',
            timeout: 30000,
            waitMsg: 'Preparing your download. Please wait!',
            title: 'Export',
            params: {
                loan_id: data.get('id')
            },
            success: function(f, action) {
                if (action.result.success) {
                    if (action.result.download) {
                        Ext.Msg.show({
                            title: 'Status',
                            msg: 'Your download is ready!',
                            buttonText: {
                                ok: "Download",
                                cancel: "Cancel"
                            },
                            fn: function(btn) {
                                if (btn == 'ok')
                                    window.location.href = action.result.download;
                            }
                        });
                    }
                }

            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    },

    onLoanDelete: function(ths) {
        var data = ths.up('menu').record;

        Ext.Msg.show({
            msg: 'Delete this record?',
            title: 'Confirm',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn) {
                if (btn == 'ok') {
                    Ext.widget('form').getForm().submit({
                        url: ExtCommon.baseUrl + 'loans/destroy',
                        params: {
                            loan_info: Ext.encode(data.data)
                        },
                        waitMsg: 'Please wait...',
                        success: function(f, action) {
                            if (action.result.success)
                                ExtCommon.successMsg(action.result.data);
                            else
                                ExtCommon.errorMsg(action.result.data);

                            ExtCommon.getCmp('loanapp').getStore().loadPage(1);
                        },
                        failure: function(f, action) {
                            ExtCommon.errorMsg(action.result.data);
                        }
                    });
                }
            }
        })
    },

    onLoanViewPayments: function(ths) {
        var data = ths.up('menu').record.data;

        var payments = new LOAN.view.LoanPayments({
            _record: data
        }).show();

        payments.setTitle('Payments - ' + data.name + ' ' + data.type);
    }
});
</script>
