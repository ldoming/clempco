
<div id="ext-view"></div>

<script type="text/javascript">
Ext.define('LOAN.view.LoanApp', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.loanapp',
    store: 'LoanApp',
    cls: 'span12',
    renderTo: 'ext-view',
    height: 500,

    initComponent: function() {

        this.tbar = [{
            xtype: 'searchfield',
            store: Ext.getStore('LoanApp'),
            emptyText: 'Search',
            fieldLabel: 'Search',
            labelWidth: 60
        }, {
            xtype: 'loantype',
            fieldLabel: 'Loan Type',
            editable: false,
            labelWidth: 80
        }, {
            xtype: 'button',
            text: 'New Record',
            hidden: true
        }];

        this.columns = [{
            text: 'Name',
            dataIndex: 'name',
            width: 250
        }, {
            text: 'Date',
            dataIndex: 'created_at',
            width: 100
        }, {
            text: 'Type',
            dataIndex: 'type',
            width: 150
        }, {
            text: 'Status',
            dataIndex: 'status',
            width: 150
        }, {
            text: 'Amount',
            dataIndex: 'principal',
            renderer: this.money_renderer,
            width: 150
        }, {
            text: 'Payable',
            dataIndex: 'payable',
            renderer: this.money_renderer,
            width: 150
        }, {
            text: 'Deductions',
            dataIndex: 'deductions',
            width: 100
        }, {
            text: 'Balance',
            dataIndex: 'balance',
            width: 100
        }, {
            text: 'Total Payable',
            dataIndex: 'total_payable',
            width: 100,
            renderer: this.money_renderer,
            hidden: true
        }, {
            text: 'Loan Balance',
            dataIndex: 'balance',
            width: 150,
            renderer: this.money_renderer,
            hidden: true
        }, {
            xtype: 'actioncolumn',
            text: 'Options',
            hidden: true,
            width: 100,
            defaults: {
                style: {
                    margin: '0px 5px !important'
                }
            },
            text: 'Actions',
            items: [{
                icon: ExtCommon.baseUrl + 'includes/images/png/Excel-XLS-file.png',
                tooltip: 'Export',
                handler: this.onExportClick,
                text: 'Action'
            }, {
                icon: ExtCommon.baseUrl + 'includes/images/png/Explorer.png',
                tooltip: 'Payments',
                handler: this.onOptionsClick
            }, {
                icon: ExtCommon.baseUrl + 'includes/images/png/Writing.png',
                tooltip: 'Edit',
                handler: this.onEditClick
            }, {
                icon: ExtCommon.baseUrl + 'includes/images/png/Xion.png',
                tooltip: 'Delete',
                handler: this.onDeleteClick
            }]
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true,
            store: Ext.getStore('LoanApp'),
            items: [{
                xtype: 'button',
                text: 'Export'
            }]
        }];

        this.callParent(arguments);
    },

    money_renderer: function(value) {
        return ExtCommon.number_format(value, 2);
    },

    onOptionsClick: function(grid, rowIndex, colIndex, re, event) {
        new LOAN.view.LoanDetails({
            _record: grid.getStore().getAt(rowIndex)
        }).show();
    },

    onDeleteClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        Ext.Msg.show({
            msg: 'Delete this record?',
            title: 'Confirm',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn) {
                if (btn == 'ok') {
                    Ext.widget('form').getForm().submit({
                        url: ExtCommon.baseUrl + 'loans/destroy',
                        params: {
                            loan_info: Ext.encode(data)
                        },
                        waitMsg: 'Please wait...',
                        success: function(f, action) {
                            if (action.result.success)
                                ExtCommon.successMsg(action.result.data);
                            else
                                ExtCommon.errorMsg(action.result.data);

                            grid.getStore().loadPage(1);
                        },
                        failure: function(f, action) {
                            ExtCommon.errorMsg(action.result.data);
                        }
                    });
                }
            }
        })
    },

    onExportClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex);

        Ext.create('widget.form').getForm().submit({
            url: ExtCommon.baseUrl + 'loans/export',
            timeout: 30000,
            waitMsg: 'Preparing your download. Please wait!',
            title: 'Export',
            params: {
                loan_id: data.get('id')
            },
            success: function(f, action) {
                if (action.result.success) {
                    if (action.result.download) {
                        Ext.Msg.show({
                            title: 'Status',
                            msg: 'Your download is ready!',
                            buttonText: {
                                ok: "Download",
                                cancel: "Cancel"
                            },
                            fn: function(btn) {
                                if (btn == 'ok')
                                    window.location.href = action.result.download;
                            }
                        });
                    }
                }

            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    },

    statusRenderer: function (value) {
        return '<a href="javascript:">' + value + '</a>';
    }
});

Ext.define('LOAN.view.LoanStatus', {
    extend: 'Ext.window.Window',
    alias: 'widget.loanstatus',
    title: 'Loan Info',
    width: 550,
    height: 350,
    layout: 'fit',
    title: 'Loan Approval Status',
    modal: true,

    initComponent: function () {
        this.items = [{
            xtype: 'loanstatusgrid'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.LoanStatusGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.loanstatusgrid',
    draggable: false,
    movable: false,
    resizable: false,
    store: 'LoanStatus',

    initComponent: function () {

        this.tbar = [{
            xtype: 'button',
            text: 'Add Status'
        }];

        this.columns = [{
            text: 'Personnel',
            dataIndex: 'personnel',
            width: 120
        }, {
            text: 'Comment',
            dataIndex: 'comment',
            flex: 1
        }, {
            text: 'Status',
            dataIndex: 'status',
            width: 120
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            store: Ext.getStore('LoanStatus'),
            displayInfo: true,
            dock: 'bottom'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.AddLoanHistory', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addloanhistory',
    floating: true,
    modal: true,
    title: 'Add Record',
    width: 350,
    height: 250,
    layout: 'anchor',
    defaults: {
        anchor: '100%',
        allowBlank: false
    },
    bodyPadding: 10,
    border: false,
    closable: true,

    initComponent: function () {
        this.items = [{
            fieldLabel: 'Status',
            xtype: 'loanstatuscmb',
            editable: false,
            name: 'status'
        }, {
            fieldLabel: 'Comment',
            xtype: 'textareafield',
            name: 'comment'
        }];

        this.buttons = [{
            text: 'Save',
            formBind: true,
            disabled: true
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.LoanStatusCmb', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.loanstatuscmb',
    store: 'LoanStatusCmb',
    displayField: 'action',
    valueField: 'action'
});

</script>
