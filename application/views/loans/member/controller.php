<script type="text/javascript">
	Ext.define('LOAN.controller.LoanApp', {
		extend : 'Ext.app.Controller',

		init : function() {
			this.control({
				'loanapp': {
	                itemclick: this.onLoanRowClick
				},
				'loanstatus': {
					render: this.onLoanStatusRender
				},
				'loanstatusgrid button[text=Add Status]': {
					click: this.onAddStatus
				},
				'addloanhistory button[text=Save]': {
					click: this.onAddHistory
				},
				'addloanhistory button[text=Reset]': {
					click: this.onAddHistoryReset
				}
			})
		},

		views : ['LoanApp', 'LoanStatus', 'LoanStatusGrid', 'AddLoanHistory', 'LoanStatusCmb'],

		models : ['LoanApp', 'LoanStatus'],

		stores : ['LoanApp', 'LoanStatus', 'LoanStatusCmb'],

	    onLoanRowClick: function(ths, rec, item, i, e) {
	        var menu = new Ext.widget('gridmenu', {
	            record: rec,
	            items: [{
	                text: 'View Payments',
	                handler: this.onLoanViewPayments
	            }, {
	                text: 'Export',
	                handler: this.onLoanExport
	            }],
	            _e: {
	                e: e,
	                grid: ths
	            }
	        });

	        ExtCommon.showAt(menu, e, ths);
	    },

	    onLoanExport: function(ths){
	        var data = ths.up('menu').record;

	        Ext.create('widget.form').getForm().submit({
	            url: ExtCommon.baseUrl + 'loans/export',
	            timeout: 30000,
	            waitMsg: 'Preparing your download. Please wait!',
	            title: 'Export',
	            params: {
	                loan_id: data.get('id')
	            },
	            success: function(f, action) {
	                if (action.result.success) {
	                    if (action.result.download) {
	                        Ext.Msg.show({
	                            title: 'Status',
	                            msg: 'Your download is ready!',
	                            buttonText: {
	                                ok: "Download",
	                                cancel: "Cancel"
	                            },
	                            fn: function(btn) {
	                                if (btn == 'ok')
	                                    window.location.href = action.result.download;
	                            }
	                        });
	                    }
	                }

	            },
	            failure: function(f, action) {
	                ExtCommon.errorMsg(action.result.data);
	            }
	        });
	    },

	    onLoanViewPayments: function(ths) {
	        var data = ths.up('menu').record.data;

	        var payments = new LOAN.view.LoanPayments({
	            _record: data
	        }).show();

	        payments.setTitle('Payments - ' + data.name + ' ' + data.type);
	    },

		onLoanAppCellClick: function(ths, td, cellIndex, record){
			if(cellIndex == 4){
				new LOAN.view.LoanStatus({
					_record: record.data
				}).show();
			}
		},

		onLoanStatusRender: function(ths){
			var grid = ExtCommon.getCmp('loanstatusgrid');

			grid.store.proxy.extraParams['loan_id'] = ths._record.id;
			grid.store.loadPage(1);
		},

		onAddStatus: function (ths) {
			new LOAN.view.AddLoanHistory({
				_record: ths.up('window')._record
			}).show();
		},

		onAddHistory: function (ths) {
			var form = ths.up('form');

			if(!form.getForm().isValid())
				return;

			form.getForm().submit({
				url: ExtCommon.baseUrl + 'loan_status_history/create',
				waitMsg: 'Please wait...',
				params: {
					loan_id: form._record.id
				},
				success: function(f, action){
					if(action.result.success){
						ExtCommon.successMsg(action.result.data);
						form.getForm().reset();
						ExtCommon.getCmp('loanstatusgrid').store.loadPage(1);
					}
					else{
						ExtCommon.errorMsg(action.result.data);
					}
				},
				failure: function(f, action){
					ExtCommon.errorMsg(action.result.data);
				}
			});
		},

		onAddHistoryReset: function(ths){
			ths.up('form').reset();
		}
	});
</script>