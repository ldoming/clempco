<div id="ext-view"></div>

<script type="text/javascript">
Ext.define('LOAN.view.LoanApp', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.loanapp',
    store: 'LoanApp',
    cls: 'span12',
    renderTo: 'ext-view',
    height: 500,

    initComponent: function() {

        this.tbar = [{
            xtype: 'searchfield',
            store: Ext.getStore('LoanApp'),
            emptyText: 'Search',
            fieldLabel: 'Search',
            labelWidth: 60
        }, {
            xtype: 'loantype',
            fieldLabel: 'Loan Type',
            editable: false,
            labelWidth: 80
        }, {
            xtype: 'button',
            text: 'New Loan'
        }];

        this.columns = [{
            text: 'Name',
            dataIndex: 'name',
            width: 250
        }, {
            text: 'Date',
            dataIndex: 'created_at',
            width: 100
        }, {
            text: 'Type',
            dataIndex: 'type',
            width: 150
        }, {
            text: 'Status',
            dataIndex: 'status',
            width: 150
        }, {
            text: 'Amount',
            dataIndex: 'principal',
            renderer: this.money_renderer,
            width: 150
        }, {
            text: 'Payable',
            dataIndex: 'payable',
            renderer: this.money_renderer,
            width: 150
        }, {
            text: 'Deductions',
            dataIndex: 'deductions',
            width: 100
        }, {
            text: 'Balance',
            dataIndex: 'balance',
            width: 100
        }, {
            text: 'Total Payable',
            dataIndex: 'total_payable',
            width: 100,
            renderer: this.money_renderer,
            hidden: true
        }, {
            text: 'Loan Balance',
            dataIndex: 'balance',
            width: 150,
            renderer: this.money_renderer,
            hidden: true
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true,
            store: Ext.getStore('LoanApp'),
            items: [{
                xtype: 'button',
                text: 'Export'
            }]
        }];

        this.callParent(arguments);
    },

    accessAllowed: function(v, r, c, item, record) {
        return !record.get('editable');
    },

    money_renderer: function(value) {
        return ExtCommon.number_format(value, 2);
    }
});

Ext.define('LOAN.view.LoanDetails', {
    extend: 'Ext.window.Window',
    alias: 'widget.loandetails',
    width: ExtCommon.windowSize('width'),
    height: ExtCommon.windowSize('height'),
    title: 'Loan Info',
    layout: 'border',
    draggable: false,
    movable: false,
    resizable: false,
    _record: null,
    border: false,

    initComponent: function() {

        var loan = this._record && this._record.data ? this._record.data : {};

        this.items = [{
            region: 'north',
            layout: 'column',
            border: false,
            height: 110,
            bodyPadding: 10,
            items: [{
                columnWidth: 0.06,
                height: 80,
                border: false,
                items: [{
                    id: 'tb-picture',
                    xtype: 'image',
                    src: ExtCommon.baseUrl + 'includes/images/png/Contact.png',
                    width: 100
                }]
            }, {
                border: false,
                defaults: {
                    xtype: 'tbtext',
                    style: {
                        'font-size': '16px !important'
                    }
                },
                columnWidth: 0.1,
                items: [{
                    id: 'tb-name',
                    text: loan.name,
                    style: {
                        'font-size': '16px !important',
                        'font-weight': 'bold !important',
                        'margin': '10px 0px 5px 0px !important'
                    }
                }, {
                    id: 'tb-position',
                    text: loan.position
                }]
            }, {
                border: false,
                defaults: {
                    xtype: 'tbtext',
                    style: {
                        'font-size': '16px !important',
                        'margin': '10px 0px 6px 0px !important'
                    }
                },
                columnWidth: 0.15,
                items: [{
                    id: 'tb-interest',
                    text: '<b>Interest:</b> ' + loan.interest + '%'
                }, {
                    id: 'tb-period',
                    text: '<b>Period:</b> ' + loan.period + ' months'
                }, {
                    id: 'tb-monthly',
                    text: '<b>Monthly:</b> ' + ExtCommon.number_format(loan.monthly, 2)
                }]
            }, {
                border: false,
                defaults: {
                    xtype: 'tbtext',
                    style: {
                        'font-size': '16px !important',
                        'margin': '10px 0px 6px 0px !important'
                    }
                },
                columnWidth: 0.3,
                items: [{
                    id: 'tb-prinicipal',
                    text: '<b>Principal:</b> ' + ExtCommon.number_format(loan.principal, 2)
                }, {
                    id: 'tb-payable',
                    text: '<b>Payable:</b> ' + ExtCommon.number_format(loan.total_payable, 2)
                }, {
                    id: 'tb-balance',
                    text: '<b>Balance:</b> ' + ExtCommon.number_format(loan.balance, 2)
                }]
            }]
        }, {
            region: 'center',
            xtype: 'paymenthistory'
        }];

        this.buttons = [{
            xtype: 'button',
            text: 'Refresh'
        }, {
            xtype: 'button',
            text: 'Close'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.PaymentHistory', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.paymenthistory',
    store: 'PaymentHistory',
    features: {
        ftype: 'summary'
    },

    initComponent: function() {

        this.tbar = [{
            xtype: 'button',
            text: 'Add Payment'
        }];

        this.columns = [{
            xtype: 'rownumberer',
            width: 40
        }, {
            text: 'Date',
            dataIndex: 'pay_date',
            width: 150
        }, {
            text: 'Payment',
            dataIndex: 'payment',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Payable',
            dataIndex: 'payable',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Principal',
            dataIndex: 'principal',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Interest',
            dataIndex: 'interest',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Running Balance',
            dataIndex: 'running_balance',
            renderer: this.moneyRenderer,
            align: 'right',
            width: 150
        }, {
            text: 'Over/Under',
            dataIndex: 'ou',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Remarks',
            dataIndex: 'remarks',
            flex: 1
        }, {
            xtype: 'actioncolumn',
            text: 'Options',
            items: [{
                icon: ExtCommon.baseUrl + 'includes/images/png/Xion.png',
                tooltip: 'Delte',
                handler: this.onDeleteClick
            }]
        }];

        this.callParent(arguments);
    },

    moneyRenderer: function(value) {
        return ExtCommon.number_format(value, 2);
    },

    summaryRenderer: function(value) {
        return ExtCommon.number_format(Math.round(value), 2);
    },

    onDeleteClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        Ext.Msg.show({
            msg: 'Delete this record?',
            title: 'Confirm',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn) {
                if (btn == 'ok') {
                    Ext.widget('form').getForm().submit({
                        url: ExtCommon.baseUrl + 'loan_payments/destroy',
                        params: {
                            payment_id: data.id
                        },
                        waitMsg: 'Please wait...',
                        success: function(f, action) {
                            if (action.result.success)
                                ExtCommon.successMsg(action.result.data);
                            else
                                ExtCommon.errorMsg(action.result.data);

                            grid.getStore().loadPage(1);
                        },
                        failure: function(f, action) {
                            ExtCommon.errorMsg(action.result.data);
                        }
                    });
                }
            }
        })
    }
});

Ext.define('LOAN.view.AddPayment', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addpayment',
    layout: 'anchor',
    floating: true,
    width: 350,
    height: 250,
    modal: true,
    closable: true,
    title: 'Add Payment',
    border: false,
    bodyPadding: 10,
    defaults: {
        allowBlank: false,
        labelWidth: 100,
        anchor: '100%'
    },

    initComponent: function() {
        this.items = [{
            xtype: 'datefield',
            name: 'pay_date',
            fieldLabel: 'Period',
            format: 'M-d'
        }, {
            xtype: 'numberfield',
            name: 'payment',
            fieldLabel: 'Amount'
        }, {
            xtype: 'textareafield',
            name: 'remarks',
            fieldLabel: 'Remarks',
            allowBlank: true
        }];

        this.buttons = [{
            text: 'Save Payment'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

</script>
