<script type="text/javascript">
	Ext.define('LOAN.controller.LoanApp', {
		extend : 'Ext.app.Controller',

		init : function() {
			this.control({
				'loanapp': {
					cellclick: this.onLoanAppCellClick
				},
				'loanstatus': {
					render: this.onLoanStatusRender
				},
				'loanstatusgrid button[text=Add Status]': {
					click: this.onAddStatus
				},
				'addloanhistory button[text=Save]': {
					click: this.onAddHistory
				},
				'addloanhistory button[text=Reset]': {
					click: this.onAddHistoryReset
				}
			})
		},

		views : ['LoanApp', 'LoanStatus', 'LoanStatusGrid', 'AddLoanHistory', 'LoanStatusCmb'],

		models : ['LoanApp', 'LoanStatus'],

		stores : ['LoanApp', 'LoanStatus', 'LoanStatusCmb'],

		onLoanAppCellClick: function(ths, td, cellIndex, record){
			if(cellIndex == 4){
				new LOAN.view.LoanStatus({
					_record: record.data
				}).show();
			}
		},

		onLoanStatusRender: function(ths){
			var grid = ExtCommon.getCmp('loanstatusgrid');

			grid.store.proxy.extraParams['loan_id'] = ths._record.id;
			grid.store.loadPage(1);
		},

		onAddStatus: function (ths) {
			new LOAN.view.AddLoanHistory({
				_record: ths.up('window')._record
			}).show();
		},

		onAddHistory: function (ths) {
			var form = ths.up('form');

			if(!form.getForm().isValid())
				return;

			form.getForm().submit({
				url: ExtCommon.baseUrl + 'loan_status_history/create',
				waitMsg: 'Please wait...',
				params: {
					loan_id: form._record.id
				},
				success: function(f, action){
					if(action.result.success){
						ExtCommon.successMsg(action.result.data);
						form.getForm().reset();
						ExtCommon.getCmp('loanstatusgrid').store.loadPage(1);
					}
					else{
						ExtCommon.errorMsg(action.result.data);
					}
				},
				failure: function(f, action){
					ExtCommon.errorMsg(action.result.data);
				}
			});
		},

		onAddHistoryReset: function(ths){
			ths.up('form').reset();
		}
	});
</script>