<script type="text/javascript">

	Ext.define('LOAN.model.LoanStatus', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'personnel',
			type : 'string'
		}, {
			name : 'comment',
			type : 'string'
		}, {
			name : 'status',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.LoanStatus', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.LoanStatus',
		storeId: 'LoanStatus',
		queryMode : 'remote',
		autoLoad: false,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'loan_status_history/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

	Ext.define('LOAN.store.LoanStatusCmb', {
		extend: 'Ext.data.Store',
		fields: [{
			name: 'action',
			type: 'string'
		}],
		data: [{
			action: 'Approved'
		}, {
			action: 'Declined'
		}]
	});

</script>