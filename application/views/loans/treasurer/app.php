<script type="text/javascript" src="<?php echo base_url();?>includes/js/SearchField.js"></script>

<script type="text/javascript">
	Ext.application({

		requires : ['Ext.container.Viewport', 'Ext.form.SearchField'],

		name : 'LOAN',

		appFolder : 'loan_application',

		controllers : ['Common', 'LoanApp'],

		launch : function() {
			new LOAN.view.LoanApp();
		}
	}); 
</script>