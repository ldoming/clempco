<script type="text/javascript">
	Ext.define('LOAN.controller.LoanApp', {
		extend : 'Ext.app.Controller',

		init : function() {
			this.control({
				'loanapp button[text=New Record]' : {
					click : this.onNewRecord
				},
				'applicantsdd' : {
					select : this.onSelectApplicant
				},
				'applicants button[text=New Member]' : {
					click : this.onNewMemberClick
				},
				'loanappadd button[text=Save Record]' : {
					click : this.onSaveRecord
				},
				'loandetails' : {
					show : this.onLoanDetailsShow
				},
				'loandetails button[text=Refresh]' : {
					click : this.onRefreshClick
				},
				'loandetails button[text=Close]' : {
					click : this.onCloseClick
				},
				'paymenthistory button[text=Add Payment]' : {
					click : this.onAddPaymentClick
				},
				'addpayment button[text=Save Payment]' : {
					click : this.onSavePaymentClick
				},
				'addpayment button[text=Reset]' : {
					click : this.onResetClick
				},
				'loanapp loantype': {
					select: this.onLoanTypeSelect
				}
			})
		},

		views : ['LoanApp', 'LoanAppAdd', 'Applicants', 'Applicantsdd', 'PaymentHistory', 'LoanDetails', 'AddPayment'],

		models : ['LoanApp', 'Applicant', 'Applicantdd', 'PaymentRow'],

		stores : ['LoanApp', 'Applicants', 'Applicantsdd', 'PaymentHistory'],

		onNewRecord : function() {
			new LOAN.view.LoanAppAdd().show();
		},

		onSelectApplicant : function(ths, rec) {
			var grid = ths.up('grid'), store = grid.store, data = rec[0].data;

			var found = store.findBy(function(row) {
				if (row.data.id == data.id) {
					return true;
				} else {
					return false;
				}
			});

			if (found == -1) {
				store.add(data);
			}

			ths.reset();

		},

		onSaveRecord : function(ths) {
			var form = ths.up('form'), applicants = ExtCommon.jsonData(ExtCommon.getCmp('applicants').store);

			if (!form.getForm().isValid())
				return;

			form.getForm().submit({
				url : ExtCommon.baseUrl + 'loans/create',
				params : {
					// applicants : Ext.encode(applicants)
				},
				waitMsg : 'Please wait...',
				success : function(f, action) {
					if (action.result.success)
						ExtCommon.successMsg(action.result.data);
					else
						ExtCommon.errorMsg(action.result.data);

					form.getForm().reset();
					ExtCommon.getCmp('loanapp').store.loadPage(1);
				},
				failure : function(f, action) {
					ExtCommon.errorMsg(action.result.data);
				}
			})
		},

		onLoanDetailsShow : function(ths) {
			var grid = ths.down('grid'), store = grid.getStore();

			store.proxy.extraParams['loan_id'] = ths._record.get('id');
			store.loadPage(1);
		},

		onRefreshClick : function(ths) {
			ths.up('window').down('grid').store.loadPage(1);
		},

		onCloseClick : function(ths) {
			ths.up('window').close();
		},

		onAddPaymentClick : function(ths) {
			new LOAN.view.AddPayment().show();
		},

		onSavePaymentClick : function(ths) {
			var form = ths.up('form');

			if (!form.getForm().isValid())
				return;

			form.getForm().submit({
				url : ExtCommon.baseUrl + 'loan_payments/create',
				params : {
					loan_info : Ext.encode(ExtCommon.getCmp('loandetails')._record.data)
				},
				waitMsg : 'Please wait',
				success : function(f, action) {
					if (action.result.success)
						ExtCommon.successMsg(action.result.data);
					else
						ExtCommon.errorMsg(action.result.data);

					form.getForm().reset();
					ExtCommon.getCmp('paymenthistory').store.loadPage(1);
				},
				failure : function(f, action) {
					ExtCommon.errorMsg(action.result.data);
				}
			});
		},

		onResetClick : function(ths) {
			ths.up('form').getForm().reset();
		},
		
		onNewMemberClick: function(){
			new LOAN.view.common.AddMember().show();
		},

		onLoanTypeSelect: function(ths){
			var grid = ths.up('grid');

			grid.store.proxy.extraParams['loan_type'] = ths.getValue();
			grid.store.load();
		}
	});

</script>