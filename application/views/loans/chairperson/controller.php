<script type="text/javascript">
	Ext.define('LOAN.controller.LoanApp', {
		extend : 'Ext.app.Controller',

		init : function() {
			this.control({
				'loanapp button[text=New Record]' : {
					click : this.onNewRecord
				},
				'applicantsdd' : {
					select : this.onSelectApplicant
				},
				'applicants button[text=New Member]' : {
					click : this.onNewMemberClick
				},
				'loanappadd button[text=Save Record]' : {
					click : this.onSaveRecord
				},
				'loandetails' : {
					show : this.onLoanDetailsShow
				},
				'loandetails button[text=Refresh]' : {
					click : this.onRefreshClick
				},
				'loandetails button[text=Close]' : {
					click : this.onCloseClick
				},
				'paymenthistory button[text=Add Payment]' : {
					click : this.onAddPaymentClick
				},
				'addpayment button[text=Save Payment]' : {
					click : this.onSavePaymentClick
				},
				'addpayment button[text=Reset]' : {
					click : this.onResetClick
				},
				'loanapp loantype': {
					select: this.onLoanTypeSelect
				},
				'loanapp': {
					cellclick: this.onLoanAppCellClick
				},
				'loanstatus': {
					render: this.onLoanStatusRender
				},
				'loanstatusgrid button[text=Add Status]': {
					click: this.onAddStatus
				},
				'addloanhistory button[text=Save]': {
					click: this.onAddHistory
				},
				'addloanhistory button[text=Reset]': {
					click: this.onAddHistoryReset
				}
			})
		},

		views : ['LoanApp', 'LoanAppAdd', 'Applicants', 'Applicantsdd', 'PaymentHistory', 'LoanDetails', 'AddPayment', 'LoanStatusGrid', 'AddLoanHistory', 'LoanStatusCmb'],

		models : ['LoanApp', 'Applicant', 'Applicantdd', 'PaymentRow'],

		stores : ['LoanApp', 'Applicants', 'Applicantsdd', 'PaymentHistory', 'LoanStatusCmb'],

		onNewRecord : function() {
			new LOAN.view.LoanAppAdd().show();
		},

		onSelectApplicant : function(ths, rec) {
			var grid = ths.up('grid'), store = grid.store, data = rec[0].data;

			var found = store.findBy(function(row) {
				if (row.data.id == data.id) {
					return true;
				} else {
					return false;
				}
			});

			if (found == -1) {
				store.add(data);
			}

			ths.reset();

		},

		onSaveRecord : function(ths) {
			var form = ths.up('form'), applicants = ExtCommon.jsonData(ExtCommon.getCmp('applicants').store);

			if (!form.getForm().isValid())
				return;

			form.getForm().submit({
				url : ExtCommon.baseUrl + 'loans/create',
				params : {
					// applicants : Ext.encode(applicants)
				},
				waitMsg : 'Please wait...',
				success : function(f, action) {
					if (action.result.success)
						ExtCommon.successMsg(action.result.data);
					else
						ExtCommon.errorMsg(action.result.data);

					form.getForm().reset();
					ExtCommon.getCmp('loanapp').store.loadPage(1);
				},
				failure : function(f, action) {
					ExtCommon.errorMsg(action.result.data);
				}
			})
		},

		onLoanDetailsShow : function(ths) {
			var grid = ths.down('grid'), store = grid.getStore();

			store.proxy.extraParams['loan_id'] = ths._record.get('id');
			store.loadPage(1);
		},

		onRefreshClick : function(ths) {
			ths.up('window').down('grid').store.loadPage(1);
		},

		onCloseClick : function(ths) {
			ths.up('window').close();
		},

		onAddPaymentClick : function(ths) {
			new LOAN.view.AddPayment().show();
		},

		onSavePaymentClick : function(ths) {
			var form = ths.up('form');

			if (!form.getForm().isValid())
				return;

			form.getForm().submit({
				url : ExtCommon.baseUrl + 'loan_payments/create',
				params : {
					loan_info : Ext.encode(ExtCommon.getCmp('loandetails')._record.data)
				},
				waitMsg : 'Please wait',
				success : function(f, action) {
					if (action.result.success)
						ExtCommon.successMsg(action.result.data);
					else
						ExtCommon.errorMsg(action.result.data);

					form.getForm().reset();
					ExtCommon.getCmp('paymenthistory').store.loadPage(1);
				},
				failure : function(f, action) {
					ExtCommon.errorMsg(action.result.data);
				}
			});
		},

		onResetClick : function(ths) {
			ths.up('form').getForm().reset();
		},
		
		onNewMemberClick: function(){
			new LOAN.view.common.AddMember().show();
		},

		onLoanTypeSelect: function(ths){
			var grid = ths.up('grid');

			grid.store.proxy.extraParams['loan_type'] = ths.getValue();
			grid.store.load();
		},
		
		onLoanAppCellClick: function(ths, td, cellIndex, record){
			if(cellIndex == 3){
				new LOAN.view.LoanStatus({
					_record: record.data
				}).show();
			}
		},

		onLoanStatusRender: function(ths){
			var grid = ExtCommon.getCmp('loanstatusgrid');

			grid.store.proxy.extraParams['loan_id'] = ths._record.id;
			grid.store.loadPage(1);
		},

		onAddStatus: function (ths) {
			new LOAN.view.AddLoanHistory({
				_record: ths.up('window')._record
			}).show();
		},

		onAddHistory: function (ths) {
			var form = ths.up('form');

			if(!form.getForm().isValid())
				return;

			form.getForm().submit({
				url: ExtCommon.baseUrl + 'loan_status_history/create',
				waitMsg: 'Please wait...',
				params: {
					loan_id: form._record.id
				},
				success: function(f, action){
					if(action.result.success){
						ExtCommon.successMsg(action.result.data);
						form.getForm().reset();
						ExtCommon.getCmp('loanstatusgrid').store.loadPage(1);
					}
					else{
						ExtCommon.errorMsg(action.result.data);
					}
				},
				failure: function(f, action){
					ExtCommon.errorMsg(action.result.data);
				}
			});
		},

		onAddHistoryReset: function(ths){
			ths.up('form').reset();
		}
	});

</script>