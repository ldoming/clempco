<script type="text/javascript">

	Ext.define('LOAN.model.Applicant', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'name',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.Applicants', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.Applicant',
		storeId: 'Applicants',
		queryMode : 'remote',
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'members/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

	Ext.define('LOAN.model.Applicantdd', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'name',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.Applicantsdd', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.Applicantdd',
		storeId: 'Applicantsdd',
		queryMode : 'remote',
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'members/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

	Ext.define('LOAN.model.PaymentRow', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'pay_date',
			type : 'string'
		}, {
			name : 'payment',
			type : 'float'
		}, {
			name : 'payable',
			type : 'float'
		}, {
			name : 'principal',
			type : 'float'
		}, {
			name : 'interest',
			type : 'float'
		}, {
			name : 'running_balance',
			type : 'float'
		}, {
			name : 'ou',
			type : 'float'
		}, {
			name : 'remarks',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.PaymentHistory', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.PaymentRow',
		storeId: 'PaymentHistory',
		queryMode : 'remote',
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'loan_payments/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

	Ext.define('LOAN.model.LoanStatus', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'personnel',
			type : 'string'
		}, {
			name : 'comment',
			type : 'string'
		}, {
			name : 'status',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.LoanStatus', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.LoanStatus',
		storeId: 'LoanStatus',
		queryMode : 'remote',
		autoLoad: false,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'loan_status_history/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

	Ext.define('LOAN.store.LoanStatusCmb', {
		extend: 'Ext.data.Store',
		fields: [{
			name: 'action',
			type: 'string'
		}],
		data: [{
			action: 'Approved'
		}, {
			action: 'Declined'
		}]
	});

</script>