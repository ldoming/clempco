<div id="ext-view"></div>

<script type="text/javascript">
Ext.define('LOAN.view.LoanApp', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.loanapp',
    store: 'LoanApp',
    cls: 'span12',
    renderTo: 'ext-view',
    height: 500,

    initComponent: function() {

        this.tbar = [{
            xtype: 'searchfield',
            store: Ext.getStore('LoanApp'),
            emptyText: 'Search',
            fieldLabel: 'Search',
            labelWidth: 60
        }, {
            xtype: 'loantype',
            fieldLabel: 'Loan Type',
            editable: false,
            labelWidth: 80
        }, {
            xtype: 'button',
            text: 'New Record',
            hidden: true
        }];

        this.columns = [{
            text: 'Name',
            dataIndex: 'name',
            width: 250
        }, {
            text: 'Date',
            dataIndex: 'created_at',
            width: 100
        }, {
            text: 'Type',
            dataIndex: 'type',
            width: 150
        }, {
            text: 'Status',
            dataIndex: 'status',
            width: 150,
            renderer: this.statusRenderer
        }, {
            text: 'Amount',
            dataIndex: 'principal',
            renderer: this.money_renderer,
            width: 150
        }, {
            text: 'Payable',
            dataIndex: 'payable',
            renderer: this.money_renderer,
            width: 150
        }, {
            text: 'Deductions',
            dataIndex: 'deductions',
            width: 100
        }, {
            text: 'Balance',
            dataIndex: 'balance',
            width: 100
        }, {
            text: 'Total Payable',
            dataIndex: 'total_payable',
            width: 100,
            renderer: this.money_renderer,
            hidden: true
        }, {
            text: 'Loan Balance',
            dataIndex: 'balance',
            width: 150,
            renderer: this.money_renderer,
            hidden: true
        }, {
            xtype: 'actioncolumn',
            text: 'Options',
            width: 100,
            defaults: {
                style: {
                    margin: '0px 5px !important'
                }
            },
            text: 'Actions',
            items: [{
                icon: ExtCommon.baseUrl + 'includes/images/png/Excel-XLS-file.png',
                tooltip: 'Export',
                handler: this.onExportClick,
                text: 'Action'
            }, {
                icon: ExtCommon.baseUrl + 'includes/images/png/Explorer.png',
                tooltip: 'Payments',
                handler: this.onOptionsClick,
                isDisabled: this.accessAllowed
            }, {
                icon: ExtCommon.baseUrl + 'includes/images/png/Xion.png',
                tooltip: 'Delete',
                handler: this.onDeleteClick,
                isDisabled: this.accessAllowed
            }]
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true,
            store: Ext.getStore('LoanApp'),
            items: [{
                xtype: 'button',
                text: 'Export'
            }]
        }];

        this.callParent(arguments);
    },

    accessAllowed: function(v, r, c, item, record){
        return !record.get('editable');
    },

    money_renderer: function(value) {
        return ExtCommon.number_format(value, 2);
    },

    onOptionsClick: function(grid, rowIndex, colIndex, re, event) {
        new LOAN.view.LoanDetails({
            _record: grid.getStore().getAt(rowIndex)
        }).show();
    },

    onDeleteClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        Ext.Msg.show({
            msg: 'Delete this record?',
            title: 'Confirm',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn) {
                if (btn == 'ok') {
                    Ext.widget('form').getForm().submit({
                        url: ExtCommon.baseUrl + 'loans/destroy',
                        params: {
                            loan_info: Ext.encode(data)
                        },
                        waitMsg: 'Please wait...',
                        success: function(f, action) {
                            if (action.result.success)
                                ExtCommon.successMsg(action.result.data);
                            else
                                ExtCommon.errorMsg(action.result.data);

                            grid.getStore().loadPage(1);
                        },
                        failure: function(f, action) {
                            ExtCommon.errorMsg(action.result.data);
                        }
                    });
                }
            }
        })
    },

    onExportClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex);

        Ext.create('widget.form').getForm().submit({
            url: ExtCommon.baseUrl + 'loans/export',
            timeout: 30000,
            waitMsg: 'Preparing your download. Please wait!',
            title: 'Export',
            params: {
                loan_id: data.get('id')
            },
            success: function(f, action) {
                if (action.result.success) {
                    if (action.result.download) {
                        Ext.Msg.show({
                            title: 'Status',
                            msg: 'Your download is ready!',
                            buttonText: {
                                ok: "Download",
                                cancel: "Cancel"
                            },
                            fn: function(btn) {
                                if (btn == 'ok')
                                    window.location.href = action.result.download;
                            }
                        });
                    }
                }

            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    },

    statusRenderer: function (value) {
        return '<a href="javascript:">' + value + '</a>';
    }
});

Ext.define('LOAN.view.LoanAppAdd', {
    extend: 'Ext.form.Panel',
    alias: 'widget.loanappadd',
    modal: true,
    floating: true,
    title: 'New Loan Application',
    bodyPadding: 15,
    width: 450,
    layout: 'anchor',
    frame: true,
    autoScroll: true,
    defaults: {
        anchor: '100%',
        style: {
            'margin-bottom': '10px'
        },
        minValue: 0
    },
    closable: true,
    draggable: true,

    initComponent: function() {

        this.items = [{
            xtype: 'members',
            fieldLabel: 'Select Member',
            name: 'member_id'
        }, {
            xtype: 'loantype',
            fieldLabel: 'Loan Type',
            name: 'loan_type'
        }, {
            xtype: 'datefield',
            name: 'date_applied',
            fieldLabel: 'Date Applied'
        }, {
            xtype: 'numberfield',
            fieldLabel: 'Loan Amount',
            name: 'principal'
        }, {
            xtype: 'loanterms',
            fieldLabel: 'Loan Term',
            name: 'terms'
        }, {
            xtype: 'container',
            layout: 'column',
            items: [{
                xtype: 'numberfield',
                fieldLabel: 'Interest Rate',
                name: 'interest',
                minValue: 1
            }, {
                xtype: 'tbtext',
                text: '&nbsp;%',
                style: {
                    'margin-top': '4px'
                }
            }]
        }, {
            xtype: 'container',
            layout: 'column',
            items: [{
                xtype: 'numberfield',
                fieldLabel: 'Payment Period',
                name: 'period',
                minValue: 1,
                step: 12
            }, {
                xtype: 'tbtext',
                text: '&nbsp;Months',
                style: {
                    'margin-top': '4px'
                }
            }]
        }, {
            anchor: '100%',
            xtype: 'fieldset',
            title: 'Applicant Info',
            layout: 'fit',
            padding: '5 0 0 0',
            hidden: true,
            style: {
                'border-bottom': 'none !important',
                'border-left': 'none !important',
                'border-right': 'none !important'
            },
            items: [{
                xtype: 'applicants',
                allowBlank: false
            }]
        }];

        this.buttons = [{
            text: 'Save Record'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.Applicants', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.applicants',
    store: 'Applicants',

    initComponent: function() {

        this.tbar = [{
            xtype: 'applicantsdd',
            emptyText: 'Search',
            fieldLabel: 'Select Applicant',
            labelWidth: 100,
            flex: 1
        }, {
            xtype: 'button',
            text: 'New Member'
        }];

        this.columns = [{
            text: 'name',
            dataIndex: 'name',
            flex: 1
        }, {
            xtype: 'actioncolumn',
            width: 40,
            items: [{
                icon: ExtCommon.baseUrl + 'includes/images/png/Xion.png',
                tooltip: 'Delete Record',
                handler: this.onDeleteClick
            }]
        }];

        this.callParent(arguments);
    },

    onDeleteClick: function(grid, rowIndex) {
        grid.store.removeAt(rowIndex);
    }
});

Ext.define('LOAN.view.Applicantsdd', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.applicantsdd',
    displayField: 'name',
    valueField: 'id',
    store: 'Applicantsdd'
});

Ext.define('LOAN.view.LoanDetails', {
    extend: 'Ext.window.Window',
    alias: 'widget.loandetails',
    width: ExtCommon.windowSize('width'),
    height: ExtCommon.windowSize('height'),
    title: 'Loan Info',
    layout: 'border',
    draggable: false,
    movable: false,
    resizable: false,
    _record: null,
    border: false,

    initComponent: function() {

        var loan = this._record && this._record.data ? this._record.data : {};

        this.items = [{
            region: 'north',
            layout: 'column',
            border: false,
            height: 110,
            bodyPadding: 10,
            items: [{
                columnWidth: 0.06,
                height: 80,
                border: false,
                items: [{
                    id: 'tb-picture',
                    xtype: 'image',
                    src: ExtCommon.baseUrl + 'includes/images/png/Contact.png',
                    width: 100
                }]
            }, {
                border: false,
                defaults: {
                    xtype: 'tbtext',
                    style: {
                        'font-size': '16px !important'
                    }
                },
                columnWidth: 0.1,
                items: [{
                    id: 'tb-name',
                    text: loan.name,
                    style: {
                        'font-size': '16px !important',
                        'font-weight': 'bold !important',
                        'margin': '10px 0px 5px 0px !important'
                    }
                }, {
                    id: 'tb-position',
                    text: loan.position
                }]
            }, {
                border: false,
                defaults: {
                    xtype: 'tbtext',
                    style: {
                        'font-size': '16px !important',
                        'margin': '10px 0px 6px 0px !important'
                    }
                },
                columnWidth: 0.15,
                items: [{
                    id: 'tb-interest',
                    text: '<b>Interest:</b> ' + loan.interest + '%'
                }, {
                    id: 'tb-period',
                    text: '<b>Period:</b> ' + loan.period + ' months'
                }, {
                    id: 'tb-monthly',
                    text: '<b>Monthly:</b> ' + ExtCommon.number_format(loan.monthly, 2)
                }]
            }, {
                border: false,
                defaults: {
                    xtype: 'tbtext',
                    style: {
                        'font-size': '16px !important',
                        'margin': '10px 0px 6px 0px !important'
                    }
                },
                columnWidth: 0.3,
                items: [{
                    id: 'tb-prinicipal',
                    text: '<b>Principal:</b> ' + ExtCommon.number_format(loan.principal, 2)
                }, {
                    id: 'tb-payable',
                    text: '<b>Payable:</b> ' + ExtCommon.number_format(loan.total_payable, 2)
                }, {
                    id: 'tb-balance',
                    text: '<b>Balance:</b> ' + ExtCommon.number_format(loan.balance, 2)
                }]
            }]
        }, {
            region: 'center',
            xtype: 'paymenthistory'
        }];

        this.buttons = [{
            xtype: 'button',
            text: 'Refresh'
        }, {
            xtype: 'button',
            text: 'Close'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.PaymentHistory', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.paymenthistory',
    store: 'PaymentHistory',
    features: {
        ftype: 'summary'
    },

    initComponent: function() {

        this.tbar = [{
            xtype: 'button',
            text: 'Add Payment'
        }];

        this.columns = [{
            xtype: 'rownumberer',
            width: 40
        }, {
            text: 'Date',
            dataIndex: 'pay_date',
            width: 150
        }, {
            text: 'Payment',
            dataIndex: 'payment',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Payable',
            dataIndex: 'payable',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Principal',
            dataIndex: 'principal',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Interest',
            dataIndex: 'interest',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Running Balance',
            dataIndex: 'running_balance',
            renderer: this.moneyRenderer,
            align: 'right',
            width: 150
        }, {
            text: 'Over/Under',
            dataIndex: 'ou',
            renderer: this.moneyRenderer,
            summaryRenderer: this.summaryRenderer,
            align: 'right',
            summaryType: 'sum',
            width: 150
        }, {
            text: 'Remarks',
            dataIndex: 'remarks',
            flex: 1
        }, {
            xtype: 'actioncolumn',
            text: 'Options',
            items: [{
                icon: ExtCommon.baseUrl + 'includes/images/png/Xion.png',
                tooltip: 'Delte',
                handler: this.onDeleteClick
            }]
        }];

        this.callParent(arguments);
    },

    moneyRenderer: function(value) {
        return ExtCommon.number_format(value, 2);
    },

    summaryRenderer: function(value) {
        return ExtCommon.number_format(Math.round(value), 2);
    },

    onDeleteClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        Ext.Msg.show({
            msg: 'Delete this record?',
            title: 'Confirm',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn) {
                if (btn == 'ok') {
                    Ext.widget('form').getForm().submit({
                        url: ExtCommon.baseUrl + 'loan_payments/destroy',
                        params: {
                            payment_id: data.id
                        },
                        waitMsg: 'Please wait...',
                        success: function(f, action) {
                            if (action.result.success)
                                ExtCommon.successMsg(action.result.data);
                            else
                                ExtCommon.errorMsg(action.result.data);

                            grid.getStore().loadPage(1);
                        },
                        failure: function(f, action) {
                            ExtCommon.errorMsg(action.result.data);
                        }
                    });
                }
            }
        })
    }
});

Ext.define('LOAN.view.AddPayment', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addpayment',
    layout: 'anchor',
    floating: true,
    width: 350,
    height: 250,
    modal: true,
    closable: true,
    title: 'Add Payment',
    border: false,
    bodyPadding: 10,
    defaults: {
        allowBlank: false,
        labelWidth: 100,
        anchor: '100%'
    },

    initComponent: function() {
        this.items = [{
            xtype: 'datefield',
            name: 'pay_date',
            fieldLabel: 'Period',
            format: 'M-d'
        }, {
            xtype: 'numberfield',
            name: 'payment',
            fieldLabel: 'Amount'
        }, {
            xtype: 'textareafield',
            name: 'remarks',
            fieldLabel: 'Remarks',
            allowBlank: true
        }];

        this.buttons = [{
            text: 'Save Payment'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.LoanStatus', {
    extend: 'Ext.window.Window',
    alias: 'widget.loanstatus',
    title: 'Loan Info',
    width: 550,
    height: 350,
    layout: 'fit',
    title: 'Loan Approval Status',
    modal: true,

    initComponent: function () {
        this.items = [{
            xtype: 'loanstatusgrid'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.LoanStatusGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.loanstatusgrid',
    draggable: false,
    movable: false,
    resizable: false,
    store: 'LoanStatus',

    initComponent: function () {

        this.tbar = [{
            xtype: 'button',
            text: 'Add Status'
        }];

        this.columns = [{
            text: 'Personnel',
            dataIndex: 'personnel',
            width: 120
        }, {
            text: 'Comment',
            dataIndex: 'comment',
            flex: 1
        }, {
            text: 'Status',
            dataIndex: 'status',
            width: 120
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            store: Ext.getStore('LoanStatus'),
            displayInfo: true,
            dock: 'bottom'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.AddLoanHistory', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addloanhistory',
    floating: true,
    modal: true,
    title: 'Add Record',
    width: 350,
    height: 250,
    layout: 'anchor',
    defaults: {
        anchor: '100%',
        allowBlank: false
    },
    bodyPadding: 10,
    border: false,
    closable: true,

    initComponent: function () {
        this.items = [{
            fieldLabel: 'Status',
            xtype: 'loanstatuscmb',
            editable: false,
            name: 'status'
        }, {
            fieldLabel: 'Comment',
            xtype: 'textareafield',
            name: 'comment'
        }];

        this.buttons = [{
            text: 'Save',
            formBind: true,
            disabled: true
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.LoanStatusCmb', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.loanstatuscmb',
    store: 'LoanStatusCmb',
    displayField: 'action',
    valueField: 'action'
});

</script>
