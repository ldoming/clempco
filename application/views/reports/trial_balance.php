<div class="span12" style="width:1324px !important">
	<table class="table striped3n">
		<thead>
			<tr>
			    <th class="text-center">Account</th>
			    <th class="text-center">&nbsp;</th>
			    <th class="text-center" colspan="2">Beginning Balances</th>
			    <th class="text-center" colspan="2">Cash Vouchers</th>
			    <th class="text-center" colspan="2">journal Vouchers</th>
			    <th class="text-center" colspan="2">Total Transactions</th>
			    <th class="text-center" colspan="2">End Balances</th>
			</tr>
			<tr>
			    <th class="text-center">Code</th>
			    <th class="text-center" width="250px">Description</th>
			    <th class="text-center">Debit</th>
			    <th class="text-center">Credit</th>
			    <th class="text-center">Debit</th>
			    <th class="text-center">Credit</th>
			    <th class="text-center">Debit</th>
			    <th class="text-center">Credit</th>
			    <th class="text-center">Debit</th>
			    <th class="text-center">Credit</th>
			    <th class="text-center">Debit</th>
			    <th class="text-center">Credit</th>
			</tr>
		</thead>

		<tbody>
			<?php foreach($grp_accounts as $key => $value) { ?>
				<tr>
					<td class="text-center"><?php echo ($value['acct_code']);?></td>
					<td class="text-left"><?php echo ($value['acct_name']);?></td>

					<!-- opening/beginning balance -->
					<?php $ob_debit = ($value['ob_type'] == 'Debit' ? $value['ob_amount'] : 0);?>
					<?php $ob_credit = ($value['ob_type'] == 'Credit' ? $value['ob_amount'] : 0);?>
					<td><?php echo float_to_money($ob_debit);?></td>
					<td><?php echo float_to_money($ob_credit);?></td>

					<!-- Cash Vouchers balance -->
					<?php list($cv_debit, $cv_credit) = $this->loan_vouchers->get_totals($value['acct_code'], 'Cash Voucher', $period);?>
					<td><?php echo float_to_money($cv_debit);?></td>
					<td><?php echo float_to_money($cv_credit);?></td>

					<!-- Journal Vouchers balance -->
					<?php list($jv_debit, $jv_credit) = $this->loan_vouchers->get_totals($value['acct_code'], 'Journal Voucher', $period);?>
					<td><?php echo float_to_money($jv_debit);?></td>
					<td><?php echo float_to_money($jv_credit);?></td>

					<!-- Total transactions -->
					<td><?php echo float_to_money($cv_debit + $jv_debit);?></td>
					<td><?php echo float_to_money($cv_credit + $jv_credit);?></td>

					<!-- Ending Balance -->
					<td><?php if($ob_debit > 0){ 
						echo float_to_money(($ob_debit + ($cv_debit + $jv_debit)) - ($cv_credit + $jv_credit));
					}else{
						echo float_to_money(0);
					}
					?></td>
					<td><?php if($ob_credit > 0) {
						echo float_to_money(($ob_credit + ($cv_credit + $jv_credit)) - ($cv_debit + $jv_debit));
					}else{
						echo float_to_money(0);
					}
					?></td>

				</tr>
					<?php $accounts = $this->loan_accounts->get_by_acct($value['id']);?>
					<?php if(count($accounts) == 0) continue;?>

					<?php foreach ($accounts as $kkey => $vvalue) { ?>
						<tr>
							<td class="text-center"><?php echo ($vvalue['acct_code']);?></td>
							<td class="text-right"><?php echo ($vvalue['acct_name']);?></td>

							<!-- opening/beginning balance -->
							<?php $ob_debit = ($vvalue['ob_type'] == 'Debit' ? $vvalue['ob_amount'] : 0);?>
							<?php $ob_credit = ($vvalue['ob_type'] == 'Credit' ? $vvalue['ob_amount'] : 0);?>
							<td><?php echo float_to_money($ob_debit);?></td>
							<td><?php echo float_to_money($ob_credit);?></td>

							<!-- Cash Vouchers balance -->
							<?php list($cv_debit, $cv_credit) = $this->loan_vouchers->get_totals($vvalue['id'], 'Cash Voucher');?>
							<td><?php echo float_to_money($cv_debit);?></td>
							<td><?php echo float_to_money($cv_credit);?></td>

							<!-- Journal Vouchers balance -->
							<?php list($jv_debit, $jv_credit) = $this->loan_vouchers->get_totals($vvalue['id'], 'Journal Voucher');?>
							<td><?php echo float_to_money($jv_debit);?></td>
							<td><?php echo float_to_money($jv_credit);?></td>

							<!-- Total transactions -->
							<td><?php echo float_to_money($cv_debit + $jv_debit);?></td>
							<td><?php echo float_to_money($cv_credit + $jv_credit);?></td>

							<!-- Ending Balance -->
							<td><?php if($ob_debit > 0){ 
								echo float_to_money(($ob_debit + ($cv_debit + $jv_debit)) - ($cv_credit + $jv_credit));
							}else{
								echo float_to_money(0);
							}
							?></td>
							<td><?php if($ob_credit > 0) {
								echo float_to_money(($ob_credit + ($cv_credit + $jv_credit)) - ($cv_debit + $jv_debit));
							}else{
								echo float_to_money(0);
							}
							?></td>
						</tr>
					<?php } ?>
			<?php } ?>
		</tbody>

		<tfoot></tfoot>
	</table>
</div>