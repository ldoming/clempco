<script type="text/javascript">
	Ext.define('LOAN.controller.Members', {
	    extend: 'Ext.app.Controller',

	    init: function() {
	        this.control({
	        	'memberinfo button[text=New Member]': {
	        		click: this.onNewMemberClick
	        	}
	        });
	    },

	    views : [
	    	'Members'
	    ],

	    models : [
	    	'Members'
	    ],

	    stores : [
	    	'Members'
	    ],
	    
	    onNewMemberClick: function(ths){
	    	new LOAN.view.common.AddMember().show();
	    }

	});
</script>