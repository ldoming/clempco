<br /><br />

<div class="panel">
    <div class="panel-header clempco-prevails">
        Transaction Reports
    </div>
    <div class="panel-content">
		<button class="btn-export command-button bg-olive fg-white" action="Trial Balance">
		    <i class="icon-file-excel on-left"></i>
		    Trial Balance
		    <small>Excel format</small>
		</button>
		<button class="btn-export command-button bg-olive fg-white" action="Income Statement">
		    <i class="icon-file-excel on-left"></i>
		    Income Statement
		    <small>Excel format</small>
		</button>
		<button class="btn-export command-button bg-olive fg-white" action="Balance Sheet">
		    <i class="icon-file-excel on-left"></i>
		    Balance sheet
		    <small>Excel format</small>
		</button>
    </div>
</div>
<br /><br />
<div class="panel">
    <div class="panel-header clempco-prevails">
        Deductions
    </div>
    <div class="panel-content">
		<button class="btn-export command-button bg-olive fg-white" action="Monthly Deduction">
		    <i class="icon-file-excel on-left"></i>
		    Monthly Deduction
		    <small>Excel format</small>
		</button>
    </div>
</div>
<br /><br />

<div class="panel">
    <div class="panel-header clempco-prevails">
        Member Reports
    </div>
    <div class="panel-content">
		
		<button class="btn-export command-button bg-olive fg-white" action="Capital Savings Withdrawal">
		    <i class="icon-file-excel on-left"></i>
		    Capital &amp; Savings Withdrawal
		    <small>Excel format</small>
		</button>

    </div>
</div>

<div style="display:none" id="rpt-options">
	<div class="padding20 span4">
		<?php echo form_open("reports/generate", array('method' => 'GET'));?>
			<input type="hidden" name="rtype" />

			<!-- <label class="rt-ct">Member</label> -->

	        <div class="input-control select rt-ct">
			    <select name="rt-type">
			        <option value="1">Generate as of</option>
			        <option value="2">Generate for the period</option>
			    </select>
			</div>

			<label class="mem-ct">Member</label>

	        <div class="input-control select mem-ct">
			    <select name="member">
			        <option value=""></option>
			        <?php foreach($members as $key => $value) { ?>
			        	<option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
			        <?php } ?>
			    </select>
			</div>

			<label>Month</label>
	        <div class="input-control select">
			    <select name="period-month">
			        <option value="01">January</option>
			        <option value="02">February</option>
			        <option value="03">March</option>
			        <option value="04">April</option>
			        <option value="05">May</option>
			        <option value="06">June</option>
			        <option value="07">July</option>
			        <option value="08">August</option>
			        <option value="09">September</option>
			        <option value="10">October</option>
			        <option value="11">November</option>
			        <option value="12">December</option>
			    </select>
			</div>
			<label>Year</label>
	        <div class="input-control select">
			    <select name="period-year">
			        <?php $years = $this->reports->get_years();?>
			        <?php foreach ($years as $key => $value) { ?>
			        	<option value="<?php echo $value['year'];?>">
			        		<?php echo $value['year'];?>
			        	</option>
			        <?php } ?>
			    </select>
			</div>
			<br /><br />
			<div class="form-actions">
				<button class="button primary">Submit</button>
				<button class="button" type="button" onclick="$.Dialog.close()">Cancel</button>
			</div>
		<?php echo form_close();?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		//disable by default
		$("#mem-ct").hide();

		$('.btn-export').on('click', function(e){
			var action = $(this).attr('action');

			$.Dialog({
		        overlay: true,
		        shadow: true,
		        flat: true,
		        icon: '<span class="icon-libreoffice"></span>',
		        title: 'Export Options',
		        content: '',
		        height: 350,
		        onShow: function(_dialog){
		            $("input[name='rtype']").val(action);

		            if(action.indexOf('Withdrawal') != -1){
		            	$(".mem-ct").show();
		            }else {
		            	$(".mem-ct").hide();
		            }

		            if(action.indexOf('Trial Balance') != -1){
		            	$(".rt-ct").show();
		            }else {
		            	$(".rt-ct").hide();
		            }

		            var content = _dialog.children('.content'),
		            	options = $('#rpt-options').html();

		            content.append(options);
		            $(".rpt-datepicker").datepicker({
		            	format: "m/y",
		            });

		        }
		    });
		});
	});
</script>