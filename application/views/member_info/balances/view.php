<div id="ext-view"></div>

<script type="text/javascript">

	Ext.define('LOAN.view.Members', {
		extend: 'Ext.grid.Panel',
		alias: 'widget.memberinfo',
		store: 'Members',
		cls: 'span12',
		renderTo: 'ext-view',
		height: 500,

		initComponent: function(){

			this.tbar = [{
				xtype: 'searchfield',
				store: Ext.getStore('Members'),
				emptyText: 'Search',
				fieldLabel: 'Search',
				labelWidth: 60
			}, {
				xtype: 'button',
				text: 'New Member'
			}];

			this.columns = [{
				text: 'ID',
				dataIndex: 'id',
				width: 100
			}, {
				text: 'name',
				dataIndex: 'name',
				flex: 1
			}, {
				text: 'Email Address',
				dataIndex: 'email_address',
				flex: 1
			}, {
				xtype : 'actioncolumn',
				text : 'Options',
				items : [{
					icon : ExtCommon.baseUrl + 'includes/images/png/Xion.png',
					tooltip : 'Delete',
					handler : this.onDeleteClick
				}]
			}];

			this.dockedItems = [{
				xtype: 'pagingtoolbar',
				dock: 'bottom',
				displayInfo: true,
				store: Ext.getStore('Members'),
				items: [{
					xtype: 'button',
					text: 'Export'
				}]
			}];

			this.callParent(arguments);
		},

		onDeleteClick : function(grid, rowIndex) {
			var data = grid.getStore().getAt(rowIndex).data;

			Ext.Msg.show({
				msg : 'Delete this record?',
				title : 'Confirm',
				icon : Ext.Msg.QUESTION,
				buttons : Ext.Msg.OKCANCEL,
				fn : function(btn) {
					if (btn == 'ok') {
						Ext.widget('form').getForm().submit({
							url : ExtCommon.baseUrl + 'members/destroy',
							params : {
								employee_id : data.id
							},
							waitMsg : 'Please wait...',
							success : function(f, action) {
								if (action.result.success)
									ExtCommon.successMsg(action.result.data);
								else
									ExtCommon.errorMsg(action.result.data);

								grid.getStore().loadPage(1);
							},
							failure : function(f, action) {
								ExtCommon.errorMsg(action.result.data);
							}
						});
					}
				}
			})
		}
	});

</script>