<script type="text/javascript">

	Ext.define('LOAN.model.Members', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'name',
			type : 'string'
		}, {
			name : 'email_address',
			type : 'string'
		}, {
			name : 'position',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.Members', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.Members',
		storeId: 'Members',
		queryMode : 'remote',
		autoLoad: true,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'members/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

</script>