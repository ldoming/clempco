<div id="ext-view"></div>

<script type="text/javascript">
Ext.define('LOAN.view.LoanApp', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.loanapp',
    store: 'LoanApp',
    cls: 'span12',
    renderTo: 'ext-view',
    height: 500,

    initComponent: function() {

        this.tbar = [{
            xtype: 'loantype',
            fieldLabel: 'Loan Type',
            editable: false,
            labelWidth: 80
        }, {
            xtype: 'button',
            text: 'Loan Application'
        }];

        this.columns = [{
            text: 'ID',
            dataIndex: 'id',
            width: 50
        }, {
            text: 'Type',
            dataIndex: 'type',
            flex: 1
        }, {
            text: 'Amount',
            dataIndex: 'principal',
            renderer: this.money_renderer,
            width: 150
        }, {
            text: 'Interest Rate',
            dataIndex: 'interest',
            width: 100
        }, {
            text: 'Period',
            dataIndex: 'period',
            width: 100
        }, {
            text: 'Loan Status',
            dataIndex: 'status',
            width: 150,
            renderer: this.statusRenderer
        }, {
            xtype: 'actioncolumn',
            text: 'Options',
            width: 100,
            defaults: {
                style: {
                    margin: '0px 5px !important'
                }
            },
            text: 'Actions',
            items: [{
                icon: ExtCommon.baseUrl + 'includes/images/stock/excel1.png',
                tooltip: 'Export',
                handler: this.onExportClick,
                text: 'Action'
            }, {
                icon: ExtCommon.baseUrl + 'includes/images/png/Explorer.png',
                tooltip: 'Payments',
                handler: this.onOptionsClick
            }, {
                icon: ExtCommon.baseUrl + 'includes/images/png/Xion.png',
                tooltip: 'Delete',
                handler: this.onDeleteClick
            }]
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true,
            store: Ext.getStore('LoanApp'),
            items: [{
                xtype: 'button',
                text: 'Export'
            }]
        }];

        this.callParent(arguments);
    },

    money_renderer: function(value) {
        return ExtCommon.number_format(value, 2);
    },

    onOptionsClick: function(grid, rowIndex, colIndex, re, event) {
        new LOAN.view.LoanDetails({
            _record: grid.getStore().getAt(rowIndex)
        }).show();
    },

    onDeleteClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex).data;

        Ext.Msg.show({
            msg: 'Delete this record?',
            title: 'Confirm',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn) {
                if (btn == 'ok') {
                    Ext.widget('form').getForm().submit({
                        url: ExtCommon.baseUrl + 'loans/destroy',
                        params: {
                            loan_info: Ext.encode(data)
                        },
                        waitMsg: 'Please wait...',
                        success: function(f, action) {
                            if (action.result.success)
                                ExtCommon.successMsg(action.result.data);
                            else
                                ExtCommon.errorMsg(action.result.data);

                            grid.getStore().loadPage(1);
                        },
                        failure: function(f, action) {
                            ExtCommon.errorMsg(action.result.data);
                        }
                    });
                }
            }
        })
    },

    onExportClick: function(grid, rowIndex) {
        var data = grid.getStore().getAt(rowIndex);

        Ext.create('widget.form').getForm().submit({
            url: ExtCommon.baseUrl + 'loans/export',
            timeout: 30000,
            waitMsg: 'Preparing your download. Please wait!',
            title: 'Export',
            params: {
                loan_id: data.get('id')
            },
            success: function(f, action) {
                if (action.result.success) {
                    if (action.result.download) {
                        Ext.Msg.show({
                            title: 'Status',
                            msg: 'Your download is ready!',
                            buttonText: {
                                ok: "Download",
                                cancel: "Cancel"
                            },
                            fn: function(btn) {
                                if (btn == 'ok')
                                    window.location.href = action.result.download;
                            }
                        });
                    }
                }

            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    },

    statusRenderer: function(value) {
        var color = 'green';
        if (value == 'Declined')
            color = 'Red';
        else if (value == 'Pending Approval')
            color = '#333';

        return '<a href="javascript:" style="color:' + color + '">' + value + '</a>';
    }
});

Ext.define('LOAN.view.LoanAppAdd', {
    extend: 'Ext.form.Panel',
    alias: 'widget.loanappadd',
    modal: true,
    floating: true,
    title: 'New Loan Application',
    bodyPadding: 15,
    width: 400,
    height: 350,
    layout: 'anchor',
    frame: true,
    autoScroll: true,
    defaults: {
        anchor: '100%',
        minValue: 1
    },
    closable: true,
    draggable: true,

    initComponent: function() {

        this.tbar = ['->', {
            xtype: 'button',
            text: 'Help',
            iconCls: 'icon-question'
        }];

        this.items = [{
            xtype: 'loantype',
            fieldLabel: 'Loan Type',
            name: 'loan_type'
        }, {
            xtype: 'numberfield',
            fieldLabel: 'Loan Amount',
            name: 'principal'
        }, {
            xtype: 'loanterms',
            fieldLabel: 'Loan Term',
            name: 'terms'
        }];

        this.buttons = [{
            text: 'Submit Application'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.LoanStatus', {
    extend: 'Ext.window.Window',
    alias: 'widget.loanstatus',
    title: 'Loan Info',
    width: 450,
    height: 350,
    layout: 'fit',
    title: 'Loan Approval Status',
    modal: true,

    initComponent: function() {
        this.items = [{
            xtype: 'loanstatusgrid'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.LoanStatusGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.loanstatusgrid',
    draggable: false,
    movable: false,
    resizable: false,
    store: 'LoanStatus',

    initComponent: function() {
        this.columns = [{
            text: 'Personnel',
            dataIndex: 'personnel',
            width: 120
        }, {
            text: 'Comment',
            dataIndex: 'comment',
            flex: 1
        }, {
            text: 'Status',
            dataIndex: 'status',
            width: 120
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            store: Ext.getStore('LoanStatus'),
            displayInfo: true,
            dock: 'bottom'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.infoWindow', {
    extend: 'Ext.form.Panel',
    alias: 'widget.infowindow',
    bodyPadding: 10,
    modal: true,
    title: 'HELP',
    floating: true,
    closable: true,
    width: 400,
    height: 200,
    layout: 'fit',

    initComponent: function() {
        this.callParent(arguments);
    }
});
</script>
