<script type="text/javascript">

	Ext.define('LOAN.model.LoanApp', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'principal',
			type : 'float'
		}, {
			name : 'type',
			type : 'string'
		}, {
			name : 'interest',
			type : 'float'
		}, {
			name : 'period',
			type : 'string'
		}, {
			name : 'payable',
			type : 'float'
		}, {
			name : 'balance',
			type : 'float'
		}, {
			name : 'position',
			type : 'string'
		}, {
			name : 'status',
			type : 'string'
		}, {
			name : 'monthly',
			type : 'float'
		}, {
			name : 'total_payable',
			type : 'float'
		}]
	});

	Ext.define('LOAN.store.LoanApp', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.LoanApp',
		storeId: 'LoanApp',
		queryMode : 'remote',
		autoLoad: true,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'loans/read1',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

	Ext.define('LOAN.model.PaymentRow', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'pay_date',
			type : 'string'
		}, {
			name : 'payment',
			type : 'float'
		}, {
			name : 'payable',
			type : 'float'
		}, {
			name : 'principal',
			type : 'float'
		}, {
			name : 'interest',
			type : 'float'
		}, {
			name : 'running_balance',
			type : 'float'
		}, {
			name : 'ou',
			type : 'float'
		}, {
			name : 'remarks',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.PaymentHistory', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.PaymentRow',
		storeId: 'PaymentHistory',
		queryMode : 'remote',
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'loan_payments/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

	Ext.define('LOAN.model.LoanStatus', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'personnel',
			type : 'string'
		}, {
			name : 'comment',
			type : 'string'
		}, {
			name : 'status',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.LoanStatus', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.LoanStatus',
		storeId: 'LoanStatus',
		queryMode : 'remote',
		autoLoad: false,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'loan_status_history/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

</script>