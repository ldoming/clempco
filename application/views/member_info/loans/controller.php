<script type="text/javascript">
Ext.define('LOAN.controller.LoanApp', {
    extend: 'Ext.app.Controller',

    init: function() {
        this.control({
            'loanapp button[text=Loan Application]': {
                click: this.onNewRecord
            },
            'loanappadd button[text=Submit Application]': {
                click: this.onSubmitApplication
            },
            'loanapp loantype': {
                select: this.onLoanTypeSelect
            },
            'loanapp': {
                cellclick: this.onLoanAppCellClick
            },
            'loanstatus': {
                render: this.onLoanStatusRender
            },
            'loanappadd loantype': {
                select: this.onLoanTypeSelect2
            },
            'loanappadd button[text=Help]': {
            	click: this.onHelpClick
            },
            'infowindow': {
            	show: this.onShowInfo
            }
        })
    },

    views: ['LoanApp', 'LoanAppAdd', 'LoanStatus', 'LoanStatusGrid', 'infoWindow'],

    models: ['LoanApp', 'PaymentRow', 'LoanStatus'],

    stores: ['LoanApp', 'PaymentHistory', 'LoanStatus'],

    onNewRecord: function() {
        var _data = <?php echo json_encode($loan_amount); ?> ;
        new LOAN.view.LoanAppAdd({
            _data: _data
        }).show();
    },

    onSubmitApplication: function(ths) {
        var form = ths.up('form');

        if (!form.getForm().isValid())
            return;

        form.getForm().submit({
            url: ExtCommon.baseUrl + 'loans/create',
            waitMsg: 'Please wait...',
            success: function(f, action) {
                if (action.result.success)
                    ExtCommon.successMsg(action.result.data);
                else
                    ExtCommon.errorMsg(action.result.data);

                form.getForm().reset();
                ExtCommon.getCmp('loanapp').store.loadPage(1);
            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    },

    onLoanTypeSelect: function(ths) {
        var grid = ths.up('grid');

        grid.store.proxy.extraParams['loan_type'] = ths.getValue();
        grid.store.load();
    },

    onLoanAppCellClick: function(ths, td, cellIndex, record) {
        if (cellIndex == 5) {
            new LOAN.view.LoanStatus({
                _record: record.data
            }).show();
        }
    },

    onLoanStatusRender: function(ths) {
        var grid = ExtCommon.getCmp('loanstatusgrid');

        grid.store.proxy.extraParams['loan_id'] = ths._record.id;
        grid.store.loadPage(1);
    },

    onLoanTypeSelect2: function(ths) {
        var form = ths.up('form'),
            win = ExtCommon.getCmp('loanappadd'),
            _data = win._data,
            principal = ExtCommon.getCmp('numberfield[name=principal]'),
            data = {};

        _data.forEach(function(d) {
            if(ths.getValue() == d.subtype){
            	data = d;
            }
        });

        var min = 1, max = 100000;
        if(data.value == 'Range'){
        	var amt = data.value2.split('-');
        	min = amt[0];
        	max = amt[1];
        }else if(data.value == 'Max'){
        	max = data.value2;
        }

        principal.reset();
    	principal.setMinValue(min);
    	principal.setMaxValue(max);
    },

    onHelpClick: function(ths){
    	new LOAN.view.infoWindow().show();
    },

    onShowInfo: function(ths){

		var tpl = new Ext.XTemplate(
		    '<div style="border-bottom: 1px solid #ccc">Loan Amount Limits</div>',
		    '<table>',
		        '<tr><td>Regular Loan - 20k to 100k (But it will depend on members capital)</td></tr>',
		        '<tr><td>Special Loan - up to 20k</td></tr>',
		        '<tr><td>Grocery Loan - up to 6k</td></tr>',
		        '<tr><td>Emergency Loan - 2k</td></tr>',
		    '</table>'
		);

		tpl.overwrite(ths.body, {});

    }
});
</script>
