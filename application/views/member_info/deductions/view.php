<?php
$session = get_session(); ?>
<?php
if ($session['access_type'] == 'Member') { ?>
    <!-- <h2>Account Balance as of <?php echo date('M, Y'); ?></h2> -->
    <div class="span6">
        <br />
        <h2>Total Capital</h2>
        <h1>Php <?php echo $total_capital; ?></h1>
        <br />
    </div>
        <div class="span6">
        <br />
        <h2>Total Savings</h2>
        <h1>Php <?php echo $total_savings; ?></h1>
        <br />
    </div>
    <?php
} ?>

<div id="ext-view"></div>

<script type="text/javascript">

var access_type = '<?php echo $this->session->userdata['User Info']['access_type']; ?>';

Ext.define('LOAN.view.ManageDeductions', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.managedeductions',
    store: 'ManageDeductions',
    cls: 'span12',
    renderTo: 'ext-view',
    height: 500,

    initComponent: function() {

        this.tbar = [{
            xtype: 'button',
            text: 'New Record',
            hidden: access_type == 'Member',
            menu: [{
                text: 'Monthly Deduction'
            }, {
                text: 'Insurance'
            }, {
                text: 'Capital'
            }, {
                text: 'Savings'
            }]
        }, {
            xtype: 'dedtype',
            width: 160
        }, {
            xtype: 'monthfield',
            format: 'F, Y',
            emptyText: 'Month',
            // value: new Date(),
            width: 130
        }, {
            xtype: 'loantype',
            emptyText: 'Loan Type',
            width: 140
        }, {
            hidden: access_type == 'Member',
            xtype: 'members',
            emptyText: 'Select Member'
        }, {
            xtype: 'button',
            text: 'Reset'
        }, {
            xtype: 'button',
            text: 'View Monthly'
        }];

        this.columns = [{
            text: 'Date/Period',
            dataIndex: 'ded_date',
            flex: 1
        }, {
            text: 'Member',
            dataIndex: 'name',
            flex: 1
        }, {
            text: 'Type',
            dataIndex: 'record_type',
            flex: 1
        }, {
            text: 'Loan Info',
            dataIndex: 'loan_info',
            renderer: this.onLoanInfoRenderer,
            flex: 1
        }, {
            text: 'Principal',
            dataIndex: 'principal',
            flex: 1
        }, {
            text: 'Interest',
            dataIndex: 'interest',
            flex: 1
        }, {
            text: 'Total',
            dataIndex: 'total',
            flex: 1
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true,
            store: Ext.getStore('ManageDeductions'),
            items: [{
                xtype: 'button',
                text: 'Export'
            }]
        }];

        this.callParent(arguments);
    },
    
    onLoanInfoRenderer: function(data) {
        if(data)
            return data.type + '<br /><small>' + data.status + ' | ' + data.principal + '</small>';
        else
            return 'n/a';
    }

});

Ext.define('LOAN.view.BrowseDeductions', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.browsedeductions',
    store: 'BrowseDeductions',
    cls: 'span12',
    renderTo: 'ext-view',
    height: 500,

    initComponent: function() {

        this.tbar = [{
            xtype: 'monthfield',
            format: 'F, Y',
            emptyText: 'Month',
            value: new Date(),
            width: 130
        }, {
            xtype: 'loantype',
            emptyText: 'Loan Type'
        }, {
            xtype: 'members',
            emptyText: 'Select Member'
        }, {
            xtype: 'button',
            text: 'New Record',
            hidden: true
        }, {
            xtype: 'button',
            text: 'Reset'
        }, {
            xtype: 'button',
            text: 'Manage'
        }];

        this.columns = [{
            text: '',
            flex: 1
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.AddDeduction', {
    extend: 'Ext.form.Panel',
    alias: 'widget.adddeduction',
    width: 350,
    modal: true,
    floating: true,
    closable: true,
    title: 'New Loan Deduction',
    layout: 'anchor',
    defaults: {
        allowBlank: false,
        xtype: 'textfield',
        anchor: '100%',
        labelWidth: 120
    },
    bodyPadding: 10,
    border: false,
    _action: 'create',
    _type: 'loan',

    initComponent: function() {

        this.items = [{
            name: 'id',
            dataIndex: 'id',
            hidden: true,
            allowBlank: true
        }, {
            fieldLabel: 'Date of Deduction',
            xtype: 'datefield',
            name: 'ded_date'
        }, {
            xtype: 'members',
            fieldLabel: 'Select Member',
            name: 'member_id'
        }, {
            xtype: 'memberloans',
            fieldLabel: 'Select Loan',
            name: 'loan_id'
        }, {
            fieldLabel: 'Principal',
            name: 'principal',
            xtype: 'numberfield',
            minValue: 1
        }, {
            fieldLabel: 'Interest',
            name: 'interest',
            xtype: 'numberfield',
            minValue: 1
        }, {
            fieldLabel: 'Total',
            name: 'total',
            xtype: 'numberfield',
            minValue: 1,
            readOnly: true
        }, {
            xtype: 'textareafield',
            name: 'remarks',
            fieldLabel: 'remarks',
            allowBlank: true
        }];

        this.buttons = [{
            text: 'Save'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.AddDeduction1', {
    extend: 'Ext.form.Panel',
    alias: 'widget.adddeduction1',
    width: 350,
    modal: true,
    floating: true,
    closable: true,
    title: 'New Deduction',
    layout: 'anchor',
    defaults: {
        allowBlank: false,
        xtype: 'textfield',
        anchor: '100%',
        labelWidth: 120
    },
    bodyPadding: 10,
    border: false,
    _action: 'create',
    _type: 'insurance',

    initComponent: function() {

        this.items = [{
            name: 'id',
            dataIndex: 'id',
            hidden: true,
            allowBlank: true
        }, {
            fieldLabel: 'Period',
            xtype: 'monthfield',
            // format: 'F, Y',
            name: 'ded_date'
        }, {
            xtype: 'members',
            fieldLabel: 'Select Member',
            name: 'member_id'
        }, {
            fieldLabel: 'Amount',
            name: 'total',
            xtype: 'numberfield',
            minValue: 1
        }];

        this.buttons = [{
            text: 'Save'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});

Ext.define('LOAN.view.AddCheckpoint', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addcheckpoint',
    modal: true,
    floating: true,
    closable: true,
    title: 'Select Option',
    layout: 'column',
    bodyPadding: 10,
    border: false,
    _action: 'create',
    defaults: {
        style: {
            'margin': '10px !important',
            'padding': '10px !important'
        }
    },

    initComponent: function() {

        this.items = [{
            xtype: 'button',
            scale: 'large',
            text: 'Loan<br />Deduction',
            action: 'loan',
            menu: {
                xtype: 'menu',
                border: false,
                items: [{
                    text: 'Monthly Deduction',
                    handler: this.onNewMonthlyDeduction
                }, {
                    text: 'Loan Payment',
                    handler: this.onNewLoanPayment
                }]
            }
        }, {
            xtype: 'button',
            scale: 'large',
            text: 'Insurance<br />Deduction',
            action: 'insurance'
        }, {
            xtype: 'button',
            scale: 'large',
            text: 'Monthly<br />Savings',
            action: 'savings'
        }, {
            xtype: 'button',
            scale: 'large',
            text: 'Capital<br />Build-up',
            action: 'capital_buildup'
        }];

        this.callParent(arguments);
    },

    onNewMonthlyDeduction: function(ths){
        ths.up('form').close();
        new LOAN.view.AddDeduction().show();
    },

    onNewLoanPayment: function(ths){
        ths.up('form').close();
        new LOAN.view.AddPayment().show();
    }
});

Ext.define('LOAN.view.DedType', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.dedtype',
    displayField: 'type',
    valueField: 'value',
    store: 'DedType'
});

Ext.define('LOAN.view.AddPayment', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addpayment',
    layout: 'anchor',
    floating: true,
    width: 350,
    modal: true,
    closable: true,
    title: 'Add Payment',
    border: false,
    bodyPadding: 10,
    defaults: {
        allowBlank: false,
        labelWidth: 100,
        anchor: '100%'
    },

    initComponent: function() {
        this.items = [{
            xtype: 'numberfield',
            name: 'id',
            hidden: true,
            allowBlank: true
        }, {
            xtype: 'members',
            name: 'member_id',
            fieldLabel: 'Member'
        }, {
            xtype: 'memberloans',
            name: 'loan_id',
            fieldLabel: 'Select Loan'
        }, {
            xtype: 'datefield',
            name: 'pay_date',
            fieldLabel: 'Period'
        }, {
            xtype: 'numberfield',
            name: 'payment',
            fieldLabel: 'Amount'
        }, {
            xtype: 'textareafield',
            name: 'remarks',
            fieldLabel: 'Remarks',
            allowBlank: true
        }];

        this.buttons = [{
            text: 'Save Payment'
        }, {
            text: 'Reset'
        }];

        this.callParent(arguments);
    }
});
</script>
