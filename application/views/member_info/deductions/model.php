<script type="text/javascript">

	Ext.define('LOAN.model.Deductions', {
		extend : 'Ext.data.Model',
		fields : [{
			name : 'id',
			type : 'int'
		}, {
			name : 'name',
			type : 'string'
		}, {
			name : 'ded_date',
			type : 'string'
		}, {
			name : 'loan_type',
			type : 'string'
		}, {
			name : 'principal',
			type : 'string'
		}, {
			name : 'interest',
			type : 'string'
		}, {
			name : 'total',
			type : 'string'
		}, {
			name : 'member_id',
			type : 'int'
		}, {
			name : 'loan_info',
			type : 'object'
		}, {
			name : 'record_type',
			type : 'string'
		}]
	});

	Ext.define('LOAN.store.BrowseDeductions', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.Deductions',
		storeId: 'BrowseDeductions',
		queryMode : 'remote',
		autoLoad: true,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'deductions/read2',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

	Ext.define('LOAN.store.ManageDeductions', {
		extend : 'Ext.data.Store',
		model : 'LOAN.model.Deductions',
		storeId: 'ManageDeductions',
		queryMode : 'remote',
		autoLoad: true,
		proxy : {
			type : 'ajax',
			url : ExtCommon.baseUrl + 'deductions/read',
			reader: {
	            type: 'json',
	            root: 'data'
	        }
		}
	});

	Ext.define('LOAN.store.DedType', {
		extend : 'Ext.data.Store',
		fields: ['type', 'value'],
		storeId: 'DedType',
		queryMode : 'local',
		data: [{
			type: 'Monthly Deduction',
			value: 'deduction'
		}, {
			type: 'Insurance Deduction',
			value: 'insurance'
		}, {
			type: 'Capital',
			value: 'capital'
		}, {
			type: 'Savings',
			value: 'savings'
		}]
	});

</script>