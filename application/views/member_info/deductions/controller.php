<script type="text/javascript">
Ext.define('LOAN.controller.Deductions', {
    extend: 'Ext.app.Controller',

    init: function() {
        this.control({
            'button[text=New Record]': {
                // click: this.onNewDeductionClick
            },
            'adddeduction button[text=Save]': {
                click: this.onSaveDeduction
            },
            'adddeduction1 button[text=Save]': {
                click: this.onSaveDeduction
            },
            'adddeduction numberfield[name=principal]': {
                change: this.onAmtChange
            },
            'adddeduction numberfield[name=interest]': {
                change: this.onAmtChange
            },
            'adddeduction members': {
                select: this.onMemberSelect
            },
            'addpayment members': {
                select: this.onMemberSelect
            },
            'browsedeductions members': {
                select: this.onFilterMember
            },
            'browsedeductions loantype': {
                select: this.onFilterMember
            },
            'browsedeductions monthfield': {
                select: this.onFilterMember
            },
            'managedeductions members': {
                select: this.onFilterMember
            },
            'managedeductions loantype': {
                select: this.onFilterMember
            },
            'managedeductions monthfield': {
                select: this.onFilterMember
            },
            'browsedeductions': {
                afterrender: this.onPanelRender
            },
            'managedeductions': {
                afterrender: this.onPanelRender,
                itemclick: this.onDedItemClick
            },
            'button[text=View Monthly]': {
                click: this.onBtnClick
            },
            'button[text=Manage]': {
                click: this.onBtnClick
            },
            'button[text=Reset]': {
                click: this.onResetClick
            },
            'addcheckpoint button': {
                click: this.onDeductionTypeClick
            },
            'managedeductions dedtype': {
                select: this.onDedTypSelect
            },
            'addpayment button[text=Save Payment]': {
                click: this.onSavePaymentClick
            },
            'addpayment button[text=Reset]': {
                click: this.onResetPaymentClick
            },
            'menu > *': {
                click: this.onMenuItemClick
            }
        });
    },

    views: [
        'BrowseDeductions',
        'AddDeduction',
        'AddDeduction1',
        'ManageDeductions',
        'AddCheckpoint',
        'DedType',
        'AddPayment'
    ],

    models: [
        'Deductions'
    ],

    stores: [
        'ManageDeductions',
        'BrowseDeductions',
        'DedType'
    ],

    onSaveDeduction: function(ths) {
        var form = ths.up('form'),
            daction = '<?php echo $ded_action;?>';

        if (!form.getForm().isValid())
            return;

        form.getForm().submit({
            url: ExtCommon.baseUrl + 'deductions/create',
            waitMsg: 'Please wait...',
            params: {
                action: form._action,
                type: form._type
            },
            success: function(f, action) {
                if (action.result.success) {
                    ExtCommon.successMsg(action.result.data);
                    form.getForm().reset();
                    ExtCommon.getCmp(daction.toLowerCase() + 'deductions').store.loadPage(1);
                } else {
                    ExtCommon.errorMsg(action.result.data);
                }
            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    },

    onAmtChange: function(ths) {
        var form = ths.up('form'),
            p = form.getForm().findField('principal').getValue(),
            i = form.getForm().findField('interest').getValue(),
            t = form.getForm().findField('total');

        if (p != null && i != null)
            t.setValue(parseFloat(p) + parseFloat(i));
    },

    onMemberSelect: function(ths) {
        var form = ths.up('form');

        form.down('memberloans').reset();
        form.down('memberloans').store.proxy.extraParams['member_id'] = ths.getValue();
        form.down('memberloans').store.load();
    },

    onFilterMember: function(ths) {
        var action = '<?php echo $ded_action;?>',
            grid = ExtCommon.getCmp(action.toLowerCase() + 'deductions'),
            member = grid.down('members'),
            month = grid.down('monthfield'),
            loan = grid.down('loantype'),
            dedtype = grid.down('dedtype');

        grid.store.proxy.extraParams = Ext.apply(grid.store.proxy.extraParams, {
            'member_id': member.getValue(),
            'period': month.getValue(),
            'loan_type': loan.getValue(),
            'ded_type': dedtype != null ? dedtype.getValue() : ''
        });

        // if (month.getValue() != null)
            grid.store.load({
                callback: this.onPanelStoreLoaded
            });

    },

    onPanelStoreLoaded: function(rec, op) {
        var action = '<?php echo $ded_action;?>';

        if (action != 'browse')
            return;

        var grid = ExtCommon.getCmp('browsedeductions'),
            dates = op.request.scope.reader.rawData.date_headers,
            column = Ext.create('Ext.grid.column.Column', {
                text: 'MEMBER NAME',
                dataIndex: 'name',
                width: 250
            });

        grid.store.removeAll();
        grid.store.model.setFields(op.request.scope.reader.rawData.fields);
        grid.headerCt.removeAll();
        grid.headerCt.insert(grid.headerCt.items.length, {
            text: 'EMP<br />NUMBER',
            dataIndex: 'id',
            align: 'center',
            width: 100
        });
        grid.headerCt.insert(grid.headerCt.items.length, column);

        for (d in dates) {
            var post = dates[d].ded_date.replace(/-/g, '') + '_' + (dates[d].type.replace(/\ /g, ''));

            var c = {
                xtype: 'gridcolumn',
                text: dates[d].ded_date + '<br />' + dates[d].type,
                width: 300,
                align: 'center',
                columns: [{
                    xtype: 'gridcolumn',
                    text: 'PRINCIPAL',
                    dataIndex: 'principal_' + post
                }, {
                    text: dates[d].type == 'Regular Loan' ? '15% INT' : 'INTEREST',
                    dataIndex: 'interest_' + post
                }, {
                    text: 'TOTAL',
                    dataIndex: 'total_' + post
                }]
            };
            grid.headerCt.insert(grid.headerCt.items.length, c);
        }

        var postCol = [{
            text: 'CAPITAL<br />BUILD UP',
            dataIndex: 'cb_amount',
            align: 'center',
            width: 120
        }, {
            text: 'SAVINGS',
            dataIndex: 'sa_amount',
            align: 'center',
            width: 120
        }, {
            text: 'PHILAM<br />INSURANCE',
            dataIndex: 'insurance',
            align: 'center',
            width: 120
        }, {
            text: 'TOTAL<br />DEDUCTION',
            dataIndex: 'total_deduction',
            align: 'center',
            width: 120
        }];

        for (p in postCol) {
            grid.headerCt.insert(grid.headerCt.items.length, postCol[p]);
        }

        grid.getView().refresh();

        var _data = op.request.scope.reader.rawData.data;
        for (d in _data) {
            grid.store.add(_data[d]);
        }
    },

    onPanelRender: function(ths) {
        this.onFilterMember();
    },

    onBtnClick: function(ths) {
        var txt = ths.text == 'Manage' ? 'manage' : 'browse';
        window.location.href = ExtCommon.baseUrl + 'deductions/index/' + txt + '?_m=<?php echo $_GET["_m"];?>';
    },

    onResetClick: function(ths) {
        var grid = ths.up('grid'),
            member = grid.down('members'),
            month = grid.down('monthfield'),
            loan = grid.down('loantype'),
            dedtype = grid.down('dedtype');

        grid.columns[3].show();
        grid.columns[4].show();
        grid.columns[5].show();
            
        member.reset();
        month.reset();
        loan.reset();

        if (dedtype)
            dedtype.reset();

        loan.setDisabled(0);

        this.onFilterMember();
    },

    onDeductionTypeClick: function(ths) {
        if (ths.action != 'loan') {
            var ded = new LOAN.view.AddDeduction1({
                _type: ths.action
            });
            ded.show();
            ths.up('form').close();
        }
    },

    onDedTypSelect: function(ths) {
        var grid = ths.up('grid'),
            set2 = ['payment', 'insurance', 'capital', 'savings'];

        grid.down('loantype').setDisabled(set2.indexOf(ths.getValue()) != -1);

        if (set2.indexOf(ths.getValue()) != -1) {
            delete grid.store.proxy.extraParams['loan_type'];
            grid.columns[3].hide();
            grid.columns[4].hide();
            grid.columns[5].hide();
        } else {
            grid.columns[3].show();
            grid.columns[4].show();
            grid.columns[5].show();
        }


        grid.store.proxy.extraParams['ded_type'] = ths.getValue();
        grid.store.load();

    },

    onSavePaymentClick: function(ths) {
        var form = ExtCommon.getCmp('addpayment');

        if (!form.getForm().isValid())
            return;

        form.getForm().submit({
            url: ExtCommon.baseUrl + 'loan_payments/create',
            params: {
                action: form._action
            },
            waitMsg: 'Please wait',
            success: function(f, action) {
                if (action.result.success)
                    ExtCommon.successMsg(action.result.data);
                else
                    ExtCommon.errorMsg(action.result.data);

                form.getForm().reset();
            },
            failure: function(f, action) {
                ExtCommon.errorMsg(action.result.data);
            }
        });
    },

    onResetPaymentClick: function(ths) {
        ExtCommon.getCmp('addpayment').getForm().reset();
    },

    onDedItemClick: function(ths, rec, item, i, e) {
        var menu = new Ext.widget('gridmenu', {
            record: rec,
            items: [{
                text: 'Modify',
                handler: this.onModifyDeduction
            }, {
                text: 'Delete',
                handler: this.onDeleteDeduction
            }],
            _e: {
                e: e,
                grid: ths
            }
        });

        ExtCommon.showAt(menu, e, ths);
    },

    onModifyDeduction: function(ths) {
        var data = ths.up('menu').record.data;

        var rtype = '';
        if(data.record_type == 'payment')
            rtype = 'Loan Payment';
        else if(data.record_type == 'deduction')
            rtype = 'Monthly Deduction';
        else if(data.record_type == 'insurance')
            rtype = 'Insurance';
        else if(data.record_type == 'capital')
            rtype = 'Capital';
        else
            rtype = 'Savings';

        fn.showWindow(rtype, 'Modify');

        for (d in data) {
            var obj = ExtCommon.getCmp('[name=' + d + ']');
            if (obj && obj.setValue) {
                obj.setValue(data[d]);
            }
        }

        // console.log(data);
        if(data.record_type == 'payment')
            ExtCommon.getCmp('numberfield[name=payment]').setValue(data.total);

        var loans = ExtCommon.getCmp('memberloans');

        if(loans && loans.store)
            loans.store.load({
                callback: function() {
                    loans.setValue(data.loan_info.id);
                }
            });

        var members = ExtCommon.getCmp('members');

        if(members && members.store)
            members.store.load({
                callback: function() {
                    members.setValue(data.member_id);
                }
            });
    },

    onDeleteDeduction: function(ths) {
        var data = ths.up('menu').record.data;

        Ext.Msg.show({
            msg: 'Delete this record?',
            title: 'Confirm',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn) {
                if (btn == 'ok') {
                    Ext.widget('form').getForm().submit({
                        url: ExtCommon.baseUrl + 'deductions/destroy',
                        params: {
                            ded_id: data.id,
                            type: data.record_type
                        },
                        waitMsg: 'Please wait...',
                        success: function(f, action) {
                            if (action.result.success)
                                ExtCommon.successMsg(action.result.data);
                            else
                                ExtCommon.errorMsg(action.result.data);

                            grid.getStore().loadPage(1);
                        },
                        failure: function(f, action) {
                            ExtCommon.errorMsg(action.result.data);
                        }
                    });
                }
            }
        })
    },

    onMenuItemClick: function(ths){
        fn.showWindow(ths.text, 'Create');
    }
});

var fn = function(){
    return {
        showWindow: function(txt, act){
            var form = null;

            switch(txt){
                case 'Loan Payment':
                    form = new LOAN.view.AddPayment({
                        _type: 'payment',
                        _action: act.toLowerCase()
                    });
                    break;
                case 'Monthly Deduction':
                    form = new LOAN.view.AddDeduction({
                        _type: 'deduction',
                        _action: act.toLowerCase()
                    });
                    break;
                case 'Insurance':
                    form = new LOAN.view.AddDeduction1({
                        _type: 'insurance',
                        _action: act.toLowerCase()
                    });
                    break;
                case 'Capital':
                    form = new LOAN.view.AddDeduction1({
                        _type: 'capital',
                        _action: act.toLowerCase()
                    });
                    break;
                case 'Savings':
                    form = new LOAN.view.AddDeduction1({
                        _type: 'savings',
                        _action: act.toLowerCase()
                    });
                    break;
                default:
                    break;
            }

            if(form != null){
                form.setTitle(act + ' ' + txt);
                form.show();
            }
        }
    };
}();
</script>
