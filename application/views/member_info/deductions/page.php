<div class="span9">
	<h2><?php $action = ($ded_action == 'manage' ? 'Manage' : 'Monthly'); echo $action . ' ' . $title;?></h2>
	<?php load_common();?>
	<?php $this->load->view("{$app}/model"); ?>
    <?php $this->load->view("{$app}/view"); ?>
    <?php $this->load->view("{$app}/controller"); ?>
    <?php $this->load->view("{$app}/app"); ?>
</div>