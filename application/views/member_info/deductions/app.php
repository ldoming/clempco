<script type="text/javascript" src="<?php echo base_url();?>includes/js/SearchField.js"></script>
<script type="text/javascript">
	Ext.application({

		requires : ['Ext.container.Viewport', 'Ext.form.SearchField'],

		name : 'LOAN',

		appFolder : 'employee_info',

		controllers : ['Deductions', 'Common'],

		launch : function() {
			var action = '<?php echo $ded_action;?>';

			if(action == 'browse')
				new LOAN.view.BrowseDeductions();
			else if(action == 'manage')
				new LOAN.view.ManageDeductions();
		}
	}); 
</script>