<?php

if(!function_exists('load_templates'))
{
	function load_template($data)
	{
		$CI =& get_instance();
		$session = get_session();

		//loan modules
		$CI->load->model('common/loan_modules_model', 'modules');
		$CI->load->model('common/loan_submodules_model', 'submodules');
		$CI->load->model('common/loan_user_access_model', 'access_typedef');

		$ids = array();
		if($session AND isset($session['access_type'])){
			$ids = $CI->access_typedef->get_modules($session['access_type']);
		}

		list($modules, $_submodules) = get_modules();

		$data = array_merge($data, $_submodules);
		$data['modules'] = $modules;
		
		if($session AND (empty($ids) OR empty($modules)))
		{
			error_msg("You do not have access to the system! Please contact your system administrator!");
			$CI->session->unset_userdata('User Info');
			redirect('user_login/index');
		}

		$data = require_csrf_tokens($data);
		
		$CI->load->view('common/header', $data);

		$CI->load->view('common/' . (isset($data['content']) ? $data['content'] : 'content'));

		$CI->load->view('common/footer');

	}
}

if(!function_exists('get_modules'))
{
	function get_modules()
	{
		$CI =& get_instance();
		$session = get_session();

		//loan modules
		$CI->load->model('common/loan_modules_model', 'modules');
		$CI->load->model('common/loan_submodules_model', 'submodules');
		$CI->load->model('common/loan_user_access_model', 'access_typedef');

		$ids = array();
		if($session AND isset($session['access_type'])){
			$ids = $CI->access_typedef->get_modules($session['access_type']);
		}

		$modules = $CI->modules->order_by('priority', 'asc')->get_many_by('active', 1);
		$_submodules = array();
		foreach ($modules as $key => $value) {

			$where = array(
				'module_id' => $value['id'],
				'active' => 1
			);
			$submodules = $CI->submodules->get_many_by($where);
			foreach($submodules as $kkey => $vvalue){
				if(isset($ids) AND !in_array($vvalue['id'], $ids) AND $session['access_type'] != 'Administrator'){
					unset($submodules[$kkey]);
				}else{
					$vvalue['link'] = $vvalue['link'] . '?_m=' . crypt($vvalue['id'], 'loan');
					$submodules[$kkey] = $vvalue;
				}
			}
			
			if(count($submodules) == 0){
				unset($modules[$key]);
				$continue;
			}else{
				$_submodules['submodule_' . $value['id']] = $submodules;
				$modules[$key] = $value;
			}
		}

		return array($modules, $_submodules);
	}
}

if(!function_exists('load_common'))
{
	function load_common()
	{
		$CI =& get_instance();
		$views = array();
		
		$CI->load->view('common/app/model');

		$CI->load->view('common/app/view');

		$CI->load->view('common/app/controller');

	}
}

if(!function_exists('minify'))
{
	function minify($buffer){
		/* remove comments */
        $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
        /* remove tabs, spaces, newlines, etc. */
        $buffer = str_replace(array("\r\n","\r","\n","\t",'  ','    ','     '), '', $buffer);
        /* remove other spaces before/after ; */
        $buffer = preg_replace(array('(( )+{)','({( )+)'), '{', $buffer);
        $buffer = preg_replace(array('(( )+})','(}( )+)','(;( )*})'), '}', $buffer);
        $buffer = preg_replace(array('(;( )+)','(( )+;)'), ';', $buffer);
        echo $buffer;
	}
}