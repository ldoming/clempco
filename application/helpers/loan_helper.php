<?php
if (!function_exists('get_pmt')) {
    function get_pmt($apr, $term, $loan, $type = 'Regular Loan') {
        switch ($type) {
            case 'Regular Loan':
                $apr = ($apr / 360) * 30;
                return $apr * -$loan * pow((1 + $apr) , $term) / (1 - pow((1 + $apr) , $term));
                break;

            default:
                return $loan * ($apr * $term + 1) / $term;
                break;
        }
    }
}

if (!function_exists('float_to_money')) {
    function float_to_money($number) {
        return number_format($number, 2, '.', ',');
    }
}

if (!function_exists('num_to_alpha')) {
    function num_to_alpha($n) {
        for ($r = ""; $n >= 0; $n = intval($n / 26) - 1) {
            $r = chr($n % 26 + 0x41) . $r;
        }
        
        return $r;
    }
}

if (!function_exists('pad_left')) {
    function pad_left($input = 0, $len = 2, $padstr = '0') {
        return str_pad($input, $len, $padstr, STR_PAD_LEFT);
    }
}

if (!function_exists('generate_pass')) {
    function generate_pass($len = 8) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()+';
        $valid = FALSE;
        
        while (!$valid) {
            $string = '';
            for ($i = 0; $i < $len; $i++) {
                $string.= $characters[rand(0, strlen($characters) - 1) ];
            }
            
            $CI = & get_instance();
            $CI->load->model('loan_user_login_model', 'user_login');
            if ($CI->user_login->count_by('password', md5($string)) > 0) {
                $valid = FALSE;
            } else {
                $valid = valid_pass($string);
            }
        }
        
        return $string;
    }
}

if (!function_exists('valid_pass')) {
    function valid_pass($candidate) {
        if (!preg_match_all('$\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$', $candidate)) return FALSE;
        
        return TRUE;
    }
}

if (!function_exists('arr_unique')) {
    function arr_unique($array) {
        return array_map("unserialize", array_unique(array_map("serialize", $array)));
    }
}

if (!function_exists('get_icons')) {
    function get_icons() {
        header("Content-type: text/css");
        
        $path = 'includes/images/icons';
        $icon_list = scandir($path);
        
        $str = '';
        foreach ($icon_list as $icon) {
            if ($icon == '.' OR $icon == '..') continue;
            
            if (strlen($icon) > 5) $str.= ".icon_" . basename($icon, ".png") . "{background-image: url('" . base_url() . "{$path}/{$icon}') !important;}";
        }
        echo $str;
    }
}

if (!function_exists('require_csrf_tokens')) {
    function require_csrf_tokens($data = NULL) {
        $CI = & get_instance();
        
        $token_name = $CI->security->get_csrf_token_name();
        $hash = $CI->security->get_csrf_hash();
        
        return array_merge($data, array(
            'token_name' => $token_name,
            'token_hash' => $hash
        ));
    }
}

if (!function_exists('last_day_of_month')) {
    function last_day_of_month($date = NULL) {
        return date('t', strtotime($date));
    }
}
?>