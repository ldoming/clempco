<?php

if(!function_exists('out_json'))
{
	function out_json($data)
	{
		die(json_encode($data));
	}
}

if(!function_exists('success_msg'))
{
	function success_msg($data)
	{
		$CI =& get_instance();

		if(!$CI->input->is_ajax_request())
		{
			set_flash("success_msg", $data);
		}
		else
		{
			$data = array(
				'success' => TRUE,
				'data' => $data
			);

			die(json_encode($data));			
		}
	}
}

if(!function_exists('error_msg'))
{
	function error_msg($data)
	{
		$CI =& get_instance();

		if(!$CI->input->is_ajax_request())
		{
			set_flash("error_msg", $data);
		}
		else
		{
			$data = array(
				'success' => FALSE,
				'data' => $data
			);

			die(json_encode($data));			
		}
	}
}

if(!function_exists('response_code'))
{
	function response_code($code)
	{
		$error_codes = array(
			403 => 'Invalid username/password',
			404 => 'Username does not exists!',
			101 => 'Account is inactive!',
			102 => 'You need to login first in order to view this page!'
		);

		set_flash("error_msg", $error_codes[$code]);
		redirect('user_login/index');
	}
}

if(!function_exists('set_flash'))
{
	function set_flash($key, $value)
	{
		$CI =& get_instance();
		$CI->session->set_flashdata($key, $value);
	}
}

?>