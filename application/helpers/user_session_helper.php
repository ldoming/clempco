<?php

if(!function_exists('get_session'))
{
	function get_session()
	{
		$CI =& get_instance();

		if(!isset($CI->session->userdata['User Info']) OR !$CI->session->userdata['User Info'])
			return FALSE;
		else
			return $CI->session->userdata['User Info'];
	}
}

if(!function_exists('sess_user_id'))
{
	function sess_user_id()
	{
		$CI =& get_instance();

		$session = get_session();

		return $session !== FALSE ? $session['id'] : $session;
	}
}


if(!function_exists('get_account_ids'))
{
	function get_account_ids($implode = FALSE)
	{
		$CI =& get_instance();

		$models = array(
			'c_account_users' => 'account_users_model'
		);

		foreach($models as $key => $value)
		{
			$CI->load->model($value, $key);
		}

		$session = get_session();

		if($session['type'] == 'Administrator')
		{
			$id = $session['id'];
		}
		else
		{
			$user = $CI->c_account_users->get_by('user_info_id', $session['id']);
			$id = $user['created_by'];
		}

		$ids = array($id);
		$users = $CI->c_account_users->get_many_by("(created_by = $id)");

		foreach($users as $key => $user)
		{
			$ids[] = $user['user_info_id'];
		}

		return $implode ? implode(',', $ids) : $ids;
	}
}

if(!function_exists('is_valid_session'))
{
	function is_valid_session()
	{
		if(!get_session())
			redirect('user_login/index');
	}
}

if(!function_exists('require_csrf_tokens'))
{
	function require_csrf_tokens($data = NULL)
	{
		$CI =& get_instance();

		$token_name = $CI->security->get_csrf_token_name();
		$hash = $CI->security->get_csrf_hash();

		return array_merge($data, 
			array(
				'token_name' => $token_name,
				'token_hash' => $hash
			)
		);
	}
}

?>