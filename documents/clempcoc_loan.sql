-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 16, 2015 at 09:47 AM
-- Server version: 5.5.40-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `clempcoc_loan`
--

-- --------------------------------------------------------

--
-- Table structure for table `balance`
--

CREATE TABLE IF NOT EXISTS `balance` (
  `date` varchar(100) NOT NULL,
  `Account_title` varchar(120) NOT NULL,
  `Balance` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cashvoucher`
--

CREATE TABLE IF NOT EXISTS `cashvoucher` (
  `No` mediumint(100) NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(100) NOT NULL,
  `Borrowers` varchar(300) NOT NULL,
  `Name` text NOT NULL,
  `CheckNo` int(15) NOT NULL,
  `Item` int(4) NOT NULL,
  `Particulars` text NOT NULL,
  `Approve` varchar(50) NOT NULL,
  `Remarks` varchar(50) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `terms` varchar(100) NOT NULL,
  `Interest` varchar(100) NOT NULL,
  `Prepared` varchar(500) NOT NULL,
  `Recieved` varchar(500) NOT NULL,
  `Date` varchar(100) NOT NULL,
  `Tin` varchar(50) NOT NULL,
  `Amount` varchar(100) NOT NULL,
  `Total` varchar(100) NOT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `cashvoucher`
--

INSERT INTO `cashvoucher` (`No`, `emp_id`, `Borrowers`, `Name`, `CheckNo`, `Item`, `Particulars`, `Approve`, `Remarks`, `Type`, `terms`, `Interest`, `Prepared`, `Recieved`, `Date`, `Tin`, `Amount`, `Total`) VALUES
(9, '23123213123', 'RobwainrightSemblantes', 'xxxxxxxxxxxxxxxxxxxxxxxx', 30063, 0, '<p>\n	xxxxxxxxxxxxxxxxxxxxxxxxxxx</p>\n', '', '', '', '', '', '', '', '08/22/2014', '', '22222', ''),
(10, '23123213123', 'RobwainrightSemblantes', '', 18922, 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(11, '23123213123', 'RobwainrightSemblantes', '', 13766, 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(12, '23123213123', 'RobwainrightSemblantes', '', 9933, 0, '<p>\n	asdfasfas</p>\n', '', '', '', '', '', '', '', '08/22/2014', '', '', ''),
(13, '23123213123', 'RobwainrightSemblantes', '', 29915, 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(14, '23123213123', 'RobwainrightSemblantes', '', 3065, 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(15, '23123213123', 'RobwainrightSemblantes', '', 16977, 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(16, '', 'xx', 'srarara', 16701, 0, '<p>\n	sadfadfafafaf</p>\n', '', '', '', '', '', '', '', '', '', '100', ''),
(17, '12000', 'reyperry', 'Cash on Hand', 15314, 0, '<p>\n	utang kog mongos</p>\n', '', '', '', '', '', '', '', '08/29/2014', '', '10000', ''),
(18, '12000', 'reyperry', 'Cash on Hand', 15314, 0, '<p>\n	utang kog kadghan</p>\n', '', '', '', '', '', '', '', '08/29/2014', '', '12000', ''),
(19, '12000', 'reyperry', 'xxxx', 1066379476, 0, '<p>\n	xxxxx</p>\n', '', '', '', '', '', '', '', '09/03/2014', '', '12000', ''),
(20, '', '', '', 1167392795, 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(21, '219', 'RE Perry Gallos', 'Advance Payment', 1167392795, 0, '<p>\n	Advance Payment for Regular Loan</p>\n', '', '', '', '', '', '', '', '09/04/2014', '', '25000', ''),
(22, '', '', '', 1331373135, 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(23, '12000', 'reyperry', '', 223425619, 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(24, '123123123', 'asdasd', 'asdasd', 223425619, 0, '<p>\n	asdasd</p>\n', '', '', '', '', '', '', '', '09/18/2014', '', '123', ''),
(25, '', '', '', 1510617089, 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(26, '', 'test', 'tets loan', 1510617089, 0, '<p>\n	test loan</p>\n', '', '', '', '', '', '', '', '09/12/2014', '', '100000', ''),
(27, '', 'test', 'tets loan', 1510617089, 0, '<p>\n	test loan</p>\n', '', '', '', '', '', '', '', '09/12/2014', '', '100000', ''),
(28, '', 'test', 'tets loan', 1510617089, 0, '<p>\n	test loan</p>\n', '', '', '', '', '', '', '', '09/12/2014', '', '100000', ''),
(29, '', 'test', 'tets loan', 1510617089, 0, '<p>\n	test loan</p>\n', '', '', '', '', '', '', '', '09/12/2014', '', '100000', ''),
(30, '', 'test', 'tets loan', 1510617089, 0, '<p>\n	test loan</p>\n', '', '', '', '', '', '', '', '09/12/2014', '', '100000', ''),
(31, '', 'test', 'tets loan', 1510617089, 0, '<p>\n	test loan</p>\n', '', '', '', '', '', '', '', '09/12/2014', '', '100000', ''),
(32, '', '', '', 1510617089, 0, '<p>\n	test loan</p>\n', '', '', '', '', '', '', '', '', '', '', ''),
(33, '', '', '', 1510617089, 0, '<p>\n	test loan</p>\n', '', '', '', '', '', '', '', '', '', '', ''),
(34, '', '', '', 1510617089, 0, '<p>\n	test loan</p>\n', '', '', '', '', '', '', '', '', '', '', ''),
(35, '', '', '', 1510617089, 0, '<p>\n	test loan</p>\n', '', '', '', '', '', '', '', '', '', '', ''),
(36, '', '', '', 1510617089, 0, '<p>\n	test loan</p>\n', '', '', '', '', '', '', '', '', '', '', ''),
(37, '', '', '', 1510617089, 0, '<p>\n	test loan</p>\n', '', '', '', '', '', '', '', '', '', '', ''),
(38, 'asdasd', 'asd', '', 250903777, 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(39, '', '', '', 993770581, 0, '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `emergency_loan_report`
--

CREATE TABLE IF NOT EXISTS `emergency_loan_report` (
  `id` mediumint(15) NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Amount` int(15) NOT NULL,
  `date` varchar(100) NOT NULL,
  `Terms` varchar(50) NOT NULL,
  `MonthlyAmortization` int(10) NOT NULL,
  `Percentage` varchar(100) NOT NULL,
  `payments` int(15) NOT NULL,
  `payable` int(15) NOT NULL,
  `principal_payment` int(15) NOT NULL,
  `interest_payment` int(10) NOT NULL,
  `Running_Balance` int(10) NOT NULL,
  `over` int(10) NOT NULL,
  `Remarks` int(10) NOT NULL,
  `PreparedBy` varchar(200) NOT NULL,
  `NoteBy` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `grocery_loan_report`
--

CREATE TABLE IF NOT EXISTS `grocery_loan_report` (
  `id` mediumint(15) NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Amount` int(15) NOT NULL,
  `date` varchar(100) NOT NULL,
  `Terms` varchar(50) NOT NULL,
  `MonthlyAmortization` int(10) NOT NULL,
  `Percentage` varchar(100) NOT NULL,
  `payments` int(15) NOT NULL,
  `payable` int(15) NOT NULL,
  `principal_payment` int(15) NOT NULL,
  `interest_payment` int(10) NOT NULL,
  `Running_Balance` int(10) NOT NULL,
  `over` int(10) NOT NULL,
  `Remarks` int(10) NOT NULL,
  `PreparedBy` varchar(200) NOT NULL,
  `NoteBy` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `journalizing`
--

CREATE TABLE IF NOT EXISTS `journalizing` (
  `Date` varchar(100) NOT NULL,
  `Account_Title` varchar(300) NOT NULL,
  `Status` varchar(300) NOT NULL,
  `Debit` varchar(300) NOT NULL,
  `Credit` varchar(300) NOT NULL,
  `Particulars` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `journalizing`
--

INSERT INTO `journalizing` (`Date`, `Account_Title`, `Status`, `Debit`, `Credit`, `Particulars`) VALUES
('2014-08-26 08:18:08 ', 'xx', '', '20000', '00', 'Regular Loanxx'),
('08/27/2014', 'Special loan', '', '2000', '00', 'Special loanxx'),
('', 'srarara', '', '100', '', 'srarara'),
('2014-08-26 08:18:08 ', 'xx', '', '100', '', 'Regular Loanxx'),
('08/29/2014', 'Cash on Hand', '', '10000', '', 'Cash on Hand'),
('08/29/2014', 'Cash on Hand', '', '12000', '', 'Cash on Hand'),
('2014-08-29 17:04:02 ', 'reyperry', '', '2000', '', 'Regular Loanreyperry'),
('2014-08-29 17:04:02 ', 'reyperry', '', '30000', '', 'Regular Loanreyperry'),
('09/03/2014', 'xxxx', '', '12000', '', 'xxxx'),
('', '', '', '', '', ''),
('09/04/2014', 'Advance Payment', '', '25000', '', 'Advance Payment'),
('', '', '', '', '', ''),
('', '', '', '', '', ''),
('09/18/2014', 'asdasd', '', '123', '', 'asdasd'),
('', '', '', '', '', ''),
('09/12/2014', 'tets loan', '', '100000', '', 'tets loan'),
('09/12/2014', 'tets loan', '', '100000', '', 'tets loan'),
('09/12/2014', 'tets loan', '', '100000', '', 'tets loan'),
('09/12/2014', 'tets loan', '', '100000', '', 'tets loan'),
('09/12/2014', 'tets loan', '', '100000', '', 'tets loan'),
('09/12/2014', 'tets loan', '', '100000', '', 'tets loan'),
('', '', '', '', '', ''),
('', '', '', '', '', ''),
('', '', '', '', '', ''),
('', '', '', '', '', ''),
('', '', '', '', '', ''),
('', '', '', '', '', ''),
('', '', '', '', '', ''),
('', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `loan_accounts`
--

CREATE TABLE IF NOT EXISTS `loan_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acct_name` varchar(255) DEFAULT NULL,
  `acct_code` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `archive` tinyint(4) DEFAULT '0',
  `acct_group` int(11) DEFAULT NULL,
  `ob_type` varchar(255) DEFAULT NULL,
  `ob_amount` decimal(10,2) DEFAULT NULL,
  `details` longtext,
  `member_id` int(11) DEFAULT NULL,
  `acct_period` date DEFAULT NULL,
  `acct_level` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `loan_accounts`
--

INSERT INTO `loan_accounts` (`id`, `acct_name`, `acct_code`, `created_by`, `created_at`, `archive`, `acct_group`, `ob_type`, `ob_amount`, `details`, `member_id`, `acct_period`, `acct_level`) VALUES
(29, 'Accounts Receivable - Regular Loan', '01.00.00', 41, '2014-11-29 11:46:56', 0, NULL, '', '0.00', '', NULL, '2014-11-01', 1),
(30, 'Juvy Changco', '0029.0001.00', 41, '2014-11-29 11:47:46', 0, 29, '', '0.00', '', NULL, '2014-11-01', 2),
(31, 'Melvin Cabantog', '0029.0002.00', 41, '2014-11-29 11:52:06', 0, 29, '', '0.00', '', NULL, '2014-11-01', 2),
(32, 'Accounts Receivable - Emergency Loan', '04.00.00', 41, '2014-11-29 11:52:48', 0, NULL, '', '0.00', '', NULL, '2014-01-01', 1),
(33, 'RGallos', '0029.0003.00', 41, '2014-11-29 11:59:19', 0, 29, '', '0.00', '', NULL, '2014-11-01', 2),
(34, 'ARLENE HEPIGA', '01.00.00-04', 41, '2014-12-09 15:04:04', 1, 29, '', '0.00', '', NULL, '2014-01-01', 2),
(35, 'Accounts Receivable - Regular Loan', '02-10-00', 41, '2014-12-09 15:05:19', 0, NULL, '', '0.00', '', NULL, '2014-03-01', 1),
(36, 'JUVY CHANGCO', '01.00.00-05', 41, '2014-12-09 15:07:36', 0, 29, '', '0.00', '', NULL, '2014-02-01', 2),
(37, 'Accounts Receivable - Regular Loan', '22.22.22.22.22', 41, '2014-12-11 16:21:15', 1, NULL, '', '0.00', '', NULL, '2014-01-01', 1),
(38, 'REG. LOAN - RE PERRY GALLOS', '01.06.00', 41, '2014-12-11 16:22:01', 0, 29, '', '0.00', '', NULL, '2014-01-01', 2),
(39, 'Cash', '01.00.00.00.00', 41, '2014-12-11 16:23:14', 0, NULL, '', '0.00', '', NULL, '2014-01-01', 1),
(40, 'CASH ON HAND', '01.01.00.00.00', 41, '2014-12-11 16:23:54', 0, 39, '', '0.00', '', NULL, '2014-01-01', 2);

-- --------------------------------------------------------

--
-- Table structure for table `loan_account_groups`
--

CREATE TABLE IF NOT EXISTS `loan_account_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=6 ;

--
-- Dumping data for table `loan_account_groups`
--

INSERT INTO `loan_account_groups` (`id`, `group`, `code`) VALUES
(1, 'Accounts Receivable - Regular Loan', '1'),
(2, 'Accounts Receivable - Grocery Loan', '2'),
(3, 'Accounts Receivable - Special Loan', '3'),
(4, 'Accounts Receivable - Emergency Loan', '4'),
(5, 'Cash', '5');

-- --------------------------------------------------------

--
-- Table structure for table `loan_application`
--

CREATE TABLE IF NOT EXISTS `loan_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `principal` decimal(10,2) DEFAULT NULL,
  `interest` decimal(10,2) DEFAULT NULL,
  `period` int(11) DEFAULT NULL,
  `monthly` decimal(10,2) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `terms` varchar(45) DEFAULT NULL,
  `archive` tinyint(4) DEFAULT '0',
  `type` varchar(45) DEFAULT NULL,
  `data` longtext,
  `last_notify` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `loan_application`
--

INSERT INTO `loan_application` (`id`, `principal`, `interest`, `period`, `monthly`, `member_id`, `created_at`, `status`, `terms`, `archive`, `type`, `data`, `last_notify`) VALUES
(44, '20000.00', '15.00', 24, '969.73', 18, '2014-11-28 10:01:14', 'Approved', '50/50', 0, 'Regular Loan', '""', NULL),
(45, '25000.00', '15.00', 24, '1212.17', 20, '2014-11-28 13:51:12', 'Approved', '50/50', 0, 'Regular Loan', '""', NULL),
(46, '10000.00', '2.00', 6, '1866.67', 18, '2014-12-02 10:27:56', 'Approved', '50/50', 0, 'Special Loan', '""', NULL),
(47, '2000.00', '1.50', 1, '2030.00', 18, '2014-12-02 10:31:41', 'Approved', '50/50', 0, 'Emergency Loan', '""', NULL),
(48, '2000.00', '2.50', 1, '2050.00', 18, '2014-12-02 10:34:28', 'Approved', '50/50', 0, 'Grocery Loan', '""', NULL),
(49, '2000.00', '2.50', 1, '2050.00', 20, '2014-12-03 10:23:35', 'New', '50/50', 0, 'Grocery Loan', '""', NULL),
(50, '20000.00', '15.00', 12, '1805.17', 18, '2014-12-16 09:24:34', 'New', '50/50', 0, 'Regular Loan', '""', NULL),
(51, '20000.00', '2.00', 6, '3733.33', 17, '2015-01-22 17:49:26', 'New', '50/50', 0, 'Special Loan', '""', NULL),
(52, '30000.00', '15.00', 24, '1454.60', 17, '2015-01-22 19:39:50', 'Approved', '50/50', 0, 'Regular Loan', '""', '2015-02-16'),
(53, '100000.00', '1.50', 12, '9833.33', 20, '2015-01-27 11:13:43', 'New', '50/50', 0, 'Emergency Loan', '""', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loan_balances`
--

CREATE TABLE IF NOT EXISTS `loan_balances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acct_id` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  `period` varchar(7) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `tag` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `loan_deductions`
--

CREATE TABLE IF NOT EXISTS `loan_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `loan_id` int(100) DEFAULT NULL,
  `principal` decimal(10,2) DEFAULT NULL,
  `interest` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `ded_date` date DEFAULT NULL,
  `archive` tinyint(4) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `ded_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=84 ;

--
-- Dumping data for table `loan_deductions`
--

INSERT INTO `loan_deductions` (`id`, `member_id`, `loan_id`, `principal`, `interest`, `total`, `ded_date`, `archive`, `updated_at`, `created_at`, `ded_type`) VALUES
(14, 18, 44, '719.73', '250.00', '969.73', '2014-11-28', 0, NULL, '2014-11-28 10:05:57', 'deduction'),
(15, 18, NULL, NULL, NULL, '100.00', '2014-01-01', 0, NULL, '2014-12-01 13:49:11', 'savings'),
(16, 18, NULL, NULL, NULL, '100.00', '2014-02-01', 0, NULL, '2014-12-01 13:49:25', 'savings'),
(17, 18, NULL, NULL, NULL, '100.00', '2014-03-01', 0, NULL, '2014-12-01 13:49:38', 'savings'),
(18, 18, NULL, NULL, NULL, '100.00', '2014-04-01', 0, NULL, '2014-12-01 13:49:49', 'savings'),
(19, 18, NULL, NULL, NULL, '100.00', '2014-05-01', 0, NULL, '2014-12-01 13:50:00', 'savings'),
(20, 18, NULL, NULL, NULL, '100.00', '2014-06-01', 0, NULL, '2014-12-01 13:50:09', 'savings'),
(21, 18, NULL, NULL, NULL, '100.00', '2014-07-01', 0, NULL, '2014-12-01 13:50:18', 'savings'),
(22, 18, NULL, NULL, NULL, '100.00', '2014-08-01', 0, NULL, '2014-12-01 13:50:28', 'savings'),
(23, 18, NULL, NULL, NULL, '100.00', '2014-08-01', 0, NULL, '2014-12-01 13:50:38', 'savings'),
(24, 18, NULL, NULL, NULL, '100.00', '2014-09-01', 0, NULL, '2014-12-01 13:50:51', 'savings'),
(25, 18, NULL, NULL, NULL, '100.00', '2014-10-01', 0, NULL, '2014-12-01 13:51:01', 'savings'),
(26, 18, NULL, NULL, NULL, '100.00', '2014-01-01', 0, NULL, '2014-12-01 13:51:14', 'deposit'),
(27, 18, NULL, NULL, NULL, '100.00', '2014-02-01', 0, NULL, '2014-12-01 13:51:23', 'deposit'),
(28, 18, NULL, NULL, NULL, '100.00', '2014-03-01', 0, NULL, '2014-12-01 13:51:33', 'deposit'),
(29, 18, NULL, NULL, NULL, '100.00', '2014-04-01', 0, NULL, '2014-12-01 13:51:44', 'deposit'),
(30, 18, NULL, NULL, NULL, '100.00', '2014-05-01', 0, NULL, '2014-12-01 13:51:54', 'deposit'),
(31, 18, NULL, NULL, NULL, '100.00', '2014-06-01', 0, NULL, '2014-12-01 13:52:04', 'deposit'),
(32, 18, NULL, NULL, NULL, '100.00', '2014-07-01', 0, NULL, '2014-12-01 13:52:13', 'deposit'),
(33, 18, NULL, NULL, NULL, '100.00', '2014-08-01', 0, NULL, '2014-12-01 13:52:24', 'deposit'),
(34, 18, NULL, NULL, NULL, '100.00', '2014-09-01', 0, NULL, '2014-12-01 13:52:33', 'deposit'),
(35, 18, NULL, NULL, NULL, '100.00', '2014-10-01', 0, NULL, '2014-12-01 13:52:41', 'deposit'),
(36, 18, NULL, NULL, NULL, '100.00', '2014-11-01', 0, NULL, '2014-12-01 13:52:50', 'deposit'),
(37, 20, NULL, NULL, NULL, '100.00', '2014-01-01', 0, NULL, '2014-12-01 13:53:12', 'savings'),
(38, 20, NULL, NULL, NULL, '100.00', '2014-02-01', 0, NULL, '2014-12-01 13:53:22', 'savings'),
(39, 20, NULL, NULL, NULL, '100.00', '2014-03-01', 0, NULL, '2014-12-01 13:53:31', 'savings'),
(40, 20, NULL, NULL, NULL, '100.00', '2014-04-01', 0, NULL, '2014-12-01 13:53:43', 'savings'),
(41, 20, NULL, NULL, NULL, '100.00', '2014-05-01', 0, NULL, '2014-12-01 13:53:57', 'savings'),
(42, 20, NULL, NULL, NULL, '300.00', '2014-06-01', 0, NULL, '2014-12-01 13:54:11', 'savings'),
(43, 20, NULL, NULL, NULL, '300.00', '2014-07-01', 0, NULL, '2014-12-01 13:54:19', 'savings'),
(44, 20, NULL, NULL, NULL, '300.00', '2014-08-01', 0, NULL, '2014-12-01 13:54:28', 'savings'),
(45, 20, NULL, NULL, NULL, '300.00', '2014-09-01', 0, NULL, '2014-12-01 13:54:36', 'savings'),
(46, 20, NULL, NULL, NULL, '300.00', '2014-10-01', 0, NULL, '2014-12-01 13:54:44', 'savings'),
(47, 20, NULL, NULL, NULL, '300.00', '2014-11-01', 0, NULL, '2014-12-01 13:54:53', 'savings'),
(48, 20, NULL, NULL, NULL, '300.00', '2014-01-01', 0, NULL, '2014-12-01 13:55:07', 'deposit'),
(49, 20, NULL, NULL, NULL, '300.00', '2014-02-01', 0, NULL, '2014-12-01 13:55:19', 'deposit'),
(50, 20, NULL, NULL, NULL, '300.00', '2014-03-01', 0, NULL, '2014-12-01 13:55:27', 'deposit'),
(51, 20, NULL, NULL, NULL, '300.00', '2014-04-01', 0, NULL, '2014-12-01 13:55:36', 'deposit'),
(52, 20, NULL, NULL, NULL, '300.00', '2014-05-01', 0, NULL, '2014-12-01 13:55:45', 'deposit'),
(53, 20, NULL, NULL, NULL, '300.00', '2014-06-01', 0, NULL, '2014-12-01 13:55:53', 'deposit'),
(54, 20, NULL, NULL, NULL, '300.00', '2014-07-01', 0, NULL, '2014-12-01 13:56:01', 'deposit'),
(55, 20, NULL, NULL, NULL, '300.00', '2014-08-01', 0, NULL, '2014-12-01 13:56:10', 'deposit'),
(56, 20, NULL, NULL, NULL, '300.00', '2014-09-01', 0, NULL, '2014-12-01 13:56:20', 'deposit'),
(57, 20, NULL, NULL, NULL, '300.00', '2014-10-01', 0, NULL, '2014-12-01 13:56:28', 'deposit'),
(58, 20, NULL, NULL, NULL, '300.00', '2014-11-01', 0, NULL, '2014-12-01 13:56:36', 'deposit'),
(59, 18, NULL, NULL, NULL, '5000.00', '2014-12-03', 0, NULL, '2014-12-03 00:02:47', 'savings'),
(60, 18, NULL, NULL, NULL, '500.00', '2014-01-05', 0, NULL, '2014-12-05 13:46:38', 'savings'),
(61, 18, NULL, NULL, NULL, '400.00', '2014-01-05', 0, NULL, '2014-12-05 13:47:23', 'deposit'),
(62, 18, NULL, NULL, NULL, '400.00', '2015-01-05', 0, NULL, '2014-12-05 13:47:23', 'deposit'),
(63, 18, NULL, NULL, NULL, '150.00', '2014-12-22', 0, NULL, '2014-12-22 23:00:15', 'capital'),
(64, 20, NULL, NULL, NULL, '200.00', '2014-11-22', 0, NULL, '2014-12-22 23:00:44', 'capital'),
(65, 18, NULL, NULL, NULL, '500.00', '2015-12-22', 0, NULL, '2015-01-22 13:41:46', 'capital'),
(66, 18, NULL, NULL, NULL, '300.00', '2015-12-22', 0, NULL, '2015-01-22 13:42:26', 'savings'),
(67, 17, NULL, NULL, NULL, '500.00', '2015-12-22', 1, '2015-01-22 13:47:42', '2015-01-22 13:44:27', 'capital'),
(68, 17, NULL, NULL, NULL, '200.00', '2015-12-22', 1, '2015-01-22 13:51:18', '2015-01-22 13:44:43', 'savings'),
(69, 17, NULL, NULL, NULL, '1000.00', '2015-12-22', 1, '2015-01-22 13:52:13', '2015-01-22 13:48:14', 'capital'),
(70, 17, NULL, NULL, NULL, '200.00', '2015-12-22', 1, '2015-01-22 13:51:12', '2015-01-22 13:48:48', 'savings'),
(71, 17, NULL, NULL, NULL, '500.00', '2015-11-22', 1, '2015-01-22 13:52:06', '2015-01-22 13:49:47', 'capital'),
(72, 17, NULL, NULL, NULL, '1000.00', '2015-12-22', 1, '2015-01-22 16:33:28', '2015-01-22 13:54:19', 'capital'),
(73, 17, NULL, NULL, NULL, '1000.00', '2015-12-22', 1, '2015-01-22 16:33:21', '2015-01-22 14:27:26', 'capital'),
(74, 19, NULL, NULL, NULL, '500.00', '2015-12-22', 1, '2015-01-22 14:39:44', '2015-01-22 14:38:56', 'capital'),
(75, 19, NULL, NULL, NULL, '1000.00', '2015-11-22', 0, NULL, '2015-01-22 14:40:10', 'capital'),
(76, 17, NULL, NULL, NULL, '5000.00', '2015-12-22', 1, '2015-01-22 16:33:13', '2015-01-22 16:01:57', 'capital'),
(77, 17, NULL, NULL, NULL, '5000.00', '2015-12-22', 1, '2015-01-22 16:33:04', '2015-01-22 16:32:47', 'capital'),
(78, 17, NULL, NULL, NULL, '1000.00', '2015-12-22', 0, NULL, '2015-01-22 16:33:54', 'capital'),
(79, 21, NULL, NULL, NULL, '500.00', '2015-01-22', 0, NULL, '2015-01-22 16:42:52', 'capital'),
(80, 21, NULL, NULL, NULL, '500.00', '2015-12-22', 0, NULL, '2015-01-22 16:43:21', 'capital'),
(81, 17, NULL, NULL, NULL, '200.00', '2015-12-22', 0, NULL, '2015-01-22 16:55:40', 'savings'),
(82, 17, NULL, NULL, NULL, '500.00', '2015-09-23', 0, NULL, '2015-01-23 13:41:15', 'capital'),
(83, 20, NULL, NULL, NULL, '250.00', '2013-12-01', 0, NULL, '2015-02-04 10:43:26', 'capital');

-- --------------------------------------------------------

--
-- Table structure for table `loan_member_info`
--

CREATE TABLE IF NOT EXISTS `loan_member_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `tel_no` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `archive` tinyint(4) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `application_id` varchar(45) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `civil_status` varchar(45) DEFAULT NULL,
  `id_no` varchar(45) DEFAULT NULL,
  `passport_no` varchar(45) DEFAULT NULL,
  `ctc_no` varchar(45) DEFAULT NULL,
  `monthly_income` decimal(10,2) DEFAULT NULL,
  `cell_no` varchar(45) DEFAULT NULL,
  `home_phone` varchar(45) DEFAULT NULL,
  `email_address1` varchar(45) DEFAULT NULL,
  `company_name` varchar(45) DEFAULT NULL,
  `company_address` varchar(45) DEFAULT NULL,
  `company_sss` varchar(45) DEFAULT NULL,
  `philhealth_no` varchar(45) DEFAULT NULL,
  `office_no` varchar(45) DEFAULT NULL,
  `office_fax` varchar(45) DEFAULT NULL,
  `employer_id` varchar(45) CHARACTER SET dec8 DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `loan_member_info`
--

INSERT INTO `loan_member_info` (`id`, `firstname`, `lastname`, `position`, `address`, `tel_no`, `email_address`, `created_by`, `created_at`, `archive`, `updated_at`, `application_id`, `birth_date`, `civil_status`, `id_no`, `passport_no`, `ctc_no`, `monthly_income`, `cell_no`, `home_phone`, `email_address1`, `company_name`, `company_address`, `company_sss`, `philhealth_no`, `office_no`, `office_fax`, `employer_id`, `file_id`) VALUES
(13, 'Member ', '1', NULL, '123456', '123456', 'member1@test.com', 41, '2014-10-20 12:44:28', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'Arlene', 'Hepiga', NULL, 'Cotabato City', '000000000000000', 'Alene.Hepiga@aboitiz.com', 50, '2014-11-03 02:28:04', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Daniel', 'Yap', NULL, 'Cotabato City', '00000000000', 'Daniel.Yap@aboitiz.com', 49, '2014-11-03 06:08:47', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'Test', 'Test', 'test', 'ste', NULL, 'teset@test.com', 48, '2014-11-04 08:30:17', 1, '2014-11-10 17:41:23', 'tset', '2014-11-04', 'test', 'set', 'asf', 'asfd', '12000.00', 'asfd', 'sdaf', NULL, 'teset@test.com', 'teset@test.com', 'teset@test.com', 'teset@test.com', 'teset@test.com', 'teset@test.com', 'teset@test.com', 2),
(17, 'Re perry', 'Gallos', 'Onsite Support Analyst', 'Cotabato City', NULL, 'Perry.Gallos@aboitiz.com', 41, '2014-11-07 03:28:33', 0, '2014-11-10 17:38:44', '11111', '1986-01-19', 'Single', '219', '1111', '111', '1111.00', '1111', '111', NULL, 'Cotabato Light and Power Company', '11111', '1111', '111', '111', '111', '1111', NULL),
(18, 'Melvin', 'Cabantog', 'Metering Staff', '11', NULL, 'Melvin.Cabantog@yahoo.com', 50, '2014-11-18 10:54:53', 0, '2015-01-26 17:10:52', '1134', '2014-11-11', 'Married', '111', '11', '11', '11.00', '11', '11', 'Melvin.Cabantog2@yahoo.com', '11', '11', '11', '11', '11', '11', '111', NULL),
(19, 'Jay jose', 'Timik', 'Metering Staff', '222222', NULL, 'Jay.Timik@yahoo.com', 50, '2014-11-18 10:57:07', 0, NULL, '1111', '2014-11-20', 'Married', '2222', '1112', '11111', '11111.00', '2222', '2222', 'Jay.Timik@yahoo.com', '2222', '2222', '2222', '222', '2222', '2222', '2222', NULL),
(20, 'Juvy', 'Changco', 'Data Controller', '222', NULL, 'Juvy.Changco@yahoo.com', 50, '2014-11-26 16:51:24', 0, '2014-11-28 14:25:09', '2222', '2014-11-20', 'Married', '222', '222', '222', '2222.00', '22222', '222', NULL, 'Cotabato Light and Power Company', 'Sinsuat Avenue, Cotabato City', '2222', '222', '222', '222', '2222', NULL),
(21, 'Frederick', 'Carlon', 'Quality Inspector', '', NULL, 'fcarlon@yahoo.com', 50, '2015-01-16 11:10:17', 0, NULL, '484878745', '1981-10-21', 'Married', '121212', '', '', '0.00', '', '', 'f_carlon@yahoo.com', '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loan_modules`
--

CREATE TABLE IF NOT EXISTS `loan_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `link` varchar(255) DEFAULT NULL,
  `priority` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `loan_modules`
--

INSERT INTO `loan_modules` (`id`, `name`, `active`, `link`, `priority`) VALUES
(1, 'Home', 0, NULL, 1),
(2, 'Members', 1, NULL, 2),
(4, 'Reports', 1, 'reports/index', 5),
(5, 'Settings', 1, NULL, 6),
(8, 'Accounts', 1, 'accounts/browse', 3),
(9, 'Vouchers', 1, NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `loan_payments`
--

CREATE TABLE IF NOT EXISTS `loan_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loan_id` int(11) DEFAULT NULL,
  `payment` decimal(10,2) DEFAULT NULL,
  `payable` decimal(10,2) DEFAULT NULL,
  `principal` decimal(10,2) DEFAULT NULL,
  `interest` decimal(10,2) DEFAULT NULL,
  `uo` decimal(10,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `pay_date` varchar(10) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `archive` tinyint(4) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `loan_payments`
--

INSERT INTO `loan_payments` (`id`, `loan_id`, `payment`, `payable`, `principal`, `interest`, `uo`, `created_by`, `created_at`, `pay_date`, `remarks`, `archive`, `updated_at`, `member_id`) VALUES
(7, 44, '6650.00', NULL, '6500.00', '150.00', NULL, NULL, '2014-11-28 15:52:12', '2014-11-28', '', 0, NULL, 18),
(8, 44, '1800.00', NULL, '1500.00', '300.00', NULL, NULL, '2014-11-28 16:24:01', '2014-11-26', '', 0, NULL, 18),
(9, 45, '1100.00', NULL, '800.00', '300.00', NULL, NULL, '2014-11-28 16:24:44', '2014-11-24', '', 0, NULL, 20),
(10, 45, '1800.00', NULL, '1500.00', '300.00', NULL, NULL, '2014-11-28 16:35:18', '2014-12-01', '', 0, NULL, 20),
(11, 44, '14832.52', NULL, '14000.00', '832.52', NULL, NULL, '2014-12-16 09:19:23', '2014-12-16', 'Paid', 0, NULL, 18),
(12, 52, '15500.00', NULL, '15000.00', '500.00', NULL, NULL, '2015-02-16 14:44:55', '2015-02-16', 'Payment', 0, NULL, 17);

-- --------------------------------------------------------

--
-- Table structure for table `loan_status_history`
--

CREATE TABLE IF NOT EXISTS `loan_status_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personnel` varchar(45) DEFAULT NULL,
  `comment` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `loan_id` int(11) DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `loan_status_history`
--

INSERT INTO `loan_status_history` (`id`, `personnel`, `comment`, `status`, `loan_id`, `user_id`) VALUES
(17, 'Chairperson', 'Ok.', 'Approved', 44, '48'),
(18, 'Chairperson', 'Ok.', 'Approved', 45, '48'),
(19, 'Chairperson', 'OK', 'Approved', 48, '48'),
(20, 'Chairperson', 'OK', 'Approved', 47, '48'),
(21, 'Chairperson', 'OK', 'Approved', 46, '48'),
(22, 'Chairperson', 'Ok.', 'Approved', 52, '48');

-- --------------------------------------------------------

--
-- Table structure for table `loan_submodules`
--

CREATE TABLE IF NOT EXISTS `loan_submodules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `module_id` int(11) DEFAULT NULL,
  `default` tinyint(1) DEFAULT '1',
  `end` tinyint(1) DEFAULT '0',
  `priority` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `loan_submodules`
--

INSERT INTO `loan_submodules` (`id`, `name`, `link`, `active`, `module_id`, `default`, `end`, `priority`) VALUES
(1, 'Home', 'members/browse', 1, 1, 0, 0, NULL),
(2, 'Manage', 'members/browse', 1, 2, 0, 0, NULL),
(5, 'Loans', 'loans/browse', 1, 2, 0, 0, NULL),
(8, 'Users', 'settings/users', 1, 5, 0, 0, NULL),
(9, 'Generate', 'reports/index', 1, 4, 0, 0, NULL),
(10, 'Loans', 'members/info/Loans', 1, 6, 0, 0, NULL),
(11, 'Balances', 'members/info/Balances', 1, 6, 0, 0, NULL),
(12, 'Savings', 'members/savings', 1, 7, 0, 0, NULL),
(13, 'Deposits', 'members/deposits', 1, 7, 0, 0, NULL),
(14, 'Cash Voucher', 'vouchers/cash', 1, 9, 0, 0, NULL),
(15, 'Journal Voucher', 'vouchers/journal', 1, 9, 0, 0, NULL),
(16, 'Petty Cash', 'vouchers/petty_cash', 1, 9, 0, 0, NULL),
(17, 'Deductions', 'deductions/index', 1, 2, 0, 0, NULL),
(18, 'Access Definitions', 'settings/user_access', 1, 5, 0, 0, NULL),
(19, 'Manage', 'accounts/browse', 1, 8, 0, 0, NULL),
(20, 'Reports', 'settings/reports/index', 1, 5, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loan_user_access`
--

CREATE TABLE IF NOT EXISTS `loan_user_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_typedef` varchar(100) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=171 ;

--
-- Dumping data for table `loan_user_access`
--

INSERT INTO `loan_user_access` (`id`, `user_typedef`, `module_id`) VALUES
(109, 'Treasurer', 2),
(110, 'Treasurer', 9),
(111, 'Treasurer', 5),
(112, 'Treasurer', 17),
(113, 'Treasurer', 14),
(114, 'Treasurer', 15),
(115, 'Treasurer', 16),
(116, 'Chairperson', 2),
(117, 'Chairperson', 5),
(118, 'Chairperson', 9),
(119, 'Chairperson', 14),
(120, 'Chairperson', 15),
(121, 'Chairperson', 16),
(122, 'Administrator', 19),
(123, 'Administrator', 2),
(124, 'Administrator', 5),
(125, 'Administrator', 17),
(126, 'Administrator', 9),
(127, 'Administrator', 8),
(128, 'Administrator', 18),
(129, 'Administrator', 16),
(130, 'Administrator', 15),
(131, 'Administrator', 14),
(132, 'Audit', 2),
(133, 'Audit', 5),
(134, 'Audit', 9),
(144, 'Credit', 2),
(145, 'Credit', 5),
(146, 'Credit', 17),
(147, 'Credit', 9),
(169, 'Member', 5),
(170, 'Member', 17);

-- --------------------------------------------------------

--
-- Table structure for table `loan_user_files`
--

CREATE TABLE IF NOT EXISTS `loan_user_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` text,
  `created_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=3 ;

--
-- Dumping data for table `loan_user_files`
--

INSERT INTO `loan_user_files` (`id`, `path`, `created_at`, `user_id`) VALUES
(1, 'documents/uploads/images.jpeg', '2014-11-04 08:29:45', 48),
(2, 'documents/uploads/article-2429311-182C780D00000578-122_634x640 (3).jpg', '2014-11-10 17:41:18', 41);

-- --------------------------------------------------------

--
-- Table structure for table `loan_user_info`
--

CREATE TABLE IF NOT EXISTS `loan_user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `tel_no` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `user_typedef` varchar(100) DEFAULT NULL,
  `email_address` varchar(100) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'Active',
  `created_by` int(11) DEFAULT NULL,
  `position` varchar(45) CHARACTER SET dec8 COLLATE dec8_bin DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `civil_status` varchar(45) DEFAULT NULL,
  `id_no` varchar(45) DEFAULT NULL,
  `passport_no` varchar(45) DEFAULT NULL,
  `ctc_no` varchar(45) DEFAULT NULL,
  `monthly_income` varchar(45) DEFAULT NULL,
  `cell_no` varchar(45) DEFAULT NULL,
  `home_phone` varchar(45) DEFAULT NULL,
  `email_address1` varchar(45) DEFAULT NULL,
  `company_name` varchar(45) DEFAULT NULL,
  `company_address` varchar(45) DEFAULT NULL,
  `company_sss` varchar(45) DEFAULT NULL,
  `philhealth_no` varchar(45) DEFAULT NULL,
  `office_no` varchar(45) DEFAULT NULL,
  `office_fax` varchar(45) DEFAULT NULL,
  `employer_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `loan_user_info`
--

INSERT INTO `loan_user_info` (`id`, `firstname`, `lastname`, `address`, `tel_no`, `created_at`, `type`, `user_typedef`, `email_address`, `status`, `created_by`, `position`, `application_id`, `birth_date`, `civil_status`, `id_no`, `passport_no`, `ctc_no`, `monthly_income`, `cell_no`, `home_phone`, `email_address1`, `company_name`, `company_address`, `company_sss`, `philhealth_no`, `office_no`, `office_fax`, `employer_id`) VALUES
(41, 'Admin', NULL, NULL, '', '2014-08-20 20:20:39', 'Administrator', NULL, 'test@test.com', 'Active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'Treasurer', '1', '123456', '123465', '2014-10-20 14:28:48', NULL, 'Treasurer', 'treasurer@clempco.com', 'Active', 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'Chairperson', '1', '132456', '123456', '2014-10-20 15:38:35', NULL, 'Chairperson', 'johncode.reymaster@gmail.com', 'Active', 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'Creditor', '1', '123456', '123456', '2014-10-20 15:39:11', NULL, 'Credit', 'creditor@clempco.com', 'Active', 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'Auditor', '1', '123456', '123456', '2014-10-20 15:39:38', NULL, 'Audit', 'auditor@clempco.com', 'Active', 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'John', 'Creditor', 'Abc St', '123456', '2014-11-27 09:57:09', NULL, 'Credit', 'johncreditor@clempco.com', 'Active', 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loan_user_login`
--

CREATE TABLE IF NOT EXISTS `loan_user_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `login_type` varchar(45) DEFAULT NULL,
  `change_password` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `loan_user_login`
--

INSERT INTO `loan_user_login` (`id`, `user_id`, `username`, `password`, `status`, `created_at`, `updated_at`, `login_type`, `change_password`) VALUES
(27, 41, 'admin@test.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'Active', '2014-08-20 20:20:40', NULL, 'Administrator', NULL),
(37, 13, 'member1@test.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'Active', '2014-10-20 12:44:28', NULL, 'Member', 0),
(38, 47, 'treasurer@clempco.com', 'e10adc3949ba59abbe56e057f20f883e', 'Active', '2014-10-20 14:28:48', NULL, 'Treasurer', 0),
(39, 48, 'chairperson@clempco.com', 'e10adc3949ba59abbe56e057f20f883e', 'Active', '2014-10-20 15:38:35', '0000-00-00 00:00:00', 'Chairperson', 0),
(40, 49, 'creditor@clempco.com', 'e10adc3949ba59abbe56e057f20f883e', 'Active', '2014-10-20 15:39:11', NULL, 'Credit', 0),
(41, 50, 'auditor@clempco.com', 'e10adc3949ba59abbe56e057f20f883e', 'Active', '2014-10-20 15:39:38', NULL, 'Audit', 0),
(42, 14, 'AHepiga', '25d55ad283aa400af464c76d713c07ad', 'Active', '2014-11-03 02:28:04', NULL, 'Member', 0),
(43, 15, 'DYap', '25d55ad283aa400af464c76d713c07ad', 'Active', '2014-11-03 06:08:47', NULL, 'Member', 0),
(44, 16, 'teset@test.com', 'e10adc3949ba59abbe56e057f20f883e', 'Active', '2014-11-04 08:30:17', '2014-11-10 17:41:23', 'Member', 0),
(45, 17, 'rgallos', '25d55ad283aa400af464c76d713c07ad', 'Active', '2014-11-07 03:28:33', '2014-11-10 17:38:44', 'Member', 0),
(46, 18, 'Melvin.Cabantog@yahoo.com', '7f75ec9b8d4e8a836a7faaeb1a0b3a18', 'Active', '2014-11-18 10:54:53', '2015-01-26 17:10:52', 'Member', 0),
(47, 19, 'Jay.Timik@yahoo.com', '7f75ec9b8d4e8a836a7faaeb1a0b3a18', 'Active', '2014-11-18 10:57:07', NULL, 'Member', 0),
(48, 20, 'Juvy.Changco@yahoo.com', '7f75ec9b8d4e8a836a7faaeb1a0b3a18', 'Active', '2014-11-26 16:51:24', '2014-11-28 14:25:09', 'Member', 0),
(49, 51, 'johncreditor@clempco.com', 'a44e659768215b3c8ffeecd6d56ab3f8', 'Active', '2014-11-27 09:57:09', NULL, 'Credit', 0),
(50, 21, 'fcarlon@yahoo.com', '7f75ec9b8d4e8a836a7faaeb1a0b3a18', 'Active', '2015-01-16 11:10:17', NULL, 'Member', 0);

-- --------------------------------------------------------

--
-- Table structure for table `loan_user_request`
--

CREATE TABLE IF NOT EXISTS `loan_user_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request` varchar(45) DEFAULT NULL,
  `email_address` varchar(100) DEFAULT NULL,
  `status` varchar(45) DEFAULT 'New',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `loan_user_typedef`
--

CREATE TABLE IF NOT EXISTS `loan_user_typedef` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `loan_user_typedef`
--

INSERT INTO `loan_user_typedef` (`id`, `type`) VALUES
(1, 'Admin'),
(2, 'Credit'),
(3, 'Audit');

-- --------------------------------------------------------

--
-- Table structure for table `loan_vars`
--

CREATE TABLE IF NOT EXISTS `loan_vars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `subtype` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `value2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `loan_vars`
--

INSERT INTO `loan_vars` (`id`, `type`, `subtype`, `value`, `value2`) VALUES
(1, 'Loan Terms', '50%-50%', '50/50', NULL),
(2, 'Loan Terms', '60%-40%', '60/40', NULL),
(4, 'Payment Period', NULL, '15TH', NULL),
(5, 'Payment Period', NULL, '30TH', NULL),
(6, 'Loan Status', NULL, 'Approved', NULL),
(7, 'Loan Status', NULL, 'Declined', NULL),
(8, 'Loan Type', '["audit","treasurer"]', 'Regular Loan', NULL),
(9, 'Loan Type', '["credit","treasurer"]', 'Special Loan', NULL),
(10, 'Loan Type', '["credit","treasurer"]', 'Emergency Loan', NULL),
(11, 'Loan Type', '["credit","treasurer"]', 'Grocery Loan', NULL),
(12, 'User Type', NULL, 'Audit', NULL),
(13, 'User Type', NULL, 'Credit', NULL),
(14, 'User Type', NULL, 'Administrator', NULL),
(16, 'User Type', NULL, 'Treasurer', NULL),
(17, 'User Type', NULL, 'Chairperson', NULL),
(18, 'Interest Rate', 'Emergency Loan', '1.5', NULL),
(19, 'Interest Rate', 'Grocery Loan', '2.5', NULL),
(20, 'Interest Rate', 'Regular Loan', '15', NULL),
(21, 'Interest Rate', 'Special Loan', '2', NULL),
(22, 'Payment Period', 'Emergency Loan', '2', NULL),
(23, 'Payment Period', 'Grocery Loan', '2', NULL),
(24, 'Payment Period', 'Regular Loan', '36', NULL),
(25, 'Payment Period', 'Special Loan', '3', NULL),
(26, 'Loan Terms', '70%-30%', '70/30', NULL),
(27, 'Loan Amount', 'Regular Loan', 'Range', '20000-100000'),
(28, 'Loan Amount', 'Special Loan', 'Max', '20000'),
(29, 'Loan Amount', 'Grocery Loan', 'Max', '6000'),
(30, 'Loan Amount', 'Emergency Loan', 'Max', '2000'),
(31, 'Vouchers', NULL, 'Cash Voucher', NULL),
(32, 'Vouchers', NULL, 'Journal Voucher', NULL),
(33, 'Vouchers', NULL, 'Petty Cash Voucher', NULL),
(34, 'Account Groups', 'Account Receivables - Regular Loan', '1', NULL),
(35, 'Account Groups', 'Cash', '1', NULL),
(36, 'Account Groups', 'Account Receivables - Special Loan', '1', NULL),
(37, 'Timezone', NULL, 'Asia/Manila', NULL),
(38, 'Report Signatories', 'Prepared_By', 'REY PERRY', NULL),
(39, 'Report Signatories', 'Noted_By', 'CHAIRMAN', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loan_vouchers`
--

CREATE TABLE IF NOT EXISTS `loan_vouchers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `amount` varchar(45) DEFAULT NULL,
  `remarks` varchar(45) DEFAULT NULL,
  `archive` tinyint(4) DEFAULT '0',
  `voucher_type` varchar(45) DEFAULT NULL,
  `acct_code` int(11) DEFAULT NULL,
  `acct_name` varchar(255) DEFAULT NULL,
  `voucher_no` varchar(255) DEFAULT NULL,
  `check_no` varchar(255) DEFAULT NULL,
  `tin_no` varchar(255) DEFAULT NULL,
  `voucher_date` date DEFAULT NULL,
  `voucher_name` varchar(45) DEFAULT NULL,
  `acct_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `loan_vouchers`
--

INSERT INTO `loan_vouchers` (`id`, `created_by`, `created_at`, `amount`, `remarks`, `archive`, `voucher_type`, `acct_code`, `acct_name`, `voucher_no`, `check_no`, `tin_no`, `voucher_date`, `voucher_name`, `acct_id`) VALUES
(1, 41, '2014-12-11 16:25:54', NULL, '', 1, 'Journal Voucher', NULL, NULL, NULL, NULL, NULL, '2014-12-11', NULL, NULL),
(2, 41, '2014-12-12 11:26:06', NULL, '111', 0, 'Cash Voucher', NULL, NULL, '11111', '', '11111', '2014-12-12', NULL, 39),
(3, 41, '2014-12-12 13:16:09', NULL, 'Test', 0, 'Journal Voucher', NULL, NULL, NULL, NULL, NULL, '2014-12-12', NULL, NULL),
(4, 41, '2014-12-12 13:16:59', '15000', 'Payment', 0, 'Petty Cash Voucher', NULL, 'Cash', '121211', NULL, NULL, '2014-12-12', NULL, 39),
(5, 41, '2014-12-15 15:24:01', '5000', 'dfdfd', 0, 'Petty Cash Voucher', NULL, 'Accounts Receivable - Regular Loan', 'P0410001', NULL, NULL, '2014-12-17', NULL, 35),
(6, 41, '2014-12-15 15:24:34', '150000', '', 0, 'Petty Cash Voucher', NULL, 'REG. LOAN - RE PERRY GALLOS', 'P0410001', NULL, NULL, '2014-12-18', NULL, 38),
(7, 41, '2014-12-15 15:25:07', NULL, 'Test', 0, 'Journal Voucher', NULL, NULL, NULL, NULL, NULL, '2014-12-17', NULL, NULL),
(8, 41, '2014-12-19 09:51:47', NULL, 'Test', 0, 'Cash Voucher', NULL, NULL, 'C0410008', '11111', '11111111', '2014-12-19', NULL, 32),
(9, 41, '2014-12-19 09:53:16', NULL, 'Test1', 0, 'Cash Voucher', NULL, NULL, 'C0410009', '121212121', '121212121', '2014-12-19', NULL, 30),
(10, 41, '2014-12-22 16:16:54', NULL, 'Test', 0, 'Cash Voucher', NULL, NULL, 'C0410010', '11112222', '1112222', '2014-12-25', NULL, 32);

-- --------------------------------------------------------

--
-- Table structure for table `loan_voucher_particulars`
--

CREATE TABLE IF NOT EXISTS `loan_voucher_particulars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) DEFAULT NULL,
  `debit` decimal(10,2) DEFAULT NULL,
  `credit` decimal(10,2) DEFAULT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `acct_code` varchar(255) DEFAULT NULL,
  `acct_id` int(11) DEFAULT NULL,
  `archive` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `loan_voucher_particulars`
--

INSERT INTO `loan_voucher_particulars` (`id`, `description`, `debit`, `credit`, `voucher_id`, `amount`, `acct_code`, `acct_id`, `archive`) VALUES
(1, 'Cash', '3.00', '0.00', 1, NULL, '01.00.00.00.00', 39, 0),
(2, 'Cash', '0.00', '374163.41', 2, NULL, '01.00.00.00.00', 39, 0),
(3, 'Cash', '1522484.72', '660274.50', 3, NULL, '01.00.00.00.00', 39, 0),
(4, 'Payment', NULL, NULL, 4, '15000.00', NULL, NULL, 0),
(5, 'test', NULL, NULL, 5, '5000.00', NULL, NULL, 0),
(6, 'Test2', NULL, NULL, 6, '150000.00', NULL, NULL, 0),
(7, 'JUVY CHANGCO', '500.00', '200.00', 7, NULL, '01.00.00-05', 36, 0),
(8, 'CASH ON HAND', '10000.00', '10000.00', 8, NULL, '01.01.00.00.00', 40, 0),
(9, 'Accounts Receivable - Emergency Loan', '1500.00', '1500.00', 9, NULL, '04.00.00', 32, 0),
(10, 'REG. LOAN - RE PERRY GALLOS', '1000.00', '15000.00', 10, NULL, '01.06.00', 38, 0);

-- --------------------------------------------------------

--
-- Table structure for table `memberlist`
--

CREATE TABLE IF NOT EXISTS `memberlist` (
  `id` mediumint(100) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `position` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `streetaddress` varchar(500) NOT NULL,
  `city` text NOT NULL,
  `state` text NOT NULL,
  `zip` int(8) NOT NULL,
  `country` varchar(10) NOT NULL,
  `primaryemail` varchar(200) NOT NULL,
  `email` text NOT NULL,
  `mobile` int(20) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `age` int(3) NOT NULL,
  `image` blob NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `applicationid` varchar(100) NOT NULL,
  `birthdate` varchar(100) NOT NULL,
  `civilstatus` varchar(100) NOT NULL,
  `employeerid` varchar(100) NOT NULL,
  `passportid` varchar(100) NOT NULL,
  `ctc` int(50) NOT NULL,
  `monthlyincome` varchar(100) NOT NULL,
  `mobilenumber` varchar(12) NOT NULL,
  `tel` int(10) NOT NULL,
  `companyname` text NOT NULL,
  `companyaddress` varchar(100) NOT NULL,
  `sss` int(15) NOT NULL,
  `tin` text NOT NULL,
  `Philhealth` varchar(100) NOT NULL,
  `officeno` varchar(100) NOT NULL,
  `officefax` varchar(100) NOT NULL,
  `Type` varchar(100) NOT NULL,
  `Recaptcha` varchar(300) NOT NULL,
  `date` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `memberlist`
--

INSERT INTO `memberlist` (`id`, `firstname`, `lastname`, `position`, `address`, `streetaddress`, `city`, `state`, `zip`, `country`, `primaryemail`, `email`, `mobile`, `gender`, `age`, `image`, `username`, `password`, `applicationid`, `birthdate`, `civilstatus`, `employeerid`, `passportid`, `ctc`, `monthlyincome`, `mobilenumber`, `tel`, `companyname`, `companyaddress`, `sss`, `tin`, `Philhealth`, `officeno`, `officefax`, `Type`, `Recaptcha`, `date`) VALUES
(4, 'x', 'x', '', 'x', 'x', 'x', 'x', 0, '', ' roberto.semblante@gmail.com', 'roberto.semblante@gmail.com', 123, '', 29, 0x6c6f676f362e6a7067, 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '', '', '', '', 0, '', ' 09224609131', 0, ' ', ' ', 0, '', ' ', '', '', 'Clempcomembers', '6864', '2014-08-26 08:18:08'),
(5, 'rey', 'perry', 'C.E.O CLEMPCO', 'cotabato', 'cotabato', 'cotabato', 'gensan', 123, '', ' roberto.semblante@gmail.com', 'roberto.semblante@gmail.com', 2147483647, '', 29, 0x44534330313635332e4a5047, 'clempco', '21232f297a57a5a743894a0e4a801fc3', '', '', 'SINGLE', '12000', '123123', 12121, '1000000', '   123213123', 123123123, '  test', ' ', 1231231, '', '123123   ', '12313', '12313', 'Clempcomembers', '', '2014-08-29 17:04:02'),
(6, 'test', 'test', '', 'test', 'test', 'test', 'test', 6015, '', '', 'faceboookrobz28@gmail.com', 2147483647, '', 12, 0x43616c656e646172203720313530702d30312e6a7067, 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '', '', 'Clempcomembers', '', '2014-11-07 19:30:23');

-- --------------------------------------------------------

--
-- Table structure for table `regular_loan_report`
--

CREATE TABLE IF NOT EXISTS `regular_loan_report` (
  `no` mediumint(100) NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Amount` int(15) NOT NULL,
  `date` varchar(100) NOT NULL,
  `Interest` varchar(100) NOT NULL,
  `Terms` varchar(50) NOT NULL,
  `MonthlyAmortization` varchar(100) NOT NULL,
  `Percentage` varchar(100) NOT NULL,
  `payments` int(15) NOT NULL,
  `payable` int(15) NOT NULL,
  `principal_payment` int(15) NOT NULL,
  `interest_payment` int(10) NOT NULL,
  `Running_Balance` int(10) NOT NULL,
  `over` int(10) NOT NULL,
  `Remarks` int(10) NOT NULL,
  `PreparedBy` varchar(200) NOT NULL,
  `NoteBy` varchar(200) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `regular_loan_report`
--

INSERT INTO `regular_loan_report` (`no`, `emp_id`, `Name`, `Amount`, `date`, `Interest`, `Terms`, `MonthlyAmortization`, `Percentage`, `payments`, `payable`, `principal_payment`, `interest_payment`, `Running_Balance`, `over`, `Remarks`, `PreparedBy`, `NoteBy`) VALUES
(23, '23123213123', 'RobwainrightSemblantes', 20000, '02/15/1985 ', '', '50%-50%', '969.73', '50%-50%', 20000, 970, 720, 250, 19280, 0, 0, '', ''),
(28, '23123213123', 'RobwainrightSemblantes', 19280, '02/15/1985 ', '', 'Choose...', '969.73', 'Choose...', 19280, 970, 729, 241, 18551, 0, 0, '', ''),
(32, '', 'xx', 20000, '2014-08-26 08:18:08 ', '', '50%-50%', '969.73', '50%-50%', 20000, 970, 720, 250, 19280, 0, 0, '', ''),
(33, '', 'xx', 100, '2014-08-26 08:18:08 ', '', '50%-50%', '300', '50%-50%', 100, 300, 299, 1, -199, 0, 0, '', ''),
(34, '12000', 'reyperry', 2000, '2014-08-29 17:04:02 ', '', '50%-50%', '123', '50%-50%', 2000, 123, 98, 25, 1902, 0, 0, '', ''),
(35, '12000', 'reyperry', 30000, '2014-08-29 17:04:02 ', '', '60%-40%', '50000', '60%-40%', 30000, 50000, 49625, 375, -19625, 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `special_loan_report`
--

CREATE TABLE IF NOT EXISTS `special_loan_report` (
  `id` mediumint(15) NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Amount` int(15) NOT NULL,
  `date` varchar(100) NOT NULL,
  `Terms` varchar(50) NOT NULL,
  `MonthlyAmortization` int(10) NOT NULL,
  `Percentage` varchar(100) NOT NULL,
  `payments` int(15) NOT NULL,
  `payable` int(15) NOT NULL,
  `principal_payment` int(15) NOT NULL,
  `interest_payment` int(10) NOT NULL,
  `Running_Balance` int(10) NOT NULL,
  `over` int(10) NOT NULL,
  `Remarks` int(10) NOT NULL,
  `PreparedBy` varchar(200) NOT NULL,
  `NoteBy` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `special_loan_report`
--

INSERT INTO `special_loan_report` (`id`, `emp_id`, `Name`, `Amount`, `date`, `Terms`, `MonthlyAmortization`, `Percentage`, `payments`, `payable`, `principal_payment`, `interest_payment`, `Running_Balance`, `over`, `Remarks`, `PreparedBy`, `NoteBy`) VALUES
(35, '23123213123', 'RobwainrightSemblantes', 3000, '08/22/2014', '50%-50%', 1060, '50%-50%', 3000, 530, 500, 30, -2500, 30, 0, '', ''),
(37, '23123213123', 'RobwainrightSemblantes', 3000, '08/22/2014', '50%-50%', 0, '50%-50%', 3000, 530, 500, 30, -2500, 30, 0, '', ''),
(38, '23123213123', 'RobwainrightSemblantes', 2500, '08/22/2014', '50%-50%', 0, '50%-50%', 2500, 530, 505, 25, -1995, 25, 0, '', ''),
(39, '', 'xx', 2000, '08/27/2014', '50%-50%', 970, '50%-50%', 2000, 0, -20, 20, -2020, 20, 0, '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
